package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.junit.After
import org.scalatest.WordSpec
import com.commscope.eco.inquire.udfs._

trait SparkBasedTest extends WordSpec {
  private val logger = Logger(this.getClass)
  implicit val appConfig = AppConfig("config/application-silver.json")
  val classifierUdf = new ClassifierUdf().call _

  val sparkConf = new SparkConf().setMaster("local[2]")
    .setAppName(appConfig.getValueAsString("name", "Inquire Test"))
    .set("spark.broadcast.compress", "false")
    .set("spark.shuffle.compress", "false")
    .set("spark.shuffle.spill.compress", "false")
    .set("spark.sql.caseSensitive", "true")
//    .set("spark.driver.extraJavaOptions", "-Duser.timezone=GMT")
//    .set("spark.executor.extraJavaOptions", "-Duser.timezone=GMT")
    .set("spark.sql.session.timeZone", appConfig.getValueAsString("default_timezone", "UTC"))

  assert(sparkConf.get("spark.sql.session.timeZone").equals("UTC"))

  implicit val spark = SparkSession.builder().config(sparkConf).getOrCreate()
  spark.udf.register("classify", classifierUdf)

  @After
  def tearDown() {
    //spark.close()
  }
}
