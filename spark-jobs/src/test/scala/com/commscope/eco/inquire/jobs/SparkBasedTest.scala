package com.commscope.eco.inquire.jobs

import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession
import org.junit.After
import org.scalatest.WordSpec


trait SparkBasedTest extends WordSpec {
  private val logger = Logger(this.getClass)

  val sparkConf = new SparkConf().setMaster("local[2]")
    .setAppName("Inquire Test")
    .set("spark.broadcast.compress", "false")
    .set("spark.shuffle.compress", "false")
    .set("spark.shuffle.spill.compress", "false")
    .set("spark.sql.caseSensitive", "true")
    .set("spark.sql.files.ignoreCorruptFiles","true")
//    .set("spark.driver.extraJavaOptions", "-Duser.timezone=GMT")
//    .set("spark.executor.extraJavaOptions", "-Duser.timezone=GMT")
    .set("spark.sql.session.timeZone", "UTC")

  assert(sparkConf.get("spark.sql.session.timeZone").equals("UTC"))

  implicit val spark = SparkSession.builder().config(sparkConf).getOrCreate()

  @After
  def tearDown() {
    //spark.close()
  }
}
