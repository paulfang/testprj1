package com.commscope.eco.inquire.jobs

import com.typesafe.scalalogging.Logger
import junit.framework.Assert

class DataExporterConfigTest extends SparkBasedTest {
  val logger = Logger(this.getClass)
  "invalid config because of empty query and generator both" in {
    val args = Array(
      "table==subscriber=src/test/resources/subscriber",
      "table==device=src/test/resources/device",
      s"query==",
      "output_location==some/location")

    Assert.assertFalse(DataExportConfig(args).isValid)
  }

  "invalid config when query but no tables" in {
    val args = Array(
      s"query==some query",
      "output_location==some/location")

    Assert.assertFalse(DataExportConfig(args).isValid)
  }

  "invalid config when query but no outputLocation" in {
    val args = Array(
      "table==subscriber=src/test/resources/subscriber",
      "table==device=src/test/resources/device",
      s"query==some query")

    Assert.assertFalse(DataExportConfig(args).isValid)
  }

  "valid config when query and tables outputLocation" in {
    val args = Array(
      "table==subscriber=src/test/resources/subscriber",
      "table==device=src/test/resources/device",
      "output_location==some_location",
      s"query==some query")

    Assert.assertTrue(DataExportConfig(args).isValid)
  }

  "invalid config when queryGenerator and no warehouseLocation" in {
    val args = Array(
      "output_location==some/location",
      "query_generator==com.commscope.eco.inquire.jobs.custom.RadioStuckQueryGenerator")

    Assert.assertFalse(DataExportConfig(args).isValid)
  }

  "valid config when queryGenerator and warehouseLocation" in {
    val args = Array(
      "output_location==some/location",
      "warehouse_location==some/location",
      "query_generator==com.commscope.eco.inquire.jobs.custom.RadioStuckQueryGenerator")

    Assert.assertTrue(DataExportConfig(args).isValid)
  }
}
