package com.commscope.eco.inquire.jobs

import com.typesafe.scalalogging.Logger
import junit.framework.Assert

import java.time.{Instant, ZoneId}

class KPITest extends SparkBasedTest {
  val logger = Logger(this.getClass)
  "nothing to do when no args" in {
    val args = Array[String]()

    KPI.main(args)
  }

  "nothing to do when no warehouse_location" in {
    val args = Array(
      "ts_date==2021-02-28",
      "output_location==some/location")

    KPI.main(args)
  }

  "nothing to do when no output_location" in {
    val args = Array(
      "ts_date==2021-02-28",
      "warehouse_location==some/location")

    KPI.main(args)
  }

  "with ts_date" in {
    val args = Array(
      "ts_date==2021-02-28",
      "warehouse_location==some/location")

    Assert.assertEquals("2021-02-28", KPIConfig(args).tsDate)
  }

  "with yesterday as ts_date" in {
    val conf = KPIConfig(Array())
    conf.defaultTsDate()

    val now = Instant.now().atZone(ZoneId.of(conf.timeZone))
    val tsDate = now.minusDays(1).format(conf.dateFormat)
    Assert.assertEquals(tsDate, conf.tsDate)
  }

  "with no ts_date_diff" in {
    val conf = KPIConfig(Array("ts_date_diff==0"))
    conf.defaultTsDate()
    val now = Instant.now().atZone(ZoneId.of(conf.timeZone))
    val tsDate = now.format(conf.dateFormat)
    Assert.assertEquals(tsDate, conf.tsDate)
  }

  "with no kpiSummary" in {
    val conf = KPIConfig(Array("summary==false"))
    Assert.assertFalse(conf.createSummary)
    Assert.assertFalse(conf.createDetails)
  }

  "with no kpiDetails" in {
    val conf = KPIConfig(Array("details==false"))
    Assert.assertTrue(conf.createSummary)
    Assert.assertFalse(conf.createDetails)
  }

  "with selected KPIs, kpiDetails" should {
    "be false" in {
      val conf = KPIConfig(Array("kpis==1"))
      logger.info("KPIConf: {}", conf)
      Assert.assertTrue(conf.createSummary)
      Assert.assertFalse(conf.createDetails)
    }
  }

  "top_n_by_model false" in {
    var conf = KPIConfig(Array("kpis==1"))
    Assert.assertFalse(conf.topNByModel)

    conf = KPIConfig(Array("top_n_by_model==false"))
    Assert.assertFalse(conf.topNByModel)
  }

  "top_n_by_model true" in {
    val conf = KPIConfig(Array("top_n_by_model==true"))
    logger.info("KPIConf: {}", conf)
    Assert.assertTrue(conf.topNByModel)
  }

  "scratchpad_export is true or default" in {
    var conf = KPIConfig(Array("scratchpad_export"))
    Assert.assertTrue(conf.exportToScratchpad)

    conf = KPIConfig(Array("scratchpad_export==true"))
    Assert.assertTrue(conf.exportToScratchpad)
  }

  "scratchpad_export is false" in {
    val conf = KPIConfig(Array("scratchpad_export==false"))
    Assert.assertFalse(conf.exportToScratchpad)
  }

}
