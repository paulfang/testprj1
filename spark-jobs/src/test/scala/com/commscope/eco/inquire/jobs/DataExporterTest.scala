package com.commscope.eco.inquire.jobs

import com.typesafe.scalalogging.Logger
import junit.framework.Assert

class DataExporterTest extends SparkBasedTest {
  val logger = Logger(this.getClass)

  "data" should {
    "be exported" in {
      val expectedCols = "device_id,mac_address,client_category,client_device_type,client_manufacturer"

      val query = s"select distinct $expectedCols from client"
      val output = "tmp/export/client"
      val args = Array(
        "table==client=src/test/resources/data/frontier/silver/client",
        s"query==$query",
        s"output_location==$output")

      DataExporter.main(args)

      val result = spark.read.parquet(output)

      val colDiff = expectedCols.split(",").diff(result.columns)
      Assert.assertTrue(colDiff.isEmpty)
      val distinctCounts = 46
      Assert.assertEquals(distinctCounts, result.count())
    }
  }

  "data" should {
    "be exported with selectColumns" in {
      val expectedCols = "*"

      val query = s"select distinct $expectedCols from client"
      val output = "tmp/export/client"
      val args = Array(
        "table==client=src/test/resources/data/frontier/silver/client",
        "select==client=device_id, subscriber_id",
        s"query==$query",
        s"output_location==$output")

      DataExporter.main(args)

      val result = spark.read.parquet(output)
      val columns = result.columns
      val selectCols = Array("device_id", "subscriber_id")
      Assert.assertEquals(selectCols.length, columns.length)
      selectCols.foreach(c => Assert.assertTrue(columns.contains(c)))
    }
  }

  "data" should {
    "be exported with droppedColumns" in {
      val expectedCols = "*"

      val query = s"select distinct $expectedCols from client"
      val output = "tmp/export/client"
      val args = Array(
        "table==client=src/test/resources/data/frontier/silver/client",
        "drop==client=ts_year, ts_day, ts_month",
        s"query==$query",
        s"output_location==$output")

      DataExporter.main(args)

      val result = spark.read.parquet(output)
      val columns = result.columns
      Array("ts_day", "ts_month", "ts_year").foreach(c => Assert.assertFalse(columns.contains(c)))
    }
  }
}
