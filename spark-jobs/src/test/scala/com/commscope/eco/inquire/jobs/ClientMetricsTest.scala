package com.commscope.eco.inquire.jobs

import com.typesafe.scalalogging.Logger

class ClientMetricsTest extends SparkBasedTest {
  val logger = Logger(this.getClass)
  "nothing to do when no args" in {
    val args = Array("")

    ClientMetrics.main(args)
  }

  "nothing to do when no warehouse_location" in {
    val args = Array(
      "ts_date=2021-02-28",
      "output_location==some/location")

    ClientMetrics.main(args)
  }

  "nothing to do when no output_location" in {
    val args = Array(
      "ts_date=2021-02-28",
      "warehouse_location==some/location")

    ClientMetrics.main(args)
  }

  "nothing to do when no ts_date" in {
    val args = Array(
      "warehouse_location=some/location",
      "output_location==some/location")

    ClientMetrics.main(args)
  }
}
