package com.commscope.eco.inquire.jobs

import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId}

object KPI extends App {
  val logger = Logger(this.getClass)
  var cachedTables = Seq[String]()
  var tempTables = Seq[String]()
  var tsDate: String = ""

  implicit var conf: KPIConfig = null

  if (args.isEmpty) {
    logger.warn("Nothing to do....no command line arguments passed")
  }
  else {
    logger.warn("args: {}", args.mkString(","))

    conf = KPIConfig(args)

    logger.warn("Going to job KPIConf: {}", conf)

    if (conf.warehouseLocation.isEmpty || conf.outputLocation.isEmpty) {
      logger.info("warehouse_location, output_location undefined")
    } else {
      implicit val spark = SparkSession.builder()
        .config(new SparkConf().setAppName("KPI"))
        .getOrCreate()

      val datesToRunFor = conf.tsDate.split(",")

      datesToRunFor.foreach(dt => {
        tsDate = dt.trim
        try {
          val devices = loadDevices()
          createTable(devices, "devices", cache = if (conf.fullPopulation) false else true)
          loadAnalysisData(devices)
          processKPIs()
        }
        catch {
          case t: Throwable => logger.error("Error running KPI for date: {}", tsDate, t)
        }
        finally
          dropTempTables()
      })
    }
  }

  def processKPIs()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    if (conf.createKPIs) createKPIs()

    if (conf.createSummary && conf.exportToScratchpad) createSummary()

    if (conf.createDetails && conf.exportToScratchpad) {
      loadKPIResults("kpi_summary")
      createKPIDetails()
    } else if (conf.createRawMetrics) {
      createKPIRawMetrics()
    }
  }

  def createKPIs()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    conf.kpiSet.foreach(k =>
      try {
        k.trim match {
          case c@"1" => kpi1()
          case c@"2" => kpi2()
          case c@"2b" => kpi2b()
          case c@"3a" => kpi3a()
          case c@"3b" => kpi3b()
          case c@"5" => kpi5()
          case c@"6" => kpi6()
          case c@"7" => kpi7()
          case c@"8" => kpi8()
        }
      } catch {
        case t: Throwable => logger.warn("Failed to process the KPI: {}", k, t)
      }
    )
  }

  def createSummary()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI-summary...")
    val joinColumns = Seq("ts_date", "subscriber_id", "serial_number", "device_model", "software_version", "hnc_enable", "extender")
    val joinType = "fullouter"

    val headKpiDf = loadKPIResults(s"kpi_${conf.kpiSet.head}_top")

    val summary = conf.kpiSet.tail
      .foldLeft(headKpiDf)((df, kpi) => {
        df.join(loadKPIResults(s"kpi_${kpi}_top"), usingColumns = joinColumns, joinType = joinType)
      })
      .na.fill(conf.kpiSet.map(k => s"kpi_$k" -> 0).toMap)
      .orderBy("subscriber_id", "serial_number","device_model", "software_version", "hnc_enable", "extender")

    createTable(summary, "kpi_summary", cache = true)

    if (conf.exportToScratchpad) {
      exportToScratchpad(summary, "kpi-summary")
    }
    logger.info("Completed KPI-summary...")
  }

  /** This is details for the subscribers that belong to kpi_summary
   *
   * @param spark
   * @param conf
   */
  def createKPIDetails()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPIDetails...")
    val devices = spark.sql("select * from devices")

    val dropCols = Seq("device_model", "device_manufacturer",
      "software_version", "hardware_version", "hnc_enable",
      "subscriber_id", "zip", "city", "state", "country", "usi", "ts_date")

    val devInformCols = Seq("device_id", "subscriber_id",
      "uptime", "bytes_sent_current", "bytes_received_current",
      "wan_access_type", "downstream_curr_rate", "upstream_curr_rate",
      "memory_free", "last_reboot_reason", "ts_inform")

    val deviceInforms = spark.sql("select * from device_ts")
      .drop(dropCols: _*)
      .join(devices, usingColumns = Seq("device_id"))
      .select(devInformCols.head, devInformCols.tail: _*)
      .withColumnRenamed("downstream_curr_rate", "wan_link_rate_down")
      .withColumnRenamed("upstream_curr_rate", "wan_link_rate_up")

    val topKPISubs = spark.sql("select * from kpi_summary")

    val topKPIDeviceStats = topKPISubs.join(deviceInforms.drop("ts"), usingColumns = Seq("subscriber_id"), joinType = "left")

    val steeringStats = spark.sql("select * from silver_ets")
      .select(col("device_id"), col("ts_inform"), explode(col("steering_stats")))
      .select("*", "col.*")
      .drop("steering_stats", "col")
      .withColumnRenamed("staMacAddress", "mac_address")
      .join(devices.drop(dropCols: _*), usingColumns = Seq("device_id"), joinType = "right")

    val clientTsCols = Seq("ts_inform", "subscriber_id", "device_id", "mac_address", "rssi", "radio",
      "bytes_received", "bytes_sent", "phy_rate", "disassociations_count_current",
      "deauthentication_count_current", "hardware_version")

    val clientInforms = spark.sql("select * from client_ts")
      .select(clientTsCols.head, clientTsCols.tail: _*)
      .join(steeringStats, usingColumns = Seq("device_id", "mac_address", "ts_inform"))

    val topKPIClientStats = topKPIDeviceStats.join(clientInforms, usingColumns = Seq("subscriber_id", "device_id", "ts_inform"), joinType = "left")

    createTable(topKPIClientStats, "kpi_details")

    val stmt =
      """
        select subscriber_id, ts_date, serial_number, device_id, kpi_1, kpi_2, kpi_2b, kpi_3a, kpi_3b,
          0 as kpi_4,
          kpi_5,
          kpi_6,
          kpi_7,
          kpi_8,
          kpi_1 + kpi_2 + kpi_2b + kpi_3a + kpi_3b + kpi_5 + kpi_6 + kpi_7 + kpi_8 as total_kpi_count,
          hardware_version, software_version, ts_inform,
          hnc_enable, extender, 0 as dns_fails, uptime,
          0 as cpu_load, memory_free, last_reboot_reason,
          uptime as wan_uptime, wan_access_type as wan_type,
          bytes_received_current as wan_bytes_rec, bytes_sent_current as wan_bytes_sent,
          (bytes_received_current*8.0)/(1.0*15*60*1000000) as wan_ds_throughput,
          (bytes_sent_current*8.0)/(1.0*15*60*1000000) as wan_us_throughput,
          wan_link_rate_down, wan_link_rate_up,
          radio, mac_address, 0 as chan_util,
          0 as pkts_dropped, 0 as pkt_errored, rssi, bytes_received, bytes_sent, phy_rate,
          deauthentication_count_current as deauth_count, disassociations_count_current as disassoc_count,
          staBtmAttempts as btm_attenmpts, staBtmSuccesses as btm_success, staSelfSteerCount as self_steer_cnt
        from kpi_details
        order by
        subscriber_id, ts_date, ts_inform, hnc_enable desc, extender desc, radio
    """
    val kpiDetails = spark.sql(stmt).distinct()
    exportToScratchpad(kpiDetails, "kpi-details")
    logger.info("Completed KPIDetails")
  }

  /** This generates metrics for entire population, unlike createKPIDetails that concerns only with the subscribers
   * that are part of kpi_summary
   *
   * @param spark
   * @param conf
   */
  def createKPIRawMetrics()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPIRawMetrics...")
    val devices = spark.sql("select * from devices")

    val dropCols = Seq("device_model", "device_manufacturer",
      "software_version", "hardware_version", "hnc_enable",
      "subscriber_id", "zip", "city", "state", "country", "usi")

    val devInformCols = Seq("device_id", "subscriber_id", "device_model", "device_manufacturer",
      "software_version", "hardware_version", "hnc_enable",
      "uptime", "bytes_sent_current", "bytes_received_current",
      "wan_access_type", "downstream_curr_rate", "upstream_curr_rate",
      "memory_free", "last_reboot_reason", "ts_inform", "ts")

    val deviceInforms = spark.read.parquet(s"${conf.warehouseLocation}/gold/device_bw_inform/ts_date=${tsDate}")
      .dropDuplicates(Seq("device_id", "ts_inform"))
      .drop(dropCols: _*)
      .join(devices, usingColumns = Seq("device_id"))
      .select(devInformCols.head, devInformCols.tail: _*)
      .withColumnRenamed("downstream_curr_rate", "wan_link_rate_down")
      .withColumnRenamed("upstream_curr_rate", "wan_link_rate_up")

    val steeringStats = spark.read.parquet(s"${conf.warehouseLocation}/silver/element-ts/ts_date=${tsDate}")
      .select(col("device_id"), col("ts_inform"), explode(col("steering_stats")))
      .select("*", "col.*")
      .drop("steering_stats", "col")
      .withColumnRenamed("staMacAddress", "mac_address")
      .join(devices.drop(dropCols: _*), usingColumns = Seq("device_id"), joinType = "right")

    val clientTsCols = Seq("ts_inform", "subscriber_id", "device_id", "mac_address", "extender", "rssi", "radio",
      "bytes_received", "bytes_sent", "phy_rate", "disassociations_count_current",
      "deauthentication_count_current")

    val clientInforms = spark.sql("select * from client_ts")
      .select(clientTsCols.head, clientTsCols.tail: _*)
      .join(steeringStats, usingColumns = Seq("device_id", "mac_address", "ts_inform"))

    val deviceAndClientStats = deviceInforms.join(clientInforms, usingColumns = Seq("subscriber_id", "device_id", "ts_inform"), joinType = "left")
    createTable(deviceAndClientStats, "kpi_raw_data")

    val stmt =
      """
        select subscriber_id, ts, from_unixtime(ts/1000,'YYYY-MM-dd') as ts_date, serial_number, device_id,
          hardware_version, software_version, ts_inform,
          hnc_enable, extender, 0 as dns_fails, uptime,
          0 as cpu_load, memory_free, last_reboot_reason,
          uptime as wan_uptime, wan_access_type as wan_type,
          bytes_received_current as wan_bytes_rec, bytes_sent_current as wan_bytes_sent,
          (bytes_received_current*8.0)/(1.0*15*60*1000000) as wan_ds_throughput,
          (bytes_sent_current*8.0)/(1.0*15*60*1000000) as wan_us_throughput,
          wan_link_rate_down, wan_link_rate_up,
          radio, mac_address, 0 as chan_util,
          0 as pkts_dropped, 0 as pkt_errored, rssi, bytes_received, bytes_sent, phy_rate,
          deauthentication_count_current as deauth_count, disassociations_count_current as disassoc_count,
          staBtmAttempts as btm_attenmpts, staBtmSuccesses as btm_success, staSelfSteerCount as self_steer_cnt
        from kpi_raw_data where subscriber_id is not null
        order by
        subscriber_id, ts_inform desc
      """
    val kpiDetails = spark.sql(stmt).distinct()
    exportToWarehouse(kpiDetails, "hpm_raw_metrics")

    logger.info("Completed KPIRawMetrics")
  }

  def loadKPIResults(tableName: String)(implicit spark: SparkSession, conf: KPIConfig): DataFrame = {
    if (tempTables.contains(tableName)) {
      spark.sql(s"select * from $tableName")
    }
    else {
      val outputLocation =
        if (conf.filePrefix.isEmpty) s"${conf.outputLocation}/${tsDate}_${tableName}"
        else s"${conf.outputLocation}/${tsDate}_${conf.filePrefix}_${tableName}"

      val df = spark.read.option("header", "true").csv((s"${outputLocation}"))
      createTable(df, tableName, cache = true)
      df
    }
  }

  def loadDevices()(implicit spark: SparkSession, conf: KPIConfig): DataFrame = {
    logger.info("Loading devices from: {}", conf.deviceDir)
    val devDropCols = Seq("oui", "ts_created", "ts_updated", "acs_url",
      "device_type", "mac_address", "product_class",
      "provisioning_code", "time_zone", "first_inform",
      "last_inform", "last_boot"
    )

    val devices = spark.read.parquet(s"${conf.deviceDir}")
      .withColumn("ts_date", lit(s"${tsDate}"))
      .drop(devDropCols: _*)
    if (conf.filter.nonEmpty) {
      logger.info("filtering devices: {}", conf.filter)
      devices.filter(conf.filter)
    }
    else devices
  }

  def loadAnalysisData(devices: DataFrame)(implicit spark: SparkSession, conf: KPIConfig) {
    logger.info("Loading clientTs and Subscribers")

    val dropCols = Seq("device_model", "serial_number", "device_manufacturer", "software_version", "hardware_version",
      "hnc_enable", "subscriber_id", "zip", "city", "state", "country")

    /* load silver element_ts and join with device_ids only from devices and host the table as silver_ets */
    createTable(spark.read.parquet(s"${conf.warehouseLocation}/silver/element-ts/ts_date=${tsDate}")
      .drop(dropCols: _*)
      .dropDuplicates(Seq("device_id", "ts_inform"))
      .join(devices, usingColumns = Seq("device_id"))
      .withColumn("extender", lit(-1)),
      "silver_ets")

    /* load gold device_bw and join with device_ids only from devices and host the table as device_ts */
    createTable(spark.read.parquet(s"${conf.warehouseLocation}/gold/device_bw_inform/ts_date=${tsDate}")
      .drop(dropCols: _*)
      .dropDuplicates(Seq("device_id", "ts_inform"))
      .join(devices, usingColumns = Seq("device_id"))
      .withColumn("extender", lit(-1)),
      "device_ts")

    var clientBw = spark.read.parquet(s"${conf.warehouseLocation}/gold/client_bw_inform/ts_date=${tsDate}")
      .withColumn("tx_rate_multiplier", lit(1000))
      .drop(dropCols: _*)

    val windowSpec = Window.partitionBy("device_id", "mac_address", "ts").orderBy(desc("ts"))

    clientBw = clientBw.select(col("*"), row_number().over(windowSpec).alias("row_number"))
      .filter("row_number = 1")
      .join(devices, usingColumns = Seq("device_id"))

    createTable(clientBw, "client_ts")

    var stmt =
      """
          select *,
            (bytes_sent_current*8.0)/(1.0*15*60*1000000) as us_throughput_mbps,
            (bytes_received_current*8.0)/(1.0*15*60*1000000) as ds_throughput_mbps
          from client_ts
      """

    var clientTs = spark.sql(stmt)

    clientTs = clientTs.withColumn("phy_rate", when(col("last_transmit_rate") <= 0, 1000).otherwise(col("last_transmit_rate")))
    clientTs = clientTs.withColumn("phy_rate", when(col("phy_rate") > 1733000, 1733000).otherwise(col("phy_rate")))
    clientTs = clientTs.filter("software_version is not null").distinct()

    clientTs = clientTs.withColumn("extender", lit(-1))

    createTable(clientTs, "client_ts", cache = if (conf.fullPopulation) false else true)

    stmt =
      """
          select device_model, software_version, hnc_enable, extender,
            count (distinct subscriber_id) as sub_count
          from client_ts
          group by device_model, software_version, hnc_enable, extender
      """

    val subscribers = spark.sql(stmt).withColumn("ts_date", lit(s"${tsDate}"))

    createTable(subscribers, "subscribers", cache = true)
  }

  def createTable(df: DataFrame, tableName: String, cache: Boolean = false)(implicit spark: SparkSession): Unit = {
    df.createOrReplaceTempView(tableName)
    tempTables = tempTables :+ tableName
    if (cache) {
      spark.sql(s"cache table $tableName")
      cachedTables = cachedTables :+ tableName
    }
  }

  def dropTempTables()(implicit spark: SparkSession): Unit = {
    tempTables.foreach(t => spark.catalog.dropTempView(t))
  }

  def unCacheTables()(implicit spark: SparkSession): Unit =
    cachedTables.foreach(t => spark.sql(s"uncache table if exists $t"))

  def exportKPICounts(kpi: String)(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info(s"In exportKPICounts: kpi: ${kpi}")
    // to export the kpi counts as row values
    val stmt =
      s"""
        select ts_date, subscriber_id, serial_number, device_model,
          software_version, hnc_enable, extender, '$kpi' as hpm_id, $kpi as hpm_count
        from ${kpi}_agg
      """

    val kpiCount = spark.sql(stmt).distinct()
    exportToWarehouse(kpiCount, "hpm_count")
    logger.info(s"Completed exportKPICounts: kpi: ${kpi}")
  }

  def exportTripDistribution(kpi: String, tripDist: DataFrame)(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info(s"In exportKPIDistribution kpi: ${kpi}")

    val selCols = Seq("hpm_id", "ts_date", "device_model", "software_version", "hnc_enable", "extender",
      "trip_count", s"${kpi}_sum", s"${kpi}_min", s"${kpi}_max", s"${kpi}_avg", s"${kpi}_stddev", s"${kpi}_eval_count")

    val kpi_trip_dist = tripDist
      .withColumn("hpm_id", lit(kpi))
      .select(selCols.head, selCols.tail: _*)
      .withColumnRenamed(s"${kpi}_sum", "hpm_sum")
      .withColumnRenamed(s"${kpi}_min", "hpm_min")
      .withColumnRenamed(s"${kpi}_max", "hpm_max")
      .withColumnRenamed(s"${kpi}_avg", "hpm_avg")
      .withColumnRenamed(s"${kpi}_stddev", "hpm_stddev")
      .withColumnRenamed(s"${kpi}_eval_count", "hpm_eval_count")

    exportToWarehouse(kpi_trip_dist, "hpm_distribution")

    logger.info(s"Completed exportKPIDistribution kpi: ${kpi}")
  }

  def kpi1()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI1...")
    var stmt =
      """
          select ts_inform, device_model, subscriber_id, serial_number, software_version,
            mac_address, hnc_enable, extender,radio,rssi,ds_throughput_mbps, phy_rate,
            CASE WHEN rssi < -75 and ds_throughput_mbps > 1 THEN 1 ELSE 0 END AS case_1a,
            CASE WHEN (radio = '2.4G' and ds_throughput_mbps >1 and (phy_rate*1.0/1000) < 30) or
                      (radio = '5G' and ds_throughput_mbps >1 and (phy_rate*1.0/1000) < 60) THEN 1
                 ELSE 0
            END AS case_1b
          from client_ts
      """

    spark.sql(stmt).createOrReplaceTempView("kpi_1")

    stmt =
      """
          select ts_inform, subscriber_id, serial_number, device_model, software_version,
            hnc_enable, extender,
            mac_address, hnc_enable, extender,radio,rssi,ds_throughput_mbps, phy_rate, case_1a, case_1b,
            (case_1a | case_1b) as tripped
          from kpi_1
      """

    evalKPI(stmt, "kpi_1")
    logger.info("Completed KPI1")
  }

  def kpi2()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI2...")
    var stmt =
      """
          select subscriber_id, serial_number, device_model, software_version, ts_inform, hnc_enable, extender,
          count(mac_address) two_g_count
          from client_ts
          where radio = '2.4G'
          group by ts_inform, subscriber_id, serial_number, device_model, software_version, hnc_enable, extender
      """
    val twoGCount = spark.sql(stmt)

    stmt =
      """
          select subscriber_id, serial_number, device_model, software_version, ts_inform, hnc_enable, extender,
          count(mac_address) five_g_count
          from client_ts
          where radio = '5G'
          group by ts_inform, subscriber_id, serial_number, device_model, software_version, hnc_enable, extender
      """

    val fiveGCount = spark.sql(stmt)
    val radio2G5G = twoGCount.join(fiveGCount, usingColumns = Seq("ts_inform", "subscriber_id", "serial_number", "device_model", "software_version", "hnc_enable", "extender"), joinType = "inner")

    val radioClientRatio = radio2G5G.withColumn("ratio_5_2", col("five_g_count") / col("two_g_count"))

    createTable(radioClientRatio, "client_ratio")

    stmt =
      """
          select ts_inform, device_model, subscriber_id, serial_number, software_version, hnc_enable, extender, ratio_5_2,
            CASE WHEN ratio_5_2 < 1 THEN 1 ELSE 0 END AS tripped
          from client_ratio
      """

    evalKPI(stmt, "kpi_2")

    logger.info("Completed KPI2...")
  }

  def kpi2b()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI2b...")

    /* get the channel utilization for households that have enableRadio=Enabled */
    val chanUtils = spark.sql("select * from silver_ets")
      .select(col("subscriber_id"), col("device_id"), col("serial_number,"),
        col("device_model"),
        col("software_version"),
        col("hnc_enable"), col("extender"),
        col("ts_inform"),
        col("hne_stats"),
        explode(col("radio_stats")))
      .select("*", "col.radio", "col.enableRadio")
      .drop("col", "radio_stats")
      .filter("enableRadio = 'Enabled'")
      .select(col("*"), explode(col("hne_stats")))
      .select("*", "col.radio24ChanUtilization", "col.radio5ChanUtilization")
      .drop("hne_stats", "col")

    createTable(chanUtils, "chan_util")

    val stmt =
      """
          select subscriber_id, serial_number, device_model, software_version, ts_inform, hnc_enable, extender,
          case when ((radio = '2.4G' and radio24ChanUtilization > 50) or (radio = '5G' and radio5ChanUtilization > 80)) then 1 else 0 end as tripped
          from chan_util
      """
    evalKPI(stmt, "kpi_2b")
    logger.info("Completed KPI2b...")
  }

  def kpi3a()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI3a...")
    val stmt =
      """
          select ts_inform, device_model, subscriber_id, serial_number, software_version,
            mac_address, hnc_enable, extender, radio,rssi,deauthentication_count_current,
            CASE WHEN deauthentication_count_current > 0 THEN 1 ELSE 0 END AS tripped
          from client_ts
      """

    evalKPI(stmt, "kpi_3a")
    logger.info("Completed KPI3a")
  }

  def kpi3b()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI3b...")
    val steeringStats = spark.sql("select * from silver_ets")
      .select(col("device_id"), col("ts_inform"), explode(col("steering_stats")))
      .select("*", "col.*").drop("steering_stats", "col")

    val clientBw = spark.sql("select * from client_ts")
    val clientSteers = clientBw
      .join(steeringStats.withColumnRenamed("staMacAddress", "mac_address"), usingColumns = Seq("device_id", "mac_address", "ts_inform"), joinType = "left")
      .na.fill(Map("staBtmSuccesses" -> 0))

    clientSteers.createOrReplaceTempView("client_steers")

    val stmt =
      """
          select ts_inform, device_model, subscriber_id, serial_number, software_version, extender,
            mac_address, hnc_enable, radio, rssi, disassociations_count_current,
            staBtmSuccesses as steer_success_count,
            CASE WHEN (disassociations_count_current - staBtmSuccesses) > 1 and rssi >=-60 THEN 1 ELSE 0 END AS tripped
          from client_steers
      """

    evalKPI(stmt, "kpi_3b")
    logger.info("Completed KPI3b")
  }

  def kpi5()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI5...")
    val stmt =
      """
            select ts_inform, device_model, subscriber_id, serial_number, software_version, hnc_enable, extender,
            CASE WHEN uptime < 15*60 THEN 1 ELSE 0 END AS tripped
            from client_ts
        """
    evalKPI(stmt, "kpi_5")
    logger.info("Completed KPI5")
  }

  def kpi6()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI6...")
    val stmt =
      """
            select ts_inform, device_model, subscriber_id, serial_number, software_version, hnc_enable, extender,
            CASE WHEN uptime > 15*60 and wan_uptime_sec >= 10*60 and wan_uptime_sec < 15*60 THEN 1 ELSE 0 END AS tripped
            from device_ts
        """

    evalKPI(stmt, "kpi_6")
    logger.info("Completed KPI6")
  }


  def kpi7()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI7...")

    val stmt =
      """
            select ts_inform, device_model, subscriber_id, serial_number, software_version, hnc_enable, extender,
            case when ((bytes_received_current * 8.0)/(15*60*1000)) > (.9 * max_download_rate) then 1 else 0 end as tripped
            from device_ts
            where wan_access_type='DSL'
        """
    evalKPI(stmt, "kpi_7")
    logger.info("Completed KPI7")
  }

  def kpi8()(implicit spark: SparkSession, conf: KPIConfig): Unit = {
    logger.info("Creating KPI8...")

    spark.sql("select * from silver_ets")
      .select(
        col("subscriber_id"),
        col("serial_number"),
        col("device_id"),
        col("ts_inform"),
        col("device_model"),
        col("software_version"),
        col("hnc_enable"),
        col("extender"),
        col("uptime"),
        col("wifi_stats"),
        explode(col("radio_stats")))
      .select(
        col("*"),
        col("col.radio").alias("radio_stats_radio"),
        col("col.enableRadio").alias("radio_stats_enable_radio"),
        col("col.status").alias("radio_status"))
      .drop("col", "radio_stats")
      .select(
        col("*"),
        explode(col("wifi_stats")))
      .select(
        col("*"),
        col("col.radio").alias("wifi_stats_radio"),
        col("col.enableRadio").alias("wifi_stats_enable_radio"))
      .drop("col", "wifi_stats")
      .filter("col.accessPoint = 1 and radio_stats_radio = wifi_stats_radio")
      .select("*")
      .dropDuplicates(Seq("device_id", "ts_inform", "radio_stats_radio", "wifi_stats_enable_radio", "radio_status"))
      .createOrReplaceTempView("kpi_8_ts")

    val stmt =
      """
          select ts_inform, device_model, subscriber_id, serial_number, software_version, hnc_enable, extender, wifi_stats_enable_radio, uptime, radio_stats_enable_radio, radio_status,
          case when (wifi_stats_enable_radio != false and uptime > 900 and radio_stats_enable_radio = 'Enabled' and radio_status !='Up') then 1 else 0 end as tripped
          from kpi_8_ts
      """

    evalKPI(stmt, "kpi_8")
    logger.info("Completed KPI8...")
  }

  def evalKPI(kpiStmt: String, kpiName: String)(implicit spark: SparkSession): Unit = {
    val result = spark.sql(kpiStmt).withColumn("ts_date", lit(s"${tsDate}")).distinct()
    createTable(result, kpiName)

    val aggStmt =
      s"""
            select ts_date, subscriber_id, serial_number, device_model, software_version, hnc_enable, extender,
            sum(tripped) $kpiName
            from $kpiName
            group by ts_date, subscriber_id, serial_number, device_model, software_version, hnc_enable, extender
            order by $kpiName desc, subscriber_id, device_model, hnc_enable
        """

    val kpiAgg = spark.sql(aggStmt)
    createTable(kpiAgg, s"${kpiName}_agg")

    if (conf.exportToWarehouse) exportKPICounts(kpiName)

    val kpiTopN = kpiAgg.limit(conf.topN)
    createTable(kpiTopN, s"${kpiName}_top", cache = true)

    var stmt =
      s"""
          select ts_date, serial_number, device_model, software_version,hnc_enable, extender,
            sum($kpiName) ${kpiName}_sum,
            min($kpiName) ${kpiName}_min,
            max($kpiName) ${kpiName}_max,
            avg($kpiName) ${kpiName}_avg,
            stddev_pop($kpiName) ${kpiName}_stddev
          from ${kpiName}_agg
          group by ts_date, serial_number, device_model, software_version, hnc_enable, extender
      """

    val subscribers = spark.sql("select * from subscribers")

    val kpiDist = spark.sql(stmt)
      .join(subscribers, usingColumns = Seq("ts_date", "device_model", "software_version", "hnc_enable", "extender"), joinType = "right")
      .withColumnRenamed("sub_count", s"${kpiName}_eval_count")
      .na.fill(0)

    stmt =
      s"""
          select ts_date, device_model, software_version, hnc_enable, extender,
            count(subscriber_id) trip_count,
            sum($kpiName) ${kpiName}_sum,
            min($kpiName) ${kpiName}_min,
            max($kpiName) ${kpiName}_max,
            avg($kpiName) ${kpiName}_avg,
            stddev_pop($kpiName) ${kpiName}_stddev
          from ${kpiName}_agg
          where $kpiName >0
          group by ts_date, device_model, software_version, hnc_enable, extender
      """

    val kpiTripDist = spark.sql(stmt)
      .join(subscribers, usingColumns = Seq("ts_date", "device_model", "software_version", "hnc_enable", "extender"), joinType = "right")
      .withColumnRenamed("sub_count", s"${kpiName}_eval_count")
      .na.drop()

    save(kpiName, kpiTopN, kpiDist, kpiTripDist)

    if (conf.exportToWarehouse) exportTripDistribution(kpiName, kpiTripDist)

    if (conf.topNByModel) {
      createTopNByModel(kpiName, kpiAgg)
    }
  }

  def createTopNByModel(kpi: String, topN: DataFrame)(implicit sparkSession: SparkSession): Unit = {
    val modelWindow = Window.partitionBy("device_model").orderBy(desc(s"$kpi"))
    val topNByModelDF = topN.select(col("*"), rank().over(modelWindow).alias("").alias("rank_by_model"))
      .filter(s"rank_by_model <= ${conf.topN}")
    exportToScratchpad(topNByModelDF, s"top_n_by_model_$kpi")
  }

  def save(kpi: String, topN: DataFrame, dist: DataFrame, tripDist: DataFrame, mode: String = "error")(implicit conf: KPIConfig): Unit = {
    if (conf.exportToScratchpad) {
      exportToScratchpad(topN, s"${kpi}_top")
      if (conf.exportOverallDist) exportToScratchpad(dist, s"${kpi}_dist")
      exportToScratchpad(tripDist, s"${kpi}_trip_dist")
    }
  }

  def exportToScratchpad(dataFrame: DataFrame, outputName: String)(implicit conf: KPIConfig): Unit = {
    val outputLocation =
      if (conf.filePrefix.isEmpty) s"${conf.outputLocation}/${tsDate}_${outputName}"
      else s"${conf.outputLocation}/${tsDate}_${conf.filePrefix}_${outputName}"

    dataFrame.repartition(1).write.mode(s"${conf.mode}").option("header", "true").csv(outputLocation)
  }

  def exportToWarehouse(dataFrame: DataFrame, outputName: String)(implicit conf: KPIConfig): Unit = {
    val outputLocation = s"${conf.warehouseLocation}/gold/csv/hpm/${outputName}_${tsDate}"
    dataFrame.write.mode("append").option("header", "false").csv(outputLocation)
  }
}

case class KPIConfig(args: Array[String]) {
  val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")

  var warehouseLocation: String = ""
  var deviceDir: String = ""
  var tsDate: String = ""
  var outputLocation: String = ""
  var mode: String = "append"
  var filter: String = ""
  var topN: Int = 10
  var exportToScratchpad: Boolean = true
  var exportToWarehouse: Boolean = true
  var fullPopulation: Boolean = false
  var createKPIs: Boolean = true
  var kpiSet: Seq[String] = Seq()
  var createSummary: Boolean = true
  var createDetails: Boolean = false
  var tsDateDiff: Int = 1
  var timeZone: String = "UTC"
  var filePrefix: String = ""
  var topNByModel: Boolean = false
  var createRawMetrics: Boolean = false
  var exportOverallDist: Boolean = false

  args.foreach(a => {
    val kv = a.split("==")
    if (kv.length > 1) {
      kv(0).trim match {
        case c@"warehouse_location" => warehouseLocation = kv(1).trim
        case c@"devices_location" => deviceDir = kv(1).trim
        case c@"output_location" => outputLocation = kv(1).trim
        case c@"ts_date" => tsDate = kv(1).trim
        case c@"time_zone" => timeZone = kv(1).trim
        case c@"ts_date_diff" => tsDateDiff = kv(1).trim.toInt
        case c@"kpis" => kpiSet = kv(1).trim.split(",").toSet.toSeq
        case c@"mode" => mode = kv(1).trim
        case c@"filter" => filter = kv(1).trim
        case c@"topN" => topN = kv(1).trim.toInt
        case c@"scratchpad_export" => exportToScratchpad = kv(1).trim.toBoolean
        case c@"warehouse_export" => exportToWarehouse = kv(1).trim.toBoolean
        case c@"create_kpis" => createKPIs = kv(1).trim.toBoolean
        case c@"full_pop" => fullPopulation = kv(1).trim.toBoolean
        case c@"details" => createDetails = kv(1).trim.toBoolean
        case c@"summary" => createSummary = kv(1).trim.toBoolean
        case c@"file_prefix" => filePrefix = kv(1).trim
        case c@"top_n_by_model" => topNByModel = kv(1).trim.toBoolean
        case c@"raw_metrics" => createRawMetrics = kv(1).trim.toBoolean
        case c@"overall_dist" => exportOverallDist = kv(1).trim.toBoolean
      }
    }
  })

  defaultTsDate()

  if (kpiSet.nonEmpty) {
    createKPIs = true
    createDetails = false
  }
  else {
    kpiSet = "1,2,2b,3a,3b,5,6,7,8".split(",")
  }

  if (!createSummary) createDetails = false

  def defaultTsDate(): Unit = {
    if (tsDate.isEmpty) {
      val now = Instant.now().atZone(ZoneId.of(timeZone))
      tsDate = if (tsDateDiff > 0) now.minusDays(tsDateDiff).format(dateFormat) else now.format(dateFormat)
    }
  }

  override def toString = Seq(s"kpis: ${kpiSet.mkString(",")}", s"warehouseLocation: $warehouseLocation", s"deviceDir: $deviceDir", s"tsDate: $tsDate",
    s"outputLocation: $outputLocation", s"mode: $mode", s"filter: $filter", s"topN: $topN", s"scratchpadExport: $exportToScratchpad", s"fullPop: $fullPopulation", s"createKPIs: $createKPIs",
    s"createSummary: $createSummary", s"createDetails: $createDetails", s"tsDateDiff: $tsDateDiff", s"timeZone: $timeZone").mkString(",")
}
