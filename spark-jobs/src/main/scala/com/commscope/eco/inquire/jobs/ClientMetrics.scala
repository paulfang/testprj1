package com.commscope.eco.inquire.jobs


import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.functions.{col, explode, lit}
import org.apache.spark.sql.{DataFrame, SparkSession}

object ClientMetrics extends App {
  val logger = Logger(this.getClass)

  if (args.isEmpty) {
    logger.warn("Nothing to do....no command line arguments passed")
  }
  else {
    implicit val conf = JobConfig(args)
    logger.warn("Going to job with following configuration: {}", conf)

    if (conf.tsDate.isEmpty || conf.warehouseLocation.isEmpty || conf.outputLocation.isEmpty) {
      logger.info("warehouse_location, ts_date or output_location undefined")
    } else {
      implicit val spark = SparkSession.builder()
        .config(new SparkConf().setAppName("clientMetrics"))
        .getOrCreate()

      val commonJoinCols = Seq("device_id", "ts_date", "ts_inform")

      val silverElementTs = loadSilverElementTs()

      val clientMetrics = queryClientMetrics(silverElementTs)
      logger.info("clientMetrics columns to join: {}, joinColumns: {}", clientMetrics.columns, commonJoinCols)

      val deviceMetrics = queryDeviceMetrics(silverElementTs)
      logger.info("deviceMetrics columns to join: {}", deviceMetrics.columns)

      logger.info("saving clientMetrics")
      clientMetrics
        .join(deviceMetrics, commonJoinCols)
        .write.partitionBy(conf.partitions: _*)
        .mode(conf.mode)
        .format(conf.format)
        .save(conf.outputLocation)
    }
  }

  private def loadSilverElementTs()(implicit sparkSession: SparkSession, conf: JobConfig): DataFrame = {
    logger.info("loading silverElementTs")
    val df = sparkSession.read.parquet(s"${conf.warehouseLocation}/silver/element-ts/ts_date=${conf.tsDate}")
      .withColumn("ts_date", lit(s"${conf.tsDate}"))
      .dropDuplicates("device_id", "ts_inform")

    if (conf.filter.isEmpty) df else df.filter(conf.filter)
  }

  def queryClientMetrics(silverElementTs:DataFrame)(implicit sparkSession: SparkSession, conf: JobConfig):DataFrame = {
    logger.info("querying queryClientMetrics")

    logger.info("loading client_bw_inform")
    var df = sparkSession.read.parquet(s"${conf.warehouseLocation}/gold/client_bw_inform/ts_date=${conf.tsDate}")
      .withColumn("ts_date", lit(s"${conf.tsDate}"))
      .withColumnRenamed("bytes_received_current","client_bytes_received")
      .withColumnRenamed("bytes_sent_current","client_bytes_sent")
      .drop("uptime")
      .dropDuplicates(Seq("device_id", "radio", "mac_address", "hnc_enable", "ts_inform"))

    df = if (conf.filter.isEmpty) df else df.filter(conf.filter)

    val steeringStats = querySteeringStats(silverElementTs)
      .withColumnRenamed("staMacAddress","mac_address")

    df.join(steeringStats, Seq("device_id", "ts_date", "ts_inform", "mac_address"))
  }

  def querySteeringStats(silverElementTs:DataFrame)(implicit sparkSession: SparkSession, conf: JobConfig): DataFrame = {
    logger.info("querying querySteeringStats")

    val steeringStats = silverElementTs
      .select(col("device_id"), col("ts_date"), col("ts_inform"), explode(col("steering_stats")))
      .select("*", "col.*")
      .drop("steering_stats", "col")

    steeringStats
  }

  def queryDeviceMetrics(silverElementTs:DataFrame)(implicit sparkSession: SparkSession, conf: JobConfig): DataFrame = {
    logger.info("querying queryDeviceMetrics")
    val commonJoinCols = Seq("device_id","ts_date","ts_inform")

    val devColumns = Seq("device_id","ts_date","ts_inform",
      "uptime", "connection_type",
      "bytes_received_current","bytes_sent_current",
      "downstream_curr_rate","upstream_curr_rate")

    logger.info("loading device_bw_inform")
    val devBwInform = sparkSession.read.parquet(s"${conf.warehouseLocation}/gold/device_bw_inform/ts_date=${conf.tsDate}")
      .withColumn("ts_date", lit(s"${conf.tsDate}"))
      .dropDuplicates(Seq("device_id", "ts_inform"))
      .select(devColumns.head, devColumns.tail :_*)
      .withColumnRenamed("bytes_received_current","wan_bytes_received")
      .withColumnRenamed("bytes_sent_current","wan_bytes_sent")

    val silverTsCols = Seq("device_id", "ts_date", "ts_inform",
      "memory_free","memory_total","last_reboot_reason",
      "wan_access_type", "wan_uptime_sec")

    val silverStats = silverElementTs.select(silverTsCols.head, silverTsCols.tail :_*)

    val devStats = devBwInform.join(silverStats, commonJoinCols, joinType="left")

    val dnsStats = silverElementTs
      .select(col("device_id"), col("ts_date"), col("ts_inform"), explode(col("dns_stats")))
      .select("*", "col.*")
      .drop("dns_stats", "col")

    val dnsServerStats = silverElementTs
      .select(col("device_id"), col("ts_date"), col("ts_inform"), explode(col("dns_server_stats")))
      .select("*", "col.*")
      .drop("dns_server_stats", "col")

    val dnsJoinColumns = Seq("device_id","ts_date","ts_inform","dnsIndex")
    val commonDnsStats = dnsServerStats.join(dnsStats, dnsJoinColumns, joinType = "left")

    devStats.join(commonDnsStats, commonJoinCols)
  }
}

case class JobConfig(args: Array[String]) {
  var warehouseLocation: String = ""
  var tsDate: String = ""
  var outputLocation: String = ""
  var partitions: Array[String] = Array("ts_date")
  var format: String = "parquet"
  var mode: String = "append"
  var filter: String = ""

  args.foreach(a => {
    val kv = a.split("==")
    if (kv.length > 1) {
      kv(0).trim match {
        case c@"warehouse_location" => warehouseLocation = kv(1).trim
        case c@"output_location" => outputLocation = kv(1).trim
        case c@"ts_date" => tsDate = kv(1).trim
        case c@"partitions" => partitions = kv(1).split(",")
        case c@"format" => format = kv(1).trim
        case c@"mode" => mode = kv(1).trim
        case c@"filter" => filter = kv(1).trim
      }
    }
  })

  override def toString = args.mkString(",")
}
