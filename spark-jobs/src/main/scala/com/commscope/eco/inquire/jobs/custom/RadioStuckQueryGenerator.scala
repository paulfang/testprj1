package com.commscope.eco.inquire.jobs.custom

import com.commscope.eco.inquire.jobs.{DataExportConfig, QueryGenerator}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions.{col, explode}

object RadioStuckQueryGenerator extends QueryGenerator {
  override def generate(implicit sparkSession: SparkSession, config: DataExportConfig): String = {
    val stuckSN = sparkSession.read.parquet(config.deviceLocation)
    getElementTsSilver
      .join(stuckSN, usingColumns = Seq("serial_number"), joinType = "right")
      .select(
        col("subscriber_id"),
        col("usi"),
        col("serial_number"),
        col("device_id"),
        col("ts_date"),
        col("ts_inform"),
        col("device_model"),
        col("software_version"),
        col("hnc_enable"),
        col("uptime"),
        col("wifi_stats"),
        explode(col("radio_stats")))
      .select(
        col("*"),
        col("col.radio").alias("radio_stats_radio"),
        col("col.enableRadio").alias("radio_stats_enable_radio"),
        col("col.status").alias("radio_status"))
      .drop("col", "radio_stats")
      .select(
        col("*"),
        explode(col("wifi_stats")))
      .select(
        col("*"),
        col("col.radio").alias("wifi_stats_radio"),
        col("col.enableRadio").alias("wifi_stats_enable_radio"))
      .drop("col", "wifi_stats")
      .filter("col.accessPoint = 1 and radio_stats_radio = wifi_stats_radio")
      .select("*")
      .dropDuplicates(Seq("device_id", "ts_date", "ts_inform", "radio_stats_radio", "wifi_stats_enable_radio", "radio_status"))
      .createOrReplaceTempView("radio_stuck")

      """
          select ts_date, ts_inform, subscriber_id, usi, serial_number, device_model, software_version, hnc_enable, uptime,
          radio_stats_radio as radio,
          radio_status as radioStatus,
          wifi_stats_enable_radio as wirelessSchedulerEnable,
          radio_stats_enable_radio radioSchedulerEnable
          from radio_stuck
          order by ts_date, usi, ts_inform, radio_stats_radio
      """
  }
}
