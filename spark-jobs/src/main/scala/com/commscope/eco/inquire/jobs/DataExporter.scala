package com.commscope.eco.inquire.jobs

import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

import java.time.format.DateTimeFormatter
import java.time.{Instant, ZoneId}
import scala.reflect.runtime.universe
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._


object DataExporter extends App {
  private val logger = Logger(DataExporter.getClass)

  if (args.isEmpty) {
    logger.warn("Nothing to do....no command line arguments passed")
  }
  else {
    implicit val conf = DataExportConfig(args)
    if (!conf.isValid) logger.warn("Invalid configuration, nothing to do")
    else {
      implicit val spark = SparkSession.builder()
        .config(new SparkConf().setAppName("data exporter"))
        .getOrCreate()

      val query =
        if (conf.queryGenerator.nonEmpty) generateQuery
        else {
          conf.tables.foreach(e => {
            var df = spark.read.format("parquet").load(e._2)
            df =
              if (conf.selectColumns.contains(e._1)) df.select(conf.selectColumns(e._1): _*)
              else df

            df =
              if (conf.dropColumns.contains(e._1)) df.drop(conf.dropColumns(e._1): _*)
              else df

            df.createOrReplaceTempView(e._1)
          })

          conf.query
        }

      val df = if (conf.repartition > 0) spark.sql(query).repartition(conf.repartition) else spark.sql(query)

      var writer = df.write.mode(conf.mode).format(conf.format)

      if (conf.format.equalsIgnoreCase("csv")) {
        if (conf.header) writer = writer.option("header", true)
        writer = writer.option("quoteAll", true).option("compression", "gzip")
      }

      if (conf.partitions.nonEmpty) {
        writer = writer.partitionBy(conf.partitions: _*)
      }

      val outputLocation =
        if (conf.fileName.nonEmpty) {
          if (conf.tsDate.nonEmpty) s"${conf.outputLocation}/${conf.tsDate}_${conf.fileName}"
          else s"${conf.outputLocation}/${conf.fileName}"
        }
        else conf.outputLocation
      writer.save(outputLocation)
    }
  }

  def generateQuery(implicit sparkSession: SparkSession, config: DataExportConfig): String = {
    val generator = config.queryGenerator

    logger.debug("queryGenerator class: {}", generator)
    try {
      val mirror = universe.runtimeMirror(getClass.getClassLoader)
      val module = mirror.staticModule(generator)
      val obj = mirror.reflectModule(module)
      obj.instance.asInstanceOf[QueryGenerator].generate
    }
    catch {
      case t: Throwable => logger.warn("Error loading queryGenerator for className {}: {}", generator, t.getMessage)
        throw t
    }
  }
}

case class DataExportConfig(args: Array[String]) {
  private val logger = Logger(DataExporter.getClass)
  val dateFormat = DateTimeFormatter.ofPattern("yyyy-MM-dd")

  var query: String = ""
  var queryGenerator: String = ""
  var outputLocation: String = ""
  var repartition: Int = -1
  var partitions: Array[String] = Array()
  var tables: Map[String, String] = Map()
  var format: String = "parquet"
  var header:Boolean = false
  var mode: String = "overwrite"
  var dropColumns: Map[String, Seq[String]] = Map()
  var selectColumns: Map[String, Seq[Column]] = Map()
  var deviceLocation: String = ""
  var warehouseLocation: String = ""
  var tsDate: String = ""
  var fileName: String = ""
  var tsDateDiff: Int = 1
  var timeZone: String = "UTC"

  args.foreach(a => {
    val kv = a.split("==")
    if (kv.length > 1) {
      kv(0).trim match {
        case c@"query" => query = kv(1).trim
        case c@"output_location" => outputLocation = kv(1).trim
          if (!outputLocation.endsWith("/")) outputLocation +"/"
        case c@"partitions" => partitions = kv(1).split(",")
        case c@"table" => val nameVal = kv(1).split("=")
          tables += (nameVal(0).trim -> nameVal(1).trim)
        case c@"drop" => val nameVal = kv(1).split("=")
          dropColumns += (nameVal(0).trim -> nameVal(1).split(",").foldLeft(Seq[String]())((s, c) => s :+ c.trim))
        case c@"select" => val nameVal = kv(1).split("=")
          selectColumns += (nameVal(0).trim -> nameVal(1).split(",").foldLeft(Seq[Column]())((s, c) => s :+ col(c.trim)))
        case c@"format" => format = kv(1).trim
        case c@"mode" => mode = kv(1).trim
        case c@"query_generator" => queryGenerator = kv(1).trim
        case c@"repartition" => repartition = kv(1).trim.toInt
        case c@"warehouse_location" => warehouseLocation = kv(1).trim
        case c@"file_name" => fileName = kv(1).trim
        case c@"device_location" => deviceLocation = kv(1).trim
        case c@"ts_date" => tsDate = kv(1).trim
        case c@"time_zone" => timeZone = kv(1).trim
        case c@"ts_date_diff" => tsDateDiff = kv(1).trim.toInt
        case c@"header" => header = kv(1).trim.toBoolean
      }
    }
  })

  if (queryGenerator.nonEmpty) defaultTsDate()

  def isValid: Boolean = {
    if (query.isEmpty && queryGenerator.isEmpty) {
      logger.warn("query and queryGenerator are both undefined")
      false
    }
    else if (outputLocation.isEmpty) {
      logger.warn("outputLocation is undefined")
      false
    }
    else if (query.nonEmpty && tables.isEmpty) {
      logger.info("tables for query are undefined")
      false
    }
    else if (queryGenerator.nonEmpty && warehouseLocation.isEmpty) {
      logger.info("warehouseLocation for queryGenerator is undefined")
      false
    }
    else true
  }

  def defaultTsDate(): Unit = {
    if (tsDate.isEmpty) {
      val now = Instant.now().atZone(ZoneId.of(timeZone))
      tsDate = if (tsDateDiff > 0) now.minusDays(tsDateDiff).format(dateFormat) else now.format(dateFormat)
    }
  }

  override def toString = args.mkString(",")
}

trait QueryGenerator {
  var tempTables = Seq[String]()

  val window = Window.partitionBy("serial_number").orderBy(desc("last_inform"))

  def getDevices(implicit sparkSession: SparkSession, config: DataExportConfig): DataFrame = {
    if (tempTables.contains("device")) {
      sparkSession.sql("select * from device")
    } else {
      val devices = sparkSession.read.parquet(s"${config.warehouseLocation}/gold/device_population")
        .select(col("*"), row_number.over(window).alias("row_num"))
        .filter("row_num = 1")
        .drop("row_num")

      devices.createOrReplaceTempView("device")
      tempTables = tempTables :+ "device"
      devices
    }
  }

  def getSubscribers(implicit sparkSession: SparkSession, config: DataExportConfig): DataFrame = {
    if (tempTables.contains("subscriber")) {
      sparkSession.sql("select * from subscriber")
    } else {
      val devices = getDevices

      val subs = sparkSession.read.parquet(s"${config.warehouseLocation}/gold/subscriber_population")
        .join(devices.select("subscriber_id"), usingColumns = Seq("subscriber_id"), joinType = "right")

      subs.createOrReplaceTempView("subscriber")
      tempTables = tempTables :+ "subscriber"
      subs
    }
  }

  def getDevicesWithUSI(implicit sparkSession: SparkSession, config: DataExportConfig): DataFrame = {
    if (tempTables.contains("device_usi_assoc")) {
      sparkSession.sql("select * from device_usi_assoc")
    }
    else {
      val deviceUsiAssoc = sparkSession.read.parquet(s"${config.warehouseLocation}/gold/subscriber_population")
        .select("subscriber_id", "usi")
        .distinct()
        .join(getDevices, usingColumns = Seq("subscriber_id"), joinType = "right")

      deviceUsiAssoc.createOrReplaceTempView("device_usi_assoc")
      tempTables = tempTables :+ "device_usi_assoc"
      deviceUsiAssoc
    }
  }

  def getElementTsSilver(implicit sparkSession: SparkSession, config: DataExportConfig): DataFrame = {
    if (tempTables.contains("element_ts")) {
      sparkSession.sql("select * from element_ts")
    } else {
      val dropCols = Seq("device_model", "serial_number", "device_manufacturer", "software_version", "hardware_version",
        "hnc_enable", "subscriber_id", "zip", "city", "state", "country")

      val ets = sparkSession.read.parquet(s"${config.warehouseLocation}/silver/element-ts/ts_date=${config.tsDate}")
        .withColumn("ts_date", lit(s"${config.tsDate}"))
        .drop(dropCols: _*)
        .dropDuplicates(Seq("device_id", "ts_inform"))
        .join(getDevicesWithUSI, usingColumns = Seq("device_id"))

      ets.createOrReplaceTempView("element_ts")
      tempTables = tempTables :+ "element_ts"
      ets
    }
  }

  def generate(implicit sparkSession: SparkSession, config: DataExportConfig): String
}
