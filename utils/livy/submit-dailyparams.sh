export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":4,"jars":["s3a://eco.inquire.dev://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire.dev/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire.dev/frontier/spark/jobs/main.py", "pyFiles":["s3a://eco.inquire.dev/frontier/spark/jobs/jobs.zip"], "args":["--job=Element","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire.dev,startdir=s3a://eco.inquire.dev/frontier/data/incoming,enddir=s3a://eco.inquire.dev/frontier/data/processed/dailyparams,filepat=/dailyparams-*,db=frontier,csv_out=1,elements_properties_json=acsURL:deviceType:macAddress:manufacturerName:modelName:hardwareVersion:softwareVersion:productClass:provisioningCode:timeZone:hncEnable,elements_properties_table=acs_url:device_type:mac_address:manufacturer:model_name:hardware_version:software_version:product_class:provisioning_code:time_zone:hnc_enabled,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool

