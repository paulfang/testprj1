#!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":2,"jars":["s3a://eco.inquire.dev://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire.dev/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire.dev/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire.dev/frontier/spark/jobs/jobs.zip"], "args":["--job=Subscriber","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire.dev,startdir=s3a://eco.inquire.dev/frontier/data/incoming,enddir=s3a://eco.inquire.dev/frontier/data/processed/subscriber,filepat=/subscriber-*,db=frontier,csv_out=1,subscriber_details_json=ZIP:equipmentName:Profile:Technology:Suspension:suspensionDate,subscriber_details_table=zip:equipment_name:profile:technology:suspension:suspension_date,subscriber_identifiers_json=DropId,subscriber_identifiers_table=drop_id,subscriber_dimension_json=country:state:city:zip,subscriber_dimension_table=country:state:city:zip,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


