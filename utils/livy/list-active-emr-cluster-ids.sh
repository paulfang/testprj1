export cluster_name=$1
/usr/local/bin/aws emr list-clusters --active --output text --query 'Clusters[*][Id,Name,Status.State]' | grep "${cluster_name}" | awk '{print $1}'
