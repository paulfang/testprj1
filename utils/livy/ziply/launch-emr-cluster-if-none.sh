#!/bin/bash

#if [ ! "$(aws emr list-clusters --active | grep Id | cut -d: -f2 | sed 's/.$//')" ]
if [ ! "$(aws emr list-clusters --active --output text --query 'Clusters[*][Id,Name,Status.State]' | grep -q 'eco.inquire.ziply')" ]
then
    echo no active emr cluster found, going to launch one
    ~/launch-emr-cluster-dev.sh
else
    echo active emr cluster found
    ~/describe-active-emr-cluster.sh
fi
