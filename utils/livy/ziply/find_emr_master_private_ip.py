import os
import boto3

emrClient = boto3.client('emr', region_name='us-east-1')
emrClusters = emrClient.list_clusters(ClusterStates=['WAITING'])


def find_any_cluster_by_name(clusterName):
    #print('finding waiting cluster by name: {}'.format(clusterName))
    if emrClusters:
        res = [c for c in emrClusters['Clusters'] if c['Name'] == clusterName]
        if res:
            return res[0]
        else:
            print("There are no active cluster by name: {}".format(clusterName))
            return None
    else:
        print("No active clusters")
        None


def find_any_cluster_id_by_name(clusterName):
    res = find_any_cluster_by_name(clusterName)
    #print('found a waiting cluster by name: {}'.format(clusterName))
    if res:
        return res['Id']
    else:
        None


def find_any_master_instance_of_cluster_by_name(clusterName):
    clusterId = find_any_cluster_id_by_name(clusterName)
    #print('found a waiting cluster id: {}'.format(clusterId))

    if clusterId:
        igs = emrClient.list_instance_groups(ClusterId=clusterId)['InstanceGroups']
        aMasterGId = [g['Id'] for g in igs if 'Master' in g['Name']][0]

        instances = emrClient.list_instances(ClusterId=clusterId)['Instances']
        if instances:
            masterInstances =  [i for i in instances if i['InstanceGroupId']==aMasterGId]
            #print("Master instances: {}".format(masterInstances))
            return masterInstances[0]
        if not instances:
            print("There are no instances found in cluster[id: {}, name: {}".format(clusterId, clusterName))
            return None

    else:
        None


def find_private_ip_of_any_master_instance_of_cluster_by_name(clusterName):
    master = find_any_master_instance_of_cluster_by_name(clusterName)
    #print('found a master instance: {}'.format(master))
    if master:
        return master['PrivateIpAddress']


################################################################################
# Main
################################################################################
if __name__ == '__main__':
    clusterName = 'eco.inquire.ziply-auto'
    if emrClusters:
        clusters = emrClusters['Clusters']
        clusterNames = [c['Name'] for c in clusters if c['Name']==clusterName]
        if clusterNames:
            masterPrivateIp = find_private_ip_of_any_master_instance_of_cluster_by_name(clusterNames[0])
            os.environ['EMR_MASTER_IP']=masterPrivateIp
            print(masterPrivateIp)
        else:
            print('There are no active clusters by name: {}'.format(clusterName))
    else:
        print('There ar no active clsuters')
        exit(-1)