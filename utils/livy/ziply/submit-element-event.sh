#!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":4,"jars":["s3a://eco.inquire.ziply/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire.ziply/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire.ziply/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire.ziply/spark/jobs/jobs.zip"], "args":["--job=ElementEvent","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire.ziply,startdir=s3a://eco.inquire.ziply/data/incoming,enddir=s3a://eco.inquire.ziply/data/processed/element-event,filepat=/element-event-*,db=public,csv_out=1,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


