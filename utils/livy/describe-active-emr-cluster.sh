launch-emr-cluster-if-none.sh                                                                       0000775 0001750 0001750 00000000413 13661271512 016034  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash

if [ ! "$(aws emr list-clusters --active | grep Id | cut -d: -f2 | sed 's/.$//')" ]
then
    echo no active emr cluster found, going to launch one
    ~/launch-emr-cluster.sh
else
    echo active emr cluster found
    ~/describe-active-emr-cluster.sh
fi
                                                                                                                                                                                                                                                     launch-emr-cluster.sh                                                                               0000775 0001750 0001750 00000004756 13661467327 014534  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash

/usr/local/bin/aws emr create-cluster --applications Name=Hadoop Name=Spark Name=Livy Name=Hive \
--tags 'Customer=Frontier' 'Profile=dev' 'Name=eco.inquire-3-emr' \
--ec2-attributes '{"KeyName":"fedauth-eco-inquire3-telmex","AdditionalSlaveSecurityGroups":["sg-057bc5db2304875d0"],"InstanceProfile":"Role-EC2-S3-EcoInquire-3","SubnetId":"subnet-0164945e","EmrManagedSlaveSecurityGroup":"sg-057bc5db2304875d0","EmrManagedMasterSecurityGroup":"sg-057bc5db2304875d0","AdditionalMasterSecurityGroups":["sg-057bc5db2304875d0"]}' \
--release-label emr-6.0.0 \
--log-uri 's3n://aws-logs-097289183747-us-east-1/elasticmapreduce/' \
--instance-groups '[{"InstanceCount":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":2}]},"InstanceGroupType":"MASTER","InstanceType":"m5.xlarge","Name":"Master - 1"},{"InstanceCount":2,"InstanceGroupType":"CORE","InstanceType":"m5d.xlarge","Name":"Core - 2"},{"InstanceCount":10,"InstanceGroupType":"TASK","InstanceType":"m5d.xlarge","Name":"Task - 3"}]' \
--configurations '[{"Classification":"yarn-env","Properties":{},"Configurations":[{"Classification":"export","Properties":{"PYSPARK_PYTHON":"/usr/bin/python3"}}]},{"Classification":"spark-defaults","Properties":{"spark.driver.memory":"10g","spark.driver.cores":"2","spark.executors.cores":"2","yarn.nodemanager.pmem-check-enabled":"false","spark.executors.memory":"10g","yarn.nodemanager.vmem-check-enabled":"false","spark.sql.caseSensitive":"true","spark.default.parallelism":"12","spark.executor.memoryOverhead":"2g","spark.executor.extraJavaOptions":"-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='\''kill -9 %p'\''","spark.executor.instances":"3","spark.dynamicAllocation.enabled":"false","spark.driver.extraJavaOptions":"-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='\''kill -9 %p'\''","spark.sql.warehouse.dir":"s3://eco.inquire-3/frontier/warehouse/"}}]' \
--bootstrap-actions '[{"Path":"s3://eco.inquire-3/install_inquire_dependencies.sh","Name":"Install inquire dependencies"}]' \
--ebs-root-volume-size 10 \
--service-role EMR_DefaultRole \
--enable-debugging \
--name 'eco.inquire-3-frontier-livy-auto' \
--scale-down-behavior TERMINATE_AT_TASK_COMPLETION \
--region us-east-1

                  launch-emr-cluster-small.sh                                                                         0000775 0001750 0001750 00000004777 13661271335 015635  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash

/usr/local/bin/aws emr create-cluster --applications Name=Hadoop Name=Spark Name=Livy Name=Hive \
--tags 'Customer=Frontier' 'Profile=dev' 'Name=eco.inquire-3-emr' \
--ec2-attributes '{"KeyName":"fedauth-eco-inquire3-telmex","AdditionalSlaveSecurityGroups":["sg-057bc5db2304875d0"],"InstanceProfile":"Role-EC2-S3-EcoInquire-3","SubnetId":"subnet-0164945e","EmrManagedSlaveSecurityGroup":"sg-057bc5db2304875d0","EmrManagedMasterSecurityGroup":"sg-057bc5db2304875d0","AdditionalMasterSecurityGroups":["sg-057bc5db2304875d0"]}' \
--release-label emr-6.0.0 \
--log-uri 's3n://aws-logs-097289183747-us-east-1/elasticmapreduce/' \
--instance-groups '[{"InstanceCount":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":2}]},"InstanceGroupType":"MASTER","InstanceType":"m5.xlarge","Name":"Master - 1"},{"InstanceCount":1,"InstanceGroupType":"CORE","InstanceType":"m5d.xlarge","Name":"Core - 2"}]' \
--configurations '[{"Classification":"yarn-env","Properties":{},"Configurations":[{"Classification":"export","Properties":{"PYSPARK_PYTHON":"/usr/bin/python3"}}]},{"Classification":"spark-defaults","Properties":{"spark.driver.memory":"10g","spark.driver.cores":"2","spark.executors.cores":"2","yarn.nodemanager.pmem-check-enabled":"false","spark.executors.memory":"10g","yarn.nodemanager.vmem-check-enabled":"false","spark.sql.caseSensitive":"true","spark.default.parallelism":"12","spark.executor.memoryOverhead":"2g","spark.executor.extraJavaOptions":"-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='\''kill -9 %p'\''","spark.executor.instances":"3","spark.dynamicAllocation.enabled":"false","spark.driver.extraJavaOptions":"-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='\''kill -9 %p'\''","spark.sql.warehouse.dir":"s3://eco.inquire-3/frontier/warehouse/"}}]' \
--bootstrap-actions '[{"Path":"s3://eco.inquire-3/install_inquire_dependencies.sh","Name":"Install inquire dependencies"},{"Path":"s3://eco.inquire-3/aws-assign-private-ip-python2.py","Args":["172.31.32.16"],"Name":"Custom action"}]' \
--ebs-root-volume-size 10 \
--service-role EMR_DefaultRole \
--enable-debugging \
--name 'eco.inquire-3-frontier-livy-small' \
--scale-down-behavior TERMINATE_AT_TASK_COMPLETION \
--region us-east-1

 list-active-emr-clusters.sh                                                                         0000775 0001750 0001750 00000000037 13661264175 015651  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 aws emr list-clusters --active
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 list-active-emr-ec2-instances.sh                                                                    0000775 0001750 0001750 00000000220 13661472266 016437  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 /usr/local/bin/aws emr list-clusters --active | grep Id | cut -d: -f2 | sed 's/.$//' | xargs /usr/local/bin/aws emr list-instances --cluster-id
                                                                                                                                                                                                                                                                                                                                                                                list-batch-by-id.sh                                                                                 0000775 0001750 0001750 00000000402 13661515351 014025  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`
echo 'master_ip' ${EMR_MASTER_IP}
curl http://${EMR_MASTER_IP}:8998/batches | python -m json.tool
export id=$1
curl http://${EMR_MASTER_IP}:8998/batches/"${id}" | python -m json.tool

                                                                                                                                                                                                                                                              list-batches.sh                                                                                     0000775 0001750 0001750 00000000254 13661514707 013364  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`
echo 'master_ip' ${EMR_MASTER_IP}
curl http://${EMR_MASTER_IP}:8998/batches | python -m json.tool
                                                                                                                                                                                                                                                                                                                                                    list-sessions.sh                                                                                    0000775 0001750 0001750 00000000255 13661531400 013607  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`
echo 'master_ip' ${EMR_MASTER_IP}
curl http://${EMR_MASTER_IP}:8998/sessions | python -m json.tool
                                                                                                                                                                                                                                                                                                                                                   submit-dailyparams.sh                                                                               0000775 0001750 0001750 00000002350 13661515636 014612  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":4,"jars":["s3a://eco.inquire-3://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire-3/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire-3/frontier/spark/jobs/main.py", "pyFiles":["s3a://eco.inquire-3/frontier/spark/jobs/jobs.zip"], "args":["--job=Element","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire-3,startdir=s3a://eco.inquire-3/frontier/data/incoming/dailyparams,enddir=s3a://eco.inquire-3/frontier/data/processed/dailyparams,filepat=/dailyparams-*,db=frontier,csv_out=1,elements_properties_json=acsURL:deviceType:macAddress:manufacturerName:modelName:hardwareVersion:softwareVersion:productClass:provisioningCode:timeZone:hncEnable,elements_properties_table=acs_url:device_type:mac_address:manufacturer:model_name:hardware_version:software_version:product_class:provisioning_code:time_zone:hnc_enabled,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool

                                                                                                                                                                                                                                                                                        submit-element-event.sh                                                                             0000775 0001750 0001750 00000001665 13661515752 015063  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":4,"jars":["s3a://eco.inquire-3://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire-3/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire-3/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire-3/frontier/spark/jobs/jobs.zip"], "args":["--job=ElementEvent","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire-3,startdir=s3a://eco.inquire-3/frontier/data/incoming/element-event,enddir=s3a://eco.inquire-3/frontier/data/processed/element-event,filepat=/element-event-*,db=frontier,csv_out=1,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


                                                                           submit-firmware.sh                                                                                  0000775 0001750 0001750 00000002077 13661515723 014123  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":2,"jars":["s3a://eco.inquire-3://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire-3/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire-3/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire-3/frontier/spark/jobs/jobs.zip"], "args":["--job=Firmware","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire-3,startdir=s3a://eco.inquire-3/frontier/data/incoming/firmware,enddir=s3a://eco.inquire-3/frontier/data/processed/firmware,filepat=/firmware-*,db=frontier,csv_out=1,firmware_json=FLOWSCHEDULE:NAME:VERSION:STATUS:FAULT_CODE:FAULT_MESSAGE,firmware_table=flow_schedule:from_version:to_version:status:fault_code:fault_message,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


                                                                                                                                                                                                                                                                                                                                                                                                                                                                 submit-host-table.sh                                                                                0000775 0001750 0001750 00000002242 13661515671 014345  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":4,"jars":["s3a://eco.inquire-3://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire-3/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire-3/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire-3/frontier/spark/jobs/jobs.zip"], "args":["--job=HostTable","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire-3,startdir=s3a://eco.inquire-3/frontier/data/incoming/host-table,enddir=s3a://eco.inquire-3/frontier/data/processed/host-table,filepat=/host-table-*,db=frontier,csv_out=1,host_table_json=active:addressSource:deviceType:hostName:interfaceType:ipAddress:macAddress:serialNumber:lastActive,host_table_table=active:address_source:device_type:host_name:interface_type:ip_address:mac_address:serial_number:active_last_changed,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


                                                                                                                                                                                                                                                                                                                                                              submit-ipchange.sh                                                                                  0000775 0001750 0001750 00000001574 13661525043 014062  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":2,"jars":["s3a://eco.inquire-3://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire-3/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire-3/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire-3/frontier/spark/jobs/jobs.zip"], "args":["--job=IPChange","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire-3,startdir=s3a://eco.inquire-3/frontier/data/incoming,enddir=s3a://eco.inquire-3/frontier/data/processed,db=frontier,csv_out=1,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


                                                                                                                                    submit-kpi-ts.sh                                                                                    0000775 0001750 0001750 00000002045 13661516060 013504  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":4,"jars":["s3a://eco.inquire-3://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire-3/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire-3/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire-3/frontier/spark/jobs/jobs.zip"], "args":["--job=KPI","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire-3,startdir=s3a://eco.inquire-3/frontier/data/incoming/kpi-ts,enddir=s3a://eco.inquire-3/frontier/data/processed/kpi-ts,filepat=/kpi-ts-*,db=frontier,csv_out=1,kpi_json=id:kpis:timestamp,kpi_table=id:kpis:timestamp,kpi_agg_json=id:name:count:total:absent,kpi_agg_table=kpi_id:kpi_name:count:total:absent,optional_methods=01,GLOBMAX=30"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           submit-subscriber.sh                                                                                0000775 0001750 0001750 00000002410 13661534102 014431  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 #!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":2,"jars":["s3a://eco.inquire-3://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire-3/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire-3/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire-3/frontier/spark/jobs/jobs.zip"], "args":["--job=Subscriber","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire-3,startdir=s3a://eco.inquire-3/frontier/data/incoming/subscribers,enddir=s3a://eco.inquire-3/frontier/data/processed/subscribers,filepat=/subscriber-*,db=frontier,csv_out=1,subscriber_details_json=ZIP:equipmentName:Profile:Technology:Suspension:suspensionDate,subscriber_details_table=zip:equipment_name:profile:technology:suspension:suspension_date,subscriber_identifiers_json=DropId,subscriber_identifiers_table=drop_id,subscriber_dimension_json=country:state:city:zip,subscriber_dimension_table=country:state:city:zip,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


                                                                                                                                                                                                                                                        terminate-emr-clusters.sh                                                                           0000775 0001750 0001750 00000000225 13661273520 015406  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 /usr/local/bin/aws emr list-clusters --active | grep Id | cut -d: -f2 | sed 's/.$//' | xargs /usr/local/bin/aws emr terminate-clusters --cluster-ids
                                                                                                                                                                                                                                                                                                                                                                           find_emr_master_private_ip.py                                                                       0000664 0001750 0001750 00000004677 13661514602 016404  0                                                                                                    ustar   centos                          centos                                                                                                                                                                                                                 import os
import boto3

emrClient = boto3.client('emr', region_name='us-east-1')
emrClusters = emrClient.list_clusters(ClusterStates=['WAITING'])


def find_any_cluster_by_name(clusterName):
    #print('finding waiting cluster by name: {}'.format(clusterName))
    if emrClusters:
        res = [c for c in emrClusters['Clusters'] if c['Name'] == clusterName]
        if res:
            return res[0]
        else:
            print("There are no active cluster by name: {}".format(clusterName))
            return None
    else:
        print("No active clusters")
        None


def find_any_cluster_id_by_name(clusterName):
    res = find_any_cluster_by_name(clusterName)
    #print('found a waiting cluster by name: {}'.format(clusterName))
    if res:
        return res['Id']
    else:
        None


def find_any_master_instance_of_cluster_by_name(clusterName):
    clusterId = find_any_cluster_id_by_name(clusterName)
    #print('found a waiting cluster id: {}'.format(clusterId))

    if clusterId:
        igs = emrClient.list_instance_groups(ClusterId=clusterId)['InstanceGroups']
        aMasterGId = [g['Id'] for g in igs if 'Master' in g['Name']][0]
        
        instances = emrClient.list_instances(ClusterId=clusterId)['Instances']
        if instances:
            masterInstances =  [i for i in instances if i['InstanceGroupId']==aMasterGId]
            #print("Master instances: {}".format(masterInstances))
            return masterInstances[0]
        if not instances:
            print("There are no instances found in cluster[id: {}, name: {}".format(clusterId, clusterName))
            return None
        
    else:
        None


def find_private_ip_of_any_master_instance_of_cluster_by_name(clusterName):
    master = find_any_master_instance_of_cluster_by_name(clusterName)
    #print('found a master instance: {}'.format(master))
    if master:
        return master['PrivateIpAddress']


################################################################################
# Main
################################################################################
if __name__ == '__main__':
    if emrClusters:
        clusters = emrClusters['Clusters']
        clusterNames = [c['Name'] for c in clusters]

        masterPrivateIp = find_private_ip_of_any_master_instance_of_cluster_by_name(clusterNames[0])
        os.environ['EMR_MASTER_IP']=masterPrivateIp
        print(masterPrivateIp)
    else:
        print('There ar no active clsuters')
        exit(-1)

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 