#!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":2,"jars":["s3a://eco.inquire.dev://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire.dev/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire.dev/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire.dev/frontier/spark/jobs/jobs.zip"], "args":["--job=Firmware","--job-args-sep=,","--job-args=spark.sql.caseSensitive=false,file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire.dev,startdir=s3a://eco.inquire.dev/frontier/data/incoming,enddir=s3a://eco.inquire.dev/frontier/data/processed/firmware,filepat=/firmware-*,db=frontier,csv_out=1,firmware_json=FLOWSCHEDULE:NAME:VERSION:STATUS:FAULT_CODE:FAULT_MESSAGE,firmware_table=flow_schedule:from_version:to_version:status:fault_code:fault_message,optional_methods=01,GLOBMAX=3000"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


