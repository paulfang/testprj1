#!/bin/bash
export EMR_MASTER_IP=`python3 /home/centos/find_emr_master_private_ip.py`

curl -X POST --data \
'{"executorMemory":"10g","executorCores":2,"numExecutors":4,"jars":["s3a://eco.inquire.dev://frontier/spark/jobs/inquire-udf.jar"],"conf":{"spark.driver.extraJavaOptions":"-Dlog4j.configuration=log4j.properties"},"files":["s3://eco.inquire.dev/frontier/spark/jobs/log4j.properties"],"file":"s3a://eco.inquire.dev/frontier/spark/jobs/main.py",  "pyFiles":["s3a://eco.inquire.dev/frontier/spark/jobs/jobs.zip"], "args":["--job=KPI","--job-args-sep=,","--job-args=file_store=s3,s3_file_system=s3a,s3_bucket_name=eco.inquire.dev,startdir=s3a://eco.inquire.dev/frontier/data/incoming,enddir=s3a://eco.inquire.dev/frontier/data/processed/kpi-ts,filepat=/kpi-ts-*,db=frontier,csv_out=1,kpi_json=id:kpis:timestamp,kpi_table=id:kpis:timestamp,kpi_agg_json=id:name:count:total:absent,kpi_agg_table=kpi_id:kpi_name:count:total:absent,optional_methods=01,GLOBMAX=30"]}' \
-H "Content-Type: application/json" \
http://${EMR_MASTER_IP}:8998/batches | python -m json.tool


