#!/bin/bash

/usr/local/bin/aws emr create-cluster --applications Name=Hadoop Name=Spark Name=Livy Name=Hive \
--tags 'Customer=Frontier' 'Stage=Production' 'Name=eco.inquire.frontier-emr' \
--ec2-attributes '{"KeyName":"fedauth-eco-inquire3-telmex","AdditionalSlaveSecurityGroups":["sg-057bc5db2304875d0"],"InstanceProfile":"Role-EC2-S3-EcoInquire-3","SubnetId":"subnet-0164945e","EmrManagedSlaveSecurityGroup":"sg-057bc5db2304875d0","EmrManagedMasterSecurityGroup":"sg-057bc5db2304875d0","AdditionalMasterSecurityGroups":["sg-057bc5db2304875d0"]}' \
--release-label emr-6.0.0 \
--log-uri 's3n://aws-logs-097289183747-us-east-1/elasticmapreduce/' \
--instance-groups '[{"InstanceCount":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":2}]},"InstanceGroupType":"MASTER","InstanceType":"m5.xlarge","Name":"Master - 1"},{"InstanceCount":2,"InstanceGroupType":"CORE","InstanceType":"m5d.xlarge","Name":"Core - 2"},{"InstanceCount":10,"InstanceGroupType":"TASK","InstanceType":"m5d.xlarge","Name":"Task - 3"}]' \
--configurations '[{"Classification":"yarn-env","Properties":{},"Configurations":[{"Classification":"export","Properties":{"PYSPARK_PYTHON":"/usr/bin/python3"}}]},{"Classification":"spark-defaults","Properties":{"spark.driver.memory":"10g","spark.driver.cores":"2","spark.executors.cores":"2","yarn.nodemanager.pmem-check-enabled":"false","spark.executors.memory":"10g","yarn.nodemanager.vmem-check-enabled":"false","spark.sql.caseSensitive":"true","spark.default.parallelism":"12","spark.executor.memoryOverhead":"2g","spark.executor.extraJavaOptions":"-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='\''kill -9 %p'\''","spark.executor.instances":"3","spark.dynamicAllocation.enabled":"false","spark.driver.extraJavaOptions":"-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='\''kill -9 %p'\''","spark.sql.warehouse.dir":"s3://eco.inquire-3/frontier/warehouse/"}}]' \
--bootstrap-actions '[{"Path":"s3://eco.inquire-3/install_inquire_dependencies.sh","Name":"Install inquire dependencies"}]' \
--ebs-root-volume-size 10 \
--service-role EMR_DefaultRole \
--enable-debugging \
--name 'eco.inquire.frontier-auto' \
--scale-down-behavior TERMINATE_AT_TASK_COMPLETION \
--region us-east-1