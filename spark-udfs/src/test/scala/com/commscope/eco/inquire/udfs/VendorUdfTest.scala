package com.commscope.eco.inquire.udfs

import org.scalatest.WordSpec

class VendorUdfTest extends WordSpec {
  val vendorUdf = new VendorUdf()
  "VendorUdfTest" should {
    "check VendorUdf is ready" in {
      assert(vendorUdf != null)
    }
    "check vendorUdf returns vendor for valid mac addresses " in {
      assert(vendorUdf.call("c8:3a:6b:d6:69:19") == "roku")
      assert(vendorUdf.call("1098C3290F2A") == "murata")
    }
    "check vendorUdf returns vendor as Unknown when mac is invalid or null " in {
      assert(vendorUdf.call("xyz") == "Unknown")
      assert(vendorUdf.call("null") == "Unknown")
      assert(vendorUdf.call("") == "Unknown")
      assert(vendorUdf.call(null) == "Unknown")
    }
  }
}
