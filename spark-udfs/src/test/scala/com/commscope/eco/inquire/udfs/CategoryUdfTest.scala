package com.commscope.eco.inquire.udfs

import org.scalatest.WordSpec

class CategoryUdfTest extends WordSpec {
  val devCategory = new CategoryUdf()
  "CategoryUdfTest" ignore  {
    "check category UDF is ready" in {
      assert(devCategory != null)
    }
    "classify devices with valid mac and host " in {
      assert(devCategory.call("c8:3a:6b:d6:69:19","Roku Streaming Stick") == "Streaming")
      assert(devCategory.call("1098C3290F2A","Galaxy-S9") == "Phone")
    }
    "classify devices valid mac but null host parameter" in {
      assert(devCategory.call("c8:3a:6b:d6:69:19",null) == "Streaming")
      assert(devCategory.call("c8:3a:6b:d6:69:19","null") == "Streaming")
    }
    "classify devices as Unknown when mac is invalid or null " in {
      assert(devCategory.call("xyz","My host name") == "Unknown")
      assert(devCategory.call("null","Galaxy-S10") == "Unknown")
      assert(devCategory.call("","Galaxy-S9") == "Unknown")
      assert(devCategory.call(null,"Galaxy-S9") == "Unknown")
    }
    "classify devices as Unknown when both inputs are null" in {
      assert(devCategory.call(null,null) == "Unknown")
    }
  }
}
