package com.commscope.eco.inquire.udfs

import org.scalatest.WordSpec

class ClassifierUdfTest extends WordSpec {
  val classifier = new ClassifierUdf()
  "ClassifierUdfTest" should {
    "check classifier is ready" in {
      assert(classifier != null)
    }
    "classify devices with valid mac and host " in {
      assert(classifier.call("c8:3a:6b:d6:69:19","Roku Streaming Stick") == ("Roku Device","Streaming","roku"))
      assert(classifier.call("c8:3a:6b:d6:69:19","Roku Streaming Stick")._1 == "Roku Device")
      assert(classifier.call("c8:3a:6b:d6:69:19","Roku Streaming Stick")._2 == "Streaming")
      assert(classifier.call("c8:3a:6b:d6:69:19","Roku Streaming Stick")._3 == "roku")

      assert(classifier.call("1098C3290F2A","Galaxy-S9") == ("Samsung Galaxy","Phone","murata"))
      assert(classifier.call("1098C3290F2A","Galaxy-S9")._1 == "Samsung Galaxy")
      assert(classifier.call("1098C3290F2A","Galaxy-S9")._2 == "Phone")
      assert(classifier.call("1098C3290F2A","Galaxy-S9")._3 == "murata")

      assert(classifier.call("D4:A3:3D:6A:03:14","Summers-HomePod") == ("Apple iPod","Streaming","apple"))
      assert(classifier.call("00:25:f0:a4:e6:35","VivintPanela4e635") == ("suga electronics limited","Unknown","suga electronics limited"))
    }

    "classify devices with valid mac but null host parameter" in {
      assert(classifier.call("c8:3a:6b:d6:69:19",null) == ("Roku Device","Streaming","roku"))
      assert(classifier.call("c8:3a:6b:d6:69:19","null"). == ("Roku Device","Streaming","roku"))
      assert(classifier.call("c8:3a:6b:d6:69:19",null)._1 == "Roku Device")
      assert(classifier.call("c8:3a:6b:d6:69:19","null")._1 == "Roku Device")
    }

    "classify devices as Unknown when mac is invalid or null " in {
      assert(classifier.call("xyz","My host name") == ("Unknown", "Unknown", "Unknown"))
      assert(classifier.call("null","Galaxy-S10") == ("Unknown", "Unknown", "Unknown"))
      assert(classifier.call("","Galaxy-S9") == ("Unknown", "Unknown", "Unknown"))
      assert(classifier.call(null,"Galaxy-S9") == ("Unknown", "Unknown", "Unknown"))
    }

    "classify devices as Unknown when both inputs are null" in {
      assert(classifier.call(null,null) == ("Unknown", "Unknown", "Unknown"))
    }
  }
}
