package com.commscope.eco.inquire.udfs

import java.util.Optional

import com.typesafe.scalalogging.Logger
import eco.deviceclassification.model.{DeviceClassification, MacAddress}
import org.apache.spark.sql.api.java.UDF2

/**
 * Spark UDF function calls the Device Classification library using the
 * input parameters mac address and host name strings and
 * returns Device Type, Device Category and Vendor name
 * as a Tuple3 structure:  (deviceType, deviceCategory, vendor).
 * Failure to find any of these will be replaced with an "Unknown" string.
 *
 */
class ClassifierUdf extends UDF2[String, String, (String, String, String)]{
  val logger = Logger(getClass)

  def call(mac: String, host: String): (String, String, String) = {
    var deviceType:String = "Unknown"
    var deviceCategory:String = "Unknown"
    var vendor:String = "Unknown"

    if (mac != null) {
      try {
        val dcs =  DeviceClassifierServiceProxy.service;

        vendor = dcs.getVendorNameByMac(MacAddress.fromString(mac))
        if (vendor == null || vendor.trim.length == 0)
          vendor = "Unknown"

        var classification: Optional[DeviceClassification] =
          dcs.classify(MacAddress.fromString(mac), host);

        if (classification.isPresent) {
          deviceType = classification.get.name
          deviceCategory = classification.get.tags().get(0).name()
        } else {
          deviceType = vendor
        }

      } catch {
        case e: Exception => {
          // do nothing, just return the Unknown type
          logger.info("ClassifierUdf: Unable to classify device mac: "+ mac +", host: "+ host)
        }
      }
    }
    (deviceType, deviceCategory, vendor)
  }
}
