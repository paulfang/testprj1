package com.commscope.eco.inquire.udfs

import com.typesafe.scalalogging.Logger
import eco.deviceclassification.model.{DeviceClassification, MacAddress}
import org.apache.spark.sql.api.java.UDF1

class VendorUdf extends UDF1[String, String]{
  val logger = Logger(getClass)

  def call(mac: String): String = {
    var vendor:String = "Unknown"

    if (mac != null) {
      try {
        val dcs =  DeviceClassifierServiceProxy.service;
        vendor = dcs.getVendorNameByMac(MacAddress.fromString(mac))
        if (vendor == null || vendor.trim.length == 0)
          vendor = "Unknown"
      } catch {
        case e: Exception => {
          // do nothing, just return the Unknown type
          logger.info("VendorUdf: Unable to find vendor for mac: "+ mac)
        }
      }
    }
    vendor
  }
}
