package com.commscope.eco.inquire.udfs

import java.util.Optional

import com.typesafe.scalalogging.Logger
import eco.deviceclassification.model.{DeviceClassification, DeviceTag, MacAddress}
import org.apache.spark.sql.api.java.UDF2

class CategoryUdf extends UDF2[String, String, String]{
  val logger = Logger(getClass)

  def call(mac: String, host: String): String = {
    var devCategory:String = "Unknown"

    if (mac != null) {
      try {
        val dcs =  DeviceClassifierServiceProxy.service;
        var classification: Optional[DeviceClassification] =
          dcs.classify(MacAddress.fromString(mac), host);

        if (classification.isPresent) {
          devCategory = classification.get.tags().get(0).name()
        }
      } catch {
        case e: Exception => {
          // do nothing, just return the Unknown category
          logger.info("CategoryUdf: Unable to classify device mac: "+ mac +", host: "+ host)
        }
      }
    }
    devCategory
  }
}
