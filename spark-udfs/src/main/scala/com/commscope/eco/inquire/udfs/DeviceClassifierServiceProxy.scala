package com.commscope.eco.inquire.udfs

import com.typesafe.scalalogging.Logger
import eco.deviceclassification.DeviceClassificationServiceImpl

object DeviceClassifierServiceProxy {
  val logger = Logger(getClass)
  logger.info("DeviceClassifierServiceProxy init...")

  val service = new DeviceClassificationServiceImpl()
  service.initialize()
}
