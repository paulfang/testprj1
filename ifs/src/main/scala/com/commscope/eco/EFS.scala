package com.commscope.eco

import org.apache.commons.lang3.StringUtils

class EFSException extends RuntimeException

trait EFS {
  @throws(classOf[EFSException])
  def ls(dir: String, pattern: String, pathDelimiter: String = "/"): Iterable[String]

  @throws(classOf[EFSException])
  def mv(src: String, target: String): Boolean

  @throws(classOf[EFSException])
  def cp(src: String, target: String): Boolean

  @throws(classOf[EFSException])
  def rename(src: String, target: String): Boolean

  @throws(classOf[EFSException])
  def rm(name: String*)

  @throws(classOf[EFSException])
  def mkDir(name: String)

  @throws(classOf[EFSException])
  def rmDir(name: String)

  @throws(classOf[EFSException])
  def exists(name: String): Boolean

  @throws(classOf[EFSException])
  def touch(name: String)

  @throws(classOf[EFSException])
  def absolutePath(name: String): String

  def getInstance(provider: String): Option[EFS] =
    if (StringUtils.isBlank(provider))
      Some(DefaultEFS)
    else None
}

object DefaultEFS extends EFS {
  override def ls(dir: String, pattern: String, pathDelimiter: String): Iterable[String] = ???

  override def mv(src: String, target: String): Boolean = ???

  override def cp(src: String, target: String): Boolean = ???

  override def rename(src: String, target: String): Boolean = ???

  override def rm(name: String*): Unit = ???

  override def mkDir(name: String): Unit = ???

  override def rmDir(name: String): Unit = ???

  override def exists(name: String): Boolean = ???

  override def touch(name: String): Unit = ???

  override def absolutePath(name: String): String = ???
}

