import argparse
import os
import time
from datetime import datetime

from pyspark import SparkConf
from pyspark.sql import SparkSession

true_arr = ['true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly', 'uh-huh']


def extract_bool(flag):
    return flag.lower() in true_arr


################################################################################
# Main
################################################################################
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run a PySpark job')

    parser.add_argument('--job', type=str, required=True, dest='job_name',
                        help="The name of the job module you want to run. (ex: poc will run job on jobs.poc package)")
    parser.add_argument('--job-args', nargs='*',
                        help="Extra arguments to send to the PySpark job (example: --job-args template=manual-email1 foo=bar")

    args = parser.parse_args()
    print("Called with arguments: {}".format(args))
    environment = {
        'PYSPARK_JOB_ARGS': ' '.join(args.job_args) if args.job_args else ''
    }
    job_args = dict()
    if args.job_args:
        job_args_tuples = [arg_str.split('=') for arg_str in args.job_args]
        print('job_args_tuples: {}'.format(job_args_tuples))
        job_args = {a[0]: a[1] for a in job_args_tuples}

    job_name = args.job_name
    print('\nRunning job {}...\nenvironment is {}\n'.format(job_name, environment))

    os.environ.update(environment)

    if "db" in job_args and job_args["db"] != "":
        database = job_args["db"]
    else:
        database = "public"

    conf = SparkConf()
    conf.setAppName('ExportConnDev')
    conf.set('spark.sql.execution.arrow.enabled', 'true')

    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    warehouseLocation = spark.conf.get("spark.sql.warehouse.dir")

    table_dependency = job_args.get('table_dependency').split(',')

    for t in table_dependency:
        t = t.strip()
        df = spark.read.parquet('{}/{}.db/{}'.format(warehouseLocation, database, t))
        df.createOrReplaceTempView(t)
        spark.sql('cache table {}'.format(t))

    queryFile = job_args['report_query_file']
    query = ''
    with open(queryFile, 'r') as file:
        query = file.read().replace('\n', ' ')

    print('Executing query: {}'.format(query))
    conn_dev = spark.sql(query)
    print(conn_dev.head())
    export_dir = job_args.get('export_dir')

    #    output_dir = '{}/conn_dev/{}'.format(export_dir, datetime.now().strftime('%Y-%m-%d-%H%m'))
    output_dir = '{}/{}/{}'.format(export_dir, job_name, datetime.now().strftime('%Y-%m-%d'))

    _st = time.time()

    file_count = job_args.get('file_count')

    if file_count:
        file_count = int(file_count)
        if file_count > 0:
            conn_dev = conn_dev.coalesce(file_count)

    csv = conn_dev.write.format('csv')

    overwrite = extract_bool(job_args.get('overwrite'))
    csv = csv.mode('overwrite') if overwrite else csv

    delimiter = job_args.get('delimiter')
    if delimiter and delimiter.strip() != ',':
        csv = csv.option('delimiter', delimiter.strip())

    header = extract_bool(job_args.get('header'))
    csv = csv.option('header', 'true') if header else csv

    quotes = extract_bool(job_args.get('quotes'))
    csv = csv.option('quoteAll', 'true') if quotes else csv

    csv.save(output_dir, compression='gzip')

    print('exported query result at {} in {} sec.'.format(output_dir, ((time.time() - _st) / 1000)))

    for t in table_dependency:
        t = t.strip()
        spark.sql('uncache table {}'.format(t))
