from pathlib import Path
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as Cipher
from base64 import b64decode,b64encode

with open('{}/.ssh/id_rsa.pub'.format(Path.home())) as f:
   raw_pub_key=f.read()

pub_key = RSA.importKey(raw_pub_key)
encrypter = Cipher.new(pub_key)

print('password:')
password = input()

encrypted = encrypter.encrypt(password.encode())
encoded = b64encode(encrypted)

print(encoded)

with open('{}/.ssh/fed_pwd'.format(Path.home()), 'wb') as f:
   f.write(encoded)

#pri_key = RSA.importKey(raw_pri_key)
#decrypter = Cipher.new(pri_key)
#print(decrypter.decrypt(encrypted,None).decode())

