export sub_dir=`date -d yesterday "+%Y%m%d"`
export Y=`date -d yesterday "+%Y"`
export M=`date -d yesterday "+%m"`
export D=`date -d yesterday "+%d"`

export target_dir='/data/inquire/frontier'
export src_dir=/opt/json/archive/$Y/$M/$D

echo $sub_dir $target_dir $src_dir

mkdir $target_dir/subscribers/${sub_dir}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:$src_dir/subscriber-*.gz $target_dir/subscribers/${sub_dir}
mkdir $target_dir/dailyparams/${sub_dir}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:$src_dir/dailyparams-*.gz $target_dir/dailyparams/${sub_dir}
mkdir $target_dir/element-event/${sub_dir}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:$src_dir/element-event-*.gz $target_dir/element-event/${sub_dir}
mkdir $target_dir/firmware/${sub_dir}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:$src_dir/firmware-*.gz $target_dir/firmware/${sub_dir}
mkdir $target_dir/host-table/${sub_dir}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:$src_dir/host-table-ts-*.gz $target_dir/host-table/${sub_dir}
mkdir $target_dir/kpi-ts/${sub_dir}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:$src_dir/kpi-ts-*.gz $target_dir/kpi-ts/${sub_dir}
