from pathlib import Path
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5 as Cipher
from base64 import b64decode,b64encode

with open('{}/.ssh/fed_pwd'.format(Path.home()), 'rb') as f:
   encoded = f.read()

print(encoded)
encrypted = b64decode(encoded)

with open('{}/.ssh/id_rsa'.format(Path.home())) as f:
   raw_pri_key=f.read()

pri_key = RSA.importKey(raw_pri_key)
decrypter = Cipher.new(pri_key)
print(decrypter.decrypt(encrypted,None).decode())

