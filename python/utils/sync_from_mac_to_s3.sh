source ~/venv-aws/bin/activate
python ~/venv-aws/src/samlapi_formauth.py

export yestdir=*`date -v -1d '+%Y%m%d'`*
export todir=*`date -v -1d '+%Y%m%d'`*
echo ${subdir}

export basedir='/Users/sanjeevmishra/data/frontier/negotiator'
echo 'source: '$basedir
export s3folder="s3://eco.inquire-3/frontier/data/incoming"
echo 'destination: '$s3folder

/usr/local/bin/aws s3 sync ${basedir}/subscribers/${todir} ${s3folder}/subscribers/ --profile saml
/usr/local/bin/aws s3 sync ${basedir}/dailyparams/${todir} ${s3folder}/dailyparams/ --profile saml
/usr/local/bin/aws s3 sync ${basedir}/element-event/${todir} ${s3folder}/element-event/ --profile saml
/usr/local/bin/aws s3 sync ${basedir}/firmware/${todir} ${s3folder}/firmware/ --profile saml
/usr/local/bin/aws s3 sync ${basedir}/host-table/${todir} ${s3folder}/host-table/  --profile saml
/usr/local/bin/aws s3 sync ${basedir}/kpi-ts/${todir} ${s3folder}/kpi-ts/ --profile saml

deactivate
