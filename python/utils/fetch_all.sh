
export M=$1
export D=$2
export today=2020$M$D
mkdir subscribers/${today}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:/opt/json/archive/2020/$M/$D/subscriber-*.gz subscribers/${today}
mkdir dailyparams/${today}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:/opt/json/archive/2020/$M/$D/dailyparams-*.gz dailyparams/${today}
mkdir element-event/${today}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:/opt/json/archive/2020/$M/$D/element-event-*.gz element-event/${today}
mkdir firmware/${today}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:/opt/json/archive/2020/$M/$D/firmware-*.gz firmware/${today}
mkdir host-table/${today}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:/opt/json/archive/2020/$M/$D/host-table-ts-*.gz host-table/${today}
mkdir kpi-ts/${today}; sshpass -p 'EcoUser1' rsync --ignore-existing -ahv eco@66.133.130.180:/opt/json/archive/2020/$M/$D/kpi-ts-*.gz kpi-ts/${today}
