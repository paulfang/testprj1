source ~/venv-aws/bin/activate
python ~/venv-aws/src/samlapi_formauth.py

export sub_dir=`date -d yesterday "+%Y%m%d"`
echo ${sub_dir}
#export year = date -d yesterday "+%Y"
#export month = date -d yesterday "+%m"
#export day = date -d yesterday "+%d"

export basedir='/data/inquire/frontier'
export s3folder="s3://eco.inquire-3/frontier/data/archive/${sub_dir}"

/usr/local/bin/aws s3 sync ${basedir}/subscribers/${sub_dir}/ ${s3folder}/subscribers/ --profile=saml
/usr/local/bin/aws s3 sync ${basedir}/dailyparams/${sub_dir}/ ${s3folder}/dailyparams/ --profile=saml
/usr/local/bin/aws s3 sync ${basedir}/element-event/${sub_dir}/ ${s3folder}/element-event/ --profile=saml
/usr/local/bin/aws s3 sync ${basedir}/firmware/${sub_dir}/ ${s3folder}/firmware/ --profile=saml
/usr/local/bin/aws s3 sync ${basedir}/host-table/${sub_dir}/ ${s3folder}/host-table/ --profile=saml
/usr/local/bin/aws s3 sync ${basedir}/kpi-ts/${sub_dir}/ ${s3folder}/kpi-ts/ --profile=saml

deactivate
