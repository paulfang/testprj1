export src_dir=/opt/pace/negotiator/export
export target_dir=/Users/sanjeevmishra/data/frontier/negotiator
export todir=`date '+%Y%m%d'`
export yestdir=*`date -v -1d '+%Y%m%d'`*

mkdir $target_dir/subscribers/$todir
mkdir $target_dir/dailyparams/$todir
mkdir $target_dir/element-event/$todir
mkdir $target_dir/firmware/$todir
mkdir $target_dir/kpi-ts/$todir
mkdir $target_dir/host-table/$todir

rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/subscriber-${todir}*.gz $target_dir/subscribers/$todir
rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/subscriber-diff-${todir}*.gz $target_dir/subscribers/$todir
rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/dailyparams-${todir}*.gz $target_dir/dailyparams/$todir
rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/firmware-${todir}*.gz $target_dir/firmware/$todir
rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/firmware-diff-${todir}*.gz $target_dir/firmware/$todir
rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/host-table-${todir}*.gz $target_dir/host-table/$todir
rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/element-event-${todir}*.gz $target_dir/element-event/$todir
rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/kpi-ts-${yestdir}*.gz $target_dir/kpi-ts/$yestdir
rsync --progress -avz -e "ssh -i /Users/sanjeevmishra/.ssh/eco_id_rsa" eco@216.52.28.179:$src_dir/kpi-ts-${todir}*.gz $target_dir/kpi-ts/$todir



