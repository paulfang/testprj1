import argparse
import os
import time

from pyspark import SparkConf
from pyspark.sql import SparkSession

true_arr = ['true', '1', 't', 'y', 'yes', 'yeah', 'yup', 'certainly', 'uh-huh']


def extract_bool(flag):
    return flag.lower() in true_arr


def parquet_2_csv(table, input_dir, output_dir):
    data = spark.read.parquet(input_dir)

    print('exporting {} to csv...'.format(input_dir))
    _st = time.time()

    data.write.mode('overwrite').option('quoteAll', 'true').csv(output_dir, compression='gzip')
    print('exported csv for {} at {} in {} sec'.format(table, output_dir, (time.time() - _st) / 1000))


################################################################################
# Main
################################################################################


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Run a PySpark job')

    parser.add_argument('--job', type=str, required=True, dest='job_name',
                        help="The name of the job module you want to run. (ex: poc will run job on jobs.poc package)")
    parser.add_argument('--job-args', nargs='*',
                        help="Extra arguments to send to the PySpark job (example: --job-args template=manual-email1 foo=bar")

    args = parser.parse_args()
    print("Called with arguments: {}".format(args))
    environment = {
        'PYSPARK_JOB_ARGS': ' '.join(args.job_args) if args.job_args else ''
    }
    job_args = dict()
    if args.job_args:
        job_args_tuples = [arg_str.split('=') for arg_str in args.job_args]
        print('job_args_tuples: {}'.format(job_args_tuples))
        job_args = {a[0]: a[1] for a in job_args_tuples}

    job_name = args.job_name
    print('\nRunning job {}...\nenvironment is {}\n'.format(job_name, environment))

    os.environ.update(environment)

    if "db" in job_args and job_args["db"] != "":
        database = job_args["db"]
        database = database[:-3] if database.endswith('.db') else database
    else:
        database = "public"

    conf = SparkConf()
    conf.setAppName('parquet_2_csv')
    conf.set('spark.sql.execution.arrow.enabled', 'true')

    spark = SparkSession.builder.config(conf=conf).getOrCreate()

    warehouseLocation = spark.conf.get("spark.sql.warehouse.dir")

    export_dir = job_args.get('export_dir')

    tables = job_args.get('tables').split(',')

    for t in tables:
        t = t.strip()
        input_dir = '{}/{}.db/{}'.format(warehouseLocation, database, t)
        output_dir = '{}/{}/{}'.format(export_dir, database, t)
        parquet_2_csv(t, input_dir, output_dir)
