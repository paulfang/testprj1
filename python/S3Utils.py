import logging
import os
import os.path
import re
from datetime import datetime
from datetime import timezone
import boto3
from s3fs.core import S3FileSystem

s3 = boto3.resource('s3')


class S3Utils:
    EMPTY_DIR_LIST = {'file_list': []}

    def __init__(self, bucket_name, aws_profile_name =None, s3_file_system='s3', logger=None):
        self.logger = logger if logger else logging.getLogger("[S3Utils]")

        self.bucket_name = bucket_name
        aws_profile = aws_profile_name if aws_profile_name else os.environ.get('AWS_PROFILE')

        self.logger.info('AWS_PROFILE: {}'.format(aws_profile))
        self.s3 = S3FileSystem(anon=False, profile_name=aws_profile) \
            if aws_profile else S3FileSystem(anon=False)

        self.s3client = boto3.client('s3')
        self.s3_file_system = s3_file_system
        self.bucket = s3.Bucket(bucket_name)
        self.logger.warning('initialized bucket: {}'.format(self.bucket.Website()))
        self.s3pattern = re.compile('s3', re.I)


    def mkdir(self, dir_path):
        self.logger.info('creating a directory: {} ...'.format(dir_path))
        dir_path = self.prefix_dir_path(dir_path)

        self.s3.invalidate_cache()
        if self.s3.isdir(dir_path):
            return True

        dummy_file = '{}{}'.format(self.fix_dir_path(dir_path), '.ph')
        self.s3.touch(dummy_file)
        # self.s3.rm(dummy_file)
        self.s3.invalidate_cache()
        return self.s3.isdir(path=dir_path)

    def touch(self, file_name):
        file_name = self.prefix_dir_path(self.fix_dir_path(file_name))
        self.s3.invalidate_cache()
        self.logger.info('[touch()] {}'.format(file_name))
        self.s3.touch(file_name)

    def rmdir(self, dir_path, recursive=True):
        dir_path = self.prefix_dir_path(self.fix_dir_path(dir_path))
        self.s3.invalidate_cache()
        self.logger.info('[rmdir()] {}'.format(dir_path))
        if self.dir_exists(dir_path):
            res = self.s3.rm(path=dir_path, recursive=True)
            self.logger.info('[rmdir()] {} removed {}'.format(dir_path, res))
            return not self.dir_exists(dir_path)
        else:
            return False

    def path_exists(self, path):
        path = self.prefix_dir_path(self.fix_dir_path(path))
        self.s3.invalidate_cache()
        exists = self.s3.exists(path)
        self.logger.debug('[path_exists()]: check if path exists: {}: {}'.format(path, exists))
        return exists

    def dir_exists(self, dir_path):
        dir_path = self.prefix_dir_path(self.fix_dir_path(dir_path))
        self.s3.invalidate_cache()
        exists = self.s3.isdir(dir_path)
        self.logger.debug('[dir_exists()]: check if directory exists: {}: {}'.format(dir_path, exists))
        return exists

    def mv(self, src_dir, dest_dir, src_list):
        src_dir = self.prefix_dir_path(self.fix_dir_path(src_dir))
        dest_dir = self.prefix_dir_path(self.fix_dir_path(dest_dir))
        self.logger.info("[mv()] Moving from {} to {}".format(src_dir, dest_dir))
        self.s3.invalidate_cache()
        for f in src_list:
            src_fl = '{}{}'.format(src_dir, f)
            dest_fl = '{}{}'.format(dest_dir, f)
            self.logger.debug("[mv()] Moving from {} to {}".format(src_fl, dest_fl))
            self.s3.mv(src_fl, dest_fl)
        self.s3.invalidate_cache()
        self.logger.warning("[Move Success]")
        return True

    def info(self, dir_path):
        dir_path = self.prefix_dir_path(self.fix_dir_path(dir_path))
        self.logger.debug('info for path: {}'.format(dir_path))
        self.s3.invalidate_cache()
        return self.s3.info(dir_path)

    def ls(self, dir_path, modified_since_datetime=None, abs_path=False):
        dir_path = self.prefix_dir_path(self.fix_dir_path(dir_path))
        self.logger.debug('ls for path: {}'.format(dir_path))
        self.s3.invalidate_cache()
        if not modified_since_datetime:
            if not abs_path:
                return [f.split('/').pop() for f in self.s3.glob(dir_path)]
            else:
                return self.s3.glob(dir_path)
        else:
            list = []
            for f in self.s3.glob(dir_path):
                file_info = self.s3.info(f)
                if file_info['LastModified'] > modified_since_datetime:
                    if not abs_path:
                        list.append(f.split('/').pop())
                    else:
                        list.append(f)
            return list

    def append_file_name(self, obj_key, file_list, file_starts_with='', file_ext='*', delimiter='/', abs_path=False):
        self.logger.debug('append_file_name -> obj_key: {} file_starts_with: {} file_ext: {}'
                          .format(obj_key, file_starts_with, file_ext))
        if obj_key.endswith(delimiter):
            return

        file_name = obj_key[obj_key.rindex(delimiter) + 1:]

        if file_ext == '*':
            if file_starts_with == '':
                file_list.append(self.make_file_name(obj_key, abs_path))
            elif file_name.startswith(file_starts_with):
                file_list.append(self.make_file_name(obj_key, abs_path))
        elif obj_key.endswith(file_ext):
            file_list.append(self.make_file_name(obj_key, abs_path))

    def make_file_name(self, obj_key, abs_path):
        file_name = obj_key
        if abs_path:
            file_name = '{}://{}/{}'.format(self.s3_file_system, self.bucket_name, obj_key)

        return file_name

    def paginated_list(self, dir_path, file_starts_with='',
                       file_ext='*', max_items=None, start_after=None,
                       abs_path=False):
        self.logger.debug('paginated_list -> dir_path: {}, file_ext: {}'.format(dir_path, file_ext))
        dir_path = self.fix_dir_path(dir_path)
        if not self.dir_exists(dir_path):
            return S3Utils.EMPTY_DIR_LIST

        paginator = self.s3client.get_paginator('list_objects_v2')
        resp_itr = paginator.paginate(Bucket=self.bucket_name,
                                      Delimiter='/',
                                      Prefix=dir_path,
                                      StartAfter=start_after if start_after else '',
                                      PaginationConfig={
                                          'MaxItems': max_items,
                                          'PageSize': max_items,
                                          'StartingToken': start_after
                                      })

        file_list = []

        for resp in resp_itr:
            self.logger.debug('resp in resp_itr: {}'.format(resp))
            if resp.get('Contents'):
                for object_data in resp['Contents']:
                    self.append_file_name(object_data['Key'], file_list, file_starts_with, file_ext, abs_path=abs_path)

        self.logger.debug(file_list)

        return {'file_list': file_list, 'start_after': resp.get('NextContinuationToken')}

    def rename(self, src, dest):
        pass

    def cp(self, src_file_name, target_dir, target_file_name=None):
        self.s3.invalidate_cache()
        if not target_file_name:
            target_file_name = src_file_name[src_file_name.rindex('/') + 1:]

        target_dir = self.fix_dir_path(self.prefix_dir_path(target_dir))
        self.logger.info('cp src_file_name: {} target_dir: {} target_file_name: {}'.format(
            src_file_name, target_dir, target_file_name))

        self.s3.copy(path1=src_file_name, path2='{}{}'.format(target_dir, target_file_name))

    def bulk_copy(self, glob, target_dir, modified_since_datetime=None):
        self.s3.invalidate_cache()
        target_dir = self.fix_dir_path(self.prefix_dir_path(target_dir))

        list = self.ls(glob, modified_since_datetime=modified_since_datetime, abs_path=True)

        logger.info("source file count: {}".format(len(list)))

        copied = []
        for l in list:
            try:
                copied.append(self.s3.copy(path1=l, path2='{}{}'.format(target_dir, l.split("/").pop())))
            except:
                logger.warning("[bulk_copy] error while copying: {}".format(l))

        return len(copied)

    def get_latest_datetime(self, dir_path):
        dir_path = self.prefix_dir_path(self.fix_dir_path(dir_path))
        self.logger.info('get_latest_timestamp for path: {}'.format(dir_path))
        self.s3.invalidate_cache()

        list = []
        for f in self.s3.glob(dir_path):
            info = self.s3.info(f)
            self.logger.debug(info)
            last_modified = info.get('LastModified')
            if last_modified:
                list.append(last_modified)
        list.sort()
        return list.pop() if list else get_utc_datetime_from_timestamp(0)

    def upload(self, src_file_name, target_dir, target_file_name=None):
        if not target_file_name:
            target_file_name = src_file_name[src_file_name.rindex('/') + 1:]
        target_dir = self.fix_dir_path(self.prefix_dir_path(target_dir))

        rpath = '{}{}'.format(target_dir, target_file_name)

        self.logger.debug('uploading {} to {}'.format(src_file_name, rpath))
        result = self.s3.put(src_file_name, rpath)
        return result

    def prefix_dir_path(self, dir_path):
        dir_path = dir_path if self.bucket_name in dir_path.split('/') else '{}/{}'.format(self.bucket_name, dir_path)
        return dir_path

    def fix_dir_path(self, dir_path):
        dir_path = dir_path if dir_path.endswith('/') else '{}/'.format(dir_path)
        return dir_path

    def overwrite(self, path, value):
        path = self.prefix_dir_path(self.fix_dir_path(path))
        self.touch(path)

        with self.s3.open(path, 'w') as f:
            f.write(str(value))

    def read(self, path):
        path = self.prefix_dir_path(self.fix_dir_path(path))
        return self.s3.cat(path)


def test_last_modified(s3_utils):
    ref_file = s3_utils.info('frontier/emr-frontier-instance-conf.json')
    logger.info(ref_file)
    l = s3_utils.ls('frontier/*.json', modified_since_datetime=ref_file['LastModified'])
    logger.info("list: {}".format(l))

    last_copied_ref_name = 'frontier/negotiator/last_copied_to_incoming'
    s3_utils.touch(last_copied_ref_name)
    logger.info(s3_utils.info(last_copied_ref_name)['LastModified'])


def test_bulk_copy(s3_utils):
    src_dir = "frontier/negotiator/export"
    target_dir = "frontier/negotiator/cp/test"
    fl_glob = '{}/*.gz'.format(src_dir)
    s3_utils.mkdir(target_dir)
    dp = s3_utils.ls(fl_glob)
    logger.info("Total file count: {}".format(len(dp)))

    now = datetime.now(timezone.tzname("PST"))
    logger.info(now)
    copied_files = s3_utils.bulk_copy(fl_glob, target_dir, modified_since_datetime=now)
    logger.info("Copied file count: {}".format(copied_files))


def get_last_sync_ts(s3_utils, last_sync_file):
    exists = s3_utils.path_exists(last_sync_file)
    logger.info('{} exists: {}'.format(last_sync_file, exists))
    if not exists:
        s3_utils.touch(last_sync_file)
        
    return s3_utils.read(last_sync_file)


def set_next_sync_ts(s3_utils, last_sync_file, next_sync_ts):
    return s3_utils.overwrite(last_sync_file, next_sync_ts)


def test_tz_aware():
    expected_time = datetime.utcnow().timestamp()
    expected_dt = get_utc_datetime_from_timestamp(expected_time)
    expected_tzinfo = expected_dt.tzinfo

    print('expected time: {}, expected_dt: {} ,tzinfo: {}'.format(expected_time, expected_dt, expected_tzinfo))
    set_next_sync_ts(s3_utils, last_sync_file, expected_time)

    actual = get_timestamp_from_binary_str(get_last_sync_ts(s3_utils, last_sync_file))
    actual_dt = get_utc_datetime_from_timestamp(actual)
    actual_tzinfo = actual_dt.tzinfo

    print('actual time: {}, tzinfo: {}'.format(actual, actual_tzinfo))

    last_sync_dt = s3_utils.info(last_sync_file)['LastModified']
    logger.info("{}, {}".format(last_sync_dt, last_sync_dt.tzinfo))
    if last_sync_dt < actual_dt:
        logger.info("smaller")


def get_utc_datetime_from_timestamp(ts):
    return datetime.utcfromtimestamp(ts).replace(tzinfo=timezone.utc)


def get_timestamp_from_binary_str(binary_str):
    return float(binary_str.decode('ascii'))


def get_timestamp_from_dt(dt, tz=timezone.utc):
    return dt.timestamp() if dt.tzinfo else dt.replace(tzinfo=tz).timestamp()


def prepare_files_for_processing(s3_utils, src_dir, target_dir, last_sync_file):
    last_sync_ts = get_last_sync_ts(s3_utils, last_sync_file)
    last_sync_dt = None
    logger.info('last_sync_ts: {}'.format(last_sync_ts))
    if last_sync_ts:
        last_sync_dt = None if not last_sync_ts else get_utc_datetime_from_timestamp(get_timestamp_from_binary_str(last_sync_ts))
        logger.info("last sync datetime: {} tzinfo: {}".format(last_sync_dt, last_sync_dt.tzinfo))

    next_sync_dt = s3_utils.get_latest_datetime(src_dir)
    next_sync_ts = get_timestamp_from_dt(next_sync_dt)

    logger.info("next_sync_dt: {} tzinfo: {}".format(next_sync_dt, next_sync_dt.tzinfo))

    fl_glob = '{}/*.gz'.format(src_dir)
    # copied_files = [1]
    copied_files = s3_utils.bulk_copy(fl_glob, target_dir, modified_since_datetime=last_sync_dt)
    logger.info("wrote {}".format(copied_files))

    if copied_files:
        set_next_sync_ts(s3_utils, last_sync_file, next_sync_ts)
        logger.info("Updated next sync_ts to {}".format(next_sync_ts))


if __name__ == '__main__':
    logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)
    logger = logging.getLogger('S3Utils')
    s3_utils = S3Utils(bucket_name='eco.inquire-3', aws_profile_name=None, logger=logger)

    src_dir = 'frontier/negotiator/export'
    target_dir = 'frontier/data/incoming/'
    last_sync_file = 'frontier/data/last_sync'

    prepare_files_for_processing(s3_utils, src_dir, target_dir, last_sync_file)


