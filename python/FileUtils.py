################################################################################
################################################################################
# Description: FileUtils.py
#
#  This file contains the following classes:
#   Class SANUtils
#   Class HDFSUtils
#
################################################################################
################################################################################

################################################################################
# Imports
################################################################################
import logging
import os
import os.path
import re
import shutil
import subprocess
from abc import ABC, abstractmethod
from glob import glob  # Python 3.5+

import boto3
from s3fs.core import S3FileSystem

s3 = boto3.resource('s3')


class FileUtils(ABC):
    SAN_STORE_TYPE = 0
    HDFS_STORE_TYPE = 1
    S3_STORE_TYPE = 2

    def get_file_utils(logger, store_type=SAN_STORE_TYPE, args=None):
        if not logger:
            logger = logging.getLogger("[FileUtils]")

        if args is None:
            args = {}
        if store_type == FileUtils.HDFS_STORE_TYPE:
            return HDFSUtils(logger)
        elif store_type == FileUtils.S3_STORE_TYPE:
            return S3Utils(logger, bucket_name=args['bucket_name'], s3_file_system=args['s3_file_system'])
        else:
            return SANUtils(logger)

    @abstractmethod
    def get_store_type(self):
        raise NotImplementedError

    @abstractmethod
    def mv(self, src_dir, dest_dir, src_list, local_src=True, local_dest=True):
        raise NotImplementedError

    @abstractmethod
    def mkdir(self, dir_path):
        raise NotImplementedError

    @abstractmethod
    def rmdir(self, dir_path, recursive=True):
        raise NotImplementedError

    @abstractmethod
    def dir_exists(self, dir_path):
        raise NotImplementedError

    @abstractmethod
    def ls(self, dir_path):
        raise NotImplementedError

    @abstractmethod
    def rename(self, src, dest):
        raise NotImplementedError

    @abstractmethod
    def write(self, file_name, content):
        raise NotImplementedError


    def cp(self, src_file_name, target_dir, target_file_name=None):
        pass

    def upload(self, src_file_name, target_dir, target_file_name=None):
        pass


class S3Utils(FileUtils):
    EMPTY_DIR_LIST = {'file_list': []}

    def __init__(self, logger, bucket_name, s3_file_system='s3'):
        self.logger = logger
        self.bucket_name = bucket_name
        aws_profile = os.environ.get('AWS_PROFILE')
        self.logger.info('AWS_PROFILE: {}'.format(aws_profile))
        self.s3 = S3FileSystem(anon=False, profile_name=aws_profile) \
            if aws_profile else S3FileSystem(anon=False)

        self.s3client = boto3.client('s3')
        self.s3_file_system = s3_file_system
        self.bucket = s3.Bucket(bucket_name)
        self.logger.warn('initialized bucket: {}'.format(self.bucket.Website()))
        self.s3pattern = re.compile('s3', re.I)

    def get_store_type(self):
        return FileUtils.S3_STORE_TYPE

    def mkdir(self, dir_path):
        self.logger.info('creating a directory: {} ...'.format(dir_path))
        dir_path = self.prefix_dir_path(dir_path)

        self.s3.invalidate_cache()
        if self.s3.isdir(dir_path):
            return True

        dummy_file = '{}{}'.format(self.fix_dir_path(dir_path), '.ph')
        self.s3.touch(dummy_file)
        # self.s3.rm(dummy_file)
        self.s3.invalidate_cache()
        return self.s3.isdir(path=dir_path)

    def rmdir(self, dir_path, recursive=True):
        dir_path = self.prefix_dir_path(self.fix_dir_path(dir_path))
        self.s3.invalidate_cache()
        self.logger.info('[rmdir()] {}'.format(dir_path))
        if self.dir_exists(dir_path):
            res = self.s3.rm(path=dir_path, recursive=True)
            self.logger.info('[rmdir()] {} removed {}'.format(dir_path, res))
            return not self.dir_exists(dir_path)
        else:
            return False

    def dir_exists(self, dir_path):
        dir_path = self.prefix_dir_path(self.fix_dir_path(dir_path))
        self.s3.invalidate_cache()
        exists = self.s3.isdir(dir_path)
        self.logger.debug('[dir_exists()]: check if directory exists: {}: {}'.format(dir_path, exists))
        return exists

    def mv(self, src_dir, dest_dir, src_list, local_src=True, local_dest=True):
        src_dir = self.prefix_dir_path(self.fix_dir_path(src_dir))
        dest_dir = self.prefix_dir_path(self.fix_dir_path(dest_dir))
        self.logger.info("[mv()] Moving from {} to {}".format(src_dir, dest_dir))
        self.s3.invalidate_cache()
        for f in src_list:
            src_fl = '{}{}'.format(src_dir, f)
            dest_fl = '{}{}'.format(dest_dir, f)
            self.logger.debug("[mv()] Moving from {} to {}".format(src_fl, dest_fl))
            self.s3.mv(src_fl, dest_fl)
        self.s3.invalidate_cache()
        self.logger.warn("[Move Success]")
        return True

    def ls(self, dir_path):
        dir_path = self.prefix_dir_path(self.fix_dir_path(dir_path))
        self.logger.debug('ls for path: {}'.format(dir_path))
        self.s3.invalidate_cache()
        return [f.split('/').pop() for f in self.s3.glob(dir_path)]

    def append_file_name(self, obj_key, file_list, file_starts_with='', file_ext='*', delimiter='/', abs_path=False):
        self.logger.debug('append_file_name -> obj_key: {} file_starts_with: {} file_ext: {}'
                          .format(obj_key, file_starts_with, file_ext))
        if obj_key.endswith(delimiter):
            return

        file_name = obj_key[obj_key.rindex(delimiter) + 1:]

        if file_ext == '*':
            if file_starts_with == '':
                file_list.append(self.make_file_name(obj_key, abs_path))
            elif file_name.startswith(file_starts_with):
                file_list.append(self.make_file_name(obj_key, abs_path))
        elif obj_key.endswith(file_ext):
            file_list.append(self.make_file_name(obj_key, abs_path))

    def make_file_name(self, obj_key, abs_path):
        file_name = obj_key
        if abs_path:
            file_name = '{}://{}/{}'.format(self.s3_file_system, self.bucket_name, obj_key)

        return file_name

    def paginated_list(self, dir_path, file_starts_with='',
                       file_ext='*', max_items=None, start_after=None,
                       abs_path=False):
        self.logger.debug('paginated_list -> dir_path: {}, file_ext: {}'.format(dir_path, file_ext))
        dir_path = self.fix_dir_path(dir_path)
        if not self.dir_exists(dir_path):
            return S3Utils.EMPTY_DIR_LIST

        paginator = self.s3client.get_paginator('list_objects_v2')
        resp_itr = paginator.paginate(Bucket=self.bucket_name,
                                      Delimiter='/',
                                      Prefix=dir_path,
                                      StartAfter=start_after if start_after else '',
                                      PaginationConfig={
                                          'MaxItems': max_items,
                                          'PageSize': max_items,
                                          'StartingToken': start_after
                                      })

        file_list = []

        for resp in resp_itr:
            self.logger.debug('resp in resp_itr: {}'.format(resp))
            if resp.get('Contents'):
                for object_data in resp['Contents']:
                    self.append_file_name(object_data['Key'], file_list, file_starts_with, file_ext, abs_path=abs_path)

        self.logger.debug(file_list)

        return {'file_list': file_list, 'start_after': resp.get('NextContinuationToken')}

    def rename(self, src, dest):
        pass

    def cp(self, src_file_name, target_dir, target_file_name=None):
        self.s3.invalidate_cache()

        if not target_file_name:
            target_file_name = src_file_name[src_file_name.rindex('/') + 1:]

        target_dir = self.fix_dir_path(self.prefix_dir_path(target_dir))
        self.logger.info('cp src_file_name: {} target_dir: {} target_file_name: {}'.format(
            src_file_name, target_dir, target_file_name))

        self.s3.copy_basic(path1=src_file_name, path2='{}{}'.format(target_dir, target_file_name))

    def upload(self, src_file_name, target_dir, target_file_name=None):
        if not target_file_name:
            target_file_name = src_file_name[src_file_name.rindex('/') + 1:]
        target_dir = self.fix_dir_path(self.prefix_dir_path(target_dir))

        rpath = '{}{}'.format(target_dir, target_file_name)

        self.logger.debug('uploading {} to {}'.format(src_file_name, rpath))
        result = self.s3.put(src_file_name, rpath)
        return result

    def write(self, path, value):
        path = self.prefix_dir_path(self.fix_dir_path(path))
        #self.touch(path)

        with self.s3.open(path, 'w') as f:
            f.write(str(value))

    def read(self, path):
        path = self.prefix_dir_path(self.fix_dir_path(path))
        return self.s3.cat(path)

    def prefix_dir_path(self, dir_path):
        dir_path = dir_path if self.bucket_name in dir_path.split('/') else '{}/{}'.format(self.bucket_name, dir_path)
        return dir_path

    def fix_dir_path(self, dir_path):
        dir_path = dir_path if dir_path.endswith('/') else '{}/'.format(dir_path)
        return dir_path


################################################################################
#  storage area network (SAN)  Class
################################################################################


class SANUtils(FileUtils):
    def __init__(self, logger):
        self.logger = logger
        self.logger.warn("[Using SANUtils]")

    def get_store_type(self):
        return FileUtils.SAN_STORE_TYPE

    def mv(self, src_dir, dest_dir, src_list, local_src=True, local_dest=True):
        """Move files in fileList into the specified directory"""
        self.logger.warn("[mv()] Moving from " + src_dir + " to " + dest_dir)
        for item in src_list:
            name = os.path.basename(item)
            s = os.path.join(src_dir, name)
            t = os.path.join(dest_dir, name)
            try:
                shutil.move(s, t)
            except shutil.Error as why:
                self.logger.warn("[Move Failed!]")
                self.logger.warn(str(why))
                return False
        self.logger.warn("[Move Success]")
        return True

    def mkdir(self, dir_path):
        """create the specified dir if it doesn't exist"""
        if not os.path.exists(dir_path):
            os.makedirs(dir_path)
            self.logger.warn("[CREATED] " + dir_path)
            return True
        else:
            self.logger.warn("[CREATED FAILED!] " + dir_path)
            return False

    def rmdir(self, dir_path):
        try:
            shutil.rmtree(dir_path)
            self.logger.warn("[DELETED] " + dir_path)
            return True
        except Exception as e:
            self.logger.warn("[DELETE FAILED!] " + dir_path)
            return False

    def dir_exists(self, dir_path):
        if not os.path.exists(dir_path):
            self.logger.warn("[DOES NOT EXISTS] " + dir_path)
            return False
        else:
            self.logger.warn("[EXISTS] " + dir_path)
            return True

    def ls(self, dir_path):
        """if the file path exists return list of files """
        self.logger.warn("[ls()] " + dir_path)
        return [os.path.basename(item) for item in glob(dir_path)]

    def rename(self, src, dest):
        """Rename file src to dest """
        self.logger.warn("[rename()] " + src + " to " + dest)
        try:
            shutil.move(src, dest)
        except shutil.Error:
            self.logger.warn("[rename Failed!]")
            return False
        self.logger.warn("[rename Success]")
        return True

    def cp(self, src_file_name, target_dir, target_file_name=None):
        _target_file_name = target_dir if target_file_name is None else '{}/{}'.format(target_dir, target_file_name)
        shutil.copy(src_file_name, _target_file_name)

    def write(self, file_name, content):
        self.logger.warn('writing {} to {}'.format(content, file_name))
        with open(file_name, 'w') as f:
            f.write(content)

################################################################################
#  HDFSUtils Class
################################################################################


class HDFSUtils(FileUtils):
    def __init__(self, logger):
        self.logger = logger
        self.logger.warn("[Using HDFSUtils]")

    def get_store_type(self):
        return FileUtils.HDFS_STORE_TYPE

    def mv(self, src_dir, dest_dir, src_list, local_src=True, local_dest=True):
        """
            move src_list from src_dir to dest_dir
        """
        mv_list = []
        self.logger.warn("[mv()] Moving from " + src_dir + " to " + dest_dir)
        for item in src_list:
            name = os.path.basename(item)
            d = os.path.join(src_dir, name)
            mv_list.append(d)

        mv_str = ' '.join(mv_list)

        shell_cmd = "hadoop fs -mv " + mv_str + " " + dest_dir + "; echo $?"
        process = subprocess.Popen(shell_cmd, shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        stdout_lines = [line.decode("utf-8").rstrip() for line in process.stdout]
        if stdout_lines[-1] == '0':
            self.logger.warn("Success")
            return True
        else:
            self.logger.warn("Failed")
            return False

    def mkdir(self, dir_path):
        """create the specified dir if it doesn't exist"""
        if self.dir_exists(dir_path):
            return False
        else:
            shell_cmd = "hadoop fs -mkdir -p " + dir_path + "; echo $?"
            process = subprocess.Popen(shell_cmd, shell=True,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT)
            stdout_lines = [line.decode("utf-8").rstrip() for line in process.stdout]
            if stdout_lines[-1] == '0':
                self.logger.warn(dir_path + " has been CREATED")
                return True
            else:
                self.logger.warn(dir_path + " has NOT been CREATED")
                return False

    def rmdir(self, dir_path):
        shell_cmd = "hadoop fs -rm -r " + dir_path + "; echo $?"
        process = subprocess.Popen(shell_cmd, shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        stdout_lines = [line.decode("utf-8").rstrip() for line in process.stdout]
        if stdout_lines[-1] == '0':
            self.logger.warn(dir_path + " has been DELETED")
            return True
        else:
            self.logger.warn(dir_path + " has NOT been DELETED")
            return False

    def dir_exists(self, dir_path):
        """-e simply checks if the path exists, regardless of file or directory """
        shell_cmd = "hadoop fs -test -e " + dir_path + "; echo $?"
        process = subprocess.Popen(shell_cmd, shell=True,
                                   stdout=subprocess.PIPE,
                                   stderr=subprocess.STDOUT)
        stdout_lines = [line.decode("utf-8").rstrip() for line in process.stdout]
        if stdout_lines[0] == '1':
            self.logger.warn("[DOES NOT EXISTS] " + dir_path)
            return False
        else:
            self.logger.warn("[EXISTS] " + dir_path)
            return True

    def ls(self, dir_path):
        """if the file path exists return list of files """
        if self.dir_exists(dir_path):
            shell_cmd = "hadoop fs -ls " + dir_path
            process = subprocess.Popen(shell_cmd, shell=True,
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.STDOUT)
            stdout_lines = [line.decode("utf-8").rstrip() for line in process.stdout]
            # Filter out the following two cases:
            #   Found X items
            #   ls: `X': No such file or directory
            if stdout_lines[0][0] == 'F' or stdout_lines[0][:3] == 'ls:':
                stdout_lines.pop(0)
            file_list = [line.split(' ')[-1] for line in stdout_lines if line.split(' ')[0][0] != 'd']
            stdout_lines = [os.path.basename(line) for line in stdout_lines]

            return file_list
        else:
            return []

    def rename(self, src, dest):
        pass
