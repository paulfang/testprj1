import logging
import time
import csv
import io
from enum import Enum
from random import randrange
from uuid import uuid4

AUDIT_BASE_DIR = 'job_audit'
AUDIT_FILE_PREFIX = 'audit_result_'


class Status(Enum):
    SUCCESS = 0
    FAILED = 1


def get_report_schema():
    ','.join(['id, job_name', 'input_location', 'output_location',
              'file_pattern', 'input_file_count',
              'processed_file_count', 'output_row_count',
              'start_time', 'end_time',
              'status_code', 'status_detail'])


class AuditData:
    def __init__(self, file_utils, task_name, input_location, output_location, file_pattern, logger=None):
        self.__file_utils = file_utils
        self.__id = uuid4()
        self.__task_name = task_name
        if not logger:
            self.logger = logging.getLogger("[Auditor]")
        else:
            self.logger = logger

        self.__input_location = input_location
        self.__output_location = output_location

        self.__file_pattern = file_pattern
        self.__input_file_count = 0
        self.__processed_file_count = 0
        self.__row_count = 0
        self.__start_time = int(time.time() * 1000)
        self.__end_time = None
        self.__status_code = Status.SUCCESS
        self.__status_detail = None
        self.__sub_task_data = []
        self.__last_sub_task_data = None

    def start(self):
        self.__start_time = int(time.time() * 1000)
        return self

    def get_task_name(self):
        return self.__task_name

    def set_task_name(self, task_name):
        self.__task_name = task_name
        return self

    def set_input_file_count(self, file_count):
        self.__input_file_count = file_count
        return self

    def append_processed_file_count(self, file_count):
        self.__processed_file_count += file_count
        return self

    def append_row_count(self, row_count):
        self.__row_count += row_count
        return self

    def set_status(self, status_code, status_detail):
        self.__status_code = status_code
        self.__status_detail = status_detail
        return self

    def start_sub_task(self, sub_task_name):
        self.__last_sub_task_data = {'name': sub_task_name,
                                     'output_location': None,
                                     'start_time': int(time.time()*1000),
                                     'end_time': None,
                                     'row_count': None,
                                     'status_code': Status.SUCCESS,
                                     'status_detail': None}
        return self

    def update_last_sub_task_output_location(self, output_location):
        self.__last_sub_task_data['output_location'] = output_location
        return self

    def update_last_sub_task_row_count(self, row_count):
        self.__last_sub_task_data['row_count'] = row_count
        return self

    def update_last_sub_task_status(self, status_code=Status.SUCCESS, status_detail=None):
        self.__last_sub_task_data['status_code'] = status_code

        if self.__last_sub_task_data.get('status_detail'):
            self.__last_sub_task_data['status_detail'] = '{} :: {}'.format(self.__last_sub_task_data.get('status_detail'), status_detail)
        else:
            self.__last_sub_task_data['status_detail'] = status_detail

        return self

    def commit_last_sub_task(self):
        if self.__last_sub_task_data:
            self.__last_sub_task_data['end_time'] = int(time.time()*1000)

            self.__sub_task_data.append(self.__last_sub_task_data)
            self.__last_sub_task_data = None
        return self

    def finish(self, status_code=Status.SUCCESS, status_detail=''):
        self.logger.warn('finish with status: {} and message: {}'.format(status_code, status_detail))
        self.__end_time = int(time.time() * 1000)
        self.__status_code = status_code if status_code else Status.SUCCESS
        self.__status_detail = status_detail
        return self

    def get_data(self):
        return {'task_name': self.__task_name,
                'sub_task_name': None,
                'input_location': self.__input_location,
                'output_location': self.__output_location,
                'file_pattern': self.__file_pattern,
                'input_file_count': self.__input_file_count,
                'processed_file_count': self.__processed_file_count,
                'output_row_count': self.__row_count,
                'start_time': self.__start_time,
                'end_time': self.__end_time,
                'status_code': self.__status_code.value,
                'status_detail': self.__status_detail
                }

    def to_csv(self, out=io.StringIO()):
        data = [self.__id, self.__task_name, 'Main', self.__input_location, self.__output_location,
                self.__file_pattern, self.__input_file_count, self.__processed_file_count,
                self.__row_count, self.__start_time, self.__end_time,
                self.__status_code.value, self.__status_detail]
        writer = csv.writer(out, quoting=csv.QUOTE_NONNUMERIC)
        content_length = writer.writerow(data)
        for sub_task in self.__sub_task_data:
            _data = [self.__id, self.__task_name, sub_task['name'], self.__input_location, sub_task['output_location'],
                     None, None, None,
                     sub_task['row_count'], sub_task['start_time'], sub_task['end_time'],
                     sub_task['status_code'].value, sub_task['status_detail']]
            content_length += writer.writerow(_data)

        return content_length

    def get_status_file_name(self, base_dir=None):
        if base_dir and not base_dir.endswith('/'):
            base_dir = '{}/'.format(base_dir)

        audit_base_dir = '{}{}'.format(base_dir, AUDIT_BASE_DIR)

        return '{}/{}{}-{}-{}.csv'.format(audit_base_dir, AUDIT_FILE_PREFIX, self.__task_name, self.__start_time, randrange(1, 1000))

    def report(self, base_dir=None):
        path = self.get_status_file_name(base_dir)
        out = io.StringIO()

        self.__end_time = int(time.time()*1000)

        content_length = self.to_csv(out)
        self.__file_utils.write(path, out.getvalue())
        return path, content_length
