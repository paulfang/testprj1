'''
AWS Lambda Function to check if there are any data present on a given AWS S3 bucket
and a specific path/prefix provided.
Expected inputs:
  bucket - S3 Bucket Name
  path - Folder/Path/Prefix to check
THis function retruns a JSON flag "dataPresent" if data is present or
retruns a custom exception - S3DataNotFound
'''

import json
import boto3
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

class S3DataNotFound(Exception):
    pass

s3 = boto3.client('s3')

def lambda_handler(event, context):

    logger.info('In Lambda CheckS3Data...')
    bucket = event['bucket']
    path = event['path']

    logger.info('bucket: {}, path: {}'.format(bucket, path))

    result = s3.list_objects_v2(Bucket=bucket, Prefix=path)
    #logger.info(result)

    response = {}
    response['bucket'] = bucket
    response['path'] = path
    if result['KeyCount'] > 0:
        response['dataPresent'] = True
    else:
        response['dataPresent'] = False
        raise S3DataNotFound("No data found @ {}/{}".format(bucket, path))

    return response
