'''
AWS Lambda Function to check if any EMR Cluster with give name already running

Expected inputs:
  clusterName - EMR Cluster Name

This function retur True if Cluster Exists  or
retruns a custom exception - DuplicateEMRClusterException
'''

import json
import boto3
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

class DuplicateEMRClusterException(Exception):
    pass

emr = boto3.client('emr')

def lambda_handler(event, context):
    #logger.info('In Lambda CheckEMRCluster...')
    clusterName = event['clusterName']
    logger.info("clusterName: {}".format(clusterName))

    try:
        # use emr client api to list all active clusters
        clusters = emr.list_clusters(ClusterStates=['STARTING', 'BOOTSTRAPPING', 'TERMINATING', 'WAITING', 'RUNNING'])
        clusters = clusters["Clusters"]
        logger.info("\nTotal Running Clusters: {}".format(str(len(clusters))))

        for cluster in clusters:
            #logger.info("\nId: {}, Name: {}, Status: {}".format(cluster["Id"], cluster["Name"], cluster["Status"]["State"]))
            if (cluster["Name"] == clusterName):
                raise DuplicateEMRClusterException("EMR Cluster '{}' already active!".format(clusterName))
    except Exception as e:
        logger.error("Failed to check EMR cluster status: {}".format(e))
        raise DuplicateEMRClusterException(e)
    return True;