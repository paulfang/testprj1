'''
AWS Lambda Function used to Sync the S3 data files from Negotiator export location
to the step function's data input locations.
This function must be linked to a S3 Event (PUT) on the negotiator/export path so that
as when data arrives in this location this function will be invoked and it copies the
data from this location to both 3.0/input and datalake/input locations.
'''

import json
import boto3
import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# boto3 S3 initialization
s3Client = boto3.client("s3")


def lambda_handler(event, context):
    #logger.info("In S3SyncFrontier...")
    #logger.info(event)

    # Destination bucket name
    destinationBucket1 = 'eco.inquire.frontier'
    destinationBucket2 = 'eco.inquire.frontier'

    # Source Bucket Name where file was uploaded
    sourceBucket = event['Records'][0]['s3']['bucket']['name']

    # Filename of object (with path)
    sourcefileKey = event['Records'][0]['s3']['object']['key']

    # Copy Source Object
    copySourceObject = {'Bucket': sourceBucket, 'Key': sourcefileKey}

    # File names
    fileKey = sourcefileKey[sourcefileKey.rindex('/'):]

    # Destination locations
    destinationPath1 = 'archive/incoming' + fileKey
    destinationPath2 = 'data/deltalake-stage/incoming' + fileKey

    #logger.info('Copy files {} from {} to {}/{}.'.format(fileKey, copySourceObject, destinationBucket1, destinationPath2))
    logger.info('Copy files {} from {} to {}/{}.'.format(fileKey, copySourceObject, destinationBucket2, destinationPath2))

    # S3 copy object operation
    s3Client.copy_object(CopySource=copySourceObject, Bucket=destinationBucket1, Key=destinationPath1)
    s3Client.copy_object(CopySource=copySourceObject, Bucket=destinationBucket2, Key=destinationPath2)
