'''
AWS Lambda function that sends notification alarm messages to a configured MS Team channel.
This Lambda function will be called from Inquire's Step Function workflow implementation and
when there is a failure in any of the Spark data processing Jobs. We are also using this function
when our Lambda function "CheckS3DataFun" returns NoDataError.
'''

import json
import logging
import urllib3
from datetime import datetime

http = urllib3.PoolManager()

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def lambda_handler(event, context):
    logger.info(event)

    # Dev - 'test' channel
    #teamswebhookurl =  'https://outlook.office.com/webhook/a5524e12-b535-444d-adf2-cbbc483f3498@31472f81-8fe4-49ec-8bc3-fa1c295640d7/IncomingWebhook/075ea13acf0649feb2553f805c7431c2/d46a8ac6-4bf8-41a1-bd34-c51798d4bd14'

    # Prod - 'Alarms' channel
    teamswebhookurl =  'https://outlook.office.com/webhook/a5524e12-b535-444d-adf2-cbbc483f3498@31472f81-8fe4-49ec-8bc3-fa1c295640d7/IncomingWebhook/32ecf8f60380430fa5106b5f9412b172/d46a8ac6-4bf8-41a1-bd34-c51798d4bd14'

    message = extractEMRStepErrorMessage(event)
    sendTeamsMessage(teamswebhookurl, message)

def extractEMRStepErrorMessage(event):
    msg = '<b>Step Function Alert:</b>'

    now = datetime.now()
    msg += '<br/>DateTime: {}'.format(now.strftime('%d/%m/%Y %H:%M:%S'))
    msg += '<br/>'

    try:
        cluster = event['cluster']['Cluster']
        msg += '<br/>EMR Cluster Id: {}'.format(cluster['Id'])
        msg += '<br/>EMR Cluster Name: <b>{}</b>'.format(cluster['Name'])
        msg += '<br/>'
    except:
        msg += ''

    try:
        errCause = json.loads(event['error-info']['Cause'])
        if "Step" in errCause:
            step = errCause['Step']
            msg += '<br/>Step <b>{} - {}</b> !!'.format(step['Name'], step['Status']['State'])
            msg += '<br/>Details: {}'.format(step['Status']['FailureDetails'])
        else:
            msg += '<br/>Step <b>Failed</b>, Error Type: {} !!'.format(errCause['errorType'])
            msg += '<br/>Details: {}'.format(errCause['errorMessage'])
    except:
        msg += '<br> Step <b>Failed</b> due to Unknown error!!'
        msg += '<br/>Details: {}'.format(event)

    #logger.info('msg: {}'.format(msg))
    return msg


def sendTeamsMessage(teamswebhookurl, message):
    logger.info('In sendTeamsMessage... teamswebhookurl: {}, message: {}'.format(teamswebhookurl, message))
    msg = {
        "text": message
    }
    try:
        encoded_msg = json.dumps(msg).encode('utf-8')
        resp = http.request('POST',teamswebhookurl, body=encoded_msg)
    except Exception as e:
        logger.error('Failed to send teams message: {}'.format(e))

