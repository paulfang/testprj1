#!/bin/bash

# '[ { "Args": [ "spark-submit", "--deploy-mode", "cluster", "--class", "com.commscope.eco.Inquire", "--jars", "s3://eco.inquire.frontier/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.frontier/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar", "--files", "s3://eco.inquire.frontier/spark/inquire-31/log4j.properties,s3://eco.inquire.frontier/spark/inquire-31/gold/application-gold.json,s3://eco.inquire.frontier/spark/inquire-31/gold/client-gold.json", "--driver-java-options", "-Dlog4j.configuration=log4j.properties", "--driver-java-options", "-Dconfig.resource=application-gold.json", "--driver-java-options", "-Dconfig.resource=client-gold.json", "s3://eco.inquire.frontier/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar", "application-gold.json", "client-gold.json" ], "Type": "CUSTOM_JAR", "ActionOnFailure": "CONTINUE", "Jar": "command-runner.jar", "Properties": "=", "Name": "ClientGold" } ]'

step_args=\'`cat gold.steps.json`\'

echo 'step args:' $step_args

/usr/local/bin/aws emr create-cluster \
--applications Name=Hadoop Name=Spark Name=Hive Name=Livy \
--tags 'Customer=${customer.name}' 'Stage=Production' 'Name=eco.inquire-31-${customer.name}' \
--ec2-attributes '{"KeyName":"${key.name}","AdditionalSlaveSecurityGroups":["${security.group}"],"InstanceProfile":"Role-EC2-S3-EcoInquire-3","SubnetId":"subnet-0164945e","EmrManagedSlaveSecurityGroup":"${security.group}","EmrManagedMasterSecurityGroup":"${security.group}","AdditionalMasterSecurityGroups":["${security.group}"]}' \
--release-label emr-5.30.1 \
--log-uri 's3n://aws-logs-097289183747-us-east-1/elasticmapreduce/${customer.name}/' \
--steps '[ { "Args": [ "spark-submit", "--deploy-mode", "cluster", "--class", "com.commscope.eco.Inquire", "--jars", "s3://eco.inquire.frontier/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.frontier/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar", "--files", "s3://eco.inquire.frontier/spark/inquire-31/log4j.properties,s3://eco.inquire.frontier/spark/inquire-31/gold/application-gold.json,s3://eco.inquire.frontier/spark/inquire-31/gold/client-gold.json", "--driver-java-options", "-Dlog4j.configuration=log4j.properties", "--driver-java-options", "-Dconfig.resource=application-gold.json", "--driver-java-options", "-Dconfig.resource=client-gold.json", "s3://eco.inquire.frontier/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar", "application-gold.json", "client-gold.json" ], "Type": "CUSTOM_JAR", "ActionOnFailure": "CONTINUE", "Jar": "command-runner.jar", "Properties": "=", "Name": "ClientGold" } ]' \
--instance-groups '[{"InstanceCount":12,"BidPrice":"OnDemandPrice","InstanceGroupType":"TASK","InstanceType":"r5d.2xlarge","Name":"Task - 3"},{"InstanceCount":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":2}]},"InstanceGroupType":"MASTER","InstanceType":"m5.xlarge","Name":"Master"},{"InstanceCount":12,"BidPrice":"OnDemandPrice","EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":2}]},"InstanceGroupType":"CORE","InstanceType":"r5d.2xlarge","Name":"Core"}]' \
--configurations '[{"Classification":"yarn-env","Properties":{},"Configurations":[{"Classification":"export","Properties":{"PYSPARK_PYTHON":"/usr/bin/python3"}}]},{"Classification":"spark-defaults","Properties":{"spark.driver.memory":"12g","spark.sql.session.timeZone":"UTC","spark.driver.cores":"2","spark.default.parallelism":"12","spark.executors.cores":"2","yarn.nodemanager.pmem-check-enabled":"false","spark.executor.memoryOverhead":"2g","spark.executor.instances":"8","spark.executors.memory":"12g","spark.dynamicAllocation.enabled":"true","yarn.nodemanager.vmem-check-enabled":"false","spark.sql.caseSensitive":"true"}}]' \
--auto-terminate --bootstrap-actions '[{"Path":"s3://eco.inquire.${customer.name}/install_inquire_dependencies.sh","Name":"Install inquire dependencies"}]' \
--visible-to-all-users \
--ebs-root-volume-size 10 \
--service-role EMR_DefaultRole \
--name 'inquire-31-${customer.name}' \
--scale-down-behavior TERMINATE_AT_TASK_COMPLETION \
--region us-east-1
