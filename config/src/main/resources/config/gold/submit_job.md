```sh 
#!/bin/bash
export data_type=$1

./from_incoming_to_processing.sh "${data_type}"

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Inquire \
  --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-gold-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar \
  --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/gold/application-gold.json,s3://eco.inquire.${customer.name}/spark/inquire-31/gold/subscriber-gold.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=application-gold.json" \
  --driver-java-options "-Dconfig.resource=subscriber-gold.json" \
  s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar application-gold.json,subscriber-gold.json

```
Step type: Custom JAR
Name: <Name>
JAR location: command-runner.jar
Arguments:

## Client ##

spark-submit --deploy-mode cluster --class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/gold/application-gold.json,s3://eco.inquire.${customer.name}/spark/inquire-31/gold/client-gold.json --driver-java-options -Dlog4j.configuration=log4j.properties --driver-java-options -Dconfig.resource=application-gold.json --driver-java-options -Dconfig.resource=client-gold.json s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar application-gold.json client-gold.json
