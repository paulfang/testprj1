#!/bin/bash
export data_type=$1

./from_incoming_to_processing.sh "${data_type}"

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Inquire \
  --jars s3://eco.inquire.${customer.name}/spark/bronze/model-silver-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/bronze/spark-udfs-jar-with-dependencies.jar \
  --files s3://eco.inquire.${customer.name}/spark/bronze/log4j.properties,s3://eco.inquire.${customer.name}/spark/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/bronze/"${data_type}"-bronze.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=application-bronze.json" \
  --driver-java-options "-Dconfig.resource=${data_type}-bronze.json" \
  s3://eco.inquire.${customer.name}/spark/bronze/data-processor-jar-with-dependencies.jar application-bronze.json,"${data_type}"-bronze.json