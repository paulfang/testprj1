```sh 
#!/bin/bash
export data_type=$1

./from_incoming_to_processing.sh "${data_type}"

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Inquire \
  --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar \
  --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/subscriber-bronze.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=application-bronze.json" \
  --driver-java-options "-Dconfig.resource=subscriber-bronze.json" \
  s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar application-bronze.json,subscriber-bronze.json

```

## Subscriber ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/subscriber-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=subscriber-bronze.json"

s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json subscriber-bronze.json

## Dailyparams ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/dailyparams-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=dailyparams-bronze.json"

s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json dailyparams-bronze.json


## ElementEvent ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/element-event-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=element-event-bronze.json"

s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json element-event-bronze.json

## Firmware Upgrade ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/firmware-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=firmware-bronze.json"

s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json firmware-bronze.json

## HostTable ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/host-table-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=host-table-bronze.json"

s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json host-table-bronze.json


## ElementTS ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-31/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-31/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-31/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/inquire-31/bronze/element-ts-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=element-ts-bronze.json"

s3://eco.inquire.${customer.name}/spark/inquire-31/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json element-ts-bronze.json

