```sh 
#!/bin/bash

# Vacuum

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Vacuum \
  --jars s3://eco.inquire.${customer.name}/spark/inquire-32/model-silver-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-32/spark-udfs-jar-with-dependencies.jar \
  --files s3://eco.inquire.${customer.name}/spark/inquire-32/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/device-vacuum.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=device-vacuum.json" \
  s3://eco.inquire.${customer.name}/spark/inquire-32/data-processor-jar-with-dependencies.jar device-vacuum.json


export data_type=$1

./from_incoming_to_processing.sh "${data_type}"

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Inquire \
  --jars s3://eco.inquire.${customer.name}/spark/inquire-32/model-silver-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-32/spark-udfs-jar-with-dependencies.jar \
  --files s3://eco.inquire.${customer.name}/spark/inquire-32/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/application-silver.json,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/subscriber-silver.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=application-silver.json" \
  --driver-java-options "-Dconfig.resource=subscriber-silver.json" \
  s3://eco.inquire.${customer.name}/spark/inquire-32/data-processor-3.1.0-jar-with-dependencies.jar application-silver.json,subscriber-silver.json

```

Step type: Custom JAR
Name: <Name>
JAR location: command-runner.jar
Arguments:
## Subscriber ##

spark-submit --deploy-mode cluster --class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-32/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-32/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-32/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/application-silver.json,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/subscriber-silver.json --driver-java-options -Dlog4j.configuration=log4j.properties --driver-java-options -Dconfig.resource=application-silver.json --driver-java-options -Dconfig.resource=subscriber-silver.json s3://eco.inquire.${customer.name}/spark/inquire-32/data-processor-3.1.0-jar-with-dependencies.jar application-silver.json subscriber-silver.json


## Dailyparams ##
spark-submit --deploy-mode cluster --class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-32/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-32/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-32/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/application-silver.json,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/dailyparams-silver.json --driver-java-options -Dlog4j.configuration=log4j.properties --driver-java-options -Dconfig.resource=application-silver.json --driver-java-options -Dconfig.resource=dailyparams-silver.json s3://eco.inquire.${customer.name}/spark/inquire-32/data-processor-3.1.0-jar-with-dependencies.jar application-silver.json dailyparams-silver.json

## ElementEvent ##
spark-submit --deploy-mode cluster --class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-32/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-32/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-32/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/application-silver.json,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/element-event-silver.json --driver-java-options -Dlog4j.configuration=log4j.properties --driver-java-options -Dconfig.resource=application-silver.json --driver-java-options -Dconfig.resource=element-event-silver.json s3://eco.inquire.${customer.name}/spark/inquire-32/data-processor-3.1.0-jar-with-dependencies.jar application-silver.json element-event-silver.json

## Firmware Upgrade ##

spark-submit --deploy-mode cluster --class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-32/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-32/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-32/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/application-silver.json,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/firmware-silver.json --driver-java-options -Dlog4j.configuration=log4j.properties --driver-java-options -Dconfig.resource=application-silver.json --driver-java-options -Dconfig.resource=firmware-silver.json s3://eco.inquire.${customer.name}/spark/inquire-32/data-processor-3.1.0-jar-with-dependencies.jar application-silver.json firmware-silver.json

## HostTable ##

spark-submit --deploy-mode cluster --class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-32/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-32/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-32/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/application-silver.json,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/host-table-silver.json --driver-java-options -Dlog4j.configuration=log4j.properties --driver-java-options -Dconfig.resource=application-silver.json --driver-java-options -Dconfig.resource=host-table-silver.json s3://eco.inquire.${customer.name}/spark/inquire-32/data-processor-3.1.0-jar-with-dependencies.jar application-silver.json host-table-silver.json


## ElementTS ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/inquire-32/model-silver-3.1.0-jar-with-dependencies.jar,s3://eco.inquire.${customer.name}/spark/inquire-32/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/inquire-32/log4j.properties,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/application-silver.json,s3://eco.inquire.${customer.name}/spark/inquire-32/silver/element-ts-silver.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-silver.json" --driver-java-options "-Dconfig.resource=element-ts-silver.json"

s3://eco.inquire.${customer.name}/spark/inquire-32/data-processor-3.1.0-jar-with-dependencies.jar

application-silver.json element-ts-silver.json

