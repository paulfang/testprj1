```sh 
#!/bin/bash
export data_type=$1

./from_incoming_to_processing.sh "${data_type}"

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Inquire \
  --jars s3://eco.inquire.${customer.name}/spark/bronze/spark-udfs-3.1.0-jar-with-dependencies.jar \
  --files s3://eco.inquire.${customer.name}/spark/bronze/log4j.properties,s3://eco.inquire.${customer.name}/spark/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/bronze/subscriber-bronze.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=application-bronze.json" \
  --driver-java-options "-Dconfig.resource=subscriber-bronze.json" \
  s3://eco.inquire.${customer.name}/spark/bronze/data-processor-3.1.0-jar-with-dependencies.jar application-bronze.json,subscriber-bronze.json

```

## Subscriber ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/bronze/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/bronze/log4j.properties,s3://eco.inquire.${customer.name}/spark/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/bronze/subscriber-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=subscriber-bronze.json"

s3://eco.inquire.${customer.name}/spark/bronze/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json subscriber-bronze.json

## Dailyparams ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/bronze/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/bronze/log4j.properties,s3://eco.inquire.${customer.name}/spark/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/bronze/dailyparams-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=dailyparams-bronze.json"

s3://eco.inquire.${customer.name}/spark/bronze/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json dailyparams-bronze.json


## ElementEvent ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/bronze/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/bronze/log4j.properties,s3://eco.inquire.${customer.name}/spark/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/bronze/element-event-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=element-event-bronze.json"

s3://eco.inquire.${customer.name}/spark/bronze/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json element-event-bronze.json

## Firmware Upgrade ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/bronze/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/bronze/log4j.properties,s3://eco.inquire.${customer.name}/spark/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/bronze/firmware-upgrade-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=firmware-upgrade-bronze.json"

s3://eco.inquire.${customer.name}/spark/bronze/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json firmware-upgrade-bronze.json

## HostTable ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/bronze/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/bronze/log4j.properties,s3://eco.inquire.${customer.name}/spark/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/bronze/host-table-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=host-table-bronze.json"

s3://eco.inquire.${customer.name}/spark/bronze/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json host-table-bronze.json


## ElementTS ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.${customer.name}/spark/bronze/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.${customer.name}/spark/bronze/log4j.properties,s3://eco.inquire.${customer.name}/spark/bronze/application-bronze.json,s3://eco.inquire.${customer.name}/spark/bronze/element-ts-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=element-ts-bronze.json"

s3://eco.inquire.${customer.name}/spark/bronze/data-processor-3.1.0-jar-with-dependencies.jar

application-bronze.json element-ts-bronze.json

