#!/bin/bash
export data_type=$1

./from_incoming_to_processing.sh "${data_type}"

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Inquire \
  --files s3://eco.inquire.${customer.name}/spark/log4j.properties,s3://eco.inquire.${customer.name}/spark/application-bronze.json,s3://eco.inquire.${customer.name}/spark/"${data_type}"-bronze.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=application-bronze.json" \
  --driver-java-options "-Dconfig.resource=${data_type}-bronze.json" \
  s3://eco.inquire.${customer.name}/spark/inquire-3.1.0-SNAPSHOT-jar-with-dependencies.jar application-bronze.json,"${data_type}"-bronze.json