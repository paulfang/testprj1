```sh 
#!/bin/bash
export data_type=$1

./from_incoming_to_processing.sh "${data_type}"

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Inquire \
  --jars s3://eco.inquire.frontier/spark/gold/model-gold-3.1.0.jar \
  --files s3://eco.inquire.frontier/spark/gold/log4j.properties,s3://eco.inquire.frontier/spark/gold/application-gold.json,s3://eco.inquire.frontier/spark/gold/subscriber-gold.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=application-gold.json" \
  --driver-java-options "-Dconfig.resource=subscriber-gold.json" \
  s3://eco.inquire.frontier/spark/gold/data-processor-3.1.0-jar-with-dependencies.jar application-gold.json,subscriber-gold.json

```

## Subscriber ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/gold/model-gold-3.1.0.jar,s3://eco.inquire.frontier/spark/gold/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/gold/log4j.properties,s3://eco.inquire.frontier/spark/gold/application-gold.json,s3://eco.inquire.frontier/spark/gold/subscriber-gold.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-gold.json" --driver-java-options "-Dconfig.resource=subscriber-gold.json"

s3://eco.inquire.frontier/spark/gold/data-processor-3.1.0-jar-with-dependencies.jar

application-gold.json subscriber-gold.json

## Dailyparams ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/gold/model-gold-3.1.0.jar,s3://eco.inquire.frontier/spark/gold/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/gold/log4j.properties,s3://eco.inquire.frontier/spark/gold/application-gold.json,s3://eco.inquire.frontier/spark/gold/dailyparams-gold.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-gold.json" --driver-java-options "-Dconfig.resource=dailyparams-gold.json"

s3://eco.inquire.frontier/spark/gold/data-processor-3.1.0-jar-with-dependencies.jar

application-gold.json dailyparams-gold.json


## ElementEvent ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/gold/model-gold-3.1.0.jar,s3://eco.inquire.frontier/spark/gold/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/gold/log4j.properties,s3://eco.inquire.frontier/spark/gold/application-gold.json,s3://eco.inquire.frontier/spark/gold/element-event-gold.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-gold.json" --driver-java-options "-Dconfig.resource=element-event-gold.json"

s3://eco.inquire.frontier/spark/gold/data-processor-3.1.0-jar-with-dependencies.jar

application-gold.json element-event-gold.json

## Firmware Upgrade ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/gold/model-gold-3.1.0.jar,s3://eco.inquire.frontier/spark/gold/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/gold/log4j.properties,s3://eco.inquire.frontier/spark/gold/application-gold.json,s3://eco.inquire.frontier/spark/gold/firmware-upgrade-gold.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-gold.json" --driver-java-options "-Dconfig.resource=firmware-upgrade-gold.json"

s3://eco.inquire.frontier/spark/gold/data-processor-3.1.0-jar-with-dependencies.jar

application-gold.json firmware-upgrade-gold.json

## HostTable ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/gold/model-gold-3.1.0.jar,s3://eco.inquire.frontier/spark/gold/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/gold/log4j.properties,s3://eco.inquire.frontier/spark/gold/application-gold.json,s3://eco.inquire.frontier/spark/gold/host-table-gold.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-gold.json" --driver-java-options "-Dconfig.resource=host-table-gold.json"

s3://eco.inquire.frontier/spark/gold/data-processor-3.1.0-jar-with-dependencies.jar

application-gold.json host-table-gold.json


## ElementTS ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/gold/model-gold-3.1.0.jar,s3://eco.inquire.frontier/spark/gold/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/gold/log4j.properties,s3://eco.inquire.frontier/spark/gold/application-gold.json,s3://eco.inquire.frontier/spark/gold/element-ts-gold.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-gold.json" --driver-java-options "-Dconfig.resource=element-ts-gold.json"

s3://eco.inquire.frontier/spark/gold/data-processor-3.1.0-jar-with-dependencies.jar

application-gold.json element-ts-gold.json

