```sh 
#!/bin/bash
export data_type=$1

./from_incoming_to_processing.sh "${data_type}"

spark-submit \
  --master yarn \
  --deploy-mode cluster \
  --class com.commscope.eco.Inquire \
  --jars s3://eco.inquire.frontier/spark/silver/model-silver-3.1.0.jar \
  --files s3://eco.inquire.frontier/spark/silver/log4j.properties,s3://eco.inquire.frontier/spark/silver/application-silver.json,s3://eco.inquire.frontier/spark/silver/subscriber-silver.json \
  --driver-java-options "-Dlog4j.configuration=log4j.properties" \
  --driver-java-options "-Dconfig.resource=application-silver.json" \
  --driver-java-options "-Dconfig.resource=subscriber-silver.json" \
  s3://eco.inquire.frontier/spark/silver/data-processor-3.1.0-jar-with-dependencies.jar application-silver.json,subscriber-silver.json

```

## Subscriber ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/silver/model-silver-3.1.0.jar,s3://eco.inquire.frontier/spark/silver/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/silver/log4j.properties,s3://eco.inquire.frontier/spark/silver/application-silver.json,s3://eco.inquire.frontier/spark/silver/subscriber-silver.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-silver.json" --driver-java-options "-Dconfig.resource=subscriber-silver.json"

s3://eco.inquire.frontier/spark/silver/data-processor-3.1.0-jar-with-dependencies.jar

application-silver.json subscriber-silver.json

## Dailyparams ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/silver/model-silver-3.1.0.jar,s3://eco.inquire.frontier/spark/silver/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/silver/log4j.properties,s3://eco.inquire.frontier/spark/silver/application-silver.json,s3://eco.inquire.frontier/spark/silver/dailyparams-silver.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-silver.json" --driver-java-options "-Dconfig.resource=dailyparams-silver.json"

s3://eco.inquire.frontier/spark/silver/data-processor-3.1.0-jar-with-dependencies.jar

application-silver.json dailyparams-silver.json


## ElementEvent ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/silver/model-silver-3.1.0.jar,s3://eco.inquire.frontier/spark/silver/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/silver/log4j.properties,s3://eco.inquire.frontier/spark/silver/application-silver.json,s3://eco.inquire.frontier/spark/silver/element-event-silver.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-silver.json" --driver-java-options "-Dconfig.resource=element-event-silver.json"

s3://eco.inquire.frontier/spark/silver/data-processor-3.1.0-jar-with-dependencies.jar

application-silver.json element-event-silver.json

## Firmware Upgrade ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/silver/model-silver-3.1.0.jar,s3://eco.inquire.frontier/spark/silver/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/silver/log4j.properties,s3://eco.inquire.frontier/spark/silver/application-silver.json,s3://eco.inquire.frontier/spark/silver/firmware-upgrade-silver.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-silver.json" --driver-java-options "-Dconfig.resource=firmware-upgrade-silver.json"

s3://eco.inquire.frontier/spark/silver/data-processor-3.1.0-jar-with-dependencies.jar

application-silver.json firmware-upgrade-silver.json

## HostTable ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/silver/model-silver-3.1.0.jar,s3://eco.inquire.frontier/spark/silver/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/silver/log4j.properties,s3://eco.inquire.frontier/spark/silver/application-silver.json,s3://eco.inquire.frontier/spark/silver/host-table-silver.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-silver.json" --driver-java-options "-Dconfig.resource=host-table-silver.json"

s3://eco.inquire.frontier/spark/silver/data-processor-3.1.0-jar-with-dependencies.jar

application-silver.json host-table-silver.json


## ElementTS ##

--class com.commscope.eco.Inquire --jars s3://eco.inquire.frontier/spark/silver/model-silver-3.1.0.jar,s3://eco.inquire.frontier/spark/silver/spark-udfs-3.1.0-jar-with-dependencies.jar --files s3://eco.inquire.frontier/spark/silver/log4j.properties,s3://eco.inquire.frontier/spark/silver/application-silver.json,s3://eco.inquire.frontier/spark/silver/element-ts-silver.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-silver.json" --driver-java-options "-Dconfig.resource=element-ts-silver.json"

s3://eco.inquire.frontier/spark/silver/data-processor-3.1.0-jar-with-dependencies.jar

application-silver.json element-ts-silver.json

