export bucket_name=$1
export warehouse_dir=s3://${bucket_name}/warehouse
echo 
[
  {
    "Classification": "yarn-env",
    "Properties": {},
    "Configurations": [
      {
        "Classification": "export",
        "Properties": {
          "PYSPARK_PYTHON": "/usr/bin/python3"
        }
      }
    ]
  },
  {
    "Classification": "spark-defaults",
    "Properties": {
      "spark.driver.memory": "10g",
      "spark.driver.cores": "2",
      "spark.executors.cores": "2",
      "yarn.nodemanager.pmem-check-enabled": "false",
      "spark.executors.memory": "10g",
      "yarn.nodemanager.vmem-check-enabled": "false",
      "spark.sql.caseSensitive": "true",
      "spark.default.parallelism": "12",
      "spark.executor.memoryOverhead": "2g",
      "spark.executor.extraJavaOptions": "-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='\''kill -9 %p'\''",
      "spark.executor.instances": "3",
      "spark.dynamicAllocation.enabled": "false",
      "spark.driver.extraJavaOptions": "-XX:+UseG1GC -XX:+UnlockDiagnosticVMOptions -XX:+G1SummarizeConcMark -XX:InitiatingHeapOccupancyPercent=35 -verbose:gc -XX:+PrintGCDetails -XX:+PrintGCDateStamps -XX:OnOutOfMemoryError='\''kill -9 %p'\''",
      "spark.sql.warehouse.dir": "${warehouse_dir}"
    }
  }
]'
