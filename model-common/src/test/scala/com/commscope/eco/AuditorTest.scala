package com.commscope.eco

import com.typesafe.scalalogging.Logger
import junit.framework.Assert
import org.apache.spark.sql.DataFrame

class AuditorTest extends SparkBasedTest {
  val logger = Logger(getClass)

  "default auditor" in {
    implicit val appConfig = AppConfig("config/audit/subscriber-raw-audit-test.json")
    val df:DataFrame = spark.read.json(s"${appConfig.inputLocation}/subscriber*.json")
    val rowCount = df.count()
    val columns = df.columns

    val result = Auditor.audit("default", df)
    val resultCols = result.columns
    val diff = resultCols.diff(columns)
    logger.info("result: {}", result.select("*").head(1))
    Array("name","row_count","processed_dt","min_ts","max_ts","distinct_ids")
      .foreach(c => Assert.assertTrue(diff.contains(c)))
    Assert.assertEquals(rowCount, result.select("row_count").head(1)(0)(0))
  }

  "subscriber auditor" in {
    implicit val appConfig = AppConfig("config/audit/subscriber-raw-audit-test.json")
    val df:DataFrame = spark.read.json(s"${appConfig.inputLocation}/subscriber*.json")
    val rowCount = df.count()
    val columns = df.columns
    val result = Auditor.audit("Subscriber", df)
    val resultCols = result.columns
    val diff = resultCols.diff(columns)
    logger.info("result: {}", result.select("*").head(1))
    Array("name","row_count","processed_dt","min_ts","max_ts","distinct_ids")
      .foreach(c => Assert.assertTrue(diff.contains(c)))
    Assert.assertEquals(rowCount, result.select("row_count").head(1)(0)(0))
  }

  "dailyparams auditor" in {
    implicit val appConfig = AppConfig("config/audit/dailyparams-raw-audit-test.json")

    val df:DataFrame = spark.read.json("src/test/resources/data/frontier/dailyparams.json")
    val rowCount = df.count()
    val columns = df.columns
    val result = Auditor.audit("Dailyparams", df)
    val resultCols = result.columns
    val diff = resultCols.diff(columns)
    logger.info("result: {}", result.select("*").head(1))
    Array("name","row_count","processed_dt","min_ts","max_ts","distinct_ids")
      .foreach(c => Assert.assertTrue(diff.contains(c)))
    Assert.assertEquals(rowCount, result.select("row_count").head(1)(0)(0))
  }

  "elementEvent auditor" in {
    implicit val appConfig = AppConfig("config/audit/dailyparams-raw-audit-test.json")

    val df:DataFrame = spark.read.json("src/test/resources/data/frontier/element-event.json")
    val rowCount = df.count()
    val columns = df.columns
    val result = Auditor.audit("ElementEvent", df)
    val resultCols = result.columns
    val diff = resultCols.diff(columns)
    logger.info("result: {}", result.select("*").head(1))
    Array("name","row_count","processed_dt","min_ts","max_ts","distinct_ids")
      .foreach(c => Assert.assertTrue(diff.contains(c)))
    Assert.assertEquals(rowCount, result.select("row_count").head(1)(0)(0))
  }

  "elementTs auditor" in {
    implicit val appConfig = AppConfig("config/audit/dailyparams-raw-audit-test.json")

    val df:DataFrame = spark.read.json("src/test/resources/data/frontier/element-ts-data.json")
    val rowCount = df.count()
    val columns = df.columns
    val result = Auditor.audit("ElementTs", df)
    val resultCols = result.columns
    val diff = resultCols.diff(columns)
    logger.info("result: {}", result.select("*").head(1))
    Array("name","row_count","processed_dt","min_ts","max_ts","distinct_ids")
      .foreach(c => Assert.assertTrue(diff.contains(c)))
    Assert.assertEquals(rowCount, result.select("row_count").head(1)(0)(0))
  }

  "elementTs auditor with ts_inform" in {
    implicit val appConfig = AppConfig("config/audit/dailyparams-gold-audit-test.json")

    val df:DataFrame = spark.read.parquet("src/test/resources/data/frontier/gold/element-ts/")
    logger.info("df columns: {}", df.columns)
    val rowCount = df.count()
    val columns = df.columns
    val result = Auditor.audit("ElementTs", df)
    val resultCols = result.columns
    val diff = resultCols.diff(columns)
    logger.info("result: {}", result.select("*").head(1))

    Array("name","row_count","processed_dt","min_ts","max_ts","distinct_ids")
      .foreach(c => Assert.assertTrue(diff.contains(c)))

    Assert.assertEquals(rowCount, result.select("row_count").head(1)(0)(0))

    val informResult = Auditor.auditInforms("ElementTs", df)
    val informResultCols = informResult.get.columns
    Array("ts_date", "ts_inform", "row_count", "distinct_ids", "name", "processed_dt")
      .foreach(c => Assert.assertTrue(informResultCols.contains(c)))

    logger.info("inform result cols: {}", informResultCols)
  }
}
