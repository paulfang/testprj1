package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.Column
import org.junit.Assert._
import org.scalatest.WordSpec

class SchemaUtilsTest extends WordSpec {
  val logger = Logger(getClass)

  "entity schema " when {
    "found" should {
      "load" in {
        val schema = SchemaUtils.loadEntitySchema("firmware-entity")
        assert(schema.isDefined)
        //val structType = DataType.fromJson(schema.get.input).asInstanceOf[StructType]
        logger.debug(schema.get.input.treeString)
        assert(!schema.get.input.isEmpty)
        assert(!schema.get.output.isEmpty)
      }
    }
  }

  "raw schema " when {
    "found" should {
      "load" in {
        val schema = SchemaUtils.loadDataFrameSchema("firmware-raw")
        assert(schema.isDefined)
        //val structType = DataType.fromJson(schema.get.input).asInstanceOf[StructType]
        logger.debug(schema.get.treeString)
      }
    }
  }

  "schema with struct type" when {
    val schema = SchemaUtils.loadDataFrameSchema("dailyparams-raw")
    assert(schema.isDefined)
    assertTrue(schema.get.sql.split(",").filter(_.toLowerCase.contains("struct")).length > 0)

    "flattened schema" should {
      val flattenSchema = SchemaUtils.flatten(schema).get

      "have only column type" in {
        flattenSchema.foreach(_.isInstanceOf[Column])
      }

      "have names prefixed with parents name" in {
        val expectedAfterFlatten = "identifiers.OUI,identifiers.Serial Number,properties.WANAccessType".split(",")
        assertEquals(expectedAfterFlatten.length, flattenSchema.filter(c => expectedAfterFlatten.contains(c.toString)).length)
      }
    }
  }

  "nested schema" when {
    val schema = SchemaUtils.loadDataFrameSchema("dailyparams-raw")
    "flattened " should {
      val flattenSchema = SchemaUtils.flatten(schema)
      assert(schema.isDefined)

      "have illegal chars" in {
        val colNames = flattenSchema.get.filter(c => SchemaUtils.illegalCharsRegex.findAllMatchIn(c.toString()).length > 0)
        logger.debug("illegal chars in flatten schema: {}", colNames)
        assert(!colNames.isEmpty)
      }

      "have no alias columns" in {
        val aliasColumns = flattenSchema.get.filter(c => c.toString().contains(" AS "))
        assertTrue(aliasColumns.isEmpty)
      }
    }

    "flattened followed by sanitization" should {
      val flattenSchemaWithSanitization = SchemaUtils.flatten(schema, sanitizeAfterFlattening = true)
      assertTrue(flattenSchemaWithSanitization.isDefined)

      "have alias columns" in {
        val aliasColumns = flattenSchemaWithSanitization.get.filter(c => c.toString().contains(" AS "))
        assertFalse(aliasColumns.isEmpty)
      }

      "if its not alias then no illegal chars" in {
        val sanitizedColNames = flattenSchemaWithSanitization.get.filter(c => !c.toString().contains(" AS ") && SchemaUtils.illegalCharsRegex.findAllMatchIn(c.toString()).length>0)
        logger.debug("illegal chars in sanitized flatten schema: {}", sanitizedColNames)
        assert(sanitizedColNames.isEmpty)
      }
    }
  }

  "schema" when {
    val schema = SchemaUtils.loadDataFrameSchema("dailyparams-raw")
    logger.debug(schema.get.sql)
    "no ignored column" should {
      "have no values in the FilterMap of columnFilter" in {
        assertTrue(ColumnFilter(Seq.empty).ignoreMap.values.isEmpty)
      }
      "return none after filter" in {
        assertFalse(SchemaUtils.filter(schema.get).isDefined)
      }
    }

    "simple ignore columns specified" should {
      val ignoreColumns = Seq("id", "identifiers.Serial Number", "subscriberIdentifiers.ISPUsername",
        "properties.tags", "properties.groups", "properties.wan.ethDuplexMode", "properties.wifi.0.SSID")

      "have no values in the FilterMap of columnFilter" in {
        val colFilter = ColumnFilter(ignoreColumns)

        assertFalse("The values should contain blanks", colFilter.ignoreMap.values.exists(StringUtils.isNotBlank(_)))
        assertEquals(ignoreColumns.size, colFilter.ignoreMap.keys.size)
        assertTrue(ignoreColumns.diff(colFilter.ignoreMap.keys.toSeq).isEmpty)
      }

      "filter ignored columns" in {
        val filtered = SchemaUtils.filter(schema.get, ignoreColumns)
        assertTrue(filtered.isDefined)
        val schemaDiff = SchemaUtils.flatten(schema).get.diff(SchemaUtils.flatten(filtered).get)
        logger.info("length: {} {}", schemaDiff.length, schemaDiff)
        schemaDiff.foreach(c => assertTrue(ignoreColumns.contains(c.toString().trim)))
        logger.debug(filtered.get.treeString)
      }
    }

    "regex ignore columns specified" should {
      val ignoreColumns = Seq("id", "identifiers.Serial Number", "subscriberIdentifiers.ISPUsername",
        "properties.tags", "properties.groups", "properties.wan.ethDuplexMode", "properties.wifi.*.SSIDAdvertisementEnabled")

      "have values in the FilterMap of columnFilter" in {
        val colFilter = ColumnFilter(ignoreColumns)

        assertTrue("The ignore column with special char should not be in the keys of ignoreColumnMap",
          ignoreColumns.diff(colFilter.ignoreMap.keys.toSeq).contains("properties.wifi.*.SSIDAdvertisementEnabled"))

        assertTrue("The values should not just contain blanks", colFilter.ignoreMap.values.exists(StringUtils.isNotBlank(_)))
        assertEquals(ignoreColumns.size, colFilter.ignoreMap.keys.size)

        logger.info("{}", colFilter.ignoreMap)
        assertTrue("The charset left to special char must be present in colFilterMap key set",
          colFilter.ignoreMap.contains("properties.wifi."))
        assertTrue("The charset right to special char must be present in colFilterMap value set",
          colFilter.ignoreMap.values.toSeq.contains(".SSIDAdvertisementEnabled"))
        assertFalse("The colFilterMap value set should contain only one not blank element",
          colFilter.ignoreMap.values.toSeq.diff(Seq(".SSIDAdvertisementEnabled")).exists(StringUtils.isNoneBlank(_)))
      }

      "filter ignored columns regex" in {
        val filtered = SchemaUtils.filter(schema.get, ignoreColumns)
        assertTrue(filtered.isDefined)
        val schemaDiff = SchemaUtils.flatten(schema).get.diff(SchemaUtils.flatten(filtered).get)
        logger.info("length: {} {}", schemaDiff.length, schemaDiff)
        //schemaDiff.foreach(c => assertTrue(ignoreColumns.contains(c.toString().trim)))
        logger.debug(filtered.get.treeString)
        assertTrue(schema.get.sql.contains("SSIDAdvertisementEnabled"))
        assertFalse(filtered.get.sql.contains("SSIDAdvertisementEnabled"))
      }
    }
  }

  "audit schema" should {
    "be loaded" in {
      val auditDf = SchemaUtils.loadDataFrameSchema("audit")
      assertTrue(auditDf.isDefined)
      val columns = Array("layer", "name", "start_ts", "end_ts", "comments")
      auditDf.get.fieldNames.foreach(c => assertTrue(columns.contains(c)))
    }
  }
}
