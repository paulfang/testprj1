package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.types.{DataType, StructType}
import org.junit.Assert._
import org.scalatest.WordSpec

import scala.reflect.runtime.universe


class AppConfigTest extends WordSpec {
  val logger = Logger(getClass)

  "minimal config" should {
    "work" in {
      val config = AppConfig("config/application-minimal.json")
      assertNotNull(config)
    }
  }

  "audit" should {
    "be false when not found" in {
      val config = AppConfig("config/application-bronze.json")
      assertFalse(config.auditor.nonEmpty)
    }

    "be false when defined false" in {
      val config = AppConfig("config/audit/application-bronze-auditFalse.json")
      assertFalse(config.auditor("audit").asInstanceOf[Boolean])
    }

    "be true when defined true" in {
      val config = AppConfig("config/audit/application-bronze-auditTrue.json")
      assertTrue(config.auditor("audit").asInstanceOf[Boolean])
    }
  }

  "audit inform" should {
    "be false when defined false" in {
      val config = AppConfig("config/audit/application-bronze-auditFalse.json")
      assertFalse(config.auditor("audit_inform").asInstanceOf[Boolean])
    }

    "be true when defined true" in {
      val config = AppConfig("config/audit/application-bronze-auditTrue.json")
      assertTrue(config.auditor("audit_inform").asInstanceOf[Boolean])
    }
  }

  "appConfig with multiple config files" should {
    "be merged" in {
      val firstConfig = AppConfig("config/application-test.json")
      val secondConfig = AppConfig("config/config-merge-test.json")
      val mergedConfig = AppConfig("config/application-test.json", "config/config-merge-test.json")

      assertNotNull(mergedConfig.config)
      logger.debug("{}", mergedConfig.config)
      assertTrue("The length should be the greater than either",
        mergedConfig.config.size > firstConfig.config.size && mergedConfig.config.size > secondConfig.config.size)
    }

    "allow property inheritance" in {
      val firstConfig = AppConfig("config/application-test.json")
      val mergedConfig = AppConfig("config/application-test.json", "config/config-merge-test.json")

      assertNotNull(mergedConfig.config)
      assertEquals("missing time_dim_columns property should be inherited",
        firstConfig.config("time_dim_columns"), mergedConfig.config("time_dim_columns"))

      assertEquals("missing time_dim_columns property should be inherited",
        firstConfig.config("time_dim_columns"), mergedConfig.config("time_dim_columns"))
    }

    "allow property overwrite" in {
      val secondConfig = AppConfig("config/config-merge-test.json")
      val mergedConfig = AppConfig("config/application-test.json", "config/config-merge-test.json")

      assertNotNull(mergedConfig.config)
      assertEquals("input_location property should be overwritten",
        secondConfig.config("input_location"), mergedConfig.config("input_location"))
      assertEquals("output_location property should be overwritten",
        secondConfig.config("output_location"), mergedConfig.config("output_location"))
      assertEquals("warehouse_location property should be overwritten",
        secondConfig.config("warehouse_location"), mergedConfig.config("warehouse_location"))

      assertEquals("timestamp_columns property should be overwritten", secondConfig.config("timestamp_columns"), mergedConfig.config("timestamp_columns"))
    }
  }

  "config file with input schema" should {
    "be correctly mapped to structType" in {
      assertNotNull(AppConfig("config/subscriber-bronze-test.json").inputSchema.get.asInstanceOf[StructType])
    }
  }

  "config file with input schema with no output schema" should {
    val inputSchema = AppConfig("config/subscriber-bronze-test.json").inputSchema.get
    val outputSchema = AppConfig("config/subscriber-bronze-test.json").outputSchema.get

    "not be null" in {
      assertNotNull(inputSchema)
      assertNotNull(outputSchema)
    }

    "have illegal chars in input schema" in {
      assertIllegalCharsFound(inputSchema)
    }

    "not have illegal chars in sanitized output schema" in {
      assertNoIllegalChars(SchemaUtils.sanitize(inputSchema).get)
    }
  }

  "config files with output_save_mode" should {
    "match spark save mode" in {
      assertEquals(SaveMode.Overwrite, AppConfig("config/application-test.json", "config/subscriber-bronze-test.json").outputSaveMode)
    }
  }

  "config file with missing output_save_mode" should {
    "match default (append) mode" in {
      assertEquals(SaveMode.Append, AppConfig("config/subscriber-bronze-test.json").outputSaveMode)
    }
  }

  "config file with unknown output_save_mode" should {
    "match default (append) mode" in {
      assertEquals(SaveMode.Append, AppConfig("config/unknown-savemode-test.json").outputSaveMode)
    }
  }

  "appConfig " should {
    val appConfig = AppConfig("config/application-bronze.json")
    "have input_location" in {
      assertNotNull(appConfig.inputLocation)
    }
    "have input_file_pattern" in {
      assertNotNull(appConfig.inputFilePattern)
    }
    "have output_location" in {
      assertNotNull(appConfig.outputLocation)
    }
    "have time_dim_columns" in {
      val expected = TimeDims(Seq("year", "month", "dayofmonth", "hour"), "ts", .001)
      assertTrue(appConfig.createTimeDims.isDefined)
      assertEquals(expected, appConfig.createTimeDims.get)
    }
  }

  "enable data conversion" in {
    val appConfig = AppConfig("config/application-test.json")
    assertEquals("Eco Inquire", appConfig.getValueAsString("name"))
    assertEquals(.001, appConfig.getValueAsDouble("timestamp_multiplier"), .001)
    val expectedPartitions = "ts,timestamp,created,modified,firstInform,lastInform,lastBoot,lastPeriodic".split(",")

    val partitions = appConfig.getValueAsSeq("timestamp_columns")
    logger.trace("partitions {}: ", partitions)

    assertTrue(partitions.diff(expectedPartitions).isEmpty)

    //    val missing = expectedPartitions.map(partitions.contains(_)).filter(!_).length
    //    assertEquals(0, missing)

    val processTs = appConfig.getValueAsBool("process_ts")
    val dataTypes = appConfig.getValueAsDict("data_types")
    logger.debug("dataTypes: {}", dataTypes)

    val expectedDataTypes = Map(
      "ts_column" -> "ts",
      "col_prefix" -> "ts_",
      "year" -> true,
      "hour" -> false,
      "hour_count" -> 24,
      "avg_multiplier" -> 0.04,
      "counter" -> Seq(1, 2, 3),
      "formula" -> Map("operand" -> "some operand",
        "division" -> Seq(1, 2, 3))
    )
    logger.debug("exepctedDataTypes {}", dataTypes.getClass)

    assertEquals(expectedDataTypes.size, dataTypes.size)

    logger.debug("dataTypes {}", dataTypes.getClass)
    dataTypes.foreach(k => {
      logger.debug("value class: {} value: {}", k._2.getClass, k._2)
    })

    expectedDataTypes.foreach(k => if (!k._1.equals("formula") && !k._1.equals("counter")) assertEquals(k._2, dataTypes.get(k._1).get))
  }

  "no partitions element" should {
    "give empty partitions" in {
      assertTrue(AppConfig("config/application-test.json").partitions.isEmpty)
    }
  }

  "partitions element defined" should {
    "give correct partitions" in {
      assertTrue(Seq("ts_year", "ts_month", "ts_day").diff(AppConfig("config/application-bronze-parquet-out-withTimeDims-partitioned.json").partitions).isEmpty)
    }
  }

  "override partitions" in {
    val firstConfig = AppConfig("config/application-bronze-parquet-out-withTimeDims-partitioned.json")
    assertTrue(Seq("ts_year","ts_month","ts_day").diff(firstConfig.partitions).isEmpty)
    val secondConfig = AppConfig("config/application-overridePartitions.json")
    assertTrue(Seq("ts_date").diff(secondConfig.partitions).isEmpty)
    val mergedConfig = AppConfig("config/application-bronze-parquet-out-withTimeDims-partitioned.json",
      "config/application-overridePartitions.json")

    assertTrue(Seq("ts_date").diff(mergedConfig.partitions).isEmpty)
  }

  "overriding from application args" when {
    "not provided" should {
      "not override properties" in {
        val secondConfig = AppConfig("config/config-merge-test.json")
        val mergedConfig = AppConfig("config/application-test.json", "config/config-merge-test.json")

        assertNotNull(mergedConfig.config)
        assertEquals("inputLocation property should be overridden",
          secondConfig.inputLocation, mergedConfig.inputLocation)
        assertEquals("outputLocation property should be overridden",
          secondConfig.outputLocation, mergedConfig.outputLocation)
        assertEquals("inputFilePattern property should be overridden",
          secondConfig.inputFilePattern, mergedConfig.inputFilePattern)
        assertEquals("outputFormat property should be overridden",
          Format.delta, mergedConfig.outputFormat)

        assertEquals("timestamp_columns property should be overridden",
          secondConfig.config("timestamp_columns"), mergedConfig.config("timestamp_columns"))

      }
    }
    "provided" should {
      "override properties" in {
        val secondConfig = AppConfig("config/config-merge-test.json")
        val mergedConfig = AppConfig("config/application-test.json", "config/config-merge-test.json"
          , "input_location=override-test-in"
          , "output_location=override-test-out"
          , "input_file_pattern=override-test/file-pattern.gz"
          , "output_format=parquet")

        assertNotNull(mergedConfig.config)
        assertEquals("timestamp_columns property should be overwritten",
          secondConfig.config("timestamp_columns"), mergedConfig.config("timestamp_columns"))
        assertEquals("inputLocation property should be overridden",
          "override-test-in", mergedConfig.inputLocation)
        assertEquals("outputLocation property should be overridden",
          "override-test-out", mergedConfig.outputLocation)
        assertEquals("inputFilePattern property should be overridden",
          "override-test/file-pattern.gz", mergedConfig.inputFilePattern)
        assertEquals("outputFormat property should be overridden",
          Format.parquet, mergedConfig.outputFormat)
      }
    }
  }

  "filterExpr" should {
    "be created" in {
      val expected = "id in ('0c765053128f72d796e583298fbcc9b9', 'a050bef0346811e99fac005056994ac2') and ts > 1590278337802 and array_contains(properties.groups, 'HSI')"
      assertEquals(expected, AppConfig("config/application-filterExpr.json").filterExpr)
    }
  }

  "passthrough" should {
    "be false because no value" in {
      assertFalse(AppConfig("config/application-test.json").passthrough)
    }

    "be false because its not 1" in {
      assertFalse(AppConfig("config/application-test.json", "passthrough=0").passthrough)
    }

    "be false because its not true" in {
      assertFalse(AppConfig("config/application-test.json", "passthrough=false").passthrough)
    }

    "be false because its not yes" in {
      assertFalse(AppConfig("config/application-test.json", "passthrough=no").passthrough)
    }

    "be true because its yes" in {
      val config = AppConfig("config/application-test.json", "passthrough=yes")
      assertTrue(config.passthrough)
    }

    "be true because its 1" in {
      val config = AppConfig("config/application-test.json", "passthrough=1")
      assertTrue(config.passthrough)
    }

    "be true because its true" in {
      val config = AppConfig("config/application-test.json", "passthrough=true")
      assertTrue(config.passthrough)
    }
  }

  "backfill" should {
    "be false because no value" in {
      assertFalse(AppConfig("config/application-test.json").backfill)
    }

    "be false because its not 1" in {
      assertFalse(AppConfig("config/application-test.json", "backfill=0").backfill)
    }

    "be false because its not true" in {
      assertFalse(AppConfig("config/application-test.json", "backfill=false").backfill)
    }

    "be false because its not yes" in {
      assertFalse(AppConfig("config/application-test.json", "backfill=no").backfill)
    }

    "be true because its yes" in {
      assertTrue(AppConfig("config/application-test.json", "backfill=yes").backfill)
    }

    "be true because its 1" in {
      assertTrue(AppConfig("config/application-test.json", "backfill=1").backfill)
    }

    "be true because its true" in {
      assertTrue(AppConfig("config/application-test.json", "backfill=true").backfill)
    }
  }

  "insertColumns" should {
    "be empty" in {
      val insertColumns = AppConfig("config/application-test.json").insertColumns
      assertTrue(insertColumns.isEmpty)
    }

    "not be empty" in {
      val insertColumns = AppConfig("config/application-withInsertColumns.json").insertColumns
      assertFalse(insertColumns.isEmpty)
      assertEquals("2021-03-05", insertColumns("ts_date"))
    }

    "insertColumns not be empty by override" in {
      val insertColVal = "ts_date: 2021-03-05, actor: 100"
      val insertColumns = AppConfig("config/application-test.json", s"insert_columns=$insertColVal").insertColumns
      assertFalse(insertColumns.isEmpty)
      assertEquals("2021-03-05", insertColumns("ts_date"))
    }
  }

  "input_file_list" should {
    "be processed" in {
      val appConfig = AppConfig("config/application-input_file_list.json")
      assertEquals(5, appConfig.inputFileList.split(",").size)
    }
  }

  "csvExport" when {
    "not defined" in {
      val appConfig = AppConfig("config/application-test.json")
      assertFalse(appConfig.csvExport.nonEmpty)
      assertFalse(appConfig.csvExportEnabled)
      assertFalse(appConfig.csvExportSaveMode.isDefined)
      assertFalse(appConfig.csvExportLocation.isDefined)
    }

    "defined" in {
      val appConfig = AppConfig("config/application-csvExport.json")
      assertTrue(appConfig.csvExport.nonEmpty)
      assertTrue(appConfig.csvExportEnabled)
      assertTrue(appConfig.csvExportSaveMode.isDefined)
      assertEquals("Overwrite", appConfig.csvExportSaveMode.get)
      assertTrue(appConfig.csvExportLocation.isDefined)
      assertEquals("/data/test/processed/csv", appConfig.csvExportLocation.get)
    }

    "defined but missing exportEnabledFlag" in {
      val appConfig = AppConfig("config/application-csvExportMissingEnabledFlag.json")
      assertTrue(appConfig.csvExport.nonEmpty)
      assertFalse(appConfig.csvExportEnabled)
      assertFalse(appConfig.csvExportSaveMode.isDefined)
      assertFalse(appConfig.csvExportLocation.isDefined)
    }

    "defined but missing values" in {
      val appConfig = AppConfig("config/application-csvExportMissingValues.json")
      assertTrue(appConfig.csvExport.nonEmpty)
      assertTrue(appConfig.csvExportEnabled)
      assertTrue(appConfig.csvExportSaveMode.isDefined && appConfig.csvExportSaveMode.get.equals("Overwrite"))
      assertFalse(appConfig.csvExportLocation.isDefined)
    }
  }

  "inputFilter " should {
    "not be found" in {
      val appConfig = AppConfig("config/application-bronze.json")
      assertTrue(appConfig.inputFilter.isEmpty)
    }

    "be found" in {
      val appConfig = AppConfig("config/application-withInputFilter.json")
      assertFalse(appConfig.inputFilter.isEmpty)
      val filterName = "com.commscope.eco.LastProcessingBasedInputFilter"
      assertEquals(filterName, appConfig.inputFilter("filter"))

      val mirror = universe.runtimeMirror(getClass.getClassLoader)
      val module = mirror.staticModule(filterName)
      val obj = mirror.reflectModule(module)
      val filter: InputFilter = obj.instance.asInstanceOf[InputFilter]
      assertEquals(LastProcessingBasedInputFilter, filter)

      assertTrue(appConfig.inputFilter.contains("args"))
      assertEquals("ts_processed", appConfig.inputFilter("args").asInstanceOf[Map[String,String]]("input_column"))
      assertEquals("ts_processed", appConfig.inputFilter("args").asInstanceOf[Map[String,String]]("output_column"))
    }
  }

  "version" should {
    "be auto determined when no pipelines" in {
      val appConfig = AppConfig("config/application-bronze.json")
      assertEquals("1.0", appConfig.version)
    }

    "be determined when pipelines" in {
      val appConfig = AppConfig("config/pipeline/multiProcessor-withSink.json")
      assertEquals("2.0", appConfig.version)
    }

    "be overwritten by version field" in {
      val appConfig = AppConfig("config/application-bronze-withVersion.json")
      assertEquals("2.0", appConfig.version)
    }

    "be downgraded even when pipelines" in {
      val appConfig = AppConfig("config/pipeline/multiProcessor-versionDowngraded.json")
      assertEquals("1.0", appConfig.version)
    }
  }

  "splitter" should {
    "be none" in {
      val appConfig = AppConfig("config/application-bronze-withVersion.json")
      assertFalse(appConfig.splitter.isDefined)
    }

    "be none for byColumnName if column_name not given" in {
      val appConfig = AppConfig("config/application-with-splitter-missing-splitOn.json")
      assertFalse(appConfig.splitter.isDefined)
    }

    "be byColumnName" in {
      val splitOn = Seq(Map("column_name" -> "ts_date", "numeric" -> false), Map("column_name" -> "ts_hour", "numeric" -> true))
      val appConfig = AppConfig("config/application-with-splitter.json")
      val expectedSplitOn = Seq(SplitColumn("ts_date"), SplitColumn("ts_hour", numeric = true))
      val splitter = appConfig.splitter.get
      assertEquals(expectedSplitOn, splitter.splitOn)
      assertTrue(splitter.sorted)
      assertEquals("asc", splitter.sortOrder)
      assertEquals("ts", splitter.orderByColumn.get)
    }
  }

  "split auditing" when {
    "split audit is disabled" in {
      val splitter = AppConfig("config/application-with-splitter.json").splitter.get
      assertFalse(splitter.auditEnabled)
      assertFalse(splitter.auditOutputLocation.isDefined)
    }

    "split audit is enabled" in {
      val splitter = AppConfig("config/application-with-splitter-withAuditor.json").splitter.get
      assertTrue(splitter.auditEnabled)
      assertEquals("src/test/resources/data/frontier/splitter/element-ts", splitter.auditOutputLocation.get)
    }
  }

  private def assertIllegalCharsFound(schema: DataType) = {
    val origSql = schema.sql.split(",").foldLeft(List[Array[String]]())((l, e) => l :+ e.split(":")).flatten.map(_.trim).flatten(SchemaUtils.illegalCharsRegex.findAllMatchIn(_))
    logger.debug("illegal chars in orig df: {}", origSql)
    assertFalse(origSql.isEmpty)
  }

  private def assertNoIllegalChars(xschema: DataType) = {
    xschema match {
      case st: StructType => logger.trace("Replaced schema:\n{}", st.treeString)
        logger.trace("Replaced sql:\n{}", st.sql)

        val modSql = st.sql.split(",").foldLeft(List[Array[String]]())((l, e) => l :+ e.split(":")).flatten.map(_.trim).flatten(SchemaUtils.illegalCharsRegex.findAllMatchIn(_))
        logger.debug("illegal chars in replaced df: {}", modSql)

        assertTrue(modSql.isEmpty)
      case _ =>
    }
  }
}


