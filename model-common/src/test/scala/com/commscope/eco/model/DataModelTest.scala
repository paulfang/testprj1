package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import junit.framework.Assert._
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions.{asc, col, desc}
import org.junit.Assert

class DataModelTest extends SparkBasedTest {
  val logger = Logger(this.getClass)

  "snake the words" in {
    assertEquals("hello_world", Generic.snakify("hello_world"))
    assertEquals("hello_world", Generic.snakify("helloWorld"))
    assertEquals("hello_world", Generic.snakify("HelloWorld"))
    assertEquals("hello_world", Generic.snakify("Hello_World"))
    assertEquals("helloworld", Generic.snakify("helloworld"))
    assertEquals("helloworld", Generic.snakify("HELLOWORLD"))
    assertEquals("_hello_world", Generic.snakify("_helloWorld"))
    assertEquals("oui", Generic.snakify("OUI"))
    assertEquals("subscriber_id", Generic.snakify("subscriberID"))
    assertEquals("subscriber_id", Generic.snakify("SubscriberID"))
    assertEquals("my_full_name", Generic.snakify("myFullName"))
    assertEquals("device_uuid", Generic.snakify("deviceUUID"))
    assertEquals("_device_uuid", Generic.snakify("_deviceUUID"))
    assertEquals("device_uuid", Generic.snakify("device_UUID"))
    assertEquals("device123", Generic.snakify("device123"))
  }

  "groupBy transformation" in {
    val config = AppConfig("config/transformation-groupBy.json")
    val htDf = spark.read.parquet(s"${config.inputLocation}")
    assertEquals(27, htDf.columns.length)

    val clientModel = Generic.model(htDf, config, args = Map())
    assertNotNull(clientModel)
    logger.debug("schema: {}\n", clientModel.schema.treeString)
    assertEquals(9, clientModel.columns.length)
    assertTrue(clientModel.columns.contains("mac_count"))
    assertTrue(clientModel.columns.contains("dev_count"))
    assertTrue(clientModel.columns.contains("mac_sum"))
    assertTrue(clientModel.columns.contains("dev_sum"))
  }

  "selectInput transformation" in {
    val config = AppConfig("config/transformation-selectColumns.json")
    val df = spark.read.parquet(s"${config.inputLocation}")
    assertEquals(98, df.columns.length)

    val clientModel = Generic.model(df, config, args = Map())
    assertNotNull(clientModel)
    logger.debug("schema: {}\n", clientModel.schema.treeString)
    assertEquals(32, clientModel.columns.length)
    assertTrue(clientModel.columns.contains("device_id"))
    assertTrue(clientModel.columns.contains("bytes_sent"))
    assertTrue(clientModel.columns.contains("ethernet_bytes_sent"))
    assertFalse(clientModel.columns.contains("last_showtime_start"))
    assertTrue(clientModel.columns.contains("uptime"))
  }

  "dropColumns transformation" in {
    val config = AppConfig("config/transformation-dropColumns.json")
    val df = spark.read.parquet(s"${config.inputLocation}")
    assertEquals(98, df.columns.length)

    val clientModel = Generic.model(df, config, args = Map())
    assertNotNull(clientModel)
    logger.info("schema: {}\n", clientModel.schema.treeString)
    logger.info("columns: {}", clientModel.columns.length)
    assertEquals(14, clientModel.columns.length)
    assertTrue(clientModel.columns.contains("device_id"))
    assertTrue(clientModel.columns.contains("bytes_received"))
    assertTrue(clientModel.columns.contains("packets_received"))
    assertFalse(clientModel.columns.contains("bytes_sent"))
    assertFalse(clientModel.columns.contains("ethernet_bytes_sent"))
    assertFalse(clientModel.columns.contains("uptime"))
  }

  "dedupColumnNames " in {
    val df = spark.read.parquet("src/test/resources/data/frontier/dfWithDupColumns.parquet")
      .withColumn("Rg", col("RG"))
      .withColumn("rg", col("RG"))
    Assert.assertTrue(df.columns.contains("RG"))
    Assert.assertFalse(df.columns.contains("RG_1"))
    val dedupDf = TestModel.deduplicateColumns(df)
    Assert.assertFalse(dedupDf.columns.contains("RG"))
    Assert.assertTrue(dedupDf.columns.contains("RG_1"))
    TestModel.caseFormat(dedupDf, "lower").write.mode("overwrite").parquet("tmp/inquire/dedup")
  }

  /**
   * Create windowSpec from
   *
   * "window": {
   *    "partitionBy": ["device_id"]
   *  }
   */
  "windowSpec" should {
    "be created" in {
      var args = Map[String, Map[String, Any]]()
      val partitions = Map("partitionBy" -> Seq("device_id", "mac_address"))
      args += ("window" -> partitions)
      logger.info("args: {}", args)

      val windowSpec = Generic.createWindowSpec(args)
      assertTrue(windowSpec.isDefined)
      logger.info("windowSpec: {}", windowSpec.get)
    }
  }

  /**
   * Create windowSpec from
   *
   * "window": {
   *    "partitionBy": ["device_id"],
   *    "orderBy": [
   *      {"desc": "ts"},
   *      {"asc" : "inform"}
   *    ]
   *  }
   */
  "windowSpec with orderBy" should {
    "be created" in {
      var args = Map[String, Map[String, Any]]()

      val partitionsOrderBy = Map(
        "partitionBy" -> Seq("device_id", "mac_address"),
        "orderBy" -> Seq(Map("desc" -> "ts"), Map("asc" -> "inform"))
      )

      args += ("window" -> partitionsOrderBy)
      logger.info("args: {}", args)

      val windowSpec = Generic.createWindowSpec(args)
      assertTrue(windowSpec.isDefined)
      val expected = createWindoSpec === windowSpec

      logger.info("equal expectation: {}", expected)
    }
  }

  /**
        "function": {
              "name": "lag",
              "offset": 1,
              "columns": ["bytes_received", "bytes_sent"],
              "output_suffix": "_prev"
        }
   */
  "windowLag should be applied" in {
    val selection = Seq("device_id", "ts", "ts_date", "ts_inform", "bytes_sent", "bytes_received", "ethernet_bytes_sent")

    val config = AppConfig("config/transformation-windowLag.json")
    val df = spark.read.parquet(s"${config.inputLocation}").select(selection.head, selection.tail :_*)
    assertEquals(selection.length, df.columns.length)
    val windowSpec = createWindoSpec
    val funcArgs = Map("name" -> "lag",
    "offset" -> 1,
    "columns" -> Seq("bytes_received", "bytes_sent")
    )

    val dfWithLag = Generic.applyWindowLag(df, windowSpec, Map("function" -> funcArgs))
    logger.debug("dfWithLag schema: {}", dfWithLag.schema.treeString)
    assertEquals(df.columns.length +2, dfWithLag.columns.length)
    assertTrue(dfWithLag.columns.contains("bytes_sent_prev"))
    assertTrue(dfWithLag.columns.contains("bytes_received_prev"))
    assertTrue(dfWithLag.columns.contains("ethernet_bytes_sent"))
    assertFalse(dfWithLag.columns.contains("ethernet_bytes_sent_prev"))

    val verifyCols = Seq("ts_date","ts_inform", "bytes_sent", "bytes_sent_prev","bytes_received", "bytes_received_prev")
    val dfTop = dfWithLag.select(verifyCols.head, verifyCols.tail :_*).limit(5).head(5)

    /* First record should have the _prev null remaining should not
    AND
    the not null values should be equal to the previous value
    */

    for (i <- 0 until 5) {
      assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_sent")))
      assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_received")))

      if (i ==0) {
        assertNull(dfTop(i)(verifyCols.indexOf("bytes_sent_prev")))
        assertNull(dfTop(i)(verifyCols.indexOf("bytes_received_prev")))
      }
      else {
        assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_sent_prev")))
        assertEquals(dfTop(i-1)(verifyCols.indexOf("bytes_sent")), dfTop(i)(verifyCols.indexOf("bytes_sent_prev")))

        assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_received_prev")))
        assertEquals(dfTop(i-1)(verifyCols.indexOf("bytes_received")), dfTop(i)(verifyCols.indexOf("bytes_received_prev")))
      }
    }
  }


  /**
  "function": {
              "name": "lag",
              "offset": 1,
              "columns": ["bytes_received", "bytes_sent"],
              "output_suffix": "_prev",
              "apply": {
                "expression": "abs(col($$) - col($$_prev))",
                "output_suffix": "_diff"
              }
        }
   */
  "windowLag with diff should be applied" in {
    val selection = Seq("device_id", "ts", "ts_date", "ts_inform", "bytes_sent", "bytes_received", "ethernet_bytes_sent")

    val config = AppConfig("config/transformation-windowLag.json")
    val df = spark.read.parquet(s"${config.inputLocation}").select(selection.head, selection.tail :_*)
    assertEquals(selection.length, df.columns.length)
    val windowSpec = createWindoSpec
    val funcArgs = Map("name" -> "lag",
      "offset" -> 1,
      "columns" -> Seq("bytes_received", "bytes_sent"),
      "output_suffix" -> "_prev",
      "apply" -> Map("expression" -> "abs($$ - $$_prev)", "output_suffix" -> "_diff")
    )

    val additionalColumnCount = 4 //_prev and _diff columns for each
    val dfWithLag = Generic.applyWindowLag(df, windowSpec, Map("function" -> funcArgs))
    logger.debug("dfWithLag schema: {}", dfWithLag.schema.treeString)
    assertEquals(df.columns.length +additionalColumnCount, dfWithLag.columns.length)
    assertTrue(dfWithLag.columns.contains("bytes_sent_prev"))
    assertTrue(dfWithLag.columns.contains("bytes_sent_diff"))
    assertTrue(dfWithLag.columns.contains("bytes_received_prev"))
    assertTrue(dfWithLag.columns.contains("bytes_received_diff"))
    assertTrue(dfWithLag.columns.contains("ethernet_bytes_sent"))
    assertFalse(dfWithLag.columns.contains("ethernet_bytes_sent_prev"))

    val verifyCols = Seq("ts_date","ts_inform",
      "bytes_sent", "bytes_sent_prev", "bytes_sent_diff",
      "bytes_received", "bytes_received_prev", "bytes_received_diff")

    val dfTop = dfWithLag.select(verifyCols.head, verifyCols.tail :_*).limit(5).head(5)

    /* First record should have the _prev null remaining should not
    AND
    the not null values should be equal to the previous value
    AND
    the diff column should be equal to curr value - prev value
    */

    for (i <- 0 until 5) {
      logger.info("{}",dfTop(i))
      assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_sent")))
      assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_received")))

      if (i ==0) {
        assertNull(dfTop(i)(verifyCols.indexOf("bytes_sent_prev")))
        assertNull(dfTop(i)(verifyCols.indexOf("bytes_received_prev")))
      }
      else {
        assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_sent_prev")))
        assertEquals(dfTop(i-1)(verifyCols.indexOf("bytes_sent")), dfTop(i)(verifyCols.indexOf("bytes_sent_prev")))

        assertEquals(dfTop(i)(verifyCols.indexOf("bytes_sent_diff")),
          dfTop(i)(verifyCols.indexOf("bytes_sent")).asInstanceOf[Long] -
            dfTop(i)(verifyCols.indexOf("bytes_sent_prev")).asInstanceOf[Long] )

        assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_received_prev")))
        assertEquals(dfTop(i-1)(verifyCols.indexOf("bytes_received")), dfTop(i)(verifyCols.indexOf("bytes_received_prev")))

        assertEquals(dfTop(i)(verifyCols.indexOf("bytes_received_diff")),
          dfTop(i)(verifyCols.indexOf("bytes_received")).asInstanceOf[Long] -
            dfTop(i)(verifyCols.indexOf("bytes_received_prev")).asInstanceOf[Long] )
      }
    }
  }

  val createWindoSpec = {
    val partitions = Seq("device_id", "ts_date")
    val orderBys = Seq(asc("ts"), desc("ts_inform"))
    Window.partitionBy(partitions.head, partitions.tail :_*).orderBy(orderBys :_*)
  }

  "windowLag transformation" in {
    val config = AppConfig("config/transformation-windowLag.json")
    val df = spark.read.parquet(s"${config.inputLocation}")
    assertEquals(98, df.columns.length)

    val clientModel = Generic.model(df, config, args = Map())
    assertNotNull(clientModel)
    logger.debug("schema: {}\n", clientModel.schema.treeString)

    assertEquals(51, clientModel.columns.length)
    assertTrue(clientModel.columns.contains("device_id"))
    assertTrue(clientModel.columns.contains("bytes_received"))
    assertFalse("dropTransformation should drop it", clientModel.columns.contains("bytes_received_prev"))
    assertTrue(clientModel.columns.contains("bytes_received_current"))
    assertTrue(clientModel.columns.contains("packets_received"))
    assertFalse("dropTransformation should drop it", clientModel.columns.contains("packets_received_prev"))
    assertTrue(clientModel.columns.contains("packets_received_current"))

    assertTrue(clientModel.columns.contains("ethernet_bytes_received"))
    assertFalse("dropTransformation should drop it", clientModel.columns.contains("ethernet_packets_received_prev"))
    assertTrue(clientModel.columns.contains("ethernet_packets_received_current"))

    val verifyCols = Seq("ts_date","ts_inform",
      "bytes_sent", "bytes_sent_current",
      "bytes_received", "bytes_received_current",
      "ethernet_bytes_sent", "ethernet_bytes_sent_current",
      "ethernet_packets_sent", "ethernet_packets_sent_current"
    )

    val dfTop = clientModel.select(verifyCols.head, verifyCols.tail :_*).limit(5).head(5)

    /* First record should have the _prev null remaining should not
    AND
    the diff column should be equal to value of current row - value of previous row
    */

    for (i <- 0 until 5) {
      logger.debug("{}", dfTop(i))
      assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_sent")))
      assertNotNull(dfTop(i)(verifyCols.indexOf("bytes_received")))

      if (i ==0) {
        assertNull(dfTop(i)(verifyCols.indexOf("bytes_sent_current")))
        assertNull(dfTop(i)(verifyCols.indexOf("bytes_received_current")))
      }
      else {
        assertEquals(dfTop(i)(verifyCols.indexOf("bytes_sent_current")),
          dfTop(i)(verifyCols.indexOf("bytes_sent")).asInstanceOf[Long] -
            dfTop(i-1)(verifyCols.indexOf("bytes_sent")).asInstanceOf[Long] )

        assertEquals(dfTop(i)(verifyCols.indexOf("bytes_received_current")),
          dfTop(i)(verifyCols.indexOf("bytes_received")).asInstanceOf[Long] -
            dfTop(i-1)(verifyCols.indexOf("bytes_received")).asInstanceOf[Long] )

        assertEquals(dfTop(i)(verifyCols.indexOf("ethernet_bytes_sent_current")),
          dfTop(i)(verifyCols.indexOf("ethernet_bytes_sent")).asInstanceOf[Long] -
            dfTop(i-1)(verifyCols.indexOf("ethernet_bytes_sent")).asInstanceOf[Long] )

        assertEquals(dfTop(i)(verifyCols.indexOf("ethernet_packets_sent_current")),
          dfTop(i)(verifyCols.indexOf("ethernet_packets_sent")).asInstanceOf[Long] -
            dfTop(i-1)(verifyCols.indexOf("ethernet_packets_sent")).asInstanceOf[Long] )
      }
    }
  }
}

object TestModel extends DataModel {
  override def model(dataFrame: DataFrame, appConfig: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = dataFrame
}