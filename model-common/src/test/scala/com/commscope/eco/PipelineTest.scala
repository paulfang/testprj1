package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.functions.{col, explode}
import org.apache.spark.sql.{DataFrame, SaveMode}
import org.junit.Assert._

class PipelineTest extends SparkBasedTest {
  val logger = Logger(getClass)

  "selectInputColumns transformer" should {
    "select columns" in {
      val df = spark.read.parquet("src/test/resources/data/frontier/deviceBW")
      val currColumns = df.columns.filter(_.endsWith("_current"))
      val tsColumns = df.columns.filter(_.startsWith("ts_"))

      assertFalse(currColumns.isEmpty)
      assertFalse(tsColumns.isEmpty)
      assertTrue(df.columns.contains("uptime"))

      val expectedColumnLen = currColumns.length + tsColumns.length + 1
      assertTrue(df.columns.length > expectedColumnLen)

      val transformer = Transformer(Map("name" -> "dummy"))
      val conf = Seq("ts_*", "*_current", "uptime")
      val xformed = transformer.selectInputColumns(df, conf)
      assertEquals(expectedColumnLen, xformed.columns.length)

      assertEquals(currColumns.length, xformed.columns.count(_.endsWith("_current")))
      assertTrue(xformed.columns.contains("uptime"))
      assertEquals(tsColumns.length, xformed.columns.count(_.startsWith("ts_")))
    }
  }

  "renameColumns transformer" should {
    "rename columns" in {
      val df = spark.read.parquet("src/test/resources/data/frontier/deviceBW")
      val currColumns = df.columns.filter(_.endsWith("_current"))
      assertFalse(currColumns.isEmpty)
      assertTrue(df.columns.contains("uptime"))
      val transformer = Transformer(Map("name" -> "dummy"))
      val conf = Map("*_current" -> "_now", "uptime" -> "uptimeSince")
      val xformed = transformer.renameColumns(df, conf)
      assertEquals(currColumns.length, xformed.columns.count(_.endsWith("_now")))
      assertFalse(xformed.columns.contains("uptime"))
      assertTrue(xformed.columns.contains("uptimeSince"))
    }
  }

  "castColumns transformer" should {
    "change the column type" in {
      val df = spark.read.parquet("src/test/resources/data/frontier/gold/element-ts").
        select(explode(col("wifi_clients"))).select("col.*").drop("wifi_clients", "col")
      val f = df.schema.fields.filter(_.name.equals("lastTransmitRate")).head
      assertEquals("string", f.dataType.typeName)
      val transformer = Transformer(Map("name" -> "dummy"))
      val conf = Map("lastTransmitRate" -> "long")
      val xformed = transformer.castColumns(df, conf)
      val xf = xformed.schema.fields.filter(_.name.equals("lastTransmitRate")).head
      assertEquals("long", xf.dataType.typeName)
    }
  }

  "groupBy transformer" should {
    "aggregate columns" in {
      val df = spark.read.parquet("src/test/resources/data/frontier/deviceBW")
      val currColumns = df.columns.filter(_.endsWith("_current"))
      assertFalse(currColumns.isEmpty)
      assertTrue(df.columns.contains("uptime"))
      val transformer = Transformer(Map("name" -> "dummy"))
      val conf = Map(
        "columns" -> Seq("device_id", "ts_year", "ts_month", "ts_day"),
        "aggregate" -> Seq("bytes_received", "bytes_sent"),
        "stats" -> Seq("count", "mean", "stddev", "min", "max", "sum")
      )
      val xformed = transformer.applyGroupBy(df, conf)
      validateGroupBy(xformed, conf)
    }
  }

  "joinDataFrame transformer" should {
    "join dataframes" in {
      val df = spark.read.parquet("src/test/resources/data/frontier/joinTest/ets")
      val origColumns = Seq[String]("device_id","ts","uptime")
      assertEquals("da025cad91fa11e9ba8b0050569979b6", df.select("device_id").distinct().head(1)(0)(0))
      val transformer = Transformer(Map("name" -> "dummy"))
      val conf = Map(
        "location" -> "src/test/resources/data/frontier/joinTest/dev",
        "on" -> Seq("device_id")
      )
      val xformed = transformer.joinDataFrame(df, conf)(spark)
      val joinedColumnsDiff = xformed.columns.diff(origColumns)
      assertEquals(1, joinedColumnsDiff.length)
      assertEquals("device_model", joinedColumnsDiff(0))
      assertEquals(df.count(), xformed.count())
      assertEquals("da025cad91fa11e9ba8b0050569979b6", xformed.select("device_id").distinct().head(1)(0)(0))
      assertEquals("NVG468MQ", xformed.select("device_model").distinct().head(1)(0)(0))
    }
  }

  def validateGroupBy(df: DataFrame, conf: Map[String, Seq[String]], withSink: Boolean = false): Unit = {
    // sink adds a column ts_processed
    val expectedColumnLength = conf("columns").length + (conf("stats").length * conf("aggregate").length) + (if (withSink) 1 else 0)

    assertEquals(expectedColumnLength, df.columns.length)
    logger.debug("{}", df.schema.treeString)
    conf("columns").foreach(c => assertTrue(df.columns.contains(c)))
    conf("aggregate")
      .foreach(a => conf("stats")
        .foreach(s => assertTrue(df.columns.contains(s"${a}_$s"))))

    assertEquals(expectedColumnLength, df.take(1).head.length)
  }

  "single pipeline" when {
    "single processor noModel noSink" should {
      implicit val appConfig = AppConfig("config/pipeline/noModel-noSink.json")

      "be parsed" in {
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        pipelines.foreach(logger.debug("{}", _))
        assertEquals(1, pipelines.length)
        assertEquals(1, pipelines.head.processors.length)
        assertEquals("deviceBW", pipelines.head.processors.head.name)
        assertEquals(3, pipelines.head.processors.head.transformers.length)
        assertNotNull(pipelines.head.processors.head.transformers.foreach(_.name))
        assertTrue(pipelines.head.processors.head.sinks.isEmpty)
      }

      "transform dataframe" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        val processor = pipelines.head.processors.head
        val transformed = processor.transform(df)
        validateTransformed(transformed)
      }

      "process dataframe" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)

        val processed = pipelines.head.processors.head.process(df)
        validateTransformed(processed._1)
        assertTrue(processed._2.isEmpty)
      }
    }

    "single processor noSink" should {
      implicit val appConfig = AppConfig("config/pipeline/singleProcessor-noSink.json")

      "be parsed" in {
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        pipelines.foreach(logger.debug("{}", _))
        assertEquals(1, pipelines.length)
        assertEquals(1, pipelines.head.processors.length)
        assertEquals("deviceBW", pipelines.head.processors.head.name)
        assertEquals(4, pipelines.head.processors.head.transformers.length)
        assertNotNull(pipelines.head.processors.head.transformers.foreach(_.name))
        assertTrue(pipelines.head.processors.head.sinks.isEmpty)
      }

      "transform dataframe" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        val processor = pipelines.head.processors.head
        val transformed = processor.transform(df)
        validateTransformed(transformed)
      }

      "process dataframe" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)

        val processed = pipelines.head.processors.head.process(df)
        validateTransformed(processed._1)
        assertTrue(processed._2.isEmpty)
      }
    }

    "single processor with Sink" should {
      implicit val appConfig = AppConfig("config/pipeline/singleProcessor-withSink.json")

      "be parsed" in {
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        pipelines.foreach(logger.debug("{}", _))
        assertEquals(1, pipelines.length)
        assertEquals(1, pipelines.head.processors.length)
        assertEquals("deviceBW", pipelines.head.processors.head.name)
        assertEquals(3, pipelines.head.processors.head.transformers.length)
        assertNotNull(pipelines.head.processors.head.transformers.foreach(_.name))
        assertEquals(1, pipelines.head.processors.head.sinks.length)
        assertEquals("tmp/frontier/warehouse/ets-pipeline", pipelines.head.processors.head.sinks.head.location)
        val saveConfig = SaveConfig(Format.parquet, SaveMode.Overwrite, Seq("ts_year", "ts_month", "ts_day"))
        assertEquals(saveConfig, pipelines.head.processors.head.sinks.head.saveConfig)
      }

      "process dataframe" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)

        val processed = pipelines.head.processors.head.process(df)
        validateTransformed(processed._1)
        assertNotNull(processed._2.head)
        val path = processed._2.head
        validateTransformed(spark.read.parquet(path), withSink = true)
      }
    }

    "single processor with multiple sinks" should {
      implicit val appConfig = AppConfig("config/pipeline/singleProcessor-multiSink.json")

      "be parsed" in {
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        pipelines.foreach(logger.debug("{}", _))
        assertEquals(1, pipelines.length)
        assertEquals(1, pipelines.head.processors.length)
        assertEquals("deviceBW", pipelines.head.processors.head.name)
        assertEquals(3, pipelines.head.processors.head.transformers.length)
        assertNotNull(pipelines.head.processors.head.transformers.foreach(_.name))
        assertEquals(2, pipelines.head.processors.head.sinks.length)
        assertEquals("tmp/frontier/warehouse/ets-parquet", pipelines.head.processors.head.sinks.head.location)
        assertEquals("tmp/frontier/warehouse/ets-csv", pipelines.head.processors.head.sinks.tail.head.location)
        val saveConfig = SaveConfig(Format.parquet, SaveMode.Overwrite, Seq("ts_year", "ts_month", "ts_day"))
        assertEquals(saveConfig, pipelines.head.processors.head.sinks.head.saveConfig)
      }

      "multi-persist dataframe" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)

        val processed = pipelines.head.processors.head.process(df)
        validateTransformed(processed._1)
        assertNotNull(processed._2.head)
        val path1 = processed._2.head
        validateTransformed(spark.read.parquet(path1), withSink = true)
        val path2 = processed._2.tail.head
        validateTransformed(spark.read.option("header", "true").csv(path2), withSink = true)
      }
    }

    "multi processor noModel noSink" should {
      implicit val appConfig = AppConfig("config/pipeline/multiProcessor-noSink.json")

      "be parsed" in {
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        pipelines.foreach(logger.debug("{}", _))
        assertEquals(1, pipelines.length)
        assertEquals(2, pipelines.head.processors.length)
        assertEquals("deviceBW", pipelines.head.processors.head.name)
        assertEquals(3, pipelines.head.processors.head.transformers.length)
        assertNotNull(pipelines.head.processors.head.transformers.foreach(_.name))
        assertTrue(pipelines.head.processors.head.sinks.isEmpty)
        assertTrue(pipelines.head.processors.tail.head.sinks.isEmpty)
      }

      "transform dataframe" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        val processor = pipelines.head.processors.head
        val transformed = processor.transform(df)
        validateTransformed(transformed)
      }

      "process dataframe" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)

        val headProcessed = pipelines.head.processors.head.process(df)
        validateTransformed(headProcessed._1)
        assertTrue(headProcessed._2.isEmpty)

        val tailProcessed = pipelines.head.processors.tail.head.process(headProcessed._1)
        logger.debug("{}", tailProcessed._1.schema.treeString)
        val conf = Map(
          "columns" -> Seq("device_id", "ts_year", "ts_month", "ts_day"),
          "aggregate" -> Seq("bytes_received", "bytes_sent", "ethernet_packets_received"),
          "stats" -> Seq("count", "mean", "stddev", "min", "max", "sum")
        )
        validateGroupBy(tailProcessed._1, conf)
      }

      "run pipeline" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty && pipelines.length == 1)

        val finalDF = pipelines.head.run(df)
        val conf = Map(
          "columns" -> Seq("device_id", "ts_year", "ts_month", "ts_day"),
          "aggregate" -> Seq("bytes_received", "bytes_sent", "ethernet_packets_received"),
          "stats" -> Seq("count", "mean", "stddev", "min", "max", "sum")
        )
        validateGroupBy(finalDF, conf)
      }
    }

    "multi processor noModel withSink" should {
      implicit val appConfig = AppConfig("config/pipeline/multiProcessor-withSink.json")

      "be parsed" in {
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty)
        pipelines.foreach(logger.debug("{}", _))
        assertEquals(1, pipelines.length)
        assertEquals(2, pipelines.head.processors.length)
        assertEquals("deviceBW", pipelines.head.processors.head.name)
        assertEquals(3, pipelines.head.processors.head.transformers.length)
        assertNotNull(pipelines.head.processors.head.transformers.foreach(_.name))
        assertEquals(1, pipelines.head.processors.head.sinks.length)
        assertEquals(1, pipelines.head.processors.tail.head.sinks.length)
      }

      "run pipeline" in {
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals(98, df.columns.length)
        val pipelines = appConfig.pipelines
        assertTrue(pipelines != null && pipelines.nonEmpty && pipelines.length == 1)

        val finalDF = pipelines.head.run(df)
        val conf = Map(
          "columns" -> Seq("device_id", "ts_year", "ts_month", "ts_day"),
          "aggregate" -> Seq("bytes_received", "bytes_sent", "ethernet_packets_received"),
          "stats" -> Seq("count", "mean", "stddev", "min", "max", "sum")
        )
        validateGroupBy(finalDF, conf)

        val firstSinkLocation = pipelines.head.processors.head.sinks.head.location
        val secondSinkLocation = pipelines.head.processors.tail.head.sinks.head.location

        val firstSinkDF = spark.read.parquet(firstSinkLocation)
        val secondSinkDF = spark.read.parquet(secondSinkLocation)
        validateGroupBy(secondSinkDF, conf, withSink = true)
      }
    }

    "pipeline with join" should {
      "join" in {
        implicit val appConfig = AppConfig("config/pipeline/joinDataFrame.json")
        val df = spark.read.parquet(s"${appConfig.inputLocation}")
        assertEquals("da025cad91fa11e9ba8b0050569979b6", df.select("device_id").distinct().head(1)(0)(0))

        val origColumns = df.columns
        val xformed = appConfig.pipelines.head.run(df)

        val joinedColumnsDiff = xformed.columns.diff(origColumns)
        assertEquals(1, joinedColumnsDiff.length)
        assertEquals("device_model", joinedColumnsDiff(0))
        assertEquals(df.count(), xformed.count())
        assertEquals("da025cad91fa11e9ba8b0050569979b6", xformed.select("device_id").distinct().head(1)(0)(0))
        assertEquals("NVG468MQ", xformed.select("device_model").distinct().head(1)(0)(0))
      }
    }
  }

  def validateTransformed(transformed: DataFrame, withSink: Boolean = false): Unit = {
    assertEquals(51 + (if (withSink) 1 else 0), transformed.columns.length)

    assertTrue(transformed.columns.contains("device_id"))
    assertTrue(transformed.columns.contains("bytes_received"))
    assertFalse("dropTransformation should drop it", transformed.columns.contains("bytes_received_prev"))
    assertTrue(transformed.columns.contains("bytes_received_current"))
    assertTrue(transformed.columns.contains("packets_received"))
    assertFalse("dropTransformation should drop it", transformed.columns.contains("packets_received_prev"))
    assertTrue(transformed.columns.contains("packets_received_current"))

    assertTrue(transformed.columns.contains("ethernet_bytes_received"))
    assertFalse("dropTransformation should drop it", transformed.columns.contains("ethernet_packets_received_prev"))
    assertTrue(transformed.columns.contains("ethernet_packets_received_current"))
  }
}
