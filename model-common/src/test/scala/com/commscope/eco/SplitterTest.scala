package com.commscope.eco

import com.typesafe.scalalogging.Logger
import junit.framework.Assert
import org.junit.Assert.{assertEquals, assertTrue}

class SplitterTest extends SparkBasedTest {
  val logger = Logger(getClass)

  "empty splitter" in {
    val splitter = Splitter(Map.empty[String, Any])
    logger.info("{}", splitter)
    Assert.assertFalse(splitter.sorted)
    Assert.assertFalse(splitter.cache)
    Assert.assertEquals("asc", splitter.sortOrder)
    Assert.assertEquals(-1, splitter.partitionSize)
  }

  "byColumnValue splitter" when {
    "default values" should {
      val splitter = Splitter(Map[String, Any]())
      Assert.assertTrue(splitter.splitOn.isEmpty)
      Assert.assertFalse(splitter.sorted)
      Assert.assertFalse(splitter.cache)
      Assert.assertEquals("asc", splitter.sortOrder)
      Assert.assertEquals(-1, splitter.partitionSize)
    }

    "default sort order by orderByColumn" should {
      val splitter = Splitter(Map[String, Any]("order_by" -> "ts"))
      Assert.assertTrue(splitter.splitOn.isEmpty)
      Assert.assertFalse(splitter.cache)
      Assert.assertTrue(splitter.sorted)
      Assert.assertEquals("ts", splitter.orderByColumn.get)
      Assert.assertEquals("asc", splitter.sortOrder)
      Assert.assertEquals(-1, splitter.partitionSize)
    }

    "asc sort order by orderByColumn and columnName" should {
      val splitOn = Seq(Map("column_name" -> "ts_date", "numeric" -> false), Map("column_name" -> "ts_hour", "numeric" -> true))
      val expectedSplitOn = Seq(SplitColumn("ts_date"), SplitColumn("ts_hour", numeric = true))
      val splitter = Splitter(Map[String, Any]("order_by" -> "ts", "split_on" -> splitOn))
      Assert.assertEquals(expectedSplitOn, splitter.splitOn.asInstanceOf[Seq[SplitColumn]])
      Assert.assertTrue(splitter.sorted)
      Assert.assertEquals("ts", splitter.orderByColumn.get)
      Assert.assertEquals("asc", splitter.sortOrder)
      Assert.assertEquals(-1, splitter.partitionSize)
    }

    "cache true split df" in {
      val splitter = Splitter(Map[String, Any]("cache" -> true))
      Assert.assertTrue(splitter.cache)
    }

    "cache false split df" in {
      val splitter = Splitter(Map[String, Any]("cache" -> false))
      Assert.assertFalse(splitter.cache)
    }
  }

  "dataframe" when {
    val appConfig = AppConfig("config/application-with-splitter.json")
    val splitter = appConfig.splitter.get
    Assert.assertFalse(splitter.cache)
    Assert.assertEquals(-1, splitter.partitionSize)
    val df = spark.read.parquet(appConfig.inputLocation)
    val columnValueCounts = splitter.distinctColumnValuesToSplitOn(df)
    val groupByCols = splitter.splitOn.map(_.name)
    val expectedDistinctVal = df.select(groupByCols.head, groupByCols.tail: _*).distinct().collect().toSeq

    "split on distinctColumnValues" in {
      expectedDistinctVal.indices.foreach(i => {
        logger.debug("distinctVals: {} columnValCounts: {}", expectedDistinctVal(i), columnValueCounts(i))
        columnValueCounts(i).foreach(cv => assertEquals(expectedDistinctVal(i)(cv.index), cv.value.get))
      })
    }

    "expression for splitOnFilter" in {
      val expressions = splitter.splitOnFilterExpressions(columnValueCounts)
      logger.info("expr: {}", expressions)
      assertEquals(expectedDistinctVal.length, expressions.length)
      expectedDistinctVal.indices.foreach(i => {
        val distinctValues = expectedDistinctVal(i)
        val expectedExpr = s"ts_date = '${distinctValues(0)}' and ts_hour = ${distinctValues(1)}"
        assertEquals(expectedExpr, expressions(i))
      })
    }

    "split dataframe" in {
      val appConfig = AppConfig("config/application-with-splitter.json")
      val splitter = appConfig.splitter.get

      val expectedSplitOn = Seq(SplitColumn("ts_date"), SplitColumn("ts_hour", numeric = true))
      assertEquals(expectedSplitOn, splitter.splitOn)

      val df = spark.read.parquet(appConfig.inputLocation)
      val rowCount = df.count()
      val colNames = expectedSplitOn.map(_.name)

      val distDaysHours = df.select(colNames.head, colNames.tail: _*).distinct().collect()

      assertEquals(2, distDaysHours.length)
      val dfItr = splitter.split(df)
      assertEquals(distDaysHours.length, dfItr.size)
      logger.info("splitterExpr: {}", dfItr.map(_.expression))
      val splitRowCount = dfItr.map(d => d.dataFrame.count()).sum
      assertEquals(rowCount, splitRowCount)

      //The default partition number is 2, equal to number of local[2]
      val defaultPartition = 2

      val result = dfItr.foldLeft(Seq[Any]())((s, df) => {
        assertEquals(defaultPartition, df.dataFrame.rdd.getNumPartitions)
        s :+ df.dataFrame.count()
      })
      logger.info("result: {}", result)

      val expectedCountByDaysHours = df.groupBy(colNames.head, colNames.tail :_*)
        .count().collect().map(_(2))

      logger.info("{}", expectedCountByDaysHours)
      assertTrue(expectedCountByDaysHours.diff(result).isEmpty)
    }

    "partition split dataframe" in {
      val appConfig = AppConfig("config/application-with-splitter-partitioned.json")
      val splitter = appConfig.splitter.get
      Assert.assertEquals(16, splitter.partitionSize)
      Assert.assertFalse(splitter.cache)

      val expectedSplitOn = Seq(SplitColumn("ts_date"), SplitColumn("ts_hour", numeric = true))
      assertEquals(expectedSplitOn, splitter.splitOn)

      val df = spark.read.parquet(appConfig.inputLocation)
      val rowCount = df.count()
      val colNames = expectedSplitOn.map(_.name)

      val distDaysHours = df.select(colNames.head, colNames.tail: _*).distinct().collect()

      assertEquals(2, distDaysHours.length)
      val dfItr = splitter.split(df)
      assertEquals(distDaysHours.length, dfItr.size)
      logger.info("splitterExpr: {}", dfItr.map(_.expression))
      val splitRowCount = dfItr.map(d => d.dataFrame.count()).sum
      assertEquals(rowCount, splitRowCount)

      val result = dfItr.foldLeft(Seq[Any]())((s, df) => {
        logger.info("partition size: {}", df.dataFrame.rdd.getNumPartitions)
        assertEquals(splitter.partitionSize, df.dataFrame.rdd.getNumPartitions)
        s :+ df.dataFrame.count()
      })
      logger.info("result: {}", result)

      val expectedCountByDaysHours = df.groupBy(colNames.head, colNames.tail :_*)
        .count().collect().map(_(2))

      logger.info("{}", expectedCountByDaysHours)
      assertTrue(expectedCountByDaysHours.diff(result).isEmpty)
    }
  }

  "cache dataframe" in {
    val appConfig = AppConfig("config/application-with-splitter-cache.json")
    val splitter = appConfig.splitter.get
    Assert.assertTrue(splitter.cache)
  }
}
