package com.commscope.eco

import java.io.InputStream

import com.commscope.eco.SchemaUtils.logger
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.typesafe.scalalogging.Logger
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.types.DataTypes._
import org.apache.spark.sql.types.{ArrayType, DataType, StructField, StructType}
import org.apache.spark.sql.{Column}

import scala.collection.mutable
import scala.io.Source


case class EntitySchema(input: StructType, var output: StructType) {
  if (output.isEmpty && !input.isEmpty) output = input
}

case class ColumnFilter(colNames: Seq[String]) {
  val ignoreMap = colNames.map(c => {
    val idx = c.indexOf('*')
    if (idx < 0) (c, StringUtils.EMPTY) else (c.substring(0, idx), c.substring(idx + 1))
  }).toMap

  def filter(sf: StructField, prefix: String = ""): Option[StructField] = {
    val name = if (prefix.isEmpty) sf.name else s"$prefix.${sf.name}"

    if (ignoreMap.contains(name) ||
      ignoreMap.exists(e => StringUtils.isNotBlank(e._2) && name.startsWith(e._1) && name.endsWith(e._2))) {
      logger.info("*********** ignoring: {}", name)
      return None
    }

    logger.debug("name: {}({})", name, sf.dataType.typeName)

    sf.dataType match {
      case st: StructType =>
        Some(StructField(sf.name, createStructType(st.fields.flatMap(this.filter(_, name)))))
      case at: ArrayType =>
        at.elementType match {
          case stAt: StructType =>
            val _stAt = createStructType(stAt.fields.flatMap(this.filter(_, name)))
            Some(StructField(sf.name, ArrayType(_stAt), sf.nullable))
          case _ => Some(StructField(sf.name, sf.dataType, sf.nullable))
        }
      case _ => Some(StructField(sf.name, sf.dataType, sf.nullable))
    }
  }
}

object SchemaUtils {
  val logger = Logger(getClass)
  val objectMapper = new ObjectMapper()
  objectMapper.registerModule(DefaultScalaModule)
  val illegalCharsRegex = "[ ,;{}()\\n\\t=]+".r()


  implicit def toStruct(jsonString: String): StructType = {
    jsonString match {
      case s if s != "{}" => DataType.fromJson(jsonString).asInstanceOf[StructType]
      case _ => new StructType()
    }
  }

  def toStruct(map: Map[String, Any]): Option[StructType] = {
    if (map != null && !map.isEmpty) {
      Some(objectMapper.writeValueAsString(map))
    }
    else None
  }

  def loadDataFrameSchema(name: String): Option[StructType] = {
    if (StringUtils.isNotBlank(name)) {
      val json = loadJson(buildSchemaFileName(name))
      if (json.isDefined) {
        return Some(toStruct(json.get.asInstanceOf[String]))
      }
    }
    None
  }

  def loadEntitySchema(name: String): Option[EntitySchema] = {
    if (StringUtils.isNotBlank(name)) {
      val schemaFileName: String = buildSchemaFileName(name)
      val json = loadJson(schemaFileName, deserialize = true)
      if (json.isDefined) {
        val schema = json.get.asInstanceOf[Map[String, Any]]
        val input = objectMapper.writeValueAsString(schema.get("input"))
        val output = objectMapper.writeValueAsString(schema.get("output"))

        val entitySchema = EntitySchema(input, output)
        logger.debug(entitySchema.toString)
        return Some(entitySchema)
      }
    }
    None
  }

  /** Removes illegal chars from schema and replaces them with _ so that it can be saved as parquet
   *
   * @param schema StructType to be sanitized
   * @return Some datatype if the datatype was modified else None
   */
  def sanitize(schema: StructType): Option[DataType] = SchemaUtils.processType(schema)

  /** Flattens the schema (converts all the structs to columns). It also sanitizes the column names:
   * replace all the chars not allowed by parquet by _
   * de-duplicates the column names (if there is a collision the column names will be suffixed by _count
   *
   * @param schema
   * @param sanitizeAfterFlattening
   * @return
   */
  def flatten(schema: Option[StructType], sanitizeAfterFlattening: Boolean = false): Option[Array[Column]] = {
    if (schema.isDefined) {
      val flattened = _flatten(schema.get)
      if (!sanitizeAfterFlattening) Some(flattened)
      else {
        val columnNames = mutable.Map[String, Int]()

        Some(
          flattened.map(c => {
            val sanitized = illegalCharsRegex.replaceAllIn(c.toString(), "_")
            val dupRemoved = columnNames.get(sanitized.toLowerCase()) match {
              case Some(i) =>
                columnNames += (sanitized.toLowerCase -> (i + 1))
                s"${sanitized}_$i"
              case _ =>
                columnNames += (sanitized.toLowerCase -> 1)
                sanitized
            }
            c.name(dupRemoved)
          })
        )
      }
    } else None
  }

  def filter(schema: StructType, ignoreColumns: Seq[String] = Seq.empty): Option[StructType] = {
    if (ignoreColumns.isEmpty) return None

    val columnFilter = ColumnFilter(ignoreColumns)

    Some(createStructType(schema.fields.flatMap(columnFilter.filter(_, StringUtils.EMPTY))))
  }

  private def _flatten(schema: StructType, prefix: String = null, delimiter: String = "."): Array[Column] = {
    if (schema == null) return null
    schema.fields.flatMap(f => {
      val colName = if (prefix == null) f.name else (prefix + delimiter + f.name)
      f.dataType match {
        case st: StructType => _flatten(st, colName, delimiter)
        case _ => Array(col(colName))
      }
    })
  }

  private def loadJson(path: String, deserialize: Boolean = false): Option[Any] = {
    getClass.getClassLoader.getResourceAsStream(path) match {
      case in: InputStream =>
        logger.debug("found the schema: {}", in)
        try {
          if (deserialize) {
            Some(objectMapper.readValue(in, classOf[Map[String, Any]]))
          } else Some(Source.fromInputStream(in).mkString)
        }
        catch {
          case e: Exception => logger.warn("Error parsing schema: {}\n{}"
            , e.getMessage, getClass.getClassLoader.getResource(path).getPath, e)
            None
        }
      case _ => logger.warn("No schema found at location {}", path)
        None
    }
  }

  private def buildSchemaFileName(name: String) = {
    val modified = if (!name.startsWith("schema/"))
      "schema/" + name
    else name

    if (!modified.endsWith(".json"))
      modified + ".json"
    else modified
  }

  /**
   * Walks the tree and if none of the children has illegal chars then returns None else returns the modified
   * datatype.
   *
   * @param dataType
   * @return
   */
  private def processType(dataType: DataType): Option[DataType] = {
    logger.trace(dataType.typeName)
    dataType match {
      case st: StructType =>
        /* create a tuple of original datatype and Option(modified datatype), the option will be None if the underlying
        * datatype had no illegal chars
        * */
        val res = st.fields.map(f => (f, processStructField(f)))

        if (res.exists(_._2.isDefined)) {
          val sfList = res.foldLeft(List[StructField]())((l, e) => if (e._2.isDefined) l :+ e._2.get else l :+ e._1)
          Some(StructType(sfList))
        } else None
      case _ => None
    }
  }

  private def processStructField(sf: StructField): Option[StructField] = {
    logger.trace("processing {}", sf.name)
    val res = sf.dataType match {
      case st: StructType => toStructField(sf.name, processType(st))
      case at: ArrayType if at.elementType.isInstanceOf[StructType] =>
        val res = processType(at.elementType.asInstanceOf[StructType])
        if (res.isDefined) Some(StructField(sf.name, ArrayType(res.get)))
        else None
      case _ => illegalCharsRegex.replaceAllIn(sf.name, "_") match {
        case s: String => if (s.equals(sf.name)) None else Some(StructField(s, sf.dataType, sf.nullable))
        case _ => None
      }
    }
    logger.trace("finished processing {}", sf.name)
    res
  }

  private def toStructField(fieldName: String, dt: Option[DataType]): Option[StructField] = {
    if (dt.isDefined) {
      Some(StructField(illegalCharsRegex.replaceAllIn(fieldName, "_"), dt.get))
    } else None
  }
}