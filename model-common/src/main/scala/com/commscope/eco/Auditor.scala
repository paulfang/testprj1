package com.commscope.eco

import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.IntegerType
import org.apache.spark.sql.{DataFrame, SparkSession, functions}

import java.util.Calendar

object Auditor {
  def audit(name:String, df:DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession): DataFrame = {
    val tsColName = appConfig.getValueAsString("primary_timestamp_column","ts")
    val minMaxTs = df.select(functions.min(tsColName), functions.max(tsColName)).head(1)(0).toSeq
    val rowCount = df.count()
    val idColumn = appConfig.auditor.getOrElse("id_column", "id").asInstanceOf[String]
    val distinctIds = df.select(idColumn).distinct().count()
    val data = List(
      (
        name,
        rowCount,
        new java.sql.Date(appConfig.now),
        minMaxTs(0).asInstanceOf[Long],
        minMaxTs(1).asInstanceOf[Long],
        distinctIds
      )
    )

    sparkSession.createDataFrame(data)
      .toDF("name", "row_count", "processed_dt", "min_ts","max_ts","distinct_ids").repartition(1)
  }

//  val getInformTs = udf((ts: Long) => {
//    val c = Calendar.getInstance()
//    c.setTimeInMillis(ts)
//    c.set(Calendar.SECOND,0)
//    c.set(Calendar.MILLISECOND,0)
//    c.set(Calendar.MINUTE, 15 * ((c.get(Calendar.MINUTE)/15) +1))
//    c.getTimeInMillis
//  })

  def auditInforms(name:String, frame: DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession): Option[DataFrame] = {
    if (frame.columns.contains("ts_inform")) {
      val tsColName = appConfig.getValueAsString("primary_timestamp_column","ts")

      //val df = frame.withColumn("inform_ts", getInformTs(col(tsColName)))

      val df = frame
        .withColumn("ts_datetime", from_unixtime(col(tsColName)/1000))
        .withColumn("inform_ts", unix_timestamp(date_trunc("Hour", col("ts_datetime"))) + (col("ts_inform")%4 *15*60).cast(IntegerType))
        .withColumn("inform_ts", col("inform_ts")*1000)
        .drop("ts_datetime")

      val idColumn = appConfig.auditor.getOrElse("id_column", "id").asInstanceOf[String]

      val rowCountByInform = df.groupBy("ts_date", "ts_inform", "inform_ts").count()
        .withColumnRenamed("count", "row_count")

      val distinctIdsByInform = df.select(idColumn, "ts_date", "ts_inform", "inform_ts")
        .distinct()
        .groupBy("ts_date","ts_inform", "inform_ts")
        .count().withColumnRenamed("count", "distinct_ids")

      Some(rowCountByInform.join(distinctIdsByInform, usingColumns = Seq("ts_date","ts_inform","inform_ts"))
        .withColumn("name", lit(name))
        .withColumn("processed_dt", to_date(from_unixtime(lit(appConfig.now/1000)))).repartition(1))
    }
    else None
  }
}