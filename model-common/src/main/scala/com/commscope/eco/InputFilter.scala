package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.functions.{max, col}
import org.apache.spark.sql.{DataFrame, SparkSession}

trait InputFilter {
  def filter(inputDf: DataFrame, config: AppConfig)(implicit sparkSession: SparkSession): DataFrame
}

object DefaultInputFilter$ extends InputFilter {
  def filter(inputDf: DataFrame, config: AppConfig)(implicit sparkSession: SparkSession): DataFrame = {
    inputDf
  }
}

object LastProcessingBasedInputFilter extends InputFilter {
  val logger: Logger = Logger(this.getClass)
  val defTimeCol = "ts_processed"

  override def filter(inputDf: DataFrame, config: AppConfig)(implicit sparkSession: SparkSession): DataFrame =
    try {
      val prevDf = sparkSession.read.format(config.outputFormat.toString).load(config.dataframeOutputLocation)
      val inputColumn = config.inputFilter.getOrElse("args", Map.empty).asInstanceOf[Map[String,String]].getOrElse("input_column", defTimeCol)
      val outputColumn = config.inputFilter.getOrElse("args", Map.empty).asInstanceOf[Map[String,String]].getOrElse("output_column", defTimeCol)

      logger.info("selecting based on input column: {} and output column: {}", inputColumn, outputColumn)

      if (!prevDf.columns.contains(outputColumn) || !inputDf.columns.contains(inputColumn)) {
        logger.info("missing: {} from inputDf or {} from outputDf", inputColumn, outputColumn)
        inputDf
      }
      else {
        val maxProcessedTime = prevDf.agg(max("ts_processed")).take(1).head(0)
        logger.info("output column {} max value: {}", outputColumn, maxProcessedTime)

        inputDf.filter(s"$inputColumn > $maxProcessedTime")
      }
    }
    catch {
      case t: Throwable => logger.warn("Exception while loading previously prcessed data from {}: {}", config.dataframeOutputLocation, t.getMessage)
        inputDf
    }
}

object ExprBasedInputFilter extends InputFilter {
  override def filter(inputDf: DataFrame, config: AppConfig)(implicit sparkSession: SparkSession): DataFrame = {
    if (config.inputFilter.contains("args")) {
      val exprs = config.inputFilter("args").asInstanceOf[Map[String,List[String]]].getOrElse("expr", List.empty)
      exprs.foldLeft(inputDf)((df, e) => df.filter(e))
    }
    else inputDf
  }
}

object SimpleInputFilter extends InputFilter {
  override def filter(inputDf: DataFrame, config: AppConfig)(implicit sparkSession: SparkSession): DataFrame = {
    if (config.inputFilter.contains("args")) {
      val expr = config.inputFilter("args").asInstanceOf[String]
      inputDf.filter(expr)
    }
    else inputDf
  }
}


