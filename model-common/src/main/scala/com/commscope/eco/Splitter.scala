package com.commscope.eco

import org.apache.spark.sql.{DataFrame, Row}

case class SplitColumn(name: String, numeric: Boolean = false)

case class Splitter(conf: Map[String, Any]) {
  val splitOn: Seq[SplitColumn] =
    conf.getOrElse("split_on", Seq.empty).asInstanceOf[Seq[Map[String, Any]]].distinct
      .foldLeft(Seq[SplitColumn]())((s, e) =>
        s :+ SplitColumn(e("column_name").toString, e.getOrElse("numeric", false).asInstanceOf[Boolean]))

  val orderByColumn = if (conf.contains("order_by")) conf.get("order_by") else None

  val cache = if (conf.contains("cache")) conf("cache").asInstanceOf[Boolean] else false

  val sorted = orderByColumn.isDefined

  val partitionSize = conf.getOrElse("partition_size", -1).asInstanceOf[Int]

  val sortOrder = {
    if (conf.contains("asc")) {
      if (!conf("asc").asInstanceOf[Boolean]) "desc" else "asc"
    }
    else if (conf.contains("desc")) {
      if (conf("desc").asInstanceOf[Boolean]) "desc" else "asc"
    }
    else "asc"
  }

  val auditOutputLocation = conf.get("audit_output_location")

  val auditEnabled = auditOutputLocation.isDefined && conf.getOrElse("audit_enabled", false).asInstanceOf[Boolean]

  def distinctColumnValuesToSplitOn(df: DataFrame): Seq[Seq[ColumnValue]] = {
    val dfCols = df.columns

    val validColumns = splitOn.filter(c => dfCols.contains(c.name))

    val indexedColumnValues = validColumns.indices
      .foldLeft(Seq[ColumnValue]())((s, i) => {
        val e = validColumns(i)
        s :+ ColumnValue(i, e.name, e.numeric)
      })

    val groupByCols = indexedColumnValues.map(e => e.name)

    df.select(groupByCols.head, groupByCols.tail: _*).distinct().collect()
      .foldLeft(Seq[Seq[ColumnValue]]())((s, r) => s :+ createValueCounts(indexedColumnValues, r))
  }

  def splitOnFilterExpressions(colValueSeq: Seq[Seq[ColumnValue]]): Seq[String] =
    colValueSeq.foldLeft(Seq[String]())((fullExpr, cols) => {
      fullExpr :+ cols.foldLeft(Seq[String]())((cvExpr, cv) =>
        cvExpr :+ {
          if (cv.numeric) s"${cv.name} = ${cv.value.get}" else s"${cv.name} = '${cv.value.get}'"
        }
      ).mkString(" and ")
    })


  private[Splitter] def createValueCounts(columns: Seq[ColumnValue], row: Row): Seq[ColumnValue] = {
    columns.foldLeft(Seq[ColumnValue]())((s, c) => s :+ ColumnValue(c.index, c.name, c.numeric, Some(row.get(c.index))))
  }

  /**
   * Uses repartitioning the split dataframe, uses hourly partition size as default
   * @param df
   * @return
   */
  def split(df: DataFrame): Iterable[SplitResult] = {
    if (splitOn.nonEmpty) {
      if (partitionSize == -1) {
        splitOnFilterExpressions(distinctColumnValuesToSplitOn(df))
          .foldLeft(Seq[SplitResult]())((s, e) => s :+ SplitResult(e, df.filter(e)))
      }
      else {
        splitOnFilterExpressions(distinctColumnValuesToSplitOn(df))
          .foldLeft(Seq[SplitResult]())((s, e) => s :+ SplitResult(e, df.filter(e).repartition(partitionSize)))
      }
    } else
      Seq(SplitResult("", df))
  }
}

case class SplitResult(expression:String, dataFrame: DataFrame)

case class ColumnValue(index: Int, name: String, numeric: Boolean, var value: Option[Any] = None)

