package com.commscope.eco

import com.commscope.eco.Format.Format
import com.commscope.eco.model.DataModel
import com.typesafe.scalalogging.Logger
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions.{asc, coalesce, col, desc, explode, expr, lag, _}
import org.apache.spark.sql.types.{StructType, TimestampType}
import org.apache.spark.sql.{Column, DataFrame, SaveMode, SparkSession}

import scala.collection.mutable
import scala.reflect.runtime.universe
import scala.util.Try

case class SaveConfig(format: Format,
                      saveMode: SaveMode = SaveMode.Append,
                      partitions: Seq[String] = Seq(),
                      options: Map[String, String] = Map.empty)

trait TPipeline {
  val processors: Seq[TProcessor]

  def run(dataFrame: DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession): DataFrame = {
    processors.foldLeft(dataFrame)((df, p) => p.process(df)._1)
  }
}

trait TProcessor {
  val name: String
  val transformers: Seq[Transformer]
  val sinks: Seq[TSink]

  def process(frame: DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession): (DataFrame, Seq[String]) = {
    persist(transformers.foldLeft(frame)((df, t) => t.transform(df)))
  }

  private[eco] def transform(frame: DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession): DataFrame = {
    transformers.foldLeft(frame)((df, t) => t.transform(df))
  }

  private[eco] def persist(frame: DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession) =
    (frame, sinks.foldLeft(Seq[String]())((r,s) => r :+ s.persist(frame)._2.get))
}

trait TTransformer {
  def transform(frame: DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession): DataFrame = frame
}

trait TSink {
  val saveConfig: SaveConfig
  val location: String

  def persist(frame: DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession) = {
    frame.withColumn("ts_processed", lit(appConfig.now))
      .write.format(saveConfig.format.toString)
      .mode(saveConfig.saveMode.name())
      .options(saveConfig.options)
      .partitionBy(saveConfig.partitions: _*)
      .save(location)

    (frame, Some(location))
  }
}

case class Pipeline(config: Map[String, Any]) extends TPipeline {
  override val processors =
    if (config.contains("processors"))
      config("processors").asInstanceOf[Seq[Map[String, Any]]]
        .foldLeft(Seq[Processor]())((s, p) => s :+ Processor(p))
    else Seq.empty
}

case class Processor(config: Map[String, Any]) extends TProcessor {
  override val transformers =
    if (config.contains("transformers"))
      config("transformers").asInstanceOf[Seq[Map[String, Any]]]
        .foldLeft(Seq[Transformer]())((s, p) => s :+ Transformer(p))
    else Seq.empty

  override val sinks: Seq[TSink] =
    if (config.contains("sinks"))
      config("sinks").asInstanceOf[Seq[Map[String, Any]]]
        .foldLeft(Seq[Sink]())((s, c) => s :+ Sink(c)) else Seq.empty

  override val name: String = config.getOrElse("name", "Anon").toString
}

case class Transformer(config: Map[String, Any]) extends TTransformer {
  val logger = Logger(getClass)
  val name: String = config("name").toString
  logger.debug("transformer: {}", name)

  override def transform(frame: DataFrame)(implicit appConfig: AppConfig, sparkSession: SparkSession): DataFrame = {
    name match {
      case k@"model" => model(frame, config("args").asInstanceOf[Map[String, Any]])
      case k@"selectColumns" => selectInputColumns(frame, config("args"))
      case k@"filter" => selectInputColumns(frame, config("args"))
      case k@"dropDuplicates" => dropDuplications(frame, config("args"))
      case k@"dropColumns" => dropColumns(frame, config("args"))
      case k@"coalesce" => coalesceValues(frame, config("args"))
      case k@"renameColumns" => renameColumns(frame, config("args"))
      case k@"caseFormat" => caseFormat(frame, config("args"))
      case k@"extractColumns" => extractColumns(frame, config("args"))
      case k@"explodeColumns" => explodeColumns(frame, config("args"))
      case k@"joinDataFrame" => joinDataFrame(frame, config("args").asInstanceOf[Map[String, Any]])(sparkSession)
      case k@"groupBy" => applyGroupBy(frame, config("args"))
      case k@"windowAgg" => windowAgg(frame, config("args"))
      case k@"createTimeDims" => createTimeDims(frame, config("args").asInstanceOf[Map[String, Any]])
      case k@"castColumns" => castColumns(frame, config("args").asInstanceOf[Map[String,String]])
      case _ =>
        logger.warn("Transform not supported: {}", config("name"))
        frame
    }
  }

  def filter(src: DataFrame, expr: Seq[String]): DataFrame = {
    expr.foldLeft(src)((df, e) => df.filter(e))
  }

  def createTimeDims(frame:DataFrame, config:Map[String, Any])(implicit appConfig: AppConfig, sparkSession: SparkSession) = {
    if (config.contains("time_dim_columns")) {
      val timeDimColumns = config("time_dim_columns").asInstanceOf[Seq[String]]
      val primaryTsColumn = config.getOrElse("primary_timestamp_column", "ts").asInstanceOf[String]
      val timestampMultiplier = config.getOrElse("timestamp_multiplier", 1.0).asInstanceOf[Double]
      val prefix = config.getOrElse("time_dim_prefix", "ts_").asInstanceOf[String]
      val tmpTs = "_time_dim_src"
      var _df = frame.withColumn(tmpTs, from_unixtime(col(primaryTsColumn) * timestampMultiplier))
      timeDimColumns.foreach {
        case c@"date" => _df = _df.withColumn(s"$prefix$c", to_date(col(tmpTs)))
        case c@"year" => _df = _df.withColumn(s"$prefix$c", year(col(tmpTs)))
        case c@"quarter" => _df = _df.withColumn(s"$prefix$c", quarter(col(tmpTs)))
        case c@"month" => _df = _df.withColumn(s"$prefix$c", month(col(tmpTs)))
        case c@"day" => _df = _df.withColumn(s"$prefix$c", dayofmonth(col(tmpTs)))
        case c@"day_of_week" => _df = _df.withColumn(s"$prefix$c", dayofweek(col(tmpTs)))
        case c@"hour" => _df = _df.withColumn(s"$prefix$c", hour(col(tmpTs)))
        case c@"inform" => _df = _df
          .withColumn("ts_datetime", col(tmpTs).cast(TimestampType))
          .withColumn(s"$prefix$c", to_inform).drop("ts_datetime")
        case _ => _df
      }
      _df
    }
    else frame
  }

  val to_inform = expr("hour(ts_datetime) % 24 *4 + int(minute(ts_datetime)/15) + 1")

  def model(frame: DataFrame, config: Map[String, Any])(implicit appConfig: AppConfig, sparkSession: SparkSession) = {
    if (config.contains("class")) {
      logger.debug("model enabled, model class: {}", config("class"))
      try {
        val mirror = universe.runtimeMirror(getClass.getClassLoader)
        val module = mirror.staticModule(config("class").toString)
        val obj = mirror.reflectModule(module)
        val dataModel: DataModel = obj.instance.asInstanceOf[DataModel]
        dataModel.model(frame, appConfig, config)
      }
      catch {
        case t: Throwable => logger.warn("Error loading model for className {}: {}", config("class"), t.getMessage, t)
          throw t
      }
    } else frame
  }

  def castColumns(frame:DataFrame, args: Map[String, String]): DataFrame = args.foldLeft(frame)((df, e) => df.withColumn(e._1, col(e._1).cast(e._2)))

  def dropDuplications(frame: DataFrame, args: Any): DataFrame = frame.dropDuplicates(args.asInstanceOf[Seq[String]])

  def dropColumns(frame: DataFrame, args: Any): DataFrame = {
    var _df = frame
    args.asInstanceOf[Map[String, Seq[String]]].foreach(e => {
      if (e._2.nonEmpty) {
        if (e._1.equals("__root__")) {
          val wildCardStart = e._2.filter(_.startsWith("*"))
            .foldLeft(Seq[String]())((s, c) => _df.columns.filter(_.endsWith(c.substring(1)))
              .foldLeft(s)((s, c1) => s :+ c1))

          val wildCardEnd = e._2.filter(_.endsWith("*"))
            .foldLeft(Seq[String]())((s, c) => _df.columns.filter(_.startsWith(c.substring(0, c.length - 1)))
              .foldLeft(s)((s, c1) => s :+ c1))

          val remaining = e._2.filter(!_.contains("*"))
          _df = _df
            .drop(wildCardStart: _*)
            .drop(wildCardEnd: _*)
            .drop(remaining: _*)
        } else if (_df.columns.contains(e._1) && Try(_df.schema.filter(f => f.name.equals(e._1)).head.dataType.asInstanceOf[StructType]).isSuccess)
          _df = _df.select("*", s"${e._1}.*").drop(e._1).drop(e._2: _*)
      }
    })
    logger.debug("dropped columns {}", args)

    _df
  }

  def windowAgg(frame: DataFrame, args: Any): DataFrame = {
    val argsMap = args.asInstanceOf[Map[String, Map[String, Any]]]

    if (!argsMap.contains("window") ||
      !argsMap("window").contains("partitionBy") ||
      !argsMap.contains("function") ||
      !argsMap("function").contains("name")) {
      return frame
    }

    val windowSpec = createWindowSpec(argsMap).get

    argsMap("function")("name").asInstanceOf[String] match {
      case c@"lag" => applyWindowLag(frame, windowSpec, argsMap)
      case _ =>
        logger.warn("windowAgg function not supported: {}", argsMap("function")("name"))
        frame
    }
  }

  def createWindowSpec(args: Map[String, Map[String, Any]]): Option[WindowSpec] = {
    if (!args("window").contains("partitionBy")) return None

    val partitionBy = args("window")("partitionBy").asInstanceOf[Seq[String]]
    val windowSpec = Window.partitionBy(partitionBy.head, partitionBy.tail: _*)

    if (args("window").contains("orderBy")) {
      val orderBys = args("window")("orderBy").asInstanceOf[Seq[Map[String, String]]]
        .foldLeft(Seq[Column]())((s, e) =>
          if (e.head._1.equalsIgnoreCase("desc")) s :+ desc(e.head._2)
          else s :+ asc(e.head._2)
        )

      Some(windowSpec.orderBy(orderBys: _*))
    } else Some(windowSpec)
  }

  def applyWindowLag(frame: DataFrame, windowSpec: WindowSpec, args: Map[String, Map[String, Any]]): DataFrame = {
    var _df = frame
    val offset = args("function").getOrElse("period", 1).asInstanceOf[Int]
    val funOutputPrefix = args("function").getOrElse("output_prefix", "_prev")
    val applyExpr = if (args("function").contains("apply")) {
      val _apply = args("function")("apply").asInstanceOf[Map[String, String]]
      Option(_apply.get("expression"), _apply.getOrElse("output_suffix", "__windowRes"))
    } else None

    args("function")("columns").asInstanceOf[Seq[String]].foreach(c => {
      _df = _df.withColumn(s"$c$funOutputPrefix", lag(c, offset) over windowSpec)
      if (applyExpr.isDefined && applyExpr.get._1.isDefined) {
        val exprStr = applyExpr.get._1.get.replace("$$", s"$c")
        logger.debug("applying expression: {}", exprStr)
        _df = _df.withColumn(s"$c${applyExpr.get._2}", expr(exprStr))
      }
    })
    logger.info("columns after applying lag function: {}", _df.columns)
    _df
  }

  /** Coalesces columns in the given dataframe
   * The key of the Map (args) will be used as the final column name that will hold
   * the result of coalescing the values.
   * The value of the Map contains the columns that will be coalesced. These columns are dropped
   * once coalesced except the key column, if the key column was also part of the coalesced columns
   *
   * @param frame
   * @param args (Map[String, Seq[String]]) - key -> columns to be coalesced (c1, c2, c3)
   * @return
   */
  def coalesceValues(frame: DataFrame, args: Any): DataFrame = {
    var _df = frame
    args.asInstanceOf[Map[String, Seq[String]]].foreach(e => {
      val coalesceCols = e._2.foldLeft(Seq[Column]())((s, c) => s :+ col(c))
      _df = _df
        .withColumn(e._1, coalesce(coalesceCols: _*))
        .drop(e._2.filterNot(c => c.equals(e._1)): _*)
    })
    logger.debug("coalesced columns {}", args)

    _df
  }

  def renameColumns(frame: DataFrame, args: Any): DataFrame = {
    var columns = frame.columns
    val mapping = args.asInstanceOf[Map[String, String]]
    val _df = mapping.filterKeys(_.startsWith("*"))
      .foldLeft(frame)((df, e) => {
        val suffix = e._1.substring(1)
        columns.filter(_.endsWith(suffix))
          .foldLeft(df)((df, c) =>
            df.withColumnRenamed(c, c.substring(0, c.lastIndexOf(suffix)) + e._2)
          )
      })

    columns = _df.columns
    mapping.filterNot(_._1.startsWith("*"))
      .foldLeft(_df)((df, e) =>
        if (columns.contains(e._1)) _df.withColumnRenamed(e._1, e._2)
        else df
      )
  }

  def caseFormat(frame: DataFrame, args: Any): DataFrame = {
    var _df = frame
    args.asInstanceOf[String] match {
      case c@"lower" => frame.columns.foreach(n => _df = _df.withColumnRenamed(n, n.toLowerCase()))
      case c@"upper" => frame.columns.foreach(n => _df = _df.withColumnRenamed(n, n.toUpperCase()))
      case c@"camel" => frame.columns.foreach(n => _df = _df.withColumnRenamed(n, StringUtils.capitalize(n)))
      case c@"snake" => frame.columns.foreach(n => _df = _df.withColumnRenamed(n, snakify(n)))
    }
    logger.debug("formatted column case {}", args)

    _df
  }

  /** Creates a snake representation for given string.
   * helloWorld -> hello_world
   * HelloWorld -> hello_world
   * SubscriberID -> subscriber_id
   * myFullName -> my_full_name
   *
   * Compares current character with the next one and creates a new word at the word boundary if the case changes
   * Result is always lowercased
   *
   * @param str
   * @return
   */
  def snakify(str: String): String = {
    if (StringUtils.isBlank(str)) return str
    if (StringUtils.isAllLowerCase(str)) return str
    else if (StringUtils.isAllUpperCase(str)) return str.toLowerCase()

    val wordSeq = mutable.MutableList[String]()

    var starting = 0
    val strLen = str.length
    for (i <- 0 until strLen) {
      if (i + 1 < strLen) {
        val c = str.charAt(i)
        val next = str.charAt(i + 1)
        if (Character.isLowerCase(c) && Character.isLetter(next) && !Character.isLowerCase(next)) {
          wordSeq += str.substring(starting, i + 1)
          starting = i + 1
        }
      }
    }

    if (starting != 0) {
      wordSeq += str.substring(starting)
    }
    val snaked = if (wordSeq.nonEmpty) wordSeq.mkString("_").toLowerCase() else str.toLowerCase()
    logger.debug("orig column name: {} snaked: {}", str, snaked)
    snaked
  }

  def extractColumns(frame: DataFrame, args: Any): DataFrame = {
    val columns = frame.columns
    var _df = frame
    args.asInstanceOf[Seq[Map[String, Seq[String]]]]
      .foreach(c => {
        val key = c.keySet.head
        val extCol = c.values.head.foldLeft(Seq[String]())((s, c) => s :+ s"$key.$c")
        if (columns.contains(key))
          _df = _df.select("*", extCol: _*).drop(key)
      })

    _df
  }

  def explodeColumns(frame: DataFrame, args: Any): DataFrame = {
    frame.columns
      .intersect(args.asInstanceOf[Seq[String]])
      .foldLeft(frame)((f, c) => {
        logger.info("exploding {}", c)
        //val res = f.select(col("*"), explode(col(c))).drop(c)
        f.select(col("*"), explode(col(c))).drop(c).withColumnRenamed("col", c)
      }
      )
  }

  def joinDataFrame(frame: DataFrame, args: Map[String,Any])(sparkSession: SparkSession): DataFrame = {
    try {
      if (args.isEmpty || !args.contains("location") || !args.contains("on")) frame
      else {
        val format = args.getOrElse("format", Format.parquet.toString).asInstanceOf[String]
        val joinDf = sparkSession.read.format(format).load(args("location").asInstanceOf[String])
        val on = args("on").asInstanceOf[Seq[String]]
        val how = args.getOrElse("how", "inner").asInstanceOf[String]

        frame.join(joinDf, usingColumns = on, joinType = how)
      }
    } catch {
      case t:Throwable => logger.warn("Exception while joining dataframe", t)
        frame
    }
  }


  /** Returns groupBy on columns applying given operations on aggColumns
   * groupBy([col1, col2].agg(
   * sum("earning"), avg("earning"), count("*"))
   *
   * groupByCols: col1, col2
   * aggCols: earning, *
   * operations: sum, avg, count
   *
   * @param frame
   * @param args
   * @return
   */
  def applyGroupBy(frame: DataFrame, args: Any): DataFrame = {
    val groupByCols = args.asInstanceOf[Map[String, Seq[String]]].getOrElse("columns", Seq.empty)

    if (groupByCols.isEmpty) return frame

    val mapping: Map[String, Column => Column] =
      Map("sum" -> sum, "min" -> min, "max" -> max, "mean" -> avg,
        "count" -> count, "stddev" -> stddev_pop)

    val aggCols = args.asInstanceOf[Map[String, Seq[String]]].getOrElse("aggregate", Seq.empty)
    val operations = args.asInstanceOf[Map[String, Seq[String]]].getOrElse("stats", Seq.empty)

    val exprs = aggCols.flatMap(c => operations.map(f => mapping(f)(col(c)).alias(s"${c}_$f")))

    frame.groupBy(groupByCols.head, groupByCols.tail: _*).agg(exprs.head, exprs.tail: _*)
  }

  def selectInputColumns(frame: DataFrame, args: Any): DataFrame = {
    var colsToSelect = args.asInstanceOf[Seq[String]].filterNot(c => c.startsWith("*") || c.endsWith("*"))

    colsToSelect = args.asInstanceOf[Seq[String]]
      .filter(_.startsWith("*"))
      .foldLeft(colsToSelect)((s, c) => {
        frame.columns
          .filter(_.endsWith(c.substring(1)))
          .foldLeft(s)((s, c) => s :+ c)
      }
      )

    colsToSelect = args.asInstanceOf[Seq[String]]
      .filter(_.endsWith("*"))
      .foldLeft(colsToSelect)((s, c) => {
        val prefix = c.substring(0, c.length -1)
        frame.columns
          .filter(_.startsWith(prefix))
          .foldLeft(s)((s, c) => s :+ c)
      }
      )

    colsToSelect.foldLeft(frame)((df, c) => {
      if (df.columns.contains(c)) df
      else {
        logger.info("column {} not found, will be defaulted", c)
        df.withColumn(c, lit(""))
      }
    }).select(colsToSelect.head, colsToSelect.tail: _*)
  }
}

case class Sink(config: Map[String, Any]) extends TSink {
  override val location = config.getOrElse("output_location", "").toString
  override val saveConfig: SaveConfig =
    SaveConfig(
      Format.withNameOrElse(config.get("format").get.toString, Format.parquet),
      SaveMode.valueOf(StringUtils.capitalize(config.getOrElse("mode", "append").toString)),
      config.getOrElse("partitions", Seq.empty).asInstanceOf[Seq[String]],
      config.getOrElse("options", Map.empty).asInstanceOf[Map[String, String]]
    )
}
