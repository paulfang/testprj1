package com.commscope.eco

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.typesafe.scalalogging.Logger
import org.apache.commons.lang3.{BooleanUtils, StringUtils}
import org.apache.spark.sql.SaveMode
import org.apache.spark.sql.types.StructType

import scala.collection.mutable

object Format extends Enumeration {
  type Format = Value
  val csv, delta, json, hdf, parquet, table = Value

  def withNameOrElse(name: String, elseValue: Format = Format.csv) =
    try {
      withName(name.toLowerCase)
    }
    catch {
      case _: Throwable => elseValue
    }
}

case class TimeDims(dimColNames: Seq[Any], primaryTSCol: String, tsMultiplier: Double = 1.0, timeDimColPrefix: String = "ts_")

case class AppConfig(configArgs: String*) {
  val logger = Logger(AppConfig.getClass)
  val objectMapper = new ObjectMapper()
  objectMapper.registerModule(DefaultScalaModule)
  val now = System.currentTimeMillis()

  val config = configArgs.filterNot(_.contains("=")).foldLeft(mutable.Map[String, Any]())((m, c) => {
    logger.info("loading the config from: {}", c)
    val aConfig = objectMapper.readValue(this.getClass.getClassLoader.getResourceAsStream(c), classOf[Map[String, Any]])
    aConfig.foreach(e => m.put(e._1, e._2))
    m
  })

  val overridden = configArgs.filter(_.contains("=")).foldLeft(mutable.Map[String, Any]())((m, c) => {
    val parts = c.split("=")
    m.put(parts(0).trim, parts(1).trim)
    m
  })

  val passthrough = getConsent("passthrough")
  val backfill = getConsent("backfill")

  private def getConsent(str: String) = {
    if (overridden.contains(str)) {
      val consent = String.valueOf(overridden.get(str).get).trim.toLowerCase
      if (consent.startsWith("1") || consent.startsWith("y") || consent.startsWith("t")) true else false
    } else false
  }

  val appName = overridden.getOrElseUpdate("name", this.getValueAsString("name"))

  val auditor = this.getValueAsDict("auditor")

  val inputLocation = overridden.getOrElseUpdate("input_location", this.getValueAsString("input_location")).toString

  val warehouseLocation = overridden.getOrElseUpdate("warehouse_location", this.getValueAsString("warehouse_location")).toString

  val inputFileList = {
    val fileList = overridden.getOrElseUpdate("input_file_list", this.getValueAsString("input_file_list")).toString
    if (StringUtils.isNotBlank(fileList) && StringUtils.isNoneBlank(inputLocation))
      fileList.split(",").foldLeft(List[String]())((l, f) => l :+ s"$inputLocation/$f").mkString(",")
    else fileList
  }

  val inputFilePattern = overridden.getOrElseUpdate("input_file_pattern", this.getValueAsString("input_file_pattern"))

  val insertColumns: Map[String, Any] = {
    val cols = overridden.getOrElse("insert_columns", config.getOrElse("insert_columns","")).toString

    if (cols.nonEmpty) {
      cols.split(",")
        .filter(_.contains(":"))
        .foldLeft(Map[String, Any]())((m, t) => {
          val p = t.split(":")
          m + (p(0).trim -> p(1).trim)
        })
    } else Map.empty
  }

  //val inputFilter:Map[String,Any] = overridden.getOrElseUpdate("input_filter", this.getValueAsDict("input_filter")).asInstanceOf[Map[String,Any]]
  val inputFilter = this.getValueAsDict("input_filter")

  overridden.getOrElseUpdate("input_format", this.getValueAsString("input_format", "json"))
  val inputFormat = Format.withNameOrElse(overridden("input_format").asInstanceOf[String])

  val outputLocation = overridden.getOrElseUpdate("output_location", this.getValueAsString("output_location")).toString

  val dataframeOutputLocation = (if (!outputLocation.endsWith("/")) outputLocation + "/" else outputLocation) + appName

  overridden.getOrElseUpdate("output_format", this.getValueAsString("output_format", "delta"))
  val outputFormat = Format.withNameOrElse(overridden.get("output_format").get.asInstanceOf[String])

  val outputSaveMode: SaveMode = {
    val mode = this.getValueAsString("output_save_mode", "append")
    SaveMode.values().collectFirst { case m if m.name().equalsIgnoreCase(mode) => m }.getOrElse(SaveMode.Append)
  }

  val filterExpr = getValueAsString("filter_expr")

  val inputSchema: Option[StructType] = {
    getSchemaByName("input")
  }

  val outputSchema: Option[StructType] = {
    val schema = getSchemaByName("output")
    if (schema.isDefined) schema else inputSchema
  }

  val csvExport = this.getValueAsDict("csv_export")

  val csvExportEnabled = csvExport.nonEmpty && csvExport.getOrElse("enabled", false).asInstanceOf[Boolean]

  val csvExportLocation: Option[String] =
    if (csvExportEnabled && csvExport.contains("output_location")) Some(csvExport("output_location").toString)
    else None

  val csvExportSaveMode: Option[String] =
    if (csvExportEnabled)
      Some(csvExport.getOrElse("save_mode", SaveMode.Overwrite.toString).asInstanceOf[String])
    else None

  val flattenOutput: Boolean = {
    val output = this.getValueAsDict("output")
    val flatten_key = "flatten"
    if (!output.isEmpty && output.get(flatten_key).isDefined) {
      BooleanUtils.toBoolean(String.valueOf(output.get(flatten_key).get))
    }
    else false
  }

  /** A tuple of boolean, name of the primary timestamp column to use and timestamp multiplier for creating the time dimensions */
  val createTimeDims: Option[TimeDims] = {
    val create = this.getValueAsBool("create_time_dims", false)
    if (create) {
      val tsCol = this.getValueAsString("primary_timestamp_column", StringUtils.EMPTY)
      if (StringUtils.isNotBlank(tsCol)) {
        val colNames = this.getValueAsSeq("time_dim_columns", Seq.empty[String])
        Some(TimeDims(colNames, tsCol, this.getValueAsDouble("timestamp_multiplier", 1.0)))
      } else None
    } else None
  }

  def processModifiedSinceEnabled: Option[(Boolean, String)] = {
    val enabled = getValueAsBool("process_modified_since_enabled")
    if (enabled) {
      Some(true, getValueAsString("modified_since_column"))
    } else None
  }

  val pipelines: Seq[TPipeline] =
    getValueAsSeq("pipelines").foldLeft(Seq[Pipeline]())((s, c) => s :+ Pipeline(c.asInstanceOf[Map[String, Any]]))

  val version =
    if (this.getValueAsString("version").isEmpty && this.pipelines.nonEmpty) "2.0"
    else this.getValueAsString("version", "1.0")

  val ignoreColumns: Seq[String] = getValueAsSeq("ignore_columns").asInstanceOf[Seq[String]]

  val saveOptions: Map[String, Any] = getValueAsDict("save_options")

  val partitions = getValueAsSeq("partitions").asInstanceOf[Seq[String]]

  val splitter: Option[Splitter] =
    if (config.contains("splitter")) {
      val s = Splitter(getValueAsDict("splitter"))
      if (s.splitOn.isEmpty) None else Some(s)
    } else None


  def getValueAsString(key: String, defaultVal: String = "") = config.getOrElse(key, defaultVal).asInstanceOf[String]

  def getValueAsLong(key: String, defaultVal: Long = 0) = config.getOrElse(key, defaultVal).asInstanceOf[Long]

  def getValueAsDouble(key: String, defaultVal: Double = 0.0) = config.getOrElse(key, defaultVal).asInstanceOf[Double]

  def getValueAsBool(key: String, defaultVal: Boolean = false) = config.getOrElse(key, defaultVal).asInstanceOf[Boolean]

  def getValueAsSeq(key: String, defaultVal: Seq[Any] = Seq[Any]()) = config.getOrElse(key, defaultVal).asInstanceOf[Seq[Any]]

  def getValueAsDict(key: String, defaultVal: Map[String, Any] = Map[String, String]()) = config.getOrElse(key, defaultVal).asInstanceOf[Map[String, Any]]

  private def getSchemaByName(name: String) = {
    val dict = this.getValueAsDict(name)
    if (dict.nonEmpty && dict.get("schema").isDefined) SchemaUtils.toStruct(dict("schema").asInstanceOf[Map[String, Any]]) else None
  }
}
