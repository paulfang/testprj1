package com.commscope.eco.model

import com.commscope.eco.{AppConfig, Format}
import com.commscope.eco.Format.Format
import com.typesafe.scalalogging.Logger
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.StructType
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

import scala.collection.mutable
import scala.util.Try

trait DataModel {
  val logger = Logger(getClass)

  def model(dataFrame: DataFrame, appConfig: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame

  def dropDuplications(frame: DataFrame, args: Any): DataFrame = frame.dropDuplicates(args.asInstanceOf[Seq[String]])

  def dropColumns(frame: DataFrame, args: Any): DataFrame = {
    var _df = frame
    args.asInstanceOf[Map[String, Seq[String]]].foreach(e => {
      if (e._2.nonEmpty) {
        if (e._1.equals("__root__")) {
          val wildCardStart = e._2.filter(_.startsWith("*"))
            .foldLeft(Seq[String]())((s, c) => _df.columns.filter(_.endsWith(c.substring(1)))
              .foldLeft(s)((s, c1) => s :+ c1))

          val wildCardEnd = e._2.filter(_.endsWith("*"))
            .foldLeft(Seq[String]())((s, c) => _df.columns.filter(_.startsWith(c.substring(0, c.length - 1)))
              .foldLeft(s)((s, c1) => s :+ c1))

          val remaining = e._2.filter(!_.contains("*"))
          _df = _df
            .drop(wildCardStart: _*)
            .drop(wildCardEnd: _*)
            .drop(remaining: _*)
        } else if (_df.columns.contains(e._1) && Try(_df.schema.filter(f => f.name.equals(e._1)).head.dataType.asInstanceOf[StructType]).isSuccess)
          _df = _df.select("*", s"${e._1}.*").drop(e._1).drop(e._2: _*)
      }
    })
    logger.debug("dropped columns {}", args)

    _df
  }

  def windowAgg(frame: DataFrame, args: Any): DataFrame = {
    val argsMap = args.asInstanceOf[Map[String, Map[String, Any]]]

    if (!argsMap.contains("window") ||
      !argsMap("window").contains("partitionBy") ||
      !argsMap.contains("function") ||
      !argsMap("function").contains("name")) {
      return frame
    }

    val windowSpec = createWindowSpec(argsMap).get

    argsMap("function")("name").asInstanceOf[String] match {
      case c@"lag" => applyWindowLag(frame, windowSpec, argsMap)
      case _ =>
        logger.warn("windowAgg function not supported: {}", argsMap("function")("name"))
        frame
    }
  }

  def createWindowSpec(args: Map[String, Map[String, Any]]): Option[WindowSpec] = {
    if (!args("window").contains("partitionBy")) return None

    val partitionBy = args("window")("partitionBy").asInstanceOf[Seq[String]]
    val windowSpec = Window.partitionBy(partitionBy.head, partitionBy.tail: _*)

    if (args("window").contains("orderBy")) {
      val orderBys = args("window")("orderBy").asInstanceOf[Seq[Map[String, String]]]
        .foldLeft(Seq[Column]())((s, e) =>
          if (e.head._1.equalsIgnoreCase("desc")) s :+ desc(e.head._2)
          else s :+ asc(e.head._2)
        )

      Some(windowSpec.orderBy(orderBys: _*))
    } else Some(windowSpec)
  }

  def applyWindowLag(frame: DataFrame, windowSpec: WindowSpec, args: Map[String, Map[String, Any]]): DataFrame = {
    var _df = frame
    val offset = args("function").getOrElse("period", 1).asInstanceOf[Int]
    val funOutputPrefix = args("function").getOrElse("output_prefix", "_prev")
    val applyExpr = if (args("function").contains("apply")) {
      val _apply = args("function")("apply").asInstanceOf[Map[String, String]]
      Option(_apply.get("expression"), _apply.getOrElse("output_suffix", "__windowRes"))
    } else None

    args("function")("columns").asInstanceOf[Seq[String]].foreach(c => {
      _df = _df.withColumn(s"$c$funOutputPrefix", lag(c, offset) over windowSpec)
      if (applyExpr.isDefined && applyExpr.get._1.isDefined) {
        val exprStr = applyExpr.get._1.get.replace("$$", s"$c")
        logger.info("applying expression: {}", exprStr)
        _df = _df.withColumn(s"$c${applyExpr.get._2}", expr(exprStr))
      }
    })
    _df
  }

  /** Coalesces columns in the given dataframe
   * The key of the Map (args) will be used as the final column name that will hold
   * the result of coalescing the values.
   * The value of the Map contains the columns that will be coalesced. These columns are dropped
   * once coalesced except the key column, if the key column was also part of the coalesced columns
   *
   * @param frame
   * @param args (Map[String, Seq[String]]) - key -> columns to be coalesced (c1, c2, c3)
   * @return
   */
  def coalesceValues(frame: DataFrame, args: Any): DataFrame = {
    var _df = frame
    args.asInstanceOf[Map[String, Seq[String]]].foreach(e => {
      val coalesceCols = e._2.foldLeft(Seq[Column]())((s, c) => s :+ col(c))
      _df = _df
        .withColumn(e._1, coalesce(coalesceCols: _*))
        .drop(e._2.filterNot(c => c.equals(e._1)): _*)
    })
    logger.debug("coalesced columns {}", args)

    _df
  }

  def renameColumns(frame: DataFrame, args: Any): DataFrame = {
    val columns = frame.columns
    var _df = frame
    args.asInstanceOf[Map[String, String]].foreach(e => if (columns.contains(e._1)) _df = _df.withColumnRenamed(e._1, e._2))
    logger.debug("renamed columns {}", args)

    _df
  }

  def caseFormat(frame: DataFrame, args: Any): DataFrame = {
    var _df = deduplicateColumns(frame)
    args.asInstanceOf[String] match {
      case c@"lower" => frame.columns.foreach(n => _df = _df.withColumnRenamed(n, n.toLowerCase()))
      case c@"upper" => frame.columns.foreach(n => _df = _df.withColumnRenamed(n, n.toUpperCase()))
      case c@"camel" => frame.columns.foreach(n => _df = _df.withColumnRenamed(n, StringUtils.capitalize(n)))
      case c@"snake" => frame.columns.foreach(n => _df = _df.withColumnRenamed(n, snakify(n)))
    }
    logger.debug("formatted column case {}", args)

    _df
  }

  /** Creates a snake representation for given string.
   * helloWorld -> hello_world
   * HelloWorld -> hello_world
   * SubscriberID -> subscriber_id
   * myFullName -> my_full_name
   *
   * Compares current character with the next one and creates a new word at the word boundary if the case changes
   * Result is always lowercased
   *
   * @param str
   * @return
   */
  def snakify(str: String): String = {
    if (StringUtils.isBlank(str)) return str
    if (StringUtils.isAllLowerCase(str)) return str
    else if (StringUtils.isAllUpperCase(str)) return str.toLowerCase()

    val wordSeq = mutable.MutableList[String]()

    var starting = 0
    val strLen = str.length
    for (i <- 0 until strLen) {
      if (i + 1 < strLen) {
        val c = str.charAt(i)
        val next = str.charAt(i + 1)
        if (Character.isLowerCase(c) && Character.isLetter(next) && !Character.isLowerCase(next)) {
          wordSeq += str.substring(starting, i + 1)
          starting = i + 1
        }
      }
    }

    if (starting != 0) {
      wordSeq += str.substring(starting)
    }
    val snaked = if (wordSeq.nonEmpty) wordSeq.mkString("_").toLowerCase() else str.toLowerCase()
    logger.debug("orig column name: {} snaked: {}", str, snaked)
    snaked
  }

  def extractColumns(frame: DataFrame, args: Any): DataFrame = {
    val columns = frame.columns
    var _df = frame
    args.asInstanceOf[Seq[Map[String, Seq[String]]]]
      .foreach(c => {
        val key = c.keySet.head
        val extCol = c.values.head.foldLeft(Seq[String]())((s, c) => s :+ s"$key.$c")
        if (columns.contains(key))
          _df = _df.select("*", extCol: _*).drop(key)
      })

    _df
  }

  def explodeColumns(frame: DataFrame, args: Any): DataFrame = {
    frame.columns
      .intersect(args.asInstanceOf[Seq[String]])
      .foldLeft(frame)((f, c) => {
        logger.info("exploding {}", c)
        //val res = f.select(col("*"), explode(col(c))).drop(c)
        f.select(col("*"), explode(col(c))).drop(c).withColumnRenamed("col", c)
      }
      )
  }

  def joinDataFrame(frame: DataFrame, args: Map[String, Any]): DataFrame = {
    //    if (args.isEmpty || !args.contains("location") || !args.contains("on")) frame
    //    else {
    //      val format = args.getOrElse("format", Format.parquet).asInstanceOf[String]
    //      val joinDf = sparkSession.read.format(format).load(args("location").asInstanceOf[String])
    //      val on = args.get("on").asInstanceOf[Array[String]]
    //      val how = args.getOrElse("how","inner").asInstanceOf[String]
    //
    //      frame.join(joinDf, usingColumns = on, joinType = how)
    //    }
    frame
  }

  /** Returns groupBy on columns applying given operations on aggColumns
   * groupBy([col1, col2].agg(
   * sum("earning"), avg("earning"), count("*"))
   *
   * groupByCols: col1, col2
   * aggCols: earning, *
   * operations: sum, avg, count
   *
   * @param frame
   * @param args
   * @return
   */
  def applyGroupBy(frame: DataFrame, args: Any): DataFrame = {
    val groupByCols = args.asInstanceOf[Map[String, Seq[String]]].getOrElse("columns", Seq.empty)

    if (groupByCols.isEmpty) return frame

    val mapping: Map[String, Column => Column] =
      Map("sum" -> sum, "min" -> min, "max" -> max, "mean" -> avg,
        "count" -> count, "first" -> first, "last" -> last,
        "stddev" -> stddev)

    val aggCols = args.asInstanceOf[Map[String, Seq[String]]].getOrElse("aggregate", Seq.empty)
    val operations = args.asInstanceOf[Map[String, Seq[String]]].getOrElse("functions", Seq.empty)

    val exprs = aggCols.flatMap(c => operations.map(f => mapping(f)(col(c))))

    frame.groupBy(groupByCols.head, groupByCols.tail: _*).agg(exprs.head, exprs.tail: _*)
  }

  def selectInputColumns(frame: DataFrame, args: Any): DataFrame = {
    val cols = args.asInstanceOf[Seq[String]]
    frame.select(cols.head, cols.tail: _*)
  }

  def deduplicateColumns(frame: DataFrame): DataFrame =
    frame.columns.foldLeft(Map[String, Int](), frame)((pair, c) => {
      var seen = pair._1
      var df = pair._2
      val column = c.toLowerCase()
      if (seen.contains(column)) {
        df = df.withColumnRenamed(c, s"${c}_${seen(column)}")
        seen += (column -> (seen(column) + 1))
        (seen, df)
      }
      else {
        seen += (column -> 1)
        (seen, df)
      }
    }
    )._2

  def selectColumns(src: DataFrame, config: AppConfig): DataFrame = {
    if (config.outputSchema.isEmpty) src else selectColumns(src, config.outputSchema.get.fieldNames)
  }

  def selectColumns(src: DataFrame, colNames: Seq[String]): DataFrame = {
    logger.debug("selecting columns: {} from df {}", colNames.mkString(","), src.columns.mkString(","))
    colNames.foldLeft(src)((df, c) => {
      if (df.columns.contains(c)) df
      else {
        logger.info("column {} not found, will be defaulted", c)
        df.withColumn(c, lit(""))
      }
    }).select(colNames.head, colNames.tail: _*)
  }

  def filter(src: DataFrame, expr: Seq[String]): DataFrame = {
    logger.debug("filtering dataframe expr: {} from df {}", expr.mkString(","), src.columns.mkString(","))
    expr.foldLeft(src)((df, e) => df.filter(e))
  }


  def transform(src: DataFrame, xform: Transformation): DataFrame = {
    logger.debug("applying transformation: {} args: {}", xform, xform.args)
    val result = Try(
      xform.name match {
        case k@"selectColumns" => selectInputColumns(src, xform.args)
        case k@"dropDuplicates" => dropDuplications(src, xform.args)
        case k@"filter" => filter(src, xform.args.asInstanceOf[Seq[String]])
        case k@"dropColumns" => dropColumns(src, xform.args)
        case k@"coalesce" => coalesceValues(src, xform.args)
        case k@"renameColumns" => renameColumns(src, xform.args)
        case k@"caseFormat" => caseFormat(src, xform.args)
        case k@"extractColumns" => extractColumns(src, xform.args)
        case k@"explodeColumns" => explodeColumns(src, xform.args)
        case k@"joinDataFrame" => joinDataFrame(src, xform.args.asInstanceOf[Map[String, Any]])
        case k@"groupBy" => applyGroupBy(src, xform.args)
        case k@"windowAgg" => windowAgg(src, xform.args)
        case _ =>
          logger.warn("Transform not supported: {}", xform.name)
          src
      })

    if (result.isSuccess) result.get
    else {
      logger.warn("Error while transformation {}: detail:\n{}", xform, result.failed.get.getMessage)
      src
    }
  }

  def applyTransformations(frame: DataFrame, config: AppConfig) = TransformationSeq(config).s.foldLeft(frame)((df, t) => transform(df, t))

  def loadDependency(dfLocation: String, format: Format)(implicit sparkSession: SparkSession): Option[DataFrame] =
    try {
      logger.info("loading dependency from {}", dfLocation)
      Some(sparkSession.read.format(format.toString).load(dfLocation))
    }
    catch {
      case t: Throwable => logger.warn(s"Error loading dataframe from: {} :{}", dfLocation, t.getMessage, t)
        None
    }
}

case class TransformationSeq(config: AppConfig) {
  val logger: Logger = Logger(TransformationSeq.getClass)
  val s: Seq[Transformation] = Try(config
    .getValueAsDict("output")
    .filter(e => e._1.equals("transformations")).head._2.asInstanceOf[Iterable[Any]]
    .foldLeft(Seq[Transformation]())((s, v) => v match {
      case m: Map[String, Map[String, Any]] =>
        s :+ Transformation(m.head._2("name").toString, m.head._2("args"))
    })).getOrElse(Seq.empty)

  logger.debug("transformation count: {}", s.length)

  def findByName(name: String): Option[Transformation] = Option(Try(s.filter(t => t.name.equals(name)).head).getOrElse(null))
}

case class Transformation(name: String, args: Any)

object Generic extends DataModel {
  override def model(dataFrame: DataFrame, appConfig: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    applyTransformations(dataFrame, appConfig)
  }
}

