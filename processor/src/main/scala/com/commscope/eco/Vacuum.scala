package com.commscope.eco

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.scala.DefaultScalaModule
import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.expressions.{Window, WindowSpec}
import org.apache.spark.sql.functions.{asc, col, desc, row_number}
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

object Vacuum extends App {
  private val logger = Logger(Vacuum.getClass)

  if (args.isEmpty) {
    logger.warn("Nothing to do....no command line arguments passed")
  }
  else {
    implicit val conf = VacuumConfig(args.head)

    implicit val spark = SparkSession.builder()
      .config(new SparkConf().setAppName(conf.name))
      .getOrCreate()

    val df = spark.read.format(conf.format).load(conf.inputLocation)
      .select(col("*"), row_number().over(conf.windowSpec).alias("row_num"))
      .filter("row_num =  1")

    if (conf.splitter.isDefined) {
      val auditLocation =
        conf.splitter.get.auditOutputLocation.getOrElse(s"${conf.outputLocation}/splitter/audit").asInstanceOf[String]
      conf.splitter.get.split(df)
        .foreach(result => {
          DataFrameUtils.logProgress(auditLocation,"start", 0, result.expression, System.currentTimeMillis())
          save(result.dataFrame, conf, mode = "append")
          DataFrameUtils.logProgress(auditLocation, "complete", 1, result.expression, System.currentTimeMillis())
        })
    }
    else {
      save(df, conf)
    }
  }

  def save(df:DataFrame, conf:VacuumConfig, mode:String = "overwrite"): Unit = {
    val writer = df.write.format(conf.outputFormat).mode(mode)
    if (conf.outputPartitions.isEmpty)
      writer.save(conf.outputLocation)
    else
      writer.partitionBy(conf.outputPartitions: _*).save(conf.outputLocation)
  }
}

case class VacuumConfig(configFile: String) {
  private val logger = Logger(AppConfig.getClass)
  val objectMapper = new ObjectMapper()
  objectMapper.registerModule(DefaultScalaModule)
  private val now = System.currentTimeMillis()

  logger.info("loading the config: {}", configFile)
  val conf: Map[String, Any] = objectMapper.readValue(this.getClass.getClassLoader.getResourceAsStream(configFile), classOf[Map[String, Any]])
  val name: String = conf.getOrElse("name", "Vacuum").toString
  val format: String = conf.getOrElse("input_format", "parquet").toString
  val inputLocation: String = conf("input_location")toString
  val outputLocation: String = conf("output_location").toString

  val partitionBy: Seq[String] = conf("window").asInstanceOf[Map[String, Any]]("partitionBy").asInstanceOf[Seq[String]]
  var windowSpec: WindowSpec = Window.partitionBy(partitionBy.head, partitionBy.tail: _*)

  private val orderBys = conf("window").asInstanceOf[Map[String, Any]]("orderBy").asInstanceOf[Seq[Map[String, String]]]
    .foldLeft(Seq[Column]())((s, e) =>
      if (e.head._1.equalsIgnoreCase("desc")) s :+ desc(e.head._2)
      else s :+ asc(e.head._2)
    )

  windowSpec = windowSpec.orderBy(orderBys: _*)

  val outputFormat:String = conf.getOrElse("output_format", "parquet").toString
  val outputPartitions:Seq[String] = conf.getOrElse("partitions", Seq.empty).asInstanceOf[Seq[String]]

  val splitter: Option[Splitter] =
    if (conf.contains("splitter")) {
      val s = Splitter(conf.getOrElse("splitter", Map.empty).asInstanceOf[Map[String, Any]])
      if (s.splitOn.isEmpty) None else Some(s)
    } else None
}
