package com.commscope.eco

import com.commscope.eco.inquire.udfs._
import com.typesafe.scalalogging.Logger
import org.apache.spark
import org.apache.spark.SparkConf
import org.apache.spark.sql.{AnalysisException, SaveMode, SparkSession}

/**
 * Hello world!
 *
 */
object Inquire extends App {
  val logger = Logger(getClass)

  if (args.isEmpty) {
    logger.warn("Nothing to do....no command line arguments passed")
  }
  else  {
    implicit val appConfig = AppConfig(args :_*)

    val sparkConf = new SparkConf()
      .setAppName(appConfig.getValueAsString("name", "Eco Inquire"))

    appConfig.config
      .filter(e => e._1.startsWith("spark."))
      .foreach(e => sparkConf.set(e._1, String.valueOf(e._2)))

    implicit val spark: SparkSession = SparkSession.builder().config(sparkConf).getOrCreate()
    registerUDFs(spark)

    logger.info("Registered classify UDF")

    val st = System.currentTimeMillis()
    logger.info("start processing {} at {}", appConfig.appName, st)
    try {
      DataProcessor.process(appConfig)
    }
    catch {
      case ae: AnalysisException =>
        if (ae.message.toLowerCase.contains("path does not exist"))
          logger.warn("{}: No files found at: {}", appConfig.appName, appConfig.inputLocation)
        else throw ae
      case t:Throwable => throw t
    }
    logger.info("finished processing {} in {} sec", appConfig.appName, (System.currentTimeMillis() - st)/1000)
  }

  def registerUDFs(implicit sparkSession: SparkSession): Unit = {
    sparkSession.udf.register("classify", new ClassifierUdf().call _)
    sparkSession.udf.register("getCategory", new CategoryUdf().call _)
    sparkSession.udf.register("getVendor", new VendorUdf().call _)
  }
}
