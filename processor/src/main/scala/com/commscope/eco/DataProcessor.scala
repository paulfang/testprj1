package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.{DataFrame, SparkSession}
import com.commscope.eco.DataFrameUtils._
import org.apache.commons.lang3.StringUtils

trait TDataProcessor {
  def process(config: AppConfig)(implicit sparkSession: SparkSession): (DataFrame, String)
}

object DataProcessor extends TDataProcessor {
  val logger = Logger(DataProcessor.getClass)

  def process(config: AppConfig)(implicit sparkSession: SparkSession): (DataFrame, String) = {
    val filesToProcess: String = getFilesToProcess(config)
    val inputFormat = config.inputFormat

    logger.debug("filesToProcess: {}", filesToProcess)

    if (config.passthrough) {
      logger.debug("processing using passthrough config")
      sparkSession.read
        .format(inputFormat.toString)
        .load(filesToProcess)
        .filter(config.filterExpr)
        .write
        .format(config.outputFormat.toString)
        .save(config.outputLocation)
      return (null, null)
    }

    val (df, location) = DataFrameUtils
      .load(inputFormat, filesToProcess)
      .trace()
      .insertColumns(config)
      .filter(config)
      .dropFields(config.ignoreColumns)
      .sanitizeColumnNames(inputFormat, outputFormat = config.outputFormat)
      .appendTimeDims(config.createTimeDims)
      .flatten(config.flattenOutput)
      .runPipelines(config)

    logger.info("processing completes, saved {} at {}", df.columns, location)

    (df, location)
  }

  private def getFilesToProcess(config: AppConfig) = {
    if (StringUtils.isNotBlank(config.inputFileList)) config.inputFileList
    else (if (!config.inputLocation.endsWith("/")) config.inputLocation + "/" else config.inputLocation) + config.inputFilePattern
  }
}
