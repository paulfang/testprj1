package com.commscope.eco

import com.commscope.eco.Format.Format
import com.commscope.eco.model.DataModel
import com.typesafe.scalalogging.Logger
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.functions.{col, _}
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Column, DataFrame, SaveMode, SparkSession}

import scala.collection.mutable
import scala.reflect.runtime.universe
import scala.util.Try


object Format extends Enumeration {
  type Format = Value
  val csv, delta, json, hdf, parquet, table = Value

  def withNameOrElse(name: String, elseValue: Format = Format.csv) =
    try {
      withName(name.toLowerCase)
    }
    catch {
      case _: Throwable => elseValue
    }
}

case class SaveConfig(format: Format,
                      saveMode: SaveMode = SaveMode.Append,
                      partitions: Seq[String] = Seq(),
                      options: Map[String, Any] = Map.empty)

object DataFrameUtils {
  val logger = Logger(getClass)
  val illegalCharsRegex = SchemaUtils.illegalCharsRegex

  /** An extension to DataFrame to support DataFrameUtils operations
   *
   * @param df
   */
  implicit class DataFrameExt(val df: DataFrame)(implicit sparkSession: SparkSession) {
    val logger = Logger(getClass)

    def sanitizeColumnNames(inputFormat: Format, outputFormat: Format = null) = inputFormat match {
      case Format.json =>
        if (outputFormat != null && outputFormat.equals(Format.json)) df
        else DataFrameUtils.sanitizeColumnNames(df)
      case _ => df
    }

    def walk = DataFrameUtils.walk(df)

    def flatten(flattenOutput: Boolean): DataFrame = {
      if (!flattenOutput) return df

      val schema = SchemaUtils.flatten(Some(df.schema), sanitizeAfterFlattening = true)
      if (schema.isDefined) df.select(schema.get: _*) else df
    }

    def dropFields(fieldNames: Seq[String]) = DataFrameUtils.dropFields(df, fieldNames)

    def filterRows(filterExpr: String = "") = if (StringUtils.isBlank(filterExpr)) df else df.filter(filterExpr)

    val to_inform = expr("hour(ts_datetime) % 24 *4 + int(minute(ts_datetime)/15) + 1")

    def appendTimeDims(timeDimsOpt: Option[TimeDims]) = {
      if (timeDimsOpt.isDefined) {
        val timeDimsColsToCreate = missingTimeDims(timeDimsOpt.get)
        if (timeDimsColsToCreate.nonEmpty) {
          val tmp_ts = "_time_dim_src"
          val timeDims = timeDimsOpt.get
          val prefix = timeDims.timeDimColPrefix
          var _df = df.withColumn(tmp_ts, from_unixtime(col(timeDims.primaryTSCol) * timeDims.tsMultiplier))
          timeDimsColsToCreate.foreach {
            case c@"date" => _df = _df.withColumn(s"$prefix$c", to_date(col(tmp_ts)))
            case c@"year" => _df = _df.withColumn(s"$prefix$c", year(col(tmp_ts)))
            case c@"quarter" => _df = _df.withColumn(s"$prefix$c", quarter(col(tmp_ts)))
            case c@"month" => _df = _df.withColumn(s"$prefix$c", month(col(tmp_ts)))
            case c@"day" => _df = _df.withColumn(s"$prefix$c", dayofmonth(col(tmp_ts)))
            case c@"day_of_week" => _df = _df.withColumn(s"$prefix$c", dayofweek(col(tmp_ts)))
            case c@"hour" => _df = _df.withColumn(s"$prefix$c", hour(col(tmp_ts)))
            case c@"inform" => _df = _df
              .withColumn("ts_datetime", col(tmp_ts).cast(TimestampType))
              .withColumn(s"$prefix$c", to_inform).drop("ts_datetime")
            case _ => _df
          }

          _df.drop(tmp_ts)
        }
        else df
      }
      else df
    }

    def filter(config: AppConfig): DataFrame =
      if (config.inputFilter.nonEmpty && config.inputFilter.contains("filter")) {
        val filterClz = config.inputFilter("filter").asInstanceOf[String]
        logger.debug("filter enabled, filter class: {}", filterClz)

        val mirror = universe.runtimeMirror(getClass.getClassLoader)
        val module = mirror.staticModule(filterClz)
        val obj = mirror.reflectModule(module)
        obj.instance.asInstanceOf[InputFilter].filter(df, config)
      }
      else df

    def insertColumns(config: AppConfig): DataFrame =
      config.insertColumns.foldLeft(df)((df, kv) => {
        df.withColumn(kv._1, lit(kv._2))
      })

    def model(implicit config: AppConfig): DataFrame = {
      val model = config.getValueAsString("model")
      if (StringUtils.isNotBlank(model)) {
        logger.debug("model enabled, model class: {}", model)
        try {
          val mirror = universe.runtimeMirror(getClass.getClassLoader)
          val module = mirror.staticModule(model)
          val obj = mirror.reflectModule(module)
          val dataModel: DataModel = obj.instance.asInstanceOf[DataModel]
          dataModel.model(df, config, config.getValueAsDict("model-args"))
        }
        catch {
          case t: Throwable => logger.warn("Error loading model for className {}: {}", model, t.getMessage)
            throw t
        }
      } else df
    }

    def runPipelines(implicit config: AppConfig): (DataFrame, String) = {
      audit(df)
      if (config.splitter.isDefined) {
        val splitterAuditLocation =
          config.splitter.get.auditOutputLocation.getOrElse(s"${config.outputLocation}/splitter/audit").asInstanceOf[String]

        config.splitter.get.split(df).foreach(result => {
          logProgress(splitterAuditLocation, "start", 0, result.expression, System.currentTimeMillis())
          if (config.splitter.get.cache) result.dataFrame.cache()
          runPipelinesInternal(result.dataFrame)
          logProgress(splitterAuditLocation, "complete", 1, result.expression, System.currentTimeMillis())
          //          saveSplitAudit(result.expression, config.splitter.get.auditOutputLocation)
        })
        (df, "Split locations")
      }
      else {
        runPipelinesInternal(df)
      }
    }

    private def runPipelinesInternal(df: DataFrame)(implicit config: AppConfig): (DataFrame, String) = {
      if (config.version.equals("1.0"))
        df.model(config)
          .stamp(config.now)
          .save(config)
      else
        (config.pipelines
          .foldLeft(Seq[Any]())((s, p) => s :+ p.run(df)).head.asInstanceOf[DataFrame]
          , "NA")
    }

    def audit(df: DataFrame)(implicit config: AppConfig): Unit = {
      if (config.auditor.nonEmpty) {
        if (config.auditor.getOrElse("audit", false).asInstanceOf[Boolean]) {
          val auditLocation = s"${config.outputLocation}/audit/${config.appName}"

          Auditor.audit(config.appName.toString, df)
            .write.mode("append")
            .format("parquet")
            .partitionBy("processed_dt").save(auditLocation)
        }

        if (config.auditor.getOrElse("audit_inform", false).asInstanceOf[Boolean]) {
          val auditInformLocation = s"${config.outputLocation}/audit_inform/${config.appName}"
          val audited = Auditor.auditInforms(config.appName.toString, df)
          if (audited.isDefined)
            audited.get.write.mode("append")
              .format("parquet")
              .partitionBy("processed_dt").save(auditInformLocation)
        }
      }
    }

    def stamp(processedAt: Long): DataFrame = df.withColumn("ts_processed", lit(processedAt))

    def trace(): DataFrame = {
      logger.whenTraceEnabled {
        logger.trace("the row count {}", df.count())
      }
      df
    }

    def save(config: AppConfig): (DataFrame, String) = {
      logger.info("save location: {}", config.dataframeOutputLocation)

      val saveConfig = SaveConfig(config.outputFormat, config.outputSaveMode, config.partitions, config.saveOptions)
      val res = (DataFrameUtils.save(df, config.dataframeOutputLocation, saveConfig), config.dataframeOutputLocation)
      csvExport(res, config)
    }

    def csvExport(tuple: (DataFrame, String), config: AppConfig): (DataFrame, String) = {
      if (config.csvExportEnabled && config.csvExportLocation.isDefined) {
        try {
          tuple._1.write.
            format(Format.csv.toString).
            mode(config.csvExportSaveMode.get).option("quoteAll", value = true).option("compression", "gzip").
            csv(config.csvExportLocation.get)
        }
        catch {
          case t: Throwable => logger.warn("Error exporting dataframe as csv at: {}. Message: {}",
            config.csvExportLocation.get, t.getMessage)
        }
      }
      tuple
    }

    def missingTimeDims(timeDims: TimeDims): Seq[String] = {
      val prefix = timeDims.timeDimColPrefix
      val prefixLen = prefix.length
      val existingTimeDims = df.columns.filter(c => c.startsWith(prefix))
      timeDims.dimColNames
        .foldLeft(Seq[String]())((s, c) => s :+ s"${prefix}$c")
        .diff(existingTimeDims)
        .foldLeft(Seq[String]())((s, c) => s :+ c.substring(prefixLen))
    }
  }

  def logProgress(location: String, state: String, stateCode: Int, expr: String, ts: Long)(implicit spark: SparkSession): Unit = {
    val statusDf = spark.createDataFrame(List((state, stateCode, expr, ts))).toDF("state", "state_code", "expression", "ts")
    statusDf.write.mode("append").format("parquet").save(location)
  }

  /*
    DataFrameExt end
   */

  def fromJson(path: String, schemaJsonPath: Option[String] = None)(implicit sparkSession: SparkSession): DataFrame =
    load(Format.json, path, schemaJsonPath)

  def fromParquet(path: String, schemaJsonPath: Option[String] = None)(implicit sparkSession: SparkSession): DataFrame =
    load(Format.parquet, path, schemaJsonPath)

  def fromDelta(path: String, schemaJsonPath: Option[String] = None)(implicit sparkSession: SparkSession): DataFrame =
    load(Format.delta, path, schemaJsonPath)

  def fromJson(path: String, schema: StructType)(implicit sparkSession: SparkSession) = {
    load(Format.json, path, schema)
  }

  def fromParquet(path: String, schema: StructType)(implicit sparkSession: SparkSession) = {
    load(Format.parquet, path, schema)
  }

  def fromDelta(path: String, schema: StructType)(implicit sparkSession: SparkSession) = {
    load(Format.delta, path, schema)
  }

  def toParquet(df: DataFrame, path: String): Unit = {
    save(df, path, SaveConfig(format = Format.parquet))
  }

  def toDelta(df: DataFrame, path: String): Unit = {
    save(df, path, SaveConfig(format = Format.delta))
  }

  def save(df: DataFrame, path: String, saveConfig: SaveConfig): DataFrame = {
    logger.debug("save called for df: {} at path: {}, saveConfig: {}", df.columns, path, saveConfig)

    var writer = df.write.format(saveConfig.format.toString).mode(saveConfig.saveMode)
    writer = if (saveConfig.partitions.isEmpty) writer else writer.partitionBy(saveConfig.partitions: _*)

    saveConfig.options.foreach(e => writer = writer.option(e._1, e._2.toString))

    try {
      writer.save(path)
    }
    catch {
      case t:Throwable => logger.error("Error saving dataframe with schema: {}", df.printSchema())
        throw t
    }
    logger.debug("save complete \n{}\n", df.columns)

    df
  }

  /** Loads the files in the given format from the given path. If path to the schema in json format is given then use that.
   *
   * @param format
   * @param path
   * @param schemaJsonPath
   * @param sparkSession
   * @return
   */
  def load(format: Format, path: String, schemaJsonPath: Option[String] = None, dummy: String = null)(implicit sparkSession: SparkSession): DataFrame = {
    val schema = if (schemaJsonPath.isDefined) SchemaUtils.loadDataFrameSchema(schemaJsonPath.get) else None
    if (schema.isDefined) {
      load(format, path, schema.get)
    }
    else
      load(format, path, null)
  }

  /** loads the files in the given format from the given path using the schema if provided.
   *
   * @param format the format in which the files are stored
   * @param path   path of where files are stored
   * @param schema schema to apply while loading the dataframe
   * @param sparkSession
   * @return dataframe
   */
  def load(format: Format, path: String, schema: StructType)(implicit sparkSession: SparkSession): DataFrame = {
    logger.debug("loading dataframe from {}", path)

    val filesToProcess = path.split(",")
    logger.debug("total files to process {}", filesToProcess.length)

    val reader = sparkSession.read.option("inferTimestamp", "false").option("prefersDecimal", "false")

    if (schema != null) {
      if (format == Format.hdf) throw new UnsupportedOperationException("hdf format not supported yet!")
      else {
        if (filesToProcess.length > 1)
          reader.schema(schema).format(format.toString).load(filesToProcess: _*)
        else
          reader.schema(schema).format(format.toString).load(filesToProcess(0))
      }
    }
    else {
      if (filesToProcess.length > 1)
        reader.format(format.toString).load(filesToProcess: _*)
      else
        reader.format(format.toString).load(filesToProcess(0))
    }
  }

  def sanitizeColumnNames(df: DataFrame): DataFrame = {
    df.schema.fields
      .flatMap(f => {
        sanitizeColumnNames(f.dataType, col(f.name), f.name) match {
          case Some(c) => Some(f.name, c)
          case None => None
        }
      })
      .foldLeft(df) {
        case (df, (colName, col)) => df.withColumn(colName, col)
      }
  }

  private def sanitizeColumnNames(columnType: DataType, column: Column, fieldName: String = ""): Option[Column] = {
    logger.debug("processing: {}", column)
    columnType match {
      case st: StructType =>
        val sc = struct(st
          .fields
          .flatMap(cf =>
            sanitizeColumnNames(cf.dataType, column.getField(cf.name), s"${cf.name}") match {
              case Some(c) => Some(c)
              case None => Some(column.getField(cf.name).alias(cf.name))
            }): _*)
        logger.debug("finished fullName: {} col: {}", fieldName, sc)
        Some(sc.alias(fieldName))
      case at: ArrayType =>
        at.elementType match {
          case atSt: StructType => None //@Todo Fix me
          case _ => sanitizeColumnNames(at.elementType, column, fieldName)
        }
      case _ =>
        illegalCharsRegex.replaceAllIn(fieldName, "_") match {
          case s: String => if (s.equals(fieldName)) None else Some(column.alias(s))
          case _ => None
        }
    }
  }

  def walk(df: DataFrame): Unit = {
    logger.info("df columns: {}", df.columns)
    df.schema.fields.foreach(f => walk(f.dataType, col(f.name), f.name))
  }

  private def walk(columnType: DataType, column: Column, fullName: String): Unit = {
    columnType match {
      case st: StructType =>
        logger.info("st -> field: {} col: {}", fullName, column)
        st.fields.foreach(cf => walk(cf.dataType, column.getField(cf.name), s"$fullName.${cf.name}"))
      case at: ArrayType =>
        logger.info("at -> field: {} col: {}", fullName, column)
        walk(at.elementType, column, fullName)
      case _ => logger.debug("field: {} col: {} type: {}", fullName, column, columnType.typeName)
    }
  }

  def dropFields(df: DataFrame, fieldNames: Seq[String]): DataFrame = {
    logger.debug("dropping fields {}", fieldNames)
    if (fieldNames == null || fieldNames.isEmpty) return df

    val fieldMap = toFieldMap(fieldNames)

    val altered = fieldMap
      .filterNot(kv => kv._1.contains('.'))
      .map(e => (e._1, dropFields(df, e._1, e._2)))
      .foldLeft(df)((df, e) =>
        e._2 match {
          case Some(x) =>
            val fieldName = e._1.split('.')(0)
            df.withColumn(fieldName, x)
          case None => df.drop(e._1)
        })

    val fieldsToDrop = fieldMap
      .filter(kv => kv._1.contains('.'))
      .foldLeft(mutable.Seq[String]())((s, kv) => {
        kv._2.foldLeft(s)((s, e) => s :+ s"${kv._1}.$e")
      })

    dropNestedFields(altered, fieldsToDrop: _*)
  }

  private def dropFields(df: DataFrame, srcField: String, fieldsToDrop: Seq[String]): Option[Column] = {
    logger.info("going to drop {} from {}", fieldsToDrop, srcField)
    if (!srcField.contains('.')) {
      if (fieldsToDrop.nonEmpty) {
        val st = structType(df, srcField)
        if (st.isSuccess) {
          return Some(struct(st.get.fields
            .filterNot(f => fieldsToDrop.contains(f.name))
            .flatMap(f => Some(df.col(srcField).alias(f.name))): _*))
        }
      }
    }
    None
    //    else {
    //      val parent = srcField.substring(0, srcField.indexOf('.'))
    //      val parentStruct = structType(df, parent)
    //
    //      logger.info("found {}: {}", srcField, parentStruct)
    //      if (parentStruct.isSuccess) {
    //        val parentDF = df.select(s"$parent.*")
    //        logger.info("childDF {} schema: {}", parent, parentDF.schema.treeString)
    //
    //        val childSrcField = srcField.substring(srcField.indexOf('.') + 1)
    //
    //        val childFields = dropFields(parentDF, childSrcField, fieldsToDrop, sanitizeNames)
    //
    //        if (!childFields.isDefined) {
    //          return None
    //        }
    //
    //        val keepFields = parentStruct.get.fieldNames.diff(Seq(childSrcField))
    //
    //        // @TODO: will this work in deeply nested case where df will be not the root df but parent df
    //        val selectedColumns = keepFields.map(c => df.col(c))
    //
    //        logger.info("selected columns: {}", selectedColumns)
    //        if (childFields.isDefined)
    //          Some(struct(selectedColumns: _*))
    //        else {
    //          None
    //          //          val appendField = struct(childFields.get: _*).as(srcField)
    //          //          logger.info("appended: {}", appendField)
    //          //          Some(selectedColumns :+ appendField)
    //          //val result = selectedColumns.foldLeft(mutable.Seq(struct(childFields.get: _*)))((s, c) => s :+ c)
    //          //Some(result)
    //        }
    //      }
    //      else
    //        None
    //    }
  }

  private def structType(df: DataFrame, columnName: String) =
    Try(df.schema.fields.filter(f => f.name.equals(columnName)).head.dataType.asInstanceOf[StructType])

  private def dropNestedFields(dataFrame: DataFrame, dropColNames: String*): DataFrame = {
    dropColNames.foldLeft(dataFrame) { (df, colName) =>
      df.schema.fields
        .flatMap(f => {
          if (colName.startsWith(s"${f.name}.")) {
            dropSubNestedFields(col(f.name), f.dataType, f.name, colName) match {
              case Some(x) => Some((f.name, x))
              case None => None
            }
          } else {
            None
          }
        })
        .foldLeft(df.drop(colName)) {
          case (df, (colName, column)) => {
            df.withColumn(colName, column)
          }
        }
    }
  }

  private def dropSubNestedFields(col: Column, colType: DataType, fullColName: String, dropColName: String): Option[Column] = {
    logger.info("dropSubColumn called col: {} fullColName: {}", col, fullColName)
    if (fullColName.equals(dropColName)) {
      None
    } else {
      colType match {
        case colType: StructType =>
          if (dropColName.startsWith(s"${fullColName}.")) {
            logger.info("dropSubColumn calling struct {} -> {}", fullColName, col)
            val x = structColumn(colType, col, fullColName, dropColName)
            logger.info("dropSubColumn finished struct {} -> {}", fullColName, x)
            Some(x)
          } else {
            logger.info("else dropSubColumn {} -> {}", fullColName, col)
            Some(col)
          }
        //        case colType: ArrayType =>
        //          colType.elementType match {
        //            case innerType: StructType => Some(structColumn(innerType, col, fullColName, dropColName))
        //          }
        case other =>
          logger.info("dropSubColumn in other {} -> {}", fullColName, col)
          Some(col)
      }
    }
  }

  private def structColumn(colType: StructType, col: Column, fullColName: String, dropColName: String) = {
    logger.info("structColumn called col: {} fullColName: {}", col, fullColName)
    val st = struct(
      colType.fields
        .flatMap(f => {
          logger.info("struct calling dropSubColumn f.name {} fullColName {} -> {}", f.name, fullColName, col)
          dropSubNestedFields(col.getField(f.name), f.dataType, s"${fullColName}.${f.name}", dropColName) match {
            case Some(x) => {
              val aliasCol = x.alias(f.name)
              logger.info("struct f.name {} fullColName {} -> orig {} alias {}", f.name, fullColName, x, aliasCol)
              Some(aliasCol)
            }
            case None =>
              logger.info("None struct {} -> {}", fullColName, col)
              None
          }
        }): _*)
    st
  }

  private[eco] def toFieldMap(fieldNames: Seq[String]): Map[String, Seq[String]] = {
    fieldNames
      .foldLeft(Node())((n, pattern) => n.addChild(pattern)).branches()
      .map(branch => {
        val lastIdx = branch.lastIndexOf('.')
        if (lastIdx > -1)
          (branch.substring(0, lastIdx), branch.substring(lastIdx + 1))
        else
          (branch, null)
      })
      .foldLeft(mutable.Map[String, mutable.Seq[String]]())((m, e) => {
        val seq = m.getOrElseUpdate(e._1, mutable.Seq())
        if (StringUtils.isNoneBlank(e._2))
          m += e._1 -> (seq :+ e._2)
        m
      }).toMap
  }
}


/**
 * Node class is used in dropColumn implementation of DataFrame
 *
 * @param name   the name of the node
 * @param parent the name of the parent node
 */
case class Node(name: String = "", parent: Node = null) {
  val logger = Logger(getClass)

  private val children = mutable.Map[String, Node]()

  def addChild(name: String): Node = {
    if (StringUtils.isBlank(name)) return this
    val childName = if (name.endsWith(".*")) name else s"$name.*"
    addChildTo(this, childName)
    this
  }

  def childrenCount = children.size

  def childByName(childName: String) = children.get(childName)

  def isLeaf = children.isEmpty

  def isRoot = parent == null

  def traverse(): Seq[String] = {
    children.values.foldLeft(Seq[String]())((s, n) => {
      if (!n.isLeaf) {
        val s1 = s :+ n.name
        s1 ++ n.traverse()
      }
      else s :+ n.name
    })
  }

  def leaves(): Seq[Node] = {
    children.values.foldLeft(Seq[Node]())((a, n) => {
      if (!n.isLeaf) a ++ n.leaves() else a :+ n
    })
  }

  def absolutePath(delimiter: String = "."): String = {
    if (isRoot) delimiter + name
    else if (parent.isRoot) parent.absolutePath() + name
    else parent.absolutePath() + delimiter + name
  }

  override def toString: String = s"$name -> ${parent.name}"

  def branches(delimiter: String = "."): Seq[String] = {
    leaves().
      foldLeft(Seq[String]())((s, n) => s :+ n.absolutePath().substring(1)).
      map(p => if (p.endsWith(".*")) p.substring(0, p.lastIndexOf('.')) else p)
  }

  private def addChildTo(node: Node, child: String): Node = {
    val branches = if (node == null) children else node.children
    val parts = child.split('.')
    var head = branches.getOrElseUpdate(parts(0), Node(parts(0), node))
    Range(1, parts.length).foreach(i => {
      if (parts(i).equals("*")) {
        head.children.clear()
        addChildTo(head, "*")
      }
      else if (!head.children.contains("*")) head = addChildTo(head, parts(i))
    })

    head
  }
}

