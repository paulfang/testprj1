package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Column, SparkSession}

object DataExporter extends App {
  private val logger = Logger(DataExporter.getClass)

  if (args.isEmpty) {
    logger.warn("Nothing to do....no command line arguments passed")
  }
  else {
    implicit val conf = DataExportConfig(args)

    if (conf.query.isEmpty || conf.tables.isEmpty || conf.outputLocation.isEmpty) {
      logger.info("query or tables used for query undefined")
    }
    else {
      implicit val spark = SparkSession.builder()
        .config(new SparkConf().setAppName("data exporter"))
        .getOrCreate()

      conf.tables.foreach(e => {
        var df = spark.read.format(Format.parquet.toString).load(e._2)
        df =
          if (conf.selectColumns.contains(e._1)) df.select(conf.selectColumns(e._1) :_*)
          else df

        df =
          if (conf.dropColumns.contains(e._1)) df.drop(conf.dropColumns(e._1) :_*)
          else df

        df.createOrReplaceTempView(e._1)
      })
      var writer = spark.sql(conf.query).write.mode(conf.mode).format(conf.format)
      if (conf.format.equalsIgnoreCase("csv")) {
        writer = writer.option("quoteAll", true).option("compression", "gzip")
      }

      if (conf.partitions.nonEmpty) {
        writer = writer.partitionBy(conf.partitions :_*)
      }

      writer.save(conf.outputLocation)
    }
  }
}

case class DataExportConfig(args: Array[String]) {
  var query: String = ""
  var outputLocation: String = ""
  var partitions: Array[String] = Array()
  var tables: Map[String, String] = Map()
  var format: String = "parquet"
  var mode: String = "overwrite"
  var dropColumns: Map[String, Seq[String]] = Map()
  var selectColumns: Map[String, Seq[Column]] = Map()


  args.foreach(a => {
    val kv = a.split("==")
    if (kv.length>1) {
      kv(0).trim match {
        case c@"query" => query = kv(1).trim
        case c@"output_location" => outputLocation = kv(1).trim
        case c@"partitions" => partitions = kv(1).split(",")
        case c@"table" => val nameVal = kv(1).split("=")
          tables += (nameVal(0).trim -> nameVal(1).trim)
        case c@"drop" => val nameVal = kv(1).split("=")
          dropColumns += (nameVal(0).trim -> nameVal(1).split(",").foldLeft(Seq[String]())((s,c) => s :+ c.trim))
        case c@"select" => val nameVal = kv(1).split("=")
          selectColumns += (nameVal(0).trim -> nameVal(1).split(",").foldLeft(Seq[Column]())((s,c) => s :+ col(c.trim)))
        case c@"format" => format = kv(1).trim
        case c@"mode" => mode = kv(1).trim
      }
    }
  })
}