package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

object PartitionUtil extends App {
  private val logger = Logger(Vacuum.getClass)

  if (args.isEmpty) {
    logger.warn("Nothing to do....no command line arguments passed")
  }
  else {
    val conf = TransformerConfig(args :_*)
    val spark = SparkSession.builder()
      .config(new SparkConf().setAppName(conf.name))
      .getOrCreate()

    val df = spark.read.format("parquet").load(conf.inputLocation)

    val writer = df.write.format("parquet").mode("overwrite")
    if (conf.partitions.isEmpty)
      writer.save(conf.outputLocation)
    else
      writer.partitionBy(conf.partitions: _*).save(conf.outputLocation)
  }
}

case class TransformerConfig(args: String*) {
  val inputLocation:String = args(0)
  val outputLocation:String = args(1)
  val partitions:Seq[String] = if (args.length>2) args(2).split(",") else Seq.empty

  val name: String = "transformer"
}
