from typing import Iterable
import logging
from datetime import datetime, timezone
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.providers.amazon.aws.hooks.s3 import S3Hook

log = logging.getLogger('S3Operator')

template_fields: Iterable[str] = ('bucket', 'prefix', 'suffix', 'object_type', 'modified_since')


class S3Operator(BaseOperator):
    @apply_defaults
    def __init__(self, *, bucket: str,
                 prefix='',
                 suffix='',
                 object_type='',
                 delimiter='/',
                 modified_since: datetime = datetime(1970, 1, 1, 0, 0, tzinfo=timezone.utc),
                 aws_conn_id='aws_default',
                 verify=None,
                 **kwargs) -> None:

        super().__init__(**kwargs)
        self.bucket = bucket
        self.prefix = prefix or ''
        self.suffix = suffix or ''
        self.object_type = object_type or ''
        self.delimiter = delimiter or '/'
        self.modified_since = modified_since
        self.aws_conn_id = aws_conn_id
        self.verify = verify

    def execute(self, context):
        hook = S3Hook(aws_conn_id=self.aws_conn_id, verify=self.verify)
        log.info(
            'Getting the list of files from bucket: {} in prefix: {} of object_type: {} with suffix: {} since: {}'.format(
                self.bucket,
                self.prefix,
                self.object_type,
                self.suffix,
                self.modified_since)
        )
        config = {
            'PageSize': None,
            'MaxItems': None,
        }

        paginator = hook.get_conn().get_paginator('list_objects_v2')
        response = paginator.paginate(
            Bucket=self.bucket,
            Delimiter=self.delimiter,
            Prefix=self.prefix,
            PaginationConfig=config
        )

        keys = []
        for page in response:
            if 'Contents' in page:
                for k in page['Contents']:
                    if k['LastModified'] > self.modified_since:
                        key = k['Key']
                        obj_name = key[key.rindex(self.delimiter)+1:]
                        log.info('obj_name: {}'.format(obj_name))
                        if obj_name.startswith(self.object_type) and obj_name.endswith(self.suffix):
                            keys.append(
                                {'Key': key,
                                 'LastModified': k['LastModified'].timestamp(),
                                 'ETag': k['ETag'],
                                 'Size': k['Size']}
                            )

        log.info('total keys to return: {}'.format(len(keys)))
        return keys if keys else None
