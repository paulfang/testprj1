from typing import Iterable, Iterator, Optional
import logging
import io
from datetime import datetime, timezone
from airflow.models import BaseOperator
from airflow.utils.decorators import apply_defaults
from airflow.providers.amazon.aws.hooks.s3 import S3Hook
from airflow.providers.postgres.hooks.postgres import PostgresHook
from contextlib import closing

log = logging.getLogger('S3Operator')

template_fields: Iterable[str] = ('bucket', 'prefix', 'suffix', 'object_type', 'modified_since')


class StringIteratorIO(io.TextIOBase):
    def __init__(self, iter: Iterator[str]):
        self._iter = iter
        self._buff = ''

    def readable(self) -> bool:
        return True

    def _read1(self, n: Optional[int] = None) -> str:
        while not self._buff:
            try:
                self._buff = next(self._iter)
            except StopIteration:
                break
        ret = self._buff[:n]
        self._buff = self._buff[len(ret):]
        return ret

    def read(self, n: Optional[int] = None) -> str:
        line = []
        if n is None or n < 0:
            while True:
                m = self._read1()
                if not m:
                    break
                line.append(m)
        else:
            while n > 0:
                m = self._read1(n)
                if not m:
                    break
                n -= len(m)
                line.append(m)
        return ''.join(line)


class S3PgSyncOperator(BaseOperator):
    @apply_defaults
    def __init__(self, *, bucket: str,
                 prefix='',
                 suffix='',
                 object_type='',
                 delimiter='/',
                 aws_conn_id='aws_default',
                 pg_conn_id='inquire_bronze',
                 verify=False,
                 **kwargs) -> None:

        super().__init__(**kwargs)
        self.bucket = bucket
        self.prefix = prefix or ''
        self.suffix = suffix or ''
        self.object_type = object_type or ''
        self.delimiter = delimiter or '/'
        self.aws_conn_id = aws_conn_id
        self.pg_conn_id = pg_conn_id
        self.verify = verify

    def get_last_sync_dt(self, pg_hook):
        with closing(pg_hook.get_conn()) as conn:
            conn.autocommit = False
            with closing(conn.cursor()) as cursor:
                cursor.execute("select last_sync_ts from last_sync where obj_type = '{}'".format(self.object_type))
                row = cursor.fetchone()

                last_sync_dt = datetime.utcfromtimestamp(row[0]).replace(tzinfo=timezone.utc) if row else datetime(1970,
                                                                                                                   1, 1,
                                                                                                                   0, 0,
                                                                                                                   tzinfo=timezone.utc)

                return last_sync_dt

    def update_sync_table(self, records, sync_ts, pg_hook):
        log.info("update_sync_table start sync_ts {} num records: {}".format(sync_ts, len(records)))
        st = datetime.now().timestamp()
        with closing(pg_hook.get_conn()) as conn:
            conn.autocommit = False
            with closing(conn.cursor()) as cursor:
                cursor.executemany("""
                    INSERT INTO s3_files (obj_name, obj_type, size, etag, obj_last_modified, created_at) VALUES (
                        %(Key)s,
                        %(obj_type)s,
                        %(Size)s,
                        %(ETag)s,
                        %(LastModified)s,
                        %(created_at)s
                    );    
                """, ({
                    **rec, 'obj_type': self.object_type, 'created_at': sync_ts
                } for rec in records))

            with closing(conn.cursor()) as cursor:
                stmt = "delete from last_sync where obj_type = '{}'; insert into last_sync values('{}',{})".format(
                    self.object_type, self.object_type, sync_ts)
                cursor.execute(stmt)

            conn.commit()
        log.info("update_sync_table completes in {} sec".format(datetime.now().timestamp() - st))

    def execute(self, context):
        curr_ts = int(datetime.now(tz=timezone.utc).timestamp())
        pg_hook = PostgresHook(postgres_conn_id=self.pg_conn_id)

        last_sync_dt = self.get_last_sync_dt(pg_hook)

        s3_hook = S3Hook(aws_conn_id=self.aws_conn_id, verify=self.verify)

        log.info(
            'Getting the list of files from bucket: {} in prefix: {} of object_type: {} with suffix: {} since: {}'.format(
                self.bucket,
                self.prefix,
                self.object_type,
                self.suffix,
                last_sync_dt)
        )

        config = {
            'PageSize': None,
            'MaxItems': None,
        }

        paginator = s3_hook.get_conn().get_paginator('list_objects_v2')
        response = paginator.paginate(
            Bucket=self.bucket,
            Delimiter=self.delimiter,
            Prefix=self.prefix,
            PaginationConfig=config
        )

        records = []
        for page in response:
            if 'Contents' in page:
                for k in page['Contents']:
                    if k['LastModified'] > last_sync_dt:
                        key = k['Key']
                        obj_name = key[key.rindex(self.delimiter) + 1:]
                        log.debug('obj_name: {}'.format(obj_name))
                        if obj_name.startswith(self.object_type) and obj_name.endswith(self.suffix):
                            records.append(
                                {'Key': key,
                                 'LastModified': int(k['LastModified'].replace(tzinfo=timezone.utc).timestamp()),
                                 'ETag': k['ETag'],
                                 'Size': k['Size']}
                            )

        log.info('total records modified since {}: {}'.format(last_sync_dt, len(records)))

        self.update_sync_table(records, curr_ts, pg_hook)
        return len(records) if records else None
