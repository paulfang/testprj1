import os
import json
from datetime import date, timedelta
import logging

from airflow import DAG
from airflow.models import Variable
from airflow.utils.helpers import chain
from airflow.operators.dummy_operator import DummyOperator
from airflow.providers.amazon.aws.operators.emr_create_job_flow import EmrCreateJobFlowOperator
from airflow.providers.amazon.aws.operators.emr_terminate_job_flow import EmrTerminateJobFlowOperator
from airflow.providers.amazon.aws.operators.emr_add_steps import EmrAddStepsOperator
from airflow.providers.amazon.aws.sensors.emr_job_flow import EmrJobFlowSensor
from airflow.providers.amazon.aws.sensors.emr_step import EmrStepSensor
from airflow.utils.dates import days_ago


logging.info("subscriber step: {}".format("{{ execution.date }}"))

yesterday = date.today() - timedelta(days=1)
date_to_process = '{}/{}/{}'.format(yesterday.year, yesterday.month, yesterday.day)
s3_location = 's3://eco.inquire.frontier'


DEFAULT_ARGS = {
    'owner': 'Commscope',
    'depends_on_past': False,
    'email': ['sanjeev.mishra@commscope.com'],
    'email_on_failure': False,
    'email_on_retry': False
}

DAG_HOME = os.getenv('dag_folder', '{}/airflow/dags/'.format(os.getenv('HOME')))
logging.info("dag home: {}".format(DAG_HOME))


with open('{}/resources/bronze/launchCluster.json'.format(DAG_HOME)) as fl:
    JOB_FLOW_OVERRIDES = json.load(fl)

COMMON_EXT = [
    'output_location={}/airflow/warehouse/bronze/'.format(s3_location),
    'spark.sql.warehouse.dir={}/airflow/warehouse/bronze/'.format(s3_location)
]

with open('{}/resources/bronze/subscriber-step.json'.format(DAG_HOME)) as fl:
    SUBSCRIBER_STEP = json.load(fl)
    args = SUBSCRIBER_STEP['HadoopJarStep']['Args']
    args.extend(COMMON_EXT)
    args.append('input_location={}/data/{}/{}/{}/subscriber'.format(s3_location,
                                                                    yesterday.year,
                                                                    yesterday.month,
                                                                    yesterday.day))


with open('{}/resources/bronze/dailyparams-step.json'.format(DAG_HOME)) as fl:
    DAILYPARAMS_STEP = json.load(fl)
    args = DAILYPARAMS_STEP['HadoopJarStep']['Args']
    args.extend(COMMON_EXT)
    args.append('input_location={}/data/{}/{}/{}/dailyparams'.format(s3_location,
                                                                     yesterday.year,
                                                                     yesterday.month,
                                                                     yesterday.day))

logging.info("dailyparams step: {}".format(DAILYPARAMS_STEP))

with DAG(
        dag_id='inquire_bronze_processing',
        default_args=DEFAULT_ARGS,
        dagrun_timeout=timedelta(hours=2),
        start_date=days_ago(2),
        schedule_interval='0 3 * * *',
        tags=['inquire', 'bronze'],
) as dag:
    job_flow_creator = EmrCreateJobFlowOperator(
        task_id='launch_emr_cluster',
        job_flow_overrides=JOB_FLOW_OVERRIDES,
        aws_conn_id='aws_default',
        emr_conn_id='emr_default'
    )

    job_flow_sensor = EmrJobFlowSensor(
        task_id='check_cluster',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        target_states=['RUNNING', 'WAITING'],
        aws_conn_id='aws_default'
    )

    subscriber_step = EmrAddStepsOperator(
        task_id='process_subscriber',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        aws_conn_id='aws_default',
        steps=[SUBSCRIBER_STEP],
    )

    subscriber_sensor = EmrStepSensor(
        task_id='watch_subscriber',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        step_id="{{ task_instance.xcom_pull(task_ids='process_subscriber', key='return_value')[0] }}",
        aws_conn_id='aws_default'
    )

    dailyparams_step = EmrAddStepsOperator(
        task_id='process_dailyparams',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        aws_conn_id='aws_default',
        steps=[DAILYPARAMS_STEP],
    )

    dailyparams_sensor = EmrStepSensor(
        task_id='watch_dailyparams',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        step_id="{{ task_instance.xcom_pull(task_ids='process_dailyparams', key='return_value')[0] }}",
        aws_conn_id='aws_default'
    )

    join = DummyOperator(task_id='join', dag=dag)

    job_flow_terminator = EmrTerminateJobFlowOperator(
        task_id='terminate_emr_cluster',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        aws_conn_id='aws_default',
        trigger_rule="all_done"
    )

    chain(job_flow_creator, job_flow_sensor, [subscriber_step, dailyparams_step], [subscriber_sensor, dailyparams_sensor],  join,  job_flow_terminator)



