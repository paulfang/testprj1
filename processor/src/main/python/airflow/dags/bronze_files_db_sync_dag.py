from datetime import timedelta, datetime
from airflow import DAG
from airflow.models import Variable
from airflow.utils.helpers import chain
from airflow.operators.dummy_operator import DummyOperator
from customer_operators.s3_pg_sync_operator import S3PgSyncOperator
from airflow.utils.dates import days_ago
from airflow.providers.amazon.aws.hooks.s3 import S3Hook

import logging

"""
## bronze_files_db_sync
### This assumes airflow to have following variables defined
bronze_source_s3_bucket = eco-inquire-3
bronze_source_s3_prefix = frontier/negotiator/export/ 

This dag syncs all the files found in s3_buckets folder prefixed by the above prefix in postgres db (s3_files).

It picks only those files that are modified since the last sync ts, found in last_sync table.

It accepts object_type, such as subscriber, that is used as file_name prefix - all the files starting with subscriber
will be synced to s3_files categorized by (obj_type column) object_type. 
"""

BUCKET_NAME = Variable.get('bronze_source_s3_bucket')
PREFIX = Variable.get('bronze_source_s3_prefix')

logging.info("bucket_name: {} prefix: {}".format(BUCKET_NAME, PREFIX))


def test_conn():
    s3_hook = S3Hook(aws_conn_id='aws_default')
    keys = s3_hook.list_keys(bucket_name=BUCKET_NAME, prefix=PREFIX, delimiter='/')
    print(keys)


DEFAULT_ARGS = {
    'owner': 'Commscope',
    'depends_on_past': False,
    'email': ['sanjeev.mishra@commscope.com'],
    'email_on_failure': True,
    'email_on_retry': False,
    'retries': 0
}


def get_last_execution_time(**kwargs):
    logging.info('last execution: {} in ts: {}'.format(kwargs.get('execution_date'), kwargs.get('ts')))
    return kwargs.get('ts')


with DAG(
        dag_id='bronze_files_db_sync',
        default_args=DEFAULT_ARGS,
        dagrun_timeout=timedelta(hours=2),
        start_date=days_ago(2),
        # start_date=this_hour,
        schedule_interval='*/30 * * * *',
        catchup=False,
        tags=['inquire', 'bronze'],
) as dag:
    start = DummyOperator(task_id='start')

    test_conn()
    sync_subs = S3PgSyncOperator(
        task_id='sync_subscribers',
        bucket=BUCKET_NAME,
        prefix=PREFIX,
        object_type='subscriber',
        aws_conn_id='aws_default',
        pg_conn_id='inquire_bronze'
    )

    sync_dailyparams = S3PgSyncOperator(
        task_id='sync_dailyparams',
        bucket=BUCKET_NAME,
        prefix=PREFIX,
        object_type='dailyparams',
        aws_conn_id='aws_default',
        pg_conn_id='inquire_bronze'
    )

    sync_element_event = S3PgSyncOperator(
        task_id='sync_element-event',
        bucket=BUCKET_NAME,
        prefix=PREFIX,
        object_type='element-event',
        aws_conn_id='aws_default',
        pg_conn_id='inquire_bronze'
    )

    sync_firmware = S3PgSyncOperator(
        task_id='sync_firmware',
        bucket=BUCKET_NAME,
        prefix=PREFIX,
        object_type='firmware',
        aws_conn_id='aws_default',
        pg_conn_id='inquire_bronze'
    )

    sync_host_table = S3PgSyncOperator(
        task_id='sync_host-table',
        bucket=BUCKET_NAME,
        prefix=PREFIX,
        object_type='host-table',
        aws_conn_id='aws_default',
        pg_conn_id='inquire_bronze'
    )

    sync_kpi_ts = S3PgSyncOperator(
        task_id='sync_kpi-ts',
        bucket=BUCKET_NAME,
        prefix=PREFIX,
        object_type='kpi-ts',
        aws_conn_id='aws_default',
        pg_conn_id='inquire_bronze'
    )

    sync_element_ts = S3PgSyncOperator(
        task_id='sync_element-ts',
        bucket=BUCKET_NAME,
        prefix=PREFIX,
        object_type='element-ts',
        aws_conn_id='aws_default',
        pg_conn_id='inquire_bronze'
    )

    join = DummyOperator(task_id='join', trigger_rule="all_done")
    end = DummyOperator(task_id='end')

    chain(
        start,
        [sync_subs, sync_dailyparams, sync_element_event, sync_firmware, sync_host_table, sync_element_ts, sync_kpi_ts],
        join,
        end
    )

    #subs_diff_files
    #subs_files >> subs_diff_files >> dp_files


