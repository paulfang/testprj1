from datetime import timedelta
from airflow import DAG
from airflow.utils.helpers import chain
from airflow.operators.dummy_operator import DummyOperator
from airflow.providers.amazon.aws.operators.s3_list import S3ListOperator
from airflow.providers.amazon.aws.hooks.S3 import S3Hook
from airflow.providers.postgres.operators.postgres import PostgresOperator
from airflow.utils.dates import days_ago

import logging

bucket_name = 'eco.inquire-3'

DEFAULT_ARGS = {
    'owner': 'Commscope',
    'depends_on_past': False,
    'email': ['smishra@commscope.com'],
    'email_on_failure': False,
    'email_on_retry': False
}

def list_files(aws_conn_id, bucket_name, prefix,):
    hook = S3Hook(aws_co)


def get_last_execution_time(**kwargs):
    logging.info('last execution: {} in ts: {}'.format(kwargs.get('execution_date'), kwargs.get('ts')))
    return kwargs.get('ts')


with DAG(
        dag_id='raw_file_inventory',
        default_args=DEFAULT_ARGS,
        dagrun_timeout=timedelta(hours=2),
        start_date=days_ago(2),
        schedule_interval='0 3 * * *',
        tags=['inquire', 'bronze'],
) as dag:
    # start = DummyOperator(task_id='start', dag=dag)

    # subs_files = S3ListOperator(
    #     task_id='list_subs_files',
    #     bucket=bucket_name,
    #     prefix='frontier/negotiator/export/subscriber-*.json.gz',
    #     delimiter='',
    #     aws_conn_id='aws_default'
    # )

    subs_diff_files = S3ListOperator(
        task_id='list_subs-diff_files',
        bucket=bucket_name,
        prefix='frontier/negotiator/export/',
        delimiter='dailyparams-*.gz',
        aws_conn_id='aws_default'
    )

    # dp_files = S3ListOperator(
    #     task_id='list_dailyparams_files',
    #     bucket=bucket_name,
    #     prefix='frontier/negotiator/export/dailyparams-*.json.gz',
    #     aws_conn_id='aws_default',
    #     delimiter=''
    # )

    # firmware_files = S3ListOperator(
    #     task_id='list_firmware_files',
    #     bucket=s3_location,
    #     prefix='frontier/negotiator/export/firmware-*',
    #     aws_conn_id='aws_default'
    # )
    #
    # host_table_files = S3ListOperator(
    #     task_id='list_host-table_files',
    #     bucket=s3_location,
    #     prefix='frontier/negotiator/export/host-table-*',
    #     aws_conn_id='aws_default'
    # )
    #
    # element_event_files = S3ListOperator(
    #     task_id='list_element-event_files',
    #     bucket=s3_location,
    #     prefix='frontier/negotiator/export/element-event-*',
    #     aws_conn_id='aws_default'
    # )
    #
    # kpi_files = S3ListOperator(
    #     task_id='list_kpi_files',
    #     bucket=s3_location,
    #     prefix='frontier/negotiator/export/kpi-ts-data-*',
    #     aws_conn_id='aws_default'
    # )
    #
    # ets_files = S3ListOperator(
    #     task_id='list_element-ts_files',
    #     bucket=s3_location,
    #     prefix='frontier/negotiator/export/element-ts-data-*',
    #     aws_conn_id='aws_default'
    # )
    #
    # diag_files = S3ListOperator(
    #     task_id='list_diagnostic-ts_files',
    #     bucket=s3_location,
    #     prefix='frontier/negotiator/export/diagnostic-ts-*',
    #     aws_conn_id='aws_default'
    # )
    #
    # end = DummyOperator(task_id='end', dag=dag)

# postgres_sync = PostgresOperator(
    #     postgres_conn_id="inquire_bronze",
    #     sql=bulk_copy_sql,
    #     database='inquire_cu'
    # )


    subs_diff_files
    #subs_files >> subs_diff_files >> dp_files


