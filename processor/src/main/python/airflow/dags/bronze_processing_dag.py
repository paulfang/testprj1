import os
import json
from datetime import date, timedelta, datetime
import logging

from airflow import DAG
from airflow.models import Variable

from airflow.utils.helpers import chain
from airflow.operators.python_operator import ShortCircuitOperator, PythonOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.providers.amazon.aws.operators.emr_create_job_flow import EmrCreateJobFlowOperator
from airflow.providers.amazon.aws.operators.emr_terminate_job_flow import EmrTerminateJobFlowOperator
from airflow.providers.amazon.aws.operators.emr_add_steps import EmrAddStepsOperator
from airflow.providers.amazon.aws.sensors.emr_job_flow import EmrJobFlowSensor
from airflow.providers.amazon.aws.sensors.emr_step import EmrStepSensor
from airflow.providers.postgres.hooks.postgres import PostgresHook
from airflow.utils.dates import days_ago
from contextlib import closing


yesterday = date.today() - timedelta(days=1)
date_to_process = '{}/{}/{}'.format(yesterday.year, yesterday.month, yesterday.day)
s3_location = 's3://eco.inquire.frontier'

DEFAULT_ARGS = {
    'owner': 'Commscope',
    'depends_on_past': False,
    'email': ['sanjeev.mishra@commscope.com'],
    'email_on_failure': False,
    'email_on_retry': False
}

DAG_HOME = os.getenv('dag_folder', '{}/airflow/dags/'.format(os.getenv('HOME')))
logging.info("dag home: {}".format(DAG_HOME))

with open('{}/resources/bronze/launchCluster.json'.format(DAG_HOME)) as fl:
    JOB_FLOW_OVERRIDES = json.load(fl)

COMMON_EXT = [
    'output_location={}/airflow/warehouse/bronze/'.format(s3_location),
    'spark.sql.warehouse.dir={}/airflow/warehouse/bronze/'.format(s3_location)
]


def files_to_process(obj_type):
    pg_hook = PostgresHook(postgres_conn_id="inquire_bronze")
    with closing(pg_hook.get_conn()) as conn:
        with closing(conn.cursor()) as cursor:

            # get list of files that are not processed
            sql = "select obj_name from s3_files where status is null and obj_type = '{}'".format(obj_type)
            cursor.execute(sql)
            file_names = [r[0] for r in cursor.fetchall()]
            logging.info("obj_type {} count {}".format(obj_type, len(file_names)))
            return file_names


def update_status(file_names=[], status='s', processing_ts=datetime.now().timestamp()):
    """
    updates the status flag in the s3_files table.

    We don't check the file_names to be empty because this call follows the ShortCircuitOperator and that
    will make sure that this function is never called if there are no files.

    :param file_names: the obj_name in s3_files table
    :param status: the status value
    :param processing_ts: this will be the modified_at in the table
    :return:
    """

    pg_hook = PostgresHook(postgres_conn_id="inquire_bronze")
    with closing(pg_hook.get_conn()) as conn:
        with closing(conn.cursor()) as cursor:
            cursor.executemany("""
                    UPDATE s3_files 
                    SET status = %(status)s,
                        modified_at = %(modified_at)s
                    WHERE obj_name = %(obj_name)s;    
                """, ({
                'obj_name': f, 'status': status, 'modified_at': processing_ts
            } for f in file_names))

        logging.info("update count {}".format(len(file_names)))
        conn.commit()


def create_processing_step(obj_type):
    _processed_ts = datetime.now().timestamp()
    _files_to_process = files_to_process(obj_type)

    _short_circuit = ShortCircuitOperator(
        task_id='skip_{}_if_no_files'.format(obj_type),
        provide_context=False,
        python_callable=lambda: len(_files_to_process) > 0,
        )

    with open('{}/resources/bronze/{}-step.json'.format(DAG_HOME, obj_type)) as fl:
        _STEP = json.load(fl)
        args = _STEP['HadoopJarStep']['Args']
        args.extend(COMMON_EXT)
        args.append('input_location={}/data/{}/{}/{}/{}'.format(s3_location,
                                                                yesterday.year,
                                                                yesterday.month,
                                                                yesterday.day,
                                                                obj_type))
    _step = EmrAddStepsOperator(
            task_id='process_{}'.format(obj_type),
            job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
            aws_conn_id='aws_default',
            steps=[_STEP],
            )

    _update_status = PythonOperator(
        task_id='update_{}_status'.format(obj_type),
        python_callable=update_status,
        trigger_rule=one_success,
        op_kwargs={'file_names': _files_to_process, 'status': 's', 'processing_ts': _processed_ts}
    )

    return _short_circuit, _step


def create_step_sensor(obj_type):
    return EmrStepSensor(
        task_id='watch_{}'.format(obj_type),
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        step_id="{{ task_instance.xcom_pull(task_ids='process_subscriber', key='return_value')[0] }}",
        aws_conn_id='aws_default'
    )


with DAG(
        dag_id='inquire_bronze_processing',
        default_args=DEFAULT_ARGS,
        dagrun_timeout=timedelta(hours=2),
        start_date=days_ago(2),
        schedule_interval='0 3 * * *',
        tags=['inquire', 'bronze'],
) as dag:
    job_flow_creator = EmrCreateJobFlowOperator(
        task_id='launch_emr_cluster',
        job_flow_overrides=JOB_FLOW_OVERRIDES,
        aws_conn_id='aws_default',
        emr_conn_id='emr_default'
    )

    job_flow_sensor = EmrJobFlowSensor(
        task_id='check_cluster',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        target_states=['RUNNING', 'WAITING'],
        aws_conn_id='aws_default'
    )

    obj_types = ['subscriber', 'dailyparams']
                 #'element-event',
                 #'firmware', 'host-table', 'element-ts', 'kpi-ts', 'diagnostic-ts']

    _processing_steps = [create_processing_step(t) for t in obj_types]
    _short_circuits = [s[0] for s in _processing_steps]
    _steps = [s[1] for s in _processing_steps]

    _sensors = [create_step_sensor(t) for t in obj_types]

    join = DummyOperator(task_id='join', dag=dag)

    job_flow_terminator = EmrTerminateJobFlowOperator(
        task_id='terminate_emr_cluster',
        job_flow_id="{{ task_instance.xcom_pull(task_ids='launch_emr_cluster', key='return_value') }}",
        aws_conn_id='aws_default',
        trigger_rule="all_done"
    )

    chain(job_flow_creator, job_flow_sensor, _short_circuits, _steps, _sensors, join, job_flow_terminator)
