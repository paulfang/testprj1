## EMRUtil
python EMRUtil.py

### Launch a cluster
python EMRUtil.py --action launch-cluster --conf-file ~/workspace/arris/eco/inquire-fire/processor/target/test-classes/config/emr-utils/launchClusterTemplate.json

python EMRUtil.py --action launch-cluster --conf-file ~/workspace/arris/eco/inquire-fire/processor/target/test-classes/config/emr-utils/launchClusterTemplate.json --action-args Name=ProcessElement-Ts Instances.Ec2KeyName=some-key Instances.InstanceGroups.Core.InstanceCount=8 Instances.InstanceGroups.Task.InstanceCount=8

python EMRUtil.py --action launch-cluster --conf-file ~/workspace/arris/eco/inquire-fire/processor/target/test-classes/config/emr-utils/launchClusterTemplate.json --action-args Name=ProcessElement-Ts Instances.InstanceGroups.Core.InstanceCount=8 Instances.InstanceGroups.Task.InstanceCount=8 spark.executor.instances=16

### Terminate a cluster ###
python EMRUtil.py --action terminate-cluster --action-args cluster_id=j-1NFLRQG50DDCW

### List clusters 
python EMRUtil.py --action list-clusters --action-args select=Id,Name
python EMRUtil.py --action list-clusters --action-args select=Id,Name ClusterStates=RUNNING
python EMRUtil.py --action list-clusters --action-args select=Id,Name ClusterStates=COMPLETED
python EMRUtil.py --action list-clusters --action-args select=Id,Name ClusterStates=TERMINATED

### List steps in a cluster ###
python EMRUtil.py --action list-steps --action-args ClusterId=j-3SV58VS93NEXJ StepStates=FAILED,CANCELLED
python EMRUtil.py --action list-steps --action-args ClusterId=j-3SV58VS93NEXJ
python EMRUtil.py --action list-steps --action-args ClusterId=j-3SV58VS93NEXJ StepStates=FAILED,CANCELLED --select Name
python EMRUtil.py --action list-steps --action-args ClusterId=j-3SV58VS93NEXJ StepStates=FAILED,CANCELLED select=Id,Name
python EMRUtil.py --action list-steps --action-args ClusterId=j-3SV58VS93NEXJ StepStates=FAILED select=Id,Name
python EMRUtil.py --action list-steps --action-args ClusterId=j-3SV58VS93NEXJ StepStates=FAILED select=Id,Name,Status

### Describe a cluster ###
python EMRUtil.py --action describe-cluster --action-args ClusterId=j-3SV58VS93NEXJ

### Add task nodes to a cluster ###
 python EMRUtil.py --action add-nodes --conf-file ~/workspace/arris/eco/inquire-fire/processor/target/test-classes/config/emr-utils/addNodesToCluster.json --action-args ClusterId=j-24CFWQ1WU6516

### Describe a step in a cluster
python EMRUtil.py --action describe-step --action-args ClusterId=j-3SV58VS93NEXJ StepId=s-3SR5DIAW8GK9Q
python EMRUtil.py --action describe-step --action-args ClusterId=j-3SV58VS93NEXJ StepId=s-3SR5DIAW8GK9Q select=Status
python EMRUtil.py --action describe-step --action-args ClusterId=j-3SV58VS93NEXJ StepId=s-3SR5DIAW8GK9Q select=Id,Name

python EMRUtil.py --action list-clusters --action-args select=Id,Name ClusterStates=TERMINATED ClusterStates=FAILED select=Id,Name
python EMRUtil.py --action list-clusters --action-args select=Id,Name ClusterStates=TERMINATED ClusterStates=TERMINATED select=Id,Name
python EMRUtil.py --action describe-step --action-args ClusterId=j-3SV58VS93NEXJ StepId=s-3SR5DIAW8GK9Q select=Id,Name

### Add steps to a cluster
python EMRUtil.py --action add-steps --conf-file ~/workspace/arris/eco/inquire-fire/processor/target/test-classes/config/emr-utils/add-diagnostic-ts-step.json --action-args ClusterId=j-24CFWQ1WU6516
python EMRUtil.py --action add-steps --conf-file ~/workspace/arris/eco/inquire-fire/processor/target/test-classes/config/emr-utils/add-element-ts-step.json --action-args ClusterId=j-2C4RQ2KQOVL9G
python EMRUtil.py --action add-nodes --conf-file ~/workspace/arris/eco/inquire-fire/processor/target/test-classes/config/emr-utils/addNodesToCluster.json --action-args ClusterId=j-24CFWQ1WU6516

### Adding a step with Hadoop parameter override ###
python EMRUtil.py --action add-steps --conf-file ~/workspace/arris/eco/inquire-fire/processor/target/test-classes/config/emr-utils/add-element-ts-step.json --action-args ClusterId=j-QQKAVYNR2YA0 HadoopJarStep.Args=input_location::s3://eco.inquire.frontier/archive/element-ts/2020/08/16/,passthrough::true