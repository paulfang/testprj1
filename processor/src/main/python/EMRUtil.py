import argparse
import boto3
import json
import logging
import logging.config
import traceback


#logging.basicConfig(filename='emr_utils.log', format='%(asctime)s %(message)s',level=logging.INFO)

logging.basicConfig(format='%(asctime)s %(message)s', level=logging.INFO)


class EMRUtils:
    def __init__(self, action, conf_json, action_args_dict, logger=None):
        self.logger = logger if logger else logging.getLogger("EMRUtils")

        self.client = boto3.client('emr')
        self.action = action
        self.conf_json = conf_json
        self.action_args_dict = action_args_dict
        self.logger.debug('action: {}\nconf: {}\naction_args:{}'.format(self.action, self.conf_json, self.action_args_dict))
        self.app_jar = "s3://eco.inquire.frontier/spark/inquire-3.1.0-SNAPSHOT-jar-with-dependencies.jar"
        self.app_conf = "application-bronze.json,subscriber-bronze.json"

    def launch_cluster(self):
        self.__override_conf()

        name = self.conf_json.get('Name')
        log_uri = self.conf_json.get('LogUri')
        release_label = self.conf_json.get('ReleaseLabel')
        instances = self.conf_json.get('Instances')
        steps = self.conf_json.get("Steps", [])
        bootstrap_actions = self.conf_json.get('BootstrapActions')
        applications = self.conf_json.get('Applications')
        configurations = self.conf_json.get('Configurations')
        service_role = self.conf_json.get('ServiceRole')
        job_flow_role = self.conf_json.get('JobFlowRole')
        tags = self.conf_json.get('Tags')
        scale_down_behavior = self.conf_json.get('ScaleDownBehavior')
        ebs_root_volume_size = self.conf_json.get('EbsRootVolumeSize')
        logger.info('conf:\n{}'.format(self.conf_json))

        return self.client.run_job_flow(Name=name, LogUri=log_uri, ReleaseLabel=release_label,
                                            Instances=instances, Steps=steps, BootstrapActions=bootstrap_actions,
                                            Applications=applications, Configurations=configurations, JobFlowRole=job_flow_role,
                                            ServiceRole=service_role, Tags=tags, ScaleDownBehavior=scale_down_behavior,
                                            EbsRootVolumeSize=ebs_root_volume_size)

    def add_steps(self):
        logger.debug(self.action_args_dict)
        self.__override_step_conf()
        _cluster_id = self.action_args_dict.get('ClusterId')
        if not _cluster_id:
            logger.fatal('Missing ClusterID argument. Help: --action add-steps --ClusterId=<ClusterId>')
            exit(-1)
        logger.info("Adding steps to cluster: {}".format(_cluster_id))

        _steps = self.conf_json
        logger.debug("_steps: {}".format(_steps))
        return self.client.add_job_flow_steps(JobFlowId=_cluster_id, Steps=_steps)

    def __override_step_conf(self):
        if self.action_args_dict.get('HadoopJarStep.Args'):
            logger.info("'HadoopJarStep.Args': {}".format(self.action_args_dict.get('HadoopJarStep.Args')))

            _args = self.conf_json[0].get('HadoopJarStep').get('Args')
            logger.info("orig args: {}".format(_args))
            for a in self.action_args_dict.get('HadoopJarStep.Args').split(','):
                kv = a.split('::')
                _args.append('{}={}'.format(kv[0].strip(), kv[1].strip()))

            self.conf_json[0].get('HadoopJarStep')['Args'] = _args
            logger.info("modified args: {}".format(self.conf_json[0].get('HadoopJarStep')['Args']))

    def cancel_step(self):
        _cluster_id = self.action_args_dict.get('ClusterId')
        _step_id = self.action_args_dict.get('StepId')
        return self.client.cancel_steps(ClusterId=_cluster_id, StepIds=[_step_id])

    def describe_step(self):
        _cluster_id = self.action_args_dict.get('ClusterId')
        _step_id = self.action_args_dict.get('StepId')
        _select = [] if not self.action_args_dict.get('select') else self.action_args_dict.get('select').split(',')

        logger.info("describe Step clusterId: {} StepId: {}".format(_cluster_id, _step_id))
        _response = self.client.describe_step(ClusterId=_cluster_id, StepId=_step_id)

        if _response:
            if _select:
                _response = _response.get('Step')
                logger.debug(_response)
                return [{s.strip(): _response[s.strip()]} for s in _select]

        return _response

    def list_steps(self):
        _cluster_id = self.action_args_dict.get('ClusterId')
        _select = [] if not self.action_args_dict.get('select') else self.action_args_dict.get('select').split(',')
        _states = [] if not self.action_args_dict.get('StepStates') else self.action_args_dict.get('StepStates').split(',')
        logger.info("listing steps for cluster_id: {} in cluster_states: {}".format(_cluster_id, _states))
        _response = self.client.list_steps(ClusterId=_cluster_id, StepStates=_states) if _states else self.client.list_steps(ClusterId=_cluster_id)

        if _response:
            if _select:
                _response = [s for s in _response.get('Steps')]
                return [{s.strip(): step[s.strip()]} for step in _response for s in _select]

        return _response

    def terminate_cluster(self):
        _cluster_id = self.action_args_dict.get('ClusterId')
        logger.info("terminating ClusterId: {}".format(_cluster_id))
        return self.client.terminate_job_flows(JobFlowIds=[_cluster_id])

    def list_clusters(self):
        _cluster_states = [] if not self.action_args_dict.get('ClusterStates') else self.action_args_dict.get('ClusterStates').split(',')
        _select = [] if not self.action_args_dict.get('select') else self.action_args_dict.get('select').split(',')
        _response = self.client.list_clusters(ClusterStates=_cluster_states) \
            if _cluster_states else self.client.list_clusters()

        if _response:
            if _select:
                _response = [c for c in _response.get('Clusters')]
                return [{s.strip(): c[s.strip()]} for c in _response for s in _select]

        return _response

    def describe_cluster(self):
        _cluster_id = self.action_args_dict.get('ClusterId')
        logger.info("describe ClusterId: {}".format(_cluster_id))
        return self.client.describe_cluster(ClusterId=_cluster_id)

    def add_nodes(self):
        _cluster_id = self.action_args_dict.get('ClusterId')
        logger.info("adding nodes to ClusterId: {}".format(_cluster_id))
        _instance_groups = self.conf_json
        return self.client.add_instance_groups(JobFlowId=_cluster_id, InstanceGroups=_instance_groups)

    def run(self):
        if self.action == 'launch-cluster':
            return self.launch_cluster()
        elif self.action == 'terminate-cluster':
            return self.terminate_cluster()
        elif self.action == 'describe-step':
            return self.describe_step()
        elif self.action == 'add-steps':
            return self.add_steps()
        elif self.action == 'cancel-step':
            return self.cancel_step()
        elif self.action == 'list-clusters':
            return self.list_clusters()
        elif self.action == 'describe-cluster':
            return self.describe_cluster()
        elif self.action == 'list-steps':
            return self.list_steps()
        elif self.action == 'add-nodes':
            return self.add_nodes()

    def __override_conf(self):
        if self.conf_json.get('Name'):
            self.conf_json['Name'] = self.conf_json['Name'] if not self.action_args_dict.get('Name') else self.action_args_dict.get('Name')

        if self.action_args_dict.get('Instances.Ec2KeyName'):
            self.conf_json.get('Instances')['Ec2KeyName'] = self.action_args_dict.get('Instances.Ec2KeyName')

        if self.action_args_dict.get('Instances.InstanceGroups.Core.InstanceCount'):
            for g in self.conf_json.get('Instances').get('InstanceGroups'):
                if g['InstanceRole'] == 'CORE':
                    g['InstanceCount'] = int(self.action_args_dict.get('Instances.InstanceGroups.Core.InstanceCount'))
                    break

        if self.action_args_dict.get('Instances.InstanceGroups.Task.InstanceCount'):
            for g in self.conf_json.get('Instances').get('InstanceGroups'):
                if g['InstanceRole'] == 'TASK':
                    g['InstanceCount'] = int(self.action_args_dict.get('Instances.InstanceGroups.Task.InstanceCount'))
                    break

        if self.action_args_dict.get('Instances.Ec2KeyName'):
            self.conf_json.get('Instances')['Ec2KeyName'] = self.action_args_dict.get('Instances.Ec2KeyName')

        _exec_inst_key = 'spark.executor.instances'
        if self.action_args_dict.get(_exec_inst_key):
            for c in self.conf_json.get('Configurations'):
                if c['Classification'] == 'spark-defaults':
                    c['Properties'][_exec_inst_key] = self.action_args_dict.get(_exec_inst_key)
                    break


if __name__ == '__main__':
    logger = logging.getLogger('EMRUtil')

    parser = argparse.ArgumentParser(description='ERM Utility')
    parser.add_argument('--action', type=str, required=True, dest='action', help='Action to be performed')
    parser.add_argument('--conf-file', type=str, required=False, dest='conf_file',
                        help='The name of the configuration file')
    parser.add_argument('--action-args', nargs='*', help='Extra arguments in key=value form')

    args = parser.parse_args()
    conf = {}
    logger.debug('action: {} conf_file: {}'.format(args.action, args.conf_file))
    if args.conf_file:
        with open(args.conf_file) as fl:
            conf = json.load(fl)
            logger.debug(conf)

    action_args = dict([a.split("=") for a in args.action_args]) if args.action_args else {}

    logger.debug('action-args: {}'.format(action_args))
    emrUtils = EMRUtils(args.action, conf, action_args, logger=logger)
    response = emrUtils.run()
    logger.info("{}".format(response))