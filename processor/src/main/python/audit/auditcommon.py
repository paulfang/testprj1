'''
Utility module to support the audit monitoring/alarm process/scripts
'''

import math
import json
import csv
import pymsteams
import os
from datetime import datetime, timedelta
import logging
import logging.config


# Return a logger for audit module
def logger():
  logConf = os.path.join(thisPath(), 'logging.conf')
  logging.config.fileConfig(logConf)
  logger = logging.getLogger('auditLogger')
  return logger

# Return the current path
def thisPath():
  return os.path.dirname(os.path.abspath(__file__))

# Load the configuration file and return as a JSON object.
def readS3Conf():
  filePath = os.path.join(thisPath(), "s3-alarms-conf.json")
  with open(filePath, 'r') as confFile:
    confObj = json.load(confFile)
  return confObj

# Writes the updated configurations back to the JSON configuration file.
def writeS3Conf(jsonObj):
  filePath = os.path.join(thisPath(), "s3-alarms-conf.json")
  with open(filePath, 'w') as confFile:
    json.dump(jsonObj, confFile, indent=4)

# Returns the configurations for a given monitor id.
def getMonitorConf(mtConf, mtId):
  for mts in mtConf['monitors']:
    if mts['monitor-id'] == mtId:
      return mts

# Patch the item path/prefix with date
def patchDatePrefix(text, dt):
  dtc = {
    "$YYYY": str(dt.year),
    "$MM": str(dt.month),
    "$0M": str(dt.month).zfill(2),
    "$DD": str(dt.day),
    "$0D": str(dt.day).zfill(2)
  }
  for i, j in dtc.items():
    text = text.replace(i, j)
  return text


# Generates date prefix array as per the s3 path requirement,
# also for the specified time period.
# def datePrefixCalc(limit, isStage):
#   dateprefix = []
#   now = datetime.now()
#   for x in range(limit):
#     last7d = now - timedelta(days=x+1)
#     dtstr = last7d.strftime('%Y%m%d')
#     if isStage:
#       dateprefix.append(dtstr)
#     else:
#       dateprefix.append('ts_year={}/ts_month={}/ts_day={}'.format(str(int(dtstr[0:4])), str(int(dtstr[4:6])), str(int(dtstr[6:8]))))
#   return dateprefix

# Extract the date value our of the date prefix string
# def formatDatePrefixCSV(dtpath):
#   if 'ts_year' in dtpath:
#     return dtpath[8:12] + dtpath[22:24] + dtpath[32:34]
#   else:
#     return dtpath

# Write the data to a CSV for future analysis
def writeCSV(csvPath, dt, bucket, path, count, size):
  filePath = os.path.join(thisPath(), csvPath)
  with open(filePath, 'a') as f:
    writer = csv.writer(f)
    writer.writerow([dt.date(), bucket, path, count, size, convertSize(size)])

# Return the vive size values in bytes into a more readable format.
def convertSize(size_bytes):
   if size_bytes == 0:
       return "0B"
   size_name = ("B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB")
   i = int(math.floor(math.log(size_bytes, 1024)))
   p = math.pow(1024, i)
   s = round(size_bytes / p, 2)
   return "%s %s" % (s, size_name[i])

# Sends messages to a MS Teams Channel using the webhook provided.
def sendTeamsMessage(teamswebhookurl, message):
  pyTeams = pymsteams.connectorcard(teamswebhookurl)
  pyTeams.text(message)
  pyTeams.send()


