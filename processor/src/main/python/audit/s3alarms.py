'''
A python script which checks a configured s3 bucket path and verifys whether data present on that path and is as per
the configured threshold intervals. Failure to meets the threshold values triggers alarms and those messages will be
send to a configured Microsoft Team channel.

This script requires a configuration file called "s3-alarms-conf.json" within the same execution path as this script.
This configuration file must provides information about the data to be monitored such as the s3 bucket name,
location/path and object prefixes along with the thresholds related information to be compared with.

Here is an example configuration block:
{
  "bucket": "eco.inquire.ziply",
  "ms-teams-webhook-url": "url",
  "average-period-days": 7,
  "threshold-percent": 10,
  "monitors": [
    {
      "monitor-id": "Stage",
      "base-path": "data/processed/",
      "objects": [
        {
          "item-path": "dailyparams",
          "item-prefix": "dailyparams",
          "size-avg": 0,
          "count-avg": 0
        },
        ...

Refer the confluence page for more details:
https://arrisworkassure.atlassian.net/wiki/spaces/TEAM/pages/1868562537/S3+Audit+and+Alarms

'''

import boto3
import os
from datetime import datetime, timedelta
import auditcommon


def main():
  auditcommon.logger().info('ECO Inquire - S3 Audit - main()...')

  #Execute the script which finds the 7-day (or configured interval) average
  filePath = os.path.join(auditcommon.thisPath(), "s3findavg.py")
  exec(open(filePath).read())

  now = datetime.now()
  s3conf = auditcommon.readS3Conf()

  bucket = s3conf['bucket']
  thresholdPercent = s3conf['threshold-percent']
  teamsUrl = s3conf['ms-teams-webhook-url']
  csvPath = s3conf['s3-daily-audit-csv']

  auditcommon.logger().debug('********* ECO INQUIRE ALERTS ************')
  auditcommon.logger().debug('Date/Time: {}'.format(now.strftime('%d/%m/%Y %H:%M:%S')))
  auditcommon.logger().debug('Bucket: {}'.format(bucket))
  message = '<b> ********* ECO INQUIRE ALERTS ************ </b>'
  message += '<br/>Date/Time: ' + now.strftime('%d/%m/%Y %H:%M:%S')
  message += '<br/>Bucket: ' + bucket

  s3 = boto3.client('s3')

  for mts in s3conf['monitors']:
    monitorId = mts['monitor-id']
    basepath = mts['base-path']
    auditcommon.logger().debug('Monitoring Data: {}'.format(monitorId))
    message += '<br/><br/><u><b>Monitor Data: {}</b></u>'.format(monitorId)

    monitorAlarm = False
    now = datetime.now()
    for objs in mts['objects']:
      for x in range(1):
        size = 0
        count = 0
        dt = now - timedelta(days=x+1)
        itemPath = auditcommon.patchDatePrefix(objs['item-path'], dt)
        itemPrefix = auditcommon.patchDatePrefix(objs['item-prefix'], dt)
        path = '{}{}/{}'.format(basepath, itemPath, itemPrefix)
        #auditcommon.logger().debug(path)

        result = s3.list_objects_v2(Bucket=bucket, Prefix=path)
        #auditcommon.logger().debug(result)

        if result['KeyCount'] > 0:
          size += sum([x['Size'] for x in result['Contents']])
          count += result['KeyCount']

          while result['IsTruncated']:
            result = s3.list_objects_v2(
                Bucket=bucket, Prefix=path,
                ContinuationToken=result['NextContinuationToken'])
            size += sum([x['Size'] for x in result['Contents']])
            count += result['KeyCount']

        #auditcommon.logger().debug('Object: ' + path + ', Count: ' + str(count) + ', Total bytes:  ' + str(size) + ' (' + auditcommon.convertSize(size) + ')')
        auditcommon.writeCSV(csvPath, dt, bucket, path, count, size)

        alarmSet = False
        #Count Alarms
        if count == 0:
          alarmSet = True
          auditcommon.logger().warning('ALARM: No Data')
          auditcommon.logger().warning('No data found @ {}'.format(path))
          message += '<br/><br/><b>ALARM: No Data </b>'
          message += '<br/>No data found @ {}'.format(path)
        else:
          countAvg = objs['count-avg']
          count10Percent = countAvg * thresholdPercent / 100
          if count < (countAvg - count10Percent):
            alarmSet = True
            auditcommon.logger().warning('ALARM: Low number of items')
            auditcommon.logger().warning('The total number of the objects are not within the {}% threshold. Average Count: {}'.format(thresholdPercent, countAvg))
            auditcommon.logger().warning('Object: {}, Count: {}, Total bytes: {} ({})'.format(path, count, size, auditcommon.convertSize(size)))
            message += '<br/><br/><b>ALARM: Low number of items </b>'
            message += '<br/>The total number of the objects are not within the {}% threshold. Average Count: {}'.format(thresholdPercent, countAvg)
            message += '<br/>Object: {}, Count: {}, Total bytes: {} ({})'.format(path, count, size, auditcommon.convertSize(size))
          if count > (count + count10Percent):
            alarmSet = True
            auditcommon.logger().warning('ALARM: Excess number of items')
            auditcommon.logger().warning('The total number of the objects are not within the {}% threshold. Average Count: {}'.format(thresholdPercent, countAvg))
            auditcommon.logger().warning('Object: {}, Count: {}, Total bytes: {} ({})'.format(path, count, size, auditcommon.convertSize(size)))
            message += '<br/><br/><b>ALARM: Excess number of items </b>'
            message += '<br/>The total number of the objects are not within the {}% threshold. Average Count: {}'.format(thresholdPercent, countAvg)
            message += '<br/>Object: {}, Count: {}, Total bytes: {} ({})'.format(path, count, size, auditcommon.convertSize(size))

        #Check the new size / count trigger any alarms
        if not alarmSet:
          sizeAvg = objs['size-avg']
          size10Percent = sizeAvg * thresholdPercent / 100
          if size < (sizeAvg - size10Percent):
            auditcommon.logger().warning('ALARM: Low content size')
            auditcommon.logger().warning('The total size of the objects are not within the {}% threshold. Average Size: {} bytes  ({})'.format(thresholdPercent,sizeAvg, auditcommon.convertSize(sizeAvg)))
            auditcommon.logger().warning('Object: {}, Count: {}, Total bytes: {} ({})'.format(path, count, size, auditcommon.convertSize(size)))
            message += '<br/><br/><b>ALARM: Low content size </b>'
            message += '<br/>The total size of the objects are not within the {}% threshold. Average Size: {} bytes  ({})'.format(thresholdPercent,sizeAvg, auditcommon.convertSize(sizeAvg))
            message += '<br/>Object: {}, Count: {}, Total bytes: {} ({})'.format(path, count, size, auditcommon.convertSize(size))
          if size > (sizeAvg + size10Percent):
            auditcommon.logger().warning('ALARM: Daily content size exceeded')
            auditcommon.logger().warning('The total size of the objects are not within the {}% threshold. Average Size: {} bytes  ({})'.format(thresholdPercent,sizeAvg, auditcommon.convertSize(sizeAvg)))
            auditcommon.logger().warning('Object: {}, Count: {}, Total bytes: {} ({})'.format(path, count, size, auditcommon.convertSize(size)))
            message += '<br/><br/><b>ALARM: Daily content size exceeded</b>'
            message += '<br/>The total size of the objects are not within the {}% threshold. Average Size: {} bytes  ({})'.format(thresholdPercent,sizeAvg, auditcommon.convertSize(sizeAvg))
            message += '<br/>Object: {}, Count: {}, Total bytes: {} ({})'.format(path, count, size, auditcommon.convertSize(size))

      if alarmSet:
        monitorAlarm = True

    if not monitorAlarm:
      auditcommon.logger().debug('No alarms')
      message += '<br/>No alarms'

  message += '<br/>'
  #auditcommon.logger().debug(message)
  auditcommon.sendTeamsMessage(teamsUrl, message.replace('_', '\_'))


#START THE MAIN
if __name__ == "__main__":
  main()