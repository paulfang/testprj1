'''
This python scripts finds the average thresholds for a S3 bucket folder based on the historic data present there and
using the given configuration interval data. For all configured s3 bucket folders, this script calculates
the average number of items and the average total size of items within that folder. The calculated threshold values
will be then persisted to a JSON file for later use for monitoring daily data and generating alarms.

This script requires a configuration file called "s3-alarms-conf.json" within the same execution path as this script.
This configuration file must provides information about the data to be monitored such as the s3 bucket name,
location/path and object prefixes along with the thresholds related information to be compared with.

Here is an example configuration block:
{
  "bucket": "eco.inquire.ziply",
  "ms-teams-webhook-url": "url",
  "average-period-days": 7,
  "threshold-percent": 10,
  "monitors": [
    {
      "monitor-id": "Stage",
      "base-path": "data/processed/",
      "objects": [
        {
          "item-path": "dailyparams",
          "item-prefix": "dailyparams",
          "size-avg": 0,
          "count-avg": 0
        },
        ...

This script is currently invoked from s3alarams.py to re-generater the threshold values. This can also be executed
independently if that is needed.

Refer the confluence page for more details:
https://arrisworkassure.atlassian.net/wiki/spaces/TEAM/pages/1868562537/S3+Audit+and+Alarms
'''

import boto3
from datetime import datetime, timedelta
import auditcommon

def main():
  auditcommon.logger().info('ECO Inquire - S3 Audit - Find Average thresholds..')

  now = datetime.now()
  s3conf = auditcommon.readS3Conf()

  bucket = s3conf['bucket']
  avgDays = s3conf['average-period-days']

  auditcommon.logger().debug('Date/Time: {}'.format(now.strftime('%d/%m/%Y %H:%M:%S')))
  auditcommon.logger().debug('Bucket: {}'.format(bucket))

  s3 = boto3.client('s3')

  for mts in s3conf['monitors']:
    monitorId = mts['monitor-id']
    basepath = mts['base-path']
    auditcommon.logger().debug('Monitor Data: {}'.format(monitorId))

    now = datetime.now()
    for objs in mts['objects']:
      sizeTot = 0
      countTot = 0

      for x in range(avgDays):
        size = 0
        count = 0
        dt = now - timedelta(days=x+1)
        itemPath = auditcommon.patchDatePrefix(objs['item-path'], dt)
        itemPrefix = auditcommon.patchDatePrefix(objs['item-prefix'], dt)
        path = '{}{}/{}'.format(basepath, itemPath, itemPrefix)
        #auditcommon.logger().debug(path)

        result = s3.list_objects_v2(Bucket=bucket, Prefix=path)
        #auditcommon.logger().debug(result)

        if result['KeyCount'] > 0:
          size += sum([x['Size'] for x in result['Contents']])
          count += result['KeyCount']

          while result['IsTruncated']:
            result = s3.list_objects_v2(
              Bucket=bucket, Prefix=path,
              ContinuationToken=result['NextContinuationToken'])
            size += sum([x['Size'] for x in result['Contents']])
            count += result['KeyCount']

        auditcommon.logger().debug('Object: {}, Count: {}, Total Size: {} ({})'.format(path, count, size, auditcommon.convertSize(size)))
        sizeTot += size
        countTot += count

      auditcommon.logger().debug('Size: Total: {}, Avg: {}'.format(sizeTot, sizeTot/avgDays))
      auditcommon.logger().debug('Count: Total: {}, Avg: {}'.format(countTot, countTot/avgDays))
      objs['size-avg'] = int(sizeTot/avgDays)
      objs['count-avg'] = int(countTot/avgDays)

  auditcommon.writeS3Conf(s3conf)

#START THE MAIN
if __name__ == "__main__":
  main()