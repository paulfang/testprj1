
# Process Subscriber
--class com.commscope.eco.Inquire --files s3://eco.inquire.frontier/spark/log4j.properties,s3://eco.inquire.frontier/spark/application-bronze.json,s3://eco.inquire.frontier/spark/subscriber-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=subscriber-bronze.json"


application-bronze.json,subscriber-bronze.json

# Process Dailyparams

--class com.commscope.eco.Inquire --files s3://eco.inquire.frontier/spark/log4j.properties,s3://eco.inquire.frontier/spark/application-bronze.json,s3://eco.inquire.frontier/spark/dailyparams-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=dailyparams-bronze.json"

application-bronze.json,dailyparams-bronze.json

# Process Element Event
--class com.commscope.eco.Inquire --files s3://eco.inquire.frontier/spark/log4j.properties,s3://eco.inquire.frontier/spark/application-bronze.json,s3://eco.inquire.frontier/spark/element-event-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=element-event-bronze.json"

application-bronze.json,element-event-bronze.json


# Process Firmware
--class com.commscope.eco.Inquire --files s3://eco.inquire.frontier/spark/log4j.properties,s3://eco.inquire.frontier/spark/application-bronze.json,s3://eco.inquire.frontier/spark/firmware-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=firmware-bronze.json"

application-bronze.json,firmware-bronze.json

# Process Host-Table
--class com.commscope.eco.Inquire --files s3://eco.inquire.frontier/spark/log4j.properties,s3://eco.inquire.frontier/spark/application-bronze.json,s3://eco.inquire.frontier/spark/host-table-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=host-table-bronze.json"

application-bronze.json,host-table-bronze.json

# Process Element-TS
--class com.commscope.eco.Inquire --files s3://eco.inquire.frontier/spark/log4j.properties,s3://eco.inquire.frontier/spark/application-bronze.json,s3://eco.inquire.frontier/spark/element-ts-bronze.json --driver-java-options "-Dlog4j.configuration=log4j.properties" --driver-java-options "-Dconfig.resource=application-bronze.json" --driver-java-options "-Dconfig.resource=element-ts-bronze.json"

application-bronze.json,element-ts-bronze.json

