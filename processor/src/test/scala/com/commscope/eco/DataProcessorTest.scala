package com.commscope.eco

import com.commscope.eco.model.DataModel
import com.typesafe.scalalogging.Logger
import org.apache.commons.io.FileUtils
import org.apache.spark.sql.types.DataTypes.{createArrayType, createStructType}
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Row, SaveMode, SparkSession}
import org.junit.Assert
import org.junit.Assert._
import org.scalatest.BeforeAndAfterEach

import java.io.File
import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}
import scala.util.Try

class DataProcessorTest extends SparkBasedTest with BeforeAndAfterEach {
  val logger = Logger(getClass)

  val baseDir = this.getClass.getClassLoader.getResource("data/frontier").getPath
  var outDir = ""
  val dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm")

  val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
  val now = calendar.getTimeInMillis()

  val sampleData = Seq(
    Row(now, "John", "Smith", dateFormat.parse("1969-12-20 07:05").toInstant.toEpochMilli),
    Row(now, "John", "Doe", dateFormat.parse("1957-02-07 03:35").toInstant.toEpochMilli),
    Row(now, "Jane", "Doe", dateFormat.parse("1977-07-07 13:23").toInstant.toEpochMilli)
  )

  val sampleSchema = StructType(Array(
    StructField("created_at", LongType, false),
    StructField("first_name", StringType, false),
    StructField("last_name", StringType, false),
    StructField("tob", LongType, false)
  ))

  val sampleDF = spark.createDataFrame(spark.sparkContext.parallelize(sampleData), sampleSchema)
  val workingDir = System.getProperty("user.dir")
  new File(s"$workingDir/tmp/processing/").mkdirs()
  FileUtils.copyInputStreamToFile(this.getClass.getClassLoader.getResourceAsStream("data/frontier/subscriber-sample.json"),
    new File(s"${workingDir}/tmp/processing/subscriber-sample.json"))

  override def beforeEach {
  }

  override def afterEach {
    super.tearDown()
    new File(outDir).delete()
  }

  "appConfig with no output schema and flatten=false " should {
    val testConfig = AppConfig("config/application-bronze-json-out.json")
    assertEquals(SaveMode.Overwrite, testConfig.outputSaveMode)
    var df: DataFrame = null

    "process files" in {
      val result = DataProcessor.process(testConfig)
      outDir = result._2
      df = result._1
      val fl = new File(outDir)
      assertTrue(fl.isDirectory)
      assertFalse(fl.list().filter(p => p.startsWith("part-")).isEmpty)
    }

    var writtenDf: DataFrame = null
    "write out in given outputFormat" in {
      writtenDf = DataFrameUtils.load(testConfig.outputFormat, outDir)
      logger.debug("written out: {}", writtenDf.schema.treeString)
    }

    "written schema should match that of original" in {
      val expected = df.schema.sql.split(":")
      val actual = writtenDf.schema.sql.split(":")
      val diff = expected.diff(actual)
      logger.debug("{}", diff)
    }

    "write out in non flat schema" in {
      val structTypes = df.schema.sql.split(",").filter(_.toLowerCase.contains("struct"))
      logger.debug("structType columns {}", structTypes)
      assertFalse(df.schema.sql.split(",").filter(_.toLowerCase.contains("struct")).isEmpty)
    }
  }

  "audit dataframe" ignore {
    val config = AppConfig("config/audit/subscriber-json-audit.json")
    val result = DataProcessor.process(config)
    Assert.assertNotNull(result)
    logger.info("result columns: {}", result._1.columns)
    val auditDf = spark.read.parquet(s"${config.outputLocation}/audit/${config.appName}")
    Assert.assertNotNull(auditDf)
//    val auditInformDf = spark.read.parquet(s"${config.outputLocation}/audit_inform/${config.appName}")
//    Assert.assertNotNull(auditInformDf)
    val cols = auditDf.columns
    logger.info("audit columns {}", cols)
    Seq("name", "row_count", "min_ts", "max_ts", "distinct_ids", "processed_dt")
      .foreach(c => Assert.assertTrue(cols.contains(c)))
  }

  "appConfig with no output schema and flatten=true " should {
    val testConfig = AppConfig("config/application-bronze-parquet-out.json")
    assertEquals(SaveMode.Overwrite, testConfig.outputSaveMode)
    var df: DataFrame = null

    "process files" in {
      val result = DataProcessor.process(testConfig)
      outDir = result._2
      df = result._1
      val fl = new File(outDir)
      assertTrue(fl.isDirectory)
      assertFalse(fl.list().filter(p => p.startsWith("part-")).isEmpty)
    }

    "the dataframe should be flattened" in {
      val expected = "created,details.BTN,details.CLLI,details.ManagementSystem,details.Serial_Number,details.ZIP,details.lastName,id,identifiers.ISPUsername,identifiers.USI,identifiers.WTN,services,timestamp".split(",")
      logger.debug("columns {}", df.columns)
      logger.warn("diff: {}", expected.diff(df.columns))
      assertTrue(expected.diff(df.columns).isEmpty)
    }
  }

  "appConfig with no output schema flatten=true and create_time_dims=true" should {
    val testConfig = AppConfig("config/application-bronze-parquet-out-withTimeDims.json")
    assertEquals(SaveMode.Overwrite, testConfig.outputSaveMode)
    var df: DataFrame = null

    "process files" in {
      val result = DataProcessor.process(testConfig)
      outDir = result._2
      df = result._1
      val fl = new File(outDir)
      assertTrue(fl.isDirectory)
      assertFalse(fl.list().filter(p => p.startsWith("part-")).isEmpty)
    }

    "the dataframe should be flattened and have time dimensions" in {
      val expected = "created,details.BTN,details.CLLI,details.ManagementSystem,details.Serial_Number,details.ZIP,details.lastName,id,identifiers.ISPUsername,identifiers.USI,identifiers.WTN,services,timestamp,ts_year,ts_date,ts_month,ts_day,ts_hour".split(",")
      logger.debug("columns {}", df.columns)
      assertTrue(expected.diff(df.columns).isEmpty)
    }
  }

  "config with flatten_output" when {
    val appConfig = AppConfig("config/subscriber-bronze-test.json")
    val expected = spark.read.json(s"${appConfig.inputLocation}/${appConfig.inputFilePattern}")
    val detailsExpr = ("details.BTN AS `details.BTN`," +
      "details.CLLI AS `details.CLLI`," +
      "details.ManagementSystem AS `details.ManagementSystem`," +
      "details.`Serial Number` AS `details.Serial_Number`," +
      "details.`lastName` AS `details.lastName`," +
      "details.`ZIP` AS `details.ZIP`"
      ).split(",")
    val expDetails = expected.selectExpr(detailsExpr: _*)
    expDetails.show(3)

//    "true" should {
//      val result = DataProcessor.process(appConfig)
//
//      "flatten " in {
//        val structTypes = result._1.schema.sql.split(",").filter(_.toLowerCase.contains("struct"))
//        logger.debug("structType columns {}", structTypes)
//      }
//
//      "have fidelity in nested struct column" in {
//        val equivDetailsExpr = Map(
//          "`details.BTN`" -> "details.BTN AS `details.BTN`",
//          "`details.CLLI`" -> "details.CLLI AS `details.CLLI`",
//          "`details.ManagementSystem`" -> "details.ManagementSystem AS `details.ManagementSystem`",
//          "`details.Serial_Number`" -> "details.`Serial Number` AS `details.Serial_Number`",
//          "`details.lastName`" -> "details.`lastName` AS `details.lastName`",
//          "`details.ZIP`" -> "details.`ZIP` AS `details.ZIP`"
//        )
//        val detailsExpr: Array[String] = equivDetailsExpr.keys.mkString(",").split(",")
//        val readBack = DataFrameUtils.load(appConfig.outputFormat, result._2)
//
//        readBack.selectExpr(detailsExpr: _*).show(3)
//        equivDetailsExpr.foreach(e => readBack.selectExpr(e._1).equals(expected.selectExpr(e._2)))
//      }
//
//      "have fidelity in nested arrays column" in {
//        val equivServicesExpr = Map(
//          "`services`" -> "services AS `services`"
//        )
//        val servicesExpr: Array[String] = equivServicesExpr.keys.mkString(",").split(",")
//        val readBack = DataFrameUtils.load(appConfig.outputFormat, result._2)
//
//        readBack.selectExpr(servicesExpr: _*).show(3)
//        equivServicesExpr.foreach(e => readBack.selectExpr(e._1).equals(expected.selectExpr(e._2)))
//      }
//    }
  }

  "config" when {
    val appConfig = AppConfig("config/application-bronze-parquet-out-withTimeDims.json")
    assertTrue(appConfig.partitions.isEmpty)

    "have no partitions defined" should {
      val location = DataProcessor.process(appConfig)._2

      "not partition the dataframe" in {
        val df = spark.read.format(appConfig.outputFormat.toString).load(location)
        val partitions = df.select(org.apache.spark.sql.functions.spark_partition_id()).distinct()
        logger.debug("partitions: {}", partitions)
        assertEquals(1, df.rdd.getNumPartitions)
      }
    }
  }

  "config" when {
    val appConfig = AppConfig("config/application-bronze-parquet-out-withTimeDims-partitioned.json")
    assertEquals(3, appConfig.partitions.size)

    "have partitions defined" should {
      val location = DataProcessor.process(appConfig)._2

      "partition the dataframe" in {
        val df = spark.read.format(appConfig.outputFormat.toString).load(location)
        val partitions = df.select(org.apache.spark.sql.functions.spark_partition_id()).distinct()
        logger.debug("partitions: {}", partitions)
        assertEquals(3, df.rdd.getNumPartitions)
      }
    }
  }

  "override partitions" in {
    val firstConfig = AppConfig("config/application-bronze-parquet-out-withTimeDims-partitioned.json")
    assertTrue(Seq("ts_year","ts_month","ts_day").diff(firstConfig.partitions).isEmpty)
    val secondConfig = AppConfig("config/application-overridePartitions.json")
    assertTrue(Seq("ts_date").diff(secondConfig.partitions).isEmpty)
    val mergedConfig = AppConfig("config/application-bronze-parquet-out-withTimeDims-partitioned.json",
      "config/application-overridePartitions.json")

    assertTrue(Seq("ts_date").diff(mergedConfig.partitions).isEmpty)

    val location = DataProcessor.process(mergedConfig)._2

    val expected = Seq("_SUCCESS", "._SUCCESS.crc")

    val foldersStartingWith_ts_date = new File(location).list().filterNot(_.startsWith("ts_date="))
    logger.debug("num folders: {}", foldersStartingWith_ts_date.length)
    assertTrue(foldersStartingWith_ts_date.diff(expected).isEmpty)
  }

  "config output_format=delta" ignore {
    val appConfig = AppConfig("config/application-bronze-delta-out-withTimeDims-partitioned.json")

    assertEquals(3, appConfig.partitions.size)
    "have partitions defined" should {
      val location = DataProcessor.process(appConfig)._2
      "partition the dataframe" in {
        val df = spark.read.format(appConfig.outputFormat.toString).load(location)
        val partitions = df.select(org.apache.spark.sql.functions.spark_partition_id()).distinct()
        logger.debug("partitions: {}", partitions)
        //assertEquals(3, df.rdd.getNumPartitions)
      }
    }
  }

  "data with nested schema" when {
    "processed with no ignoreColumns and no output schema" should {
      "have all columns and column names should be sanitized if output format is not json" in {
        val appConfig = AppConfig("config/application-bronze-parquet-out-non-flat.json")
        val df = DataProcessor.process(appConfig)._1
        assertTrue(Try(df("details.lastName")).isSuccess)
        assertTrue(Try(df("details.Serial Number")).isFailure)
        assertTrue(Try(df("details.Serial_Number")).isSuccess)
        assertTrue(Try(df("services")).isSuccess)
        assertTrue(Try(df.select("identifiers.*")).isSuccess)
      }

      "have all columns and column names should be non sanitized if output format is json" in {
        val appConfig = AppConfig("config/application-bronze-json-out.json")
        val df = DataProcessor.process(appConfig)._1
        assertTrue(Try(df("details.lastName")).isSuccess)
        assertTrue(Try(df("details.Serial Number")).isSuccess)
        assertTrue(Try(df("details.Serial_Number")).isFailure)
        assertTrue(Try(df("services")).isSuccess)
        assertTrue(Try(df.select("identifiers.*")).isSuccess)
      }
    }

    "processed with ignoreColumns and flatten=false" should {
      "have no columns that were dropped" in {
        val appConfig = AppConfig("config/application-json-out-flattenFalse-ignoreColumns.json")
        val ignoreColumns = appConfig.ignoreColumns
        val expectedIgnoreColumns = "details.lastName,details.Serial Number,services,identifiers".split(",")
        assertTrue(expectedIgnoreColumns.diff(ignoreColumns).isEmpty)

        val df = DataProcessor.process(appConfig)._1
        logger.info(df.schema.treeString)
        assertTrue(Try(df("details.lastName")).isFailure)
        assertTrue(Try(df("details.Serial Number")).isFailure)
        assertTrue(Try(df("services")).isFailure)
        assertTrue(Try(df.select("identifiers.*")).isFailure)
      }
    }
  }

  "model" when {
    "not found same dataframe" should {
      "throw exception" in {
        val appConfig = AppConfig("config/application-modelUnknown.json")
        try {
          val df = DataProcessor.process(appConfig)
          Assert.fail("exception should be thrown")
        }
        catch {
          case t:Throwable =>
        }
      }
    }

    "found dataframe" should {
      "be transformed" in {
        val appConfig = AppConfig("config/application-modelSubscriber.json")
        val df = DataProcessor.process(appConfig)
        assertFalse(df._1.columns.contains("details"))
        assertFalse(df._1.columns.contains("identifiers"))
      }
    }
  }

  "csvExport" in {
    val appConfig = AppConfig("config/application-subscriber-csvExport.json")
    assertTrue(appConfig.csvExportEnabled)
    assertTrue(appConfig.csvExportLocation.isDefined)
    val df = DataProcessor.process(appConfig)
    val csvDf = spark.read.csv(appConfig.csvExportLocation.get)
    assertEquals(df._1.count(), csvDf.count())
  }

  "input_file_list" should {
    "be processed" in {
      val appConfig = AppConfig("config/application-input_file_list.json")
      assertEquals(1, appConfig.inputFileList.split(",").size)
      val df = DataProcessor.process(appConfig)
      assertEquals(100, df._1.count())
    }
  }

  "insertColumns" in {
    var appConfig = AppConfig("config/application-input_file_list.json")
    assertEquals(1, appConfig.inputFileList.split(",").size)
    var df = DataProcessor.process(appConfig)
    val origColumns = df._1.columns.length
    logger.info("orig column count: {}: {}", origColumns, df._1.columns)

    val insertColVal = "ts_date: 2021-03-05, testNum: 10"

    appConfig = AppConfig("config/application-input_file_list.json", s"insert_columns=${insertColVal}")
    df = DataProcessor.process(appConfig)
    assertEquals(origColumns+2, df._1.columns.length)
    assertTrue(df._1.columns.contains("ts_date"))
    assertTrue(df._1.columns.contains("testNum"))
    val tsDate = df._1.select("ts_date").head(1)(0)(0)
    logger.info("tsDate: {}", tsDate)
    assertEquals("2021-03-05", tsDate)
    logger.info("{}", df._1.printSchema())
  }


  "pipeline" should {
    "run with single processor single sink" in {
      implicit val appConfig = AppConfig("config/pipelines/singleProcessor.json")
      val pipelines = appConfig.pipelines
      assertTrue(pipelines != null && pipelines.nonEmpty)
      pipelines.foreach(logger.debug("{}", _))
      assertEquals(1, pipelines.length)
      assertEquals(1, pipelines.head.processors.length)
      assertEquals(1, pipelines.head.processors.head.sinks.length)

      val result = DataProcessor.process(appConfig)
      assertEquals("NA", result._2)
      val path = pipelines.head.processors.head.sinks.head.location
      validateTransformed(spark.read.parquet(path))
    }

    "run with single processor multi sink" in {
      implicit val appConfig = AppConfig("config/pipelines/singleProcessorMultiSink.json")
      val pipelines = appConfig.pipelines
      assertTrue(pipelines != null && pipelines.nonEmpty)
      pipelines.foreach(logger.debug("{}", _))
      assertEquals(1, pipelines.length)
      assertEquals(1, pipelines.head.processors.length)
      assertEquals(2, pipelines.head.processors.head.sinks.length)

      val result = DataProcessor.process(appConfig)
      assertEquals("NA", result._2)
      val firstSinkPath = pipelines.head.processors.head.sinks.head.location
      validateTransformed(spark.read.parquet(firstSinkPath))

      val secondSinkPath = pipelines.head.processors.head.sinks.tail.head.location
      validateTransformed(spark.read.option("header","true").csv(secondSinkPath))
    }

    "run with multiple processor" in {
      implicit val appConfig = AppConfig("config/pipelines/multiProcessor.json")
      val pipelines = appConfig.pipelines
      assertTrue(pipelines != null && pipelines.nonEmpty)
      pipelines.foreach(logger.debug("{}", _))
      assertEquals(1, pipelines.length)
      assertEquals(2, pipelines.head.processors.length)
      assertEquals(1, pipelines.head.processors.head.sinks.length)
      assertEquals(1, pipelines.head.processors.tail.head.sinks.length)

      val result = DataProcessor.process(appConfig)
      assertEquals("NA", result._2)
      val firstPath = pipelines.head.processors.head.sinks.head.location
      validateTransformed(spark.read.parquet(firstPath))

      val secondPath = pipelines.head.processors.tail.head.sinks.head.location
      val conf = Map(
        "columns" -> Seq("device_id", "ts_year", "ts_month", "ts_day"),
        "aggregate" -> Seq("bytes_received", "bytes_sent", "ethernet_packets_received"),
        "stats" -> Seq("count", "mean", "stddev", "min", "max", "sum")
      )
      validateGroupBy(spark.read.parquet(secondPath), conf)
    }

    "split processing " in {
      implicit val appConfig = AppConfig("config/pipelines/singleProcessor-withSplitter.json")
      val pipelines = appConfig.pipelines
      assertTrue(pipelines != null && pipelines.nonEmpty)
      pipelines.foreach(logger.debug("{}", _))
      assertEquals(1, pipelines.length)
      assertEquals(1, pipelines.head.processors.length)
      assertEquals(1, pipelines.head.processors.head.sinks.length)

      val result = DataProcessor.process(appConfig)
      assertEquals("Split locations", result._2)
      val path = pipelines.head.processors.head.sinks.head.location
      validateTransformed(spark.read.parquet(path))
    }

    def validateTransformed(transformed: DataFrame): Unit = {
      assertEquals(52, transformed.columns.length)

      assertTrue(transformed.columns.contains("device_id"))
      assertTrue(transformed.columns.contains("bytes_received"))
      assertFalse("dropTransformation should drop it", transformed.columns.contains("bytes_received_prev"))
      assertTrue(transformed.columns.contains("bytes_received_current"))
      assertTrue(transformed.columns.contains("packets_received"))
      assertFalse("dropTransformation should drop it", transformed.columns.contains("packets_received_prev"))
      assertTrue(transformed.columns.contains("packets_received_current"))

      assertTrue(transformed.columns.contains("ethernet_bytes_received"))
      assertFalse("dropTransformation should drop it", transformed.columns.contains("ethernet_packets_received_prev"))
      assertTrue(transformed.columns.contains("ethernet_packets_received_current"))
    }

    def validateGroupBy(df: DataFrame, conf: Map[String, Seq[String]]): Unit = {
      // sink adds a column ts_processed
      val expectedColumnLength = conf("columns").length + (conf("stats").length * conf("aggregate").length) + 1

      assertEquals(expectedColumnLength, df.columns.length)
      logger.debug("{}", df.schema.treeString)
      conf("columns").foreach(c => assertTrue(df.columns.contains(c)))
      conf("aggregate")
        .foreach(a => conf("stats")
          .foreach(s => assertTrue(df.columns.contains(s"${a}_$s"))))

      assertEquals(expectedColumnLength, df.take(1).head.length)
    }
  }

  val subSchema = new StructType().
    add("created", LongType).
    add(
      StructField("details", createStructType(
        Array(
          StructField("BTN", StringType),
          StructField("Serial Number", StringType),
          StructField("ZIP", StringType),
          StructField("lastName", StringType)
        )
      )
      )).
    add("id", StringType).
    add(
      StructField("identifiers", createStructType(
        Array(
          StructField("ISPUsername", StringType),
          StructField("USI", StringType),
          StructField("WTN", StringType),
          StructField("OUI", StringType)
        )
      )
      )).
    add(
      StructField("properties", createStructType(
        Array(
          StructField("deviceType", StringType),
          StructField("deviceCategory", StringType),
          StructField("dnsServers", createArrayType(StringType))
        )
      )
      )).
    add(
      StructField("services", createArrayType(StringType))
    ).
    add("timestamp", LongType)

  val detailsMod = StructType(Array(StructField("ZIP", StringType), StructField("SerialNumber", StringType)))
  //val fullSubs2 = df.select($"created",$"details".cast(detailsMod))
  //sub.withColumn("timestamp", from_unixtime($"timestamp"*time_multiplier)).withColumn("ts_date", to_date($"timestamp")).withColumn("ts_day", dayofyear($"timestamp")).withColumn("ts_month", month($"timestamp")).show
}

object Subscriber extends DataModel {
  override def model(dataFrame: DataFrame, appConfig: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    dataFrame.drop("details", "identifiers")
  }
}
