package com.commscope.eco

import com.typesafe.scalalogging.Logger
import junit.framework.Assert
import org.scalatest.WordSpec

class DataExportConfigTest extends WordSpec {
  val logger = Logger(this.getClass)
  "tables" in {
    val conf = DataExportConfig(Array("table==subscriber=src/test/resources/subscriber", "table==device=src/test/resources/device"))
    Assert.assertEquals(2, conf.tables.size)
    Assert.assertEquals("src/test/resources/subscriber", conf.tables("subscriber"))
    Assert.assertEquals("src/test/resources/device", conf.tables("device"))
  }

  "query" in {
    val query = "select * from subscriber s join device d on (s.id = d.sub_id)"
    val conf = DataExportConfig(Array(s"query==$query"))
    Assert.assertEquals(query, conf.query)
  }

  "format" should {
    "be default" in {
      Assert.assertEquals("parquet", DataExportConfig(Array()).format)
    }
    "be args value" in {
      val format = "csv"
      val conf = DataExportConfig(Array(s"format==$format"))
      Assert.assertEquals(format, conf.format)
    }
  }

  "mode" should {
    "be default" in {
      Assert.assertEquals("overwrite", DataExportConfig(Array()).mode)
    }

    "be the arg value" in {
      val mode = "append"
      val conf = DataExportConfig(Array(s"mode==$mode"))
      Assert.assertEquals(mode, conf.mode)
    }
  }

  "partitions" should {
    "be default" in {
      Assert.assertTrue(DataExportConfig(Array()).partitions.isEmpty)
    }

    "be the arg value" in {
      val partitions = "ts_date,ts_day"
      val conf = DataExportConfig(Array(s"partitions==$partitions"))
      Assert.assertEquals(partitions, conf.partitions.mkString(","))
    }
  }

  "output location" in {
    val output = "src/test/joined"
    val conf = DataExportConfig(Array(s"output_location==$output"))
    Assert.assertEquals(output, conf.outputLocation)
  }

  "DataExport conf" in {
    val query = "select * from subscriber s join device d on (s.id = d.sub_id)"
    val tables = Map(
      "subscriber" -> "src/test/resources/subscriber",
      "device" -> "src/test/resources/device"
    )

    val output = "src/test/joined"
    val args = Array(
      "table==subscriber=src/test/resources/subscriber",
      "table==device=src/test/resources/device",
      s"query==$query",
      s"output_location==$output")
    val conf = DataExportConfig(args)

    Assert.assertEquals(query, conf.query)
    Assert.assertEquals("parquet", conf.format)
    Assert.assertEquals("overwrite", conf.mode)
    Assert.assertEquals(output, conf.outputLocation)
    Assert.assertEquals(tables("subscriber"), conf.tables("subscriber"))
    Assert.assertEquals(tables("device"), conf.tables("device"))
  }

  "DataExport conf with select columns" in {
    val query = "select * from subscriber s join device d on (s.id = d.sub_id)"
    val tables = Map(
      "subscriber" -> "src/test/resources/subscriber",
      "device" -> "src/test/resources/device"
    )
    val selectColumns = Map(
      "subscriber" -> "subscriber_id",
      "device" -> "device_id,subscriber_id"
    )

    val output = "src/test/joined"
    val args = Array(
      "table==subscriber=src/test/resources/subscriber",
      "table==device=src/test/resources/device",
      "select==subscriber=subscriber_id",
      "select==device=device_id, subscriber_id",
      s"query==$query",
      s"output_location==$output")
    val conf = DataExportConfig(args)

    Assert.assertEquals(query, conf.query)
    Assert.assertEquals("parquet", conf.format)
    Assert.assertEquals("overwrite", conf.mode)
    Assert.assertEquals(output, conf.outputLocation)
    Assert.assertEquals(tables("subscriber"), conf.tables("subscriber"))
    Assert.assertEquals(tables("device"), conf.tables("device"))
    selectColumns.foreach(e => Assert.assertTrue(conf.selectColumns(e._1).mkString(",").equals(e._2)))
  }

  "DataExport conf with drop columns" in {
    val query = "select * from subscriber s join device d on (s.id = d.sub_id)"
    val tables = Map(
      "subscriber" -> "src/test/resources/subscriber",
      "device" -> "src/test/resources/device"
    )

    val output = "src/test/joined"
    val args = Array(
      "table==subscriber=src/test/resources/subscriber",
      "table==device=src/test/resources/device",
      s"query==$query",
      s"output_location==$output")
    val conf = DataExportConfig(args)

    Assert.assertEquals(query, conf.query)
    Assert.assertEquals("parquet", conf.format)
    Assert.assertEquals("overwrite", conf.mode)
    Assert.assertEquals(output, conf.outputLocation)
    Assert.assertEquals(tables("subscriber"), conf.tables("subscriber"))
    Assert.assertEquals(tables("device"), conf.tables("device"))
  }
}
