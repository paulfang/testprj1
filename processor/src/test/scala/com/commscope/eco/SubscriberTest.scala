package com.commscope.eco

import org.junit.Assert._
import org.junit._
import org.slf4j.LoggerFactory

@Test
class SubscriberTest extends SparkBasedTest {
    val logger = LoggerFactory.getLogger(getClass)
    
    val data_base_dir = this.getClass.getClassLoader.getResource("data/frontier").getPath

    @After
    override def tearDown() {
        super.tearDown()
    }

    @Test
    def testReadJsonWithoutSchema() = {
        val df = DataFrameUtils.fromJson(data_base_dir + "/subscriber.json")
        assertNotNull(df)
        val expectedCols = Seq("created", "details", "id", "identifiers", "services", "timestamp")
        logger.debug("Subscriber Start ***************")
        logger.debug("columns: {}", df.columns)
        logger.debug("schema: {}", df.schema.treeString)
        logger.debug("head: {}", df.head(1))
        logger.trace("schema in json: {}", df.schema.prettyJson)
        logger.debug("Subscriber End ***************")
        val foundCols = df.columns
        assertTrue(expectedCols.diff(foundCols).isEmpty)
    }

    @Test
    def testSchemaLoader(): Unit = {
        val schema = SchemaUtils.loadEntitySchema("subscriber-entity")
        assert(schema.isDefined)
        logger.debug(schema.get.input.treeString)
        assert(!schema.get.input.isEmpty)
        assert(!schema.get.output.isEmpty)
    }

    @Test
    def testLoadDataUsingSchemaWithLessColumns(): Unit = {
        val df = DataFrameUtils.fromJson(data_base_dir + "/subscriber.json", schemaJsonPath = Some("subscriberLessColumns"))
        assertNotNull(df)
        logger.debug("columns: {}", df.columns)
        logger.debug("schema: {}", df.schema.treeString)
        logger.debug("head {}", df.head(1))
    }

    @Test
    def testLoadDataUsingSchemaTxColumns(): Unit = {
        val df = DataFrameUtils.fromJson(data_base_dir + "/subscriber.json", schemaJsonPath = Some("subscriberTxColumns"))
        assertNotNull(df)
        logger.debug("columns: {}", df.columns)
        logger.debug("schema: {}", df.schema.treeString)
        logger.debug("head {}", df.head(1))
    }

}


