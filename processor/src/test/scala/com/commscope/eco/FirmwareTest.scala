package com.commscope.eco

import org.junit.Assert._
import org.junit._
import org.slf4j.LoggerFactory

@Test
class FirmwareTest extends SparkBasedTest {
    val logger = LoggerFactory.getLogger(getClass)

    @After
    override def tearDown() {
        super.tearDown()
    }

    @Test
    def testReadJson() = {
        val jsonPath = this.getClass.getClassLoader.getResource("data/frontier/firmware-diff.json").getPath
        val df = DataFrameUtils.fromJson(jsonPath)
        assertNotNull(df)
        logger.info("firmware columns: {}", df.columns)
        logger.info("firmware schema: {}", df.schema.treeString)
        logger.info("firmware head: {}", df.head(1))
        logger.info("firmware schema in json: {}", df.schema.prettyJson)
    }
}


