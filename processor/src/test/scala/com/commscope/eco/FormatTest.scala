package com.commscope.eco

import org.junit.Assert._
import org.junit._
import org.scalatest.WordSpec
import org.slf4j.LoggerFactory

@Test
class FormatTest extends WordSpec {
  val logger = LoggerFactory.getLogger(getClass)

  "name" when {
    "found" should {
      "succeed " in {
        assertEquals(Format.csv, Format.withNameOrElse("csv"))
        assertEquals(Format.parquet, Format.withNameOrElse("parquet"))
      }
    }

    "not found" should {
      "get default " in {
        assertEquals(Format.csv, Format.withNameOrElse("unknown"))
      }
    }

    "not found with else" should {
      "return else" in {
        assertEquals(Format.parquet, Format.withNameOrElse("unknown", Format.parquet))
      }
    }

    "null" should {
      "return default or else" in {
        assertEquals(Format.csv, Format.withNameOrElse(null))
        assertEquals(Format.parquet, Format.withNameOrElse(null, Format.parquet))
      }
    }
  }
}


