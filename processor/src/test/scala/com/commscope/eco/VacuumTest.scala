package com.commscope.eco

import org.junit.Assert

class VacuumTest extends SparkBasedTest {
  "vacuum conf" should {
    "be parsed" in {
      val config = VacuumConfig("config/vacuum-conf.json")
      Assert.assertEquals("src/test/resources/data/frontier/silver/device-vacuum", config.inputLocation)

      Assert.assertNotNull(config.windowSpec)
      Assert.assertTrue(Seq("ts_date").diff(config.outputPartitions).isEmpty)
    }
  }

  "vacuum data" in  {
    val config = VacuumConfig("config/vacuum-conf.json")
    Assert.assertEquals("tmp/vacuum/device", config.outputLocation)
    Vacuum.main(Array[String]("config/vacuum-conf.json"))
    val origCount = spark.read.parquet(config.inputLocation).count
    Assert.assertEquals(10996, origCount)
    val countAfterVac = spark.read.parquet(config.outputLocation).count
    Assert.assertEquals(5501, countAfterVac)
  }
}
