package com.commscope.eco
import org.apache.spark.sql.{DataFrame, SparkSession}

class MockDataProcessor extends TDataProcessor {
  override def process(config: AppConfig)(implicit sparkSession: SparkSession): (DataFrame, String) = {
    null
  }
}
