package com.commscope.eco

import com.typesafe.scalalogging.Logger

class LoadPerformanceTest extends SparkBasedTest {
  val logger = Logger(this.getClass)
  "data" should {
    "be loaded" ignore  {
      val st = System.currentTimeMillis()
      val dp = spark.read.json("/Users/sanjeevmishra/data/frontier/negotiator/dailyparams/20200528")
      logger.warn("total time in load: {} sec.", (System.currentTimeMillis() - st)/1000)
      logger.info(dp.schema.treeString)
    }
  }
}
