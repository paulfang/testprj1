package com.commscope.eco

import com.typesafe.scalalogging.Logger
import junit.framework.Assert
import org.scalatest.BeforeAndAfterEach

class BronzeTest extends SparkBasedTest with BeforeAndAfterEach {
  val logger = Logger(getClass)

  "subscriber data" ignore {
    val config = AppConfig("config/bronze/application-bronze.json", "config/bronze/subscriber-bronze.json")
    DataProcessor.process(config)
  }

  "dailyparams data" ignore {
    val config = AppConfig("config/bronze/application-bronze.json", "config/bronze/dailyparams-bronze.json")
    DataProcessor.process(config)
  }

  "hosttable data" ignore {
    val config = AppConfig("config/bronze/application-bronze.json", "config/bronze/host-table-bronze.json")
    DataProcessor.process(config)
  }


  "element-event data" ignore {
    val config = AppConfig("config/bronze/application-bronze.json", "config/bronze/element-event-bronze.json")
    DataProcessor.process(config)
  }

  "firmware data" ignore {
    val config = AppConfig("config/bronze/application-bronze.json", "config/bronze/firmware-upgrade-bronze.json")
    DataProcessor.process(config)
  }
}

