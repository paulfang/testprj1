package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.junit.Assert.{assertEquals, assertFalse, assertTrue}
import org.scalatest.BeforeAndAfterEach

class GoldTest extends SparkBasedTest with BeforeAndAfterEach {
  val logger = Logger(getClass)

  val baseDir = "/Users/sanjeevmishra/data/frontier/warehouse"

  "client silver data" should {
    "be processed " ignore   {
      val config = AppConfig("config/gold/application-gold.json", "config/gold/client-gold.json")
      DataProcessor.process(config)
    }

    "be exported to csv" ignore {
      val config = AppConfig("config/gold/application-gold.json", "config/gold/client-gold-csvExport.json")
      DataProcessor.process(config)
    }
  }

  "clientBW" should {
    "be processed" in {
      implicit val appConfig = AppConfig("config/gold/client-bw-gold.json")
      val pipelines = appConfig.pipelines
      assertTrue(pipelines != null && pipelines.nonEmpty)
      pipelines.foreach(logger.debug("{}", _))
      assertEquals(1, pipelines.length)
      assertEquals(2, pipelines.head.processors.length)
      assertEquals(1, pipelines.head.processors.head.sinks.length)
      assertEquals(1, pipelines.head.processors.tail.head.sinks.length)

      val result = DataProcessor.process(appConfig)
      assertEquals("NA", result._2)
      val firstPath = pipelines.head.processors.head.sinks.head.location
      val firstDf = spark.read.parquet(firstPath)
      assertEquals(313, firstDf.count())
      assertEquals(30, firstDf.columns.length)
      assertEquals(5, firstDf.select("device_id","ts_day", "radio", "mac_address").distinct.count)

      val secondPath = pipelines.head.processors.tail.head.sinks.head.location
      val secondDf = spark.read.parquet(secondPath)
      assertEquals(5, secondDf.count())
      val secondDfColumns = secondDf.columns
      assertEquals(43, secondDfColumns.length)
      Seq("bytes_received", "bytes_sent", "rssi","signal_strength","last_transmit_rate").
        foreach(c => Seq("count", "mean", "stddev", "min", "max", "sum").
          foreach(f => assertTrue(secondDfColumns.contains(s"${c}_$f"))))
    }
  }

  "clientBW with category" should {
    "be modeled" in {
      implicit val appConfig = AppConfig("config/gold/client-bw-gold-with-category.json")
      val pipelines = appConfig.pipelines
      assertTrue(pipelines != null && pipelines.nonEmpty)

      val result = DataProcessor.process(appConfig)
      assertEquals("NA", result._2)
      val firstPath = pipelines.head.processors.head.sinks.head.location
      val firstDf = spark.read.parquet(firstPath)
      logger.info("firstDf: {}", firstDf.schema.treeString)
      val firstDfColumns = firstDf.columns
      assertEquals(2549, firstDf.count())
      assertEquals(33, firstDfColumns.length)
      assertFalse(firstDfColumns.contains("radio_clients"))
      assertFalse(firstDfColumns.contains("wifi_clients"))
      assertTrue(firstDfColumns.contains("client_category"))
      assertTrue(firstDfColumns.contains("client_device_type"))
      assertTrue(firstDfColumns.contains("client_manufacturer"))
      assertEquals(96, firstDf.select("ts").distinct.count)
      assertEquals(30, firstDf.select("device_id","ts_day", "radio", "mac_address").distinct.count)

      val dfHead = firstDf.head(3)
      logger.debug("{}", dfHead)
      val expectedCategories = Seq("DLNA","Phone","Set Top Box", "Computer")
      val categories = firstDf.select("client_category").distinct().head(100).map(_.getString(0))

      logger.debug("client_categories: {}", categories)
      expectedCategories.foreach(c => assertTrue(categories.contains(c)))

      val secondPath = pipelines.head.processors.tail.head.sinks.head.location
      val secondDf = spark.read.parquet(secondPath)
      logger.info("secondDf: {}", secondDf.schema.treeString)
      val secondDfColumns = secondDf.columns
      assertEquals(46, secondDfColumns.length)
      Seq("bytes_received", "bytes_sent", "rssi","signal_strength","last_transmit_rate").
        foreach(c => Seq("count", "mean", "stddev", "min", "max", "sum").
          foreach(f => assertTrue(secondDfColumns.contains(s"${c}_$f"))))

      assertEquals(30, secondDf.count())
    }
  }

  "clientBW pipeline" should {
    "run" in {
      implicit val appConfig = AppConfig("config/gold/clientBWPipeline.json")
      val df = spark.read.parquet(appConfig.inputLocation)
      assertEquals(96, df.count())
      val columns = df.columns

      assertTrue(columns.contains("wifi_clients"))
      assertTrue(columns.contains("radio_clients"))
      assertFalse(columns.contains("client_category"))
      assertFalse(columns.contains("client_device_type"))
      assertFalse(columns.contains("client_manufacturer"))

      val result = appConfig.pipelines.head.run(df)

      val baseColumns = Seq("device_id", "subscriber_id", "device_model", "zip",
        "city", "state", "country", "uptime", "ts_year",
        "ts_month", "ts_day", "ts_hour", "ts_inform", "ts")

      val modeledColumns = result.columns
      assertFalse(modeledColumns.contains("radio_clients"))
      assertFalse(modeledColumns.contains("wifi_clients"))

      assertTrue(modeledColumns.contains("client_category"))
      assertTrue(modeledColumns.contains("client_device_type"))
      assertTrue(modeledColumns.contains("client_manufacturer"))

      logger.debug(result.schema.treeString)

      assertEquals(53, modeledColumns.length)

      assertEquals(30, result.count())
      val dfHead = result.head(3)
      logger.debug("{}", dfHead)
      val expectedCategories = Seq("DLNA","Phone","Set Top Box", "Computer")
      val categories = result.select("client_category").distinct().head(100).map(_.getString(0))

      logger.debug("client_categories: {}", categories)
      expectedCategories.foreach(c => assertTrue(categories.contains(c)))
    }
  }
}
