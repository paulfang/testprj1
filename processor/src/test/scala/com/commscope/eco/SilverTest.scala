package com.commscope.eco

import com.typesafe.scalalogging.Logger
import junit.framework.Assert
import org.scalatest.BeforeAndAfterEach

class SilverTest extends SparkBasedTest with BeforeAndAfterEach {
  val logger = Logger(getClass)

  "subscriber bronze data" ignore {
    val config = AppConfig("config/silver/application-silver.json", "config/silver/subscriber-silver.json")
    Assert.assertNotNull(config.outputSchema)
    DataProcessor.process(config)
  }

  "dailyparams bronze data" ignore {
    val config = AppConfig("config/silver/application-silver.json", "config/silver/dailyparams-silver.json")
    Assert.assertNotNull(config.outputSchema)
    DataProcessor.process(config)
  }

  "hosttable bronze data" ignore {
    val config = AppConfig("config/silver/application-silver.json", "config/silver/host-table-silver.json")
    Assert.assertNotNull(config.outputSchema)
    DataProcessor.process(config)
  }

  "element-event bronze data" ignore {
    val config = AppConfig("config/silver/application-silver.json", "config/silver/element-event-silver.json")
    Assert.assertNotNull(config.outputSchema)
    val result = DataProcessor.process(config)
    logger.info("head: {}", result._1.select("device_id","type","software_version","hnc_enable","ts_date").head(3))
  }

  "firmware bronze data" ignore {
    val config = AppConfig("config/silver/application-silver.json", "config/silver/firmware-upgrade-silver.json")
    Assert.assertNotNull(config.outputSchema)
    DataProcessor.process(config)
  }

  "element-ts bronze data" ignore {
    val config = AppConfig("config/silver/application-silver.json", "config/silver/element-ts-silver.json")
    Assert.assertNotNull(config.outputSchema)
    DataProcessor.process(config)
  }
}
