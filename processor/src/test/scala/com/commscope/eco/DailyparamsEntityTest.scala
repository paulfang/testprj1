package com.commscope.eco

import java.io.File

import org.apache.spark.sql.types.{DataType, StructType}
import org.junit.Assert._
import org.junit._
import org.slf4j.LoggerFactory

@Test
class DailyparamsTest extends SparkBasedTest {
    val logger = LoggerFactory.getLogger(getClass)

    val data_base_dir = this.getClass.getClassLoader.getResource("data/frontier").getPath
    val testOutDir = "/tmp/dailyparams_"+System.nanoTime()

    @Before
    def setup(): Unit = {
        new File(testOutDir).delete()
    }

    @After
    override def tearDown() {
        super.tearDown()
        new File(testOutDir).delete()
    }

    @Test
    def testReadJsonWithoutSchema() = {
        val expectedFields = 7
        val expectedSqlFields = 73
        val df = DataFrameUtils.fromJson(data_base_dir + "/dailyparams.json")
        logger.trace(df.schema.sql)
        logger.trace("schema.sql count: {}",df.schema.sql.split(":").length)
        assertNotNull(df)
        assertEquals(expectedFields, df.schema.fieldNames.length)
        assertEquals(expectedSqlFields, df.schema.sql.split(":").length)
    }

    @Test
    def testLoadFormatJsonWithoutSchema() = {
        val expectedFields = 7
        val expectedSqlFields = 73
        val df = DataFrameUtils.load(Format.json,data_base_dir + "/dailyparams.json")
        logger.trace(df.schema.sql)
        assertNotNull(df)
        assertEquals(expectedFields, df.schema.fieldNames.length)
        val sqlFields = df.schema.sql.split(":")
        assertEquals(expectedSqlFields, sqlFields.length)

        val dfComplex = DataFrameUtils.fromJson(data_base_dir + "/dailyparams-test-replaceChars-complex.json")
        val complexSqlFields = dfComplex.schema.sql.split(":")
        logger.debug("field difference between full data and selected rows: {}", sqlFields.length - complexSqlFields.length)
    }


    @Test
    def testFromJsonWithSchema(): Unit = {
        val expectedSimpleSqlCount = 7
        val expectedComplexSqlCount = 65
        val dfSimple = DataFrameUtils.fromJson(data_base_dir + "/dailyparams-test-replaceChars-simple.json",
            schemaJsonPath = Some("schema/dailyparams-test-replaceChars-simple.json"))

        assertNotNull(dfSimple)
        logger.trace("simple columns: {}", dfSimple.columns)
        logger.trace("simple sql length: {}", dfSimple.schema.sql.split(":").length)
        assertEquals(expectedSimpleSqlCount, dfSimple.schema.sql.split(":").length)
        logger.trace("simple sql: {}", dfSimple.schema.sql)

        val dfComplex = DataFrameUtils.fromJson(data_base_dir + "/dailyparams-test-replaceChars-complex.json",
            schemaJsonPath = Some("schema/dailyparams-test-replaceChars-complex.json"))
        assertNotNull(dfComplex)
        logger.trace("complex columns: {}", dfComplex.columns)
        logger.trace("complex sql length: {}", dfComplex.schema.sql.split(":").length)
        assertEquals(expectedComplexSqlCount, dfComplex.schema.sql.split(":").length)
        logger.trace("complex sql: {}", dfComplex.schema.sql)
    }

    @Test
    def testLoadFormatJsonWithSchema(): Unit = {
        val expectedSimpleSqlCount = 7
        val expectedComplexSqlCount = 65
        val dfSimple = DataFrameUtils.load(Format.json, data_base_dir + "/dailyparams-test-replaceChars-simple.json",
            schemaJsonPath = Some("schema/dailyparams-test-replaceChars-simple.json"))

        assertNotNull(dfSimple)
        logger.trace("simple columns: {}", dfSimple.columns)
        logger.trace("simple sql length: {}", dfSimple.schema.sql.split(":").length)
        assertEquals(expectedSimpleSqlCount, dfSimple.schema.sql.split(":").length)
        logger.trace("simple sql: {}", dfSimple.schema.sql)


        val dfComplex = DataFrameUtils.load(Format.json, data_base_dir + "/dailyparams-test-replaceChars-complex.json",
            schemaJsonPath = Some("schema/dailyparams-test-replaceChars-complex.json"))
        assertNotNull(dfComplex)
        logger.trace("complex columns: {}", dfComplex.columns)
        logger.trace("complex sql length: {}", dfComplex.schema.sql.split(":").length)
        assertEquals(expectedComplexSqlCount, dfComplex.schema.sql.split(":").length)
        logger.trace("complex sql: {}", dfComplex.schema.sql)
    }


    @Test
    def testReplaceIllegalCharsFromSchemaSimple(): Unit = {
        val df = DataFrameUtils.fromJson(data_base_dir + "/dailyparams-test-replaceChars-simple.json",
            schemaJsonPath = Some("schema/dailyparams-test-replaceChars-simple.json"))
        logger.trace("Original schema\n{}", df.schema.treeString)
        logger.trace("Original sql: {}", df.schema.sql)

        assertIllegalCharsFound(df.schema)

        val removed = SchemaUtils.sanitize(df.schema)
        assertNoIllegalChars(removed.get)
    }

    @Test
    def testReplaceIllegalCharsFromSchemaComplex(): Unit = {
        val df = DataFrameUtils.fromJson(data_base_dir + "/dailyparams-test-replaceChars-complex.json")
        logger.trace("Original schema\n{}", df.schema.treeString)
        logger.trace("Original schema\n{}", df.schema.prettyJson)

        logger.trace("Original sql: {}", df.schema.sql)

        assertIllegalCharsFound(df.schema)

        val removed = SchemaUtils.sanitize(df.schema)
        assertNoIllegalChars(removed.get)
    }

    @Test
    def testReplaceIllegalCharsLoadFormatJson(): Unit = {
        val path = data_base_dir + "/dailyparams-test-replaceChars-simple.json"
        val simpleDF = DataFrameUtils.load(Format.json, path,
            schemaJsonPath = Some("schema/dailyparams-test-replaceChars-simple.json"))
        logger.trace("Original schema\n{}", simpleDF.schema.treeString)
        logger.trace("Original sql: {}", simpleDF.schema.sql)

        assertIllegalCharsFound(simpleDF.schema)

        val removed = SchemaUtils.sanitize(simpleDF.schema)
        assertNoIllegalChars(removed.get)

        val xdf = DataFrameUtils.load(Format.json, path, removed.get.asInstanceOf[StructType])
        val xschema = xdf.schema
        assertNoIllegalChars(xschema)
        DataFrameUtils.toParquet(xdf, testOutDir)
    }

    @Test
    def testReplaceIllegalCharsFromSchemaSimpleDFtoParquet(): Unit = {
        val path = data_base_dir + "/dailyparams-test-replaceChars-simple.json"
        val df = DataFrameUtils.load(Format.json, path,
            schemaJsonPath = Some("schema/dailyparams-test-replaceChars-simple.json"))

        logger.trace("Original schema\n{}", df.schema.treeString)
        logger.trace("Original sql: {}", df.schema.sql)

        assertIllegalCharsFound(df.schema)

        val removed = SchemaUtils.sanitize(df.schema)
        val xdf = DataFrameUtils.load(Format.json, path, removed.get.asInstanceOf[StructType])
        val xschema = xdf.schema
        assertNoIllegalChars(xschema)
        DataFrameUtils.toParquet(xdf, testOutDir)
    }

    @Test
    def givenComplexSchemaWithIllegalChars2ParquetShouldThrowException(): Unit = {
        val path = data_base_dir + "/dailyparams-test-replaceChars-complex.json"
        val df = DataFrameUtils.fromJson(path)
        assertIllegalCharsFound(df.schema)
        var exceptionThrows = false
        try {
            DataFrameUtils.toParquet(df, testOutDir)
        }
        catch {
            case e: Error =>
            case t: Throwable => exceptionThrows = true
        }
        assertTrue(exceptionThrows)
    }

    @Test
    def sanitizingDataFrameSchemaAndSaving2ParquetShouldSucceed(): Unit = {
        {
            val path = data_base_dir + "/dailyparams-test-replaceChars-complex.json"
            val df = DataFrameUtils.fromJson(path)
            assertIllegalCharsFound(df.schema)

            val removed = SchemaUtils.sanitize(df.schema)
            val xdf = DataFrameUtils.fromJson(path, removed.get.asInstanceOf[StructType])
            val xschema = xdf.schema
            assertNoIllegalChars(xschema)
            DataFrameUtils.toParquet(xdf, testOutDir)
        }
        {
            val path = data_base_dir + "/dailyparams.json"
            val df = DataFrameUtils.fromJson(path)
            assertIllegalCharsFound(df.schema)

            val removed = SchemaUtils.sanitize(df.schema)
            val xdf = DataFrameUtils.fromJson(path, removed.get.asInstanceOf[StructType])
            val xschema = xdf.schema
            assertNoIllegalChars(xschema)
            val fl = new File(testOutDir + Math.random())
            fl.deleteOnExit()
            DataFrameUtils.toParquet(xdf, fl.getAbsolutePath)
        }
    }

    @Test
    def sanitizingSchemaBeforeLoadingJsonAndSavingToParquetShouldSucceed(): Unit = {
        {
            val path = data_base_dir + "/dailyparams-test-replaceChars-complex.json"
            val schema = SchemaUtils.loadDataFrameSchema("schema/dailyparams-test-replaceChars-complex.json")
            val sanitized = SchemaUtils.sanitize(schema.get).get.asInstanceOf[StructType]
            val df = DataFrameUtils.fromJson(path, sanitized)
            assertNoIllegalChars(df.schema)
            logger.debug("sql col count: {}", df.schema.sql.split(":").length)
        }
        {
            val path = data_base_dir + "/dailyparams.json"
            val schema = SchemaUtils.loadDataFrameSchema("schema/dailyparams-test-replaceChars-complex.json")
            val sanitized = SchemaUtils.sanitize(schema.get).get.asInstanceOf[StructType]
            val df = DataFrameUtils.fromJson(path, sanitized)
            assertNoIllegalChars(df.schema)
            logger.debug("sql col count: {}", df.schema.sql.split(":").length)
        }
        {
            val path = data_base_dir + "/dailyparams.json"
            val schema = SchemaUtils.loadDataFrameSchema("schema/dailyparams-test-replaceChars-simple.json")
            val sanitized = SchemaUtils.sanitize(schema.get).get.asInstanceOf[StructType]
            val df = DataFrameUtils.fromJson(path, sanitized)
            assertNoIllegalChars(df.schema)
            logger.debug("sql col count: {}", df.schema.sql.split(":").length)
        }
    }

    @Test
    def json2ParquetRoundTripShouldSucceed(): Unit = {
        val path = data_base_dir + "/dailyparams-test-replaceChars-complex.json"
        val schema = SchemaUtils.loadDataFrameSchema("schema/dailyparams-test-replaceChars-complex.json")
        val sanitized = SchemaUtils.sanitize(schema.get).get.asInstanceOf[StructType]
        val jsonDF = DataFrameUtils.fromJson(path, sanitized)
        val origSqlColumnCount = jsonDF.schema.sql.split(":").length
        val fl = new File(testOutDir + Math.random())
        fl.deleteOnExit()
        DataFrameUtils.toParquet(jsonDF, fl.getAbsolutePath)

        val parquetDF = DataFrameUtils.fromParquet(fl.getAbsolutePath)
        val parquetSqlColumnCount = parquetDF.schema.sql.split(":").length
        assertEquals(origSqlColumnCount, parquetSqlColumnCount)
    }

    @Test
    def json2DeltaRoundTripShouldSucceed(): Unit = {
        val path = data_base_dir + "/dailyparams-test-replaceChars-complex.json"
        val schema = SchemaUtils.loadDataFrameSchema("schema/dailyparams-test-replaceChars-complex.json")
        val sanitized = SchemaUtils.sanitize(schema.get).get.asInstanceOf[StructType]
        val jsonDF = DataFrameUtils.fromJson(path, sanitized)
        val origRows = jsonDF.count()
        val origSqlColumnCount = jsonDF.schema.sql.split(":").length
        val fl = new File(testOutDir + Math.random())
        fl.deleteOnExit()
        DataFrameUtils.toDelta(jsonDF, fl.getAbsolutePath)

        val deltaDF = DataFrameUtils.fromDelta(fl.getAbsolutePath)
        val deltaRowCount = deltaDF.count()
        val deltaSqlColumnCount = deltaDF.schema.sql.split(":").length
        val parquetDF = DataFrameUtils.fromParquet(fl.getAbsolutePath)
        val parquetRowCount = parquetDF.count()
        assertEquals(origRows, deltaRowCount)
        assertEquals(parquetRowCount, deltaRowCount)
        assertEquals(origSqlColumnCount, deltaSqlColumnCount)
    }

    private def assertIllegalCharsFound(schema: DataType) = {
        val origSql = schema.sql.split(",").foldLeft(List[Array[String]]())((l, e) => l :+ e.split(":")).flatten.map(_.trim).flatten(DataFrameUtils.illegalCharsRegex.findAllMatchIn(_))
        logger.debug("illegal chars in orig df: {}", origSql)
        assertFalse(origSql.isEmpty)
    }

    private def assertNoIllegalChars(xschema: DataType) = {
        xschema match {
            case st: StructType => logger.trace("Replaced schema:\n{}", st.treeString)
                logger.trace("Replaced sql:\n{}", st.sql)

                val modSql = st.sql.split(",").foldLeft(List[Array[String]]())((l, e) => l :+ e.split(":")).flatten.map(_.trim).flatten(DataFrameUtils.illegalCharsRegex.findAllMatchIn(_))
                logger.debug("illegal chars in replaced df: {}", modSql)

                assertTrue(modSql.isEmpty)
            case _ =>
        }
    }
}


