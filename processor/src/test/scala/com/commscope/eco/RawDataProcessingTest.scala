package com.commscope.eco

import com.typesafe.scalalogging.Logger

import scala.collection.mutable

class RawDataProcessingTest extends SparkBasedTest {
  val logger = Logger(getClass)

  "Given raw data and schema " when {
    "processed" should {
      "succeed" in {
        val schema = SchemaUtils.loadEntitySchema("firmware-entity")
        assert(schema.isDefined)
        //val structType = DataType.fromJson(schema.get.input).asInstanceOf[StructType]
        logger.debug(schema.get.input.treeString)
        assert(!schema.get.input.isEmpty)
        assert(!schema.get.output.isEmpty)
      }
    }
  }

  "Firmware schema " when {
    "found" should {
      "load" in {
        val schema = SchemaUtils.loadDataFrameSchema("firmware-raw")
        assert(schema.isDefined)
        //val structType = DataType.fromJson(schema.get.input).asInstanceOf[StructType]
        logger.debug(schema.get.treeString)
      }
    }
  }

  "Schema" when {
    "nested" should {
      "flatten" in {
        val schema = SchemaUtils.loadDataFrameSchema("dailyparams-raw")
        assert(schema.isDefined)
        val sqlColCount = schema.get.sql.split(":").length
        logger.debug("col count before flattening: {}", sqlColCount)
        logger.debug("struct type count: {}", schema.get.sql.split(",").filter(_.toLowerCase.contains("struct")).length)

        val flattenSchema = SchemaUtils.flatten(schema)
        val flattenSqlColCount = flattenSchema.get.length
        logger.debug("sql col count after flattening: {}", flattenSqlColCount)
        logger.debug("schema type count: {}", flattenSchema.get)
      }
    }
  }

  "Nested schema" when {
    "sanitized and flatten" should {
      "have no illegal chars" in {
        val schema = SchemaUtils.loadDataFrameSchema("dailyparams-raw")
        assert(schema.isDefined)
        val flattenSchema = SchemaUtils.flatten(schema)
        val colNames = flattenSchema.get.filter(c => DataFrameUtils.illegalCharsRegex.findAllMatchIn(c.toString()).length > 0)
        logger.debug("illegal chars in flatten schema: {}", colNames)
        assert(!colNames.isEmpty)

        val flattenSchemaWithSanitization = SchemaUtils.flatten(schema, sanitizeAfterFlattening = true)
        val illegalCharColumns = flattenSchemaWithSanitization.get.mkString(",").
          split(",").foldLeft(mutable.Map[String, String]())((m, c) => {
          val arr = c.split(" AS ")
          m += (arr(0) -> arr(1))
          m
        }).values.filter(c => DataFrameUtils.illegalCharsRegex.findAllMatchIn(c).length > 0)

        assert(illegalCharColumns.isEmpty)
      }
    }
  }
}
