package com.commscope.eco

import org.junit.Assert

class PartitionUtilTest extends SparkBasedTest {
  "transformer conf" should {
    "be parsed" in {
      val config = TransformerConfig("src/test/input", "src/test/output", "ts_date,ts_day")

      Assert.assertEquals("src/test/input", config.inputLocation)
      Assert.assertEquals("src/test/output", config.outputLocation)
      Assert.assertTrue(Seq("ts_date","ts_day").diff(config.partitions).isEmpty)
    }
  }
}
