package com.commscope.eco

import com.typesafe.scalalogging.Logger
import org.junit.Assert._
import org.scalatest.WordSpec

class NodeTest extends WordSpec {
  val logger = Logger(getClass)

  "node" when {
    val node = Node()

    "with no children" should {
      "be a leaf node" in {
        assertTrue(node.isLeaf)
      }
    }

    "added a null" should {
      "still be a leaf node" in {
        node.addChild(null)
        assertTrue(node.isLeaf)
      }
    }

    "added an empty string" should {
      "still be a leaf node" in {
        node.addChild(" ")
        assertTrue(node.isLeaf)
      }
    }
  }

  "node" when {
    val node = Node()
    "added a string" should {
      node.addChild("some")

      "not be a leaf node" in {
        assertFalse(node.isLeaf)
      }

      "have child by added name" in {
        assertTrue(node.childByName("some").isDefined)
      }

      "have a terminal grand child" in {
        val child = node.childByName("some")
        assertTrue(child.get.childByName("*").isDefined)
      }
    }

    "added a string" should {
      "create a terminal leaf node" in {
        assertTrue(node.addChild("some").childByName("some").get.childByName("*").isDefined)
      }
    }
  }

  "node" when {
    val node = Node()

    "added children with same parent" should {
      "have only one parent" in {
        node.addChild("p.c1.c11")
        node.addChild("p.c1.c12")
        assertEquals(1, node.childrenCount)
        assertEquals(1, node.childByName("p").get.childrenCount)
        assertEquals(2, node.childByName("p").get.childByName("c1").get.childrenCount)
      }
    }
  }

  "adding various long/short with terminal patterns" should {
    "add/remove nodes" in {
      val node = Node()
      var pattern = "a.b.c.d"
      node.addChild(pattern)
      var collector = node.traverse()
      logger.debug("collector after adding {}: {}", pattern, collector)
      var expectedSize = pattern.split('.').length + 1
      assertEquals("a.b.c.d.* should be added", expectedSize, collector.size)

      pattern = "p.q.r"
      node.addChild(pattern)
      collector = node.traverse()
      logger.debug("collector after adding {}: {}", pattern, collector)
      expectedSize += pattern.split('.').length + 1
      assertEquals("p.q.r.* should be added", expectedSize, collector.size)

      pattern = "a.b"
      node.addChild(pattern)
      collector = node.traverse()
      logger.debug("collector after adding {}: {}", pattern, collector)
      expectedSize -= "c.d".split('.').length
      assertEquals("it should drop c.d when a.b.* is added", expectedSize, collector.size)

      pattern = "p.q.1"
      node.addChild(pattern)
      collector = node.traverse()
      logger.debug("collector after adding {}: {}", pattern, collector)
      expectedSize += "1.*".split('.').length
      assertEquals("p.q.1.* should be added", expectedSize, collector.size)

      pattern = "a"
      node.addChild(pattern)
      collector = node.traverse()
      logger.debug("collector after adding {}: {}", pattern, collector)
      expectedSize -= "b".split('.').length
      assertEquals("it should drop b when a.* is added", expectedSize, collector.size)

      pattern = "p.q.r.s"
      node.addChild(pattern)
      collector = node.traverse()
      logger.debug("collector after adding {}: {}", pattern, collector)
      assertEquals("nothing new should be added as p.q.r.* already there", expectedSize, collector.size)

      pattern = "p.2"
      node.addChild(pattern)
      collector = node.traverse()
      logger.debug("collector after adding {}: {}", pattern, collector)
      expectedSize += "2.*".split('.').length
      assertEquals("2.* should be added after p", expectedSize, collector.size)
    }

    "have different abs path" in {
      val node = Node()

      node.addChild("a.b.c.d")
      assertEquals(".", node.absolutePath())
      assertEquals(".a", node.childByName("a").get.absolutePath())
      assertEquals(".a.b", node.childByName("a").get.childByName("b").get.absolutePath())

      val cNode = node.childByName("a").get.childByName("b").get.childByName("c").get
      assertEquals(".a.b.c", cNode.absolutePath())

      node.addChild("a.b.1.2")

      val oneNode = node.childByName("a").get.childByName("b").get.childByName("1").get
      assertEquals(".a.b.1", oneNode.absolutePath())

      val leavesPath = node.leaves().foldLeft(Seq[String]())((s,n) =>  s :+ n.absolutePath())
      assertTrue(leavesPath.contains(".a.b.1.2.*"))
      assertTrue(leavesPath.contains(".a.b.c.d.*"))
    }

    "add/remove branches" in {
      val node = Node()
      var branches = node.branches()

      var pattern = "a.b.c.d"
      node.addChild(pattern)
      logger.info("abs path: {}", node.absolutePath())
      branches = node.branches()

      logger.debug("adding {} collector: {} branches: {}", pattern, branches)
      assertEquals("a.b.c.d.* should be added", 1, branches.size)
      assertTrue(branches.contains("a.b.c.d"))

      pattern = "p.q.r"
      node.addChild(pattern)
      branches = node.branches()
      logger.debug("adding {} collector: {} branches: {}", pattern, node.traverse(), branches)

      assertEquals("p.q.r.* should be added", 2, branches.size)
      assertTrue(branches.contains("a.b.c.d"))
      assertTrue(branches.contains("p.q.r"))

      pattern = "a.b"
      node.addChild(pattern)
      branches = node.branches()
      logger.debug("adding {} collector: {} branches: {}", pattern, node.traverse(), branches)
      assertEquals("a.b.* should replace a.b.c.d.*", 2, branches.size)
      assertTrue(branches.contains("a.b"))
      assertTrue(branches.contains("p.q.r"))

      pattern = "p.q.1"
      node.addChild(pattern)
      branches = node.branches()
      logger.debug("adding {} collector: {} branches: {}", pattern, node.traverse(), branches)
      assertEquals("p.q.1.* should be added", 3, branches.size)
      assertTrue(branches.contains("a.b"))
      assertTrue(branches.contains("p.q.r"))
      assertTrue(branches.contains("p.q.1"))

      branches = node.branches()
      pattern = "a"
      node.addChild(pattern)
      branches = node.branches()
      logger.debug("adding {} collector: {} branches: {}", pattern, node.traverse(), branches)
      assertEquals("it should drop b when a.* is added", 3, branches.size)
      assertTrue(branches.contains("a"))
      assertTrue(branches.contains("p.q.r"))
      assertTrue(branches.contains("p.q.1"))

      pattern = "p.q.r.s"
      node.addChild(pattern)
      branches = node.branches()
      logger.debug("adding {} collector: {} branches: {}", pattern, node.traverse(), branches)
      assertEquals("nothing new should be added as p.q.r.* already there", 3, branches.size)
      assertTrue(branches.contains("a"))
      assertTrue(branches.contains("p.q.r"))
      assertTrue(branches.contains("p.q.1"))

      pattern = "p.2"
      node.addChild(pattern)
      branches = node.branches()
      logger.debug("adding {} collector: {} branches: {}", pattern, node.traverse(), branches)
      assertEquals("p.2.* should", 4, branches.size)
      assertTrue(branches.contains("a"))
      assertTrue(branches.contains("p.q.r"))
      assertTrue(branches.contains("p.q.1"))
      assertTrue(branches.contains("p.2"))
    }
  }
}


