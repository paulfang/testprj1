package com.commscope.eco

import java.text.SimpleDateFormat
import java.util.{Calendar, TimeZone}

import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.Row
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}
import org.junit.Assert._
import org.scalatest.BeforeAndAfterAll
import com.commscope.eco.DataFrameUtils._

import scala.util.{Failure, Success, Try}

class DataFrameUtilsTest extends SparkBasedTest with BeforeAndAfterAll {
  val logger = Logger(getClass)

  val data_base_dir = this.getClass.getClassLoader.getResource("data/frontier").getPath

  val dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm")

  val calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"))
  val now = calendar.getTimeInMillis()
  val sampleData = Seq(
    Row(now, "John", "Smith", dateFormat.parse("1969-12-20 07:05").toInstant.toEpochMilli),
    Row(now, "John", "Doe", dateFormat.parse("1957-02-07 03:35").toInstant.toEpochMilli),
    Row(now, "Jane", "Doe", dateFormat.parse("1977-07-07 13:23").toInstant.toEpochMilli)
  )

  val sampleSchema = StructType(Array(
    StructField("created_at", LongType, false),
    StructField("first_name", StringType, false),
    StructField("last_name", StringType, false),
    StructField("tob", LongType, false)
  ))

  val sampleDF = spark.createDataFrame(spark.sparkContext.parallelize(sampleData), sampleSchema)

  val dailyparamsLocation = s"$data_base_dir/dailyparams"

  override def beforeAll(): Unit = {
  }

  "loading json data" when {
    "no schema" should {
      "have all the columns" in {
        val df = DataFrameUtils.fromJson(data_base_dir + "/subscriber-sample.json")
        assertNotNull(df)
        val expectedCols = Seq("created", "details", "id", "identifiers", "services", "timestamp")
        val foundCols = df.columns
        assertTrue(expectedCols.diff(foundCols).isEmpty)
      }
    }

    "given schema with simple dropped columns" should {
      val df = DataFrameUtils.fromJson(data_base_dir + "/subscriber.json", schemaJsonPath = Some("subscriberLessColumns"))
      assertNotNull(df)

      "have only non dropped columns" in {
        val expectedCols = Seq("created", "details", "id", "services", "timestamp")
        val foundCols = df.columns
        assertTrue(expectedCols.diff(foundCols).isEmpty)
      }

      "dropped columns should be missing" in {
        val unexpectedCol = "identifiers"
        val foundCols = df.columns
        assertTrue(foundCols.filter(c => c.equalsIgnoreCase(unexpectedCol)).isEmpty)
      }
    }

    "given schema with nested dropped columns" should {
      val df = DataFrameUtils.fromJson(data_base_dir + "/subscriber.json", schemaJsonPath = Some("subscriberLessNestedColumns"))
      assertNotNull(df)

      "have only non dropped columns" in {
        val expectedCols = Seq("created", "details", "id", "identifiers", "services", "timestamp")
        val foundCols = df.columns
        assertTrue(expectedCols.diff(foundCols).isEmpty)
      }

      "have no dropped details columns" in {
        val details = df.select("details.*")
        val detailsColumns = details.columns
        val droppedFromDetails = Seq("Serial Number", "firstName", "lastName")
        droppedFromDetails.foreach(c => assertFalse(detailsColumns.contains(c)))
      }

      "have no dropped identifiers columns" in {
        val identifiers = df.select("identifiers.*")
        val identifiersColumns = identifiers.columns
        val droppedFromIdentifiers = Seq("ISPUserName")
        droppedFromIdentifiers.foreach(c => assertFalse(identifiersColumns.contains(c)))
      }
    }

    "given schema with illegal chars" should {
      "fail to save as parquet if schema not sanitized" in {

      }
    }

    "have columns with illegal chars" should {
      "fail to save as parquet" in {
        val df = DataFrameUtils.fromJson(data_base_dir + "/subscriber.json")
        val saved = Try(DataFrameUtils.toParquet(df, "/tmp/test_" + Math.random()))
        saved match {
          case Success(value) => fail()
          case Failure(e) => assertTrue(e.getCause.getMessage.contains("\"Serial Number\" contains invalid character"))
        }
      }
    }
  }

  "convert to colMap" when {
    "sequence of fields" should {
      "have no delimiter ." in {
        DataFrameUtils.toFieldMap(Seq("id", "subscriberID")).foreach(e => {
          logger.info("entry: {}", e)
          assertTrue(e._2.isEmpty)
        })
      }

      "have delimiter ." in {
        DataFrameUtils.toFieldMap(Seq("properties.wifi", "properties.acsUrls", "identifiers.ISPUserName")).foreach(e => {
          logger.info("entry: {}", e)
          assertTrue(e._2.nonEmpty)
          if (e._1.equals("properties")) assertTrue(Seq("wifi", "acsUrls").diff(e._2).isEmpty)
          if (e._1.equals("identifiers")) assertTrue(Seq("ISPUserName").diff(e._2).isEmpty)
        })
      }
    }

    "have delimiter . in nested fields" in {
      val colMap = DataFrameUtils.toFieldMap(Seq("properties.wifi.SSID", "properties.wifi.acsUrls", "identifiers.ISPUserName"))
      colMap.foreach(e => {
        logger.info("entry: {}", e)
        assertTrue(e._2.nonEmpty)
        if (e._1.equals("properties.wifi")) assertTrue(Seq("SSID", "acsUrls").diff(e._2).isEmpty)
        if (e._1.equals("identifiers")) assertTrue(Seq("ISPUserName").diff(e._2).isEmpty)
      })
    }
  }

  "sanitize column names" should {
    "remove illegal chars from df" in {
      val df = DataFrameUtils.fromJson(dailyparamsLocation)
      assertTrue(df.select("identifiers.*").columns.contains("Serial Number") &&
        !df.select("identifiers.*").columns.contains("Serial_Number"))

      val cleaned = DataFrameUtils.sanitizeColumnNames(df)

      assertTrue(!cleaned.select("identifiers.*").columns.contains("Serial Number") &&
        cleaned.select("identifiers.*").columns.contains("Serial_Number"))

      assertFalse("`identifiers.Serial Number` should be replaced by identifiers.Serial_Number should selected",
        cleaned.select("identifiers.Serial_Number").head(1).isEmpty)
    }
  }

  "dropped columns" when {
    "dropping top level non-struct columns" should {
      "be absent" in {
        val df = DataFrameUtils.fromJson(dailyparamsLocation)
        logger.info("original columns: {}", df.columns)
        val dropColumns = Seq("id", "subscriberID")

        val filteredDF = DataFrameUtils.dropFields(df, dropColumns)

        dropColumns.foreach(c =>
          assertTrue(s"col $c should exist in original but not in dropped", df.columns.contains(c) && !filteredDF.columns.contains(c))
        )
      }
    }

    "dropping top level combo of simple and struct columns" should {
      "be absent" in {
        val df = DataFrameUtils.fromJson(dailyparamsLocation)
        logger.info("original columns: {}", df.columns)
        val dropColumns = Seq("id", "properties", "subscriberIdentifiers", "identifiers")

        val filteredDF = DataFrameUtils.dropFields(df, dropColumns)

        dropColumns.foreach(c =>
          assertTrue(s"col $c should exist in original but not in dropped", df.columns.contains(c) && !filteredDF.columns.contains(c))
        )
      }
    }

    "dropping top level nested non-struct and struct columns" should {
      "be absent" in {
        val df = DataFrameUtils.fromJson(dailyparamsLocation)
        logger.info("original columns: {}", df.columns)
        val dropColumns = Seq("identifiers.Serial Number", "properties.wifi", "properties.wan")

        val filteredDF = DataFrameUtils.dropFields(df, dropColumns)
        val identifierCols = df.select("identifiers.*").columns
        val nestedIdentifierCols = filteredDF.select("identifiers.*").columns

        val propsCols = df.select("properties.*").columns
        val filteredPropsCols = filteredDF.select("properties.*").columns
        logger.info("props columns: {}", filteredPropsCols)

        Seq("Serial Number", "wan", "wifi").foreach(c =>
          if (c.startsWith("Ser"))
            assertTrue(s"col $c should exist in original but not in dropped", identifierCols.contains(c) && !nestedIdentifierCols.contains(c))
          else if (c.startsWith("wifi") || c.startsWith("wan"))
            assertTrue(s"col $c should exist in original but not in dropped", propsCols.contains(c) && !filteredPropsCols.contains(c))
        )
      }
    }

    "dropping inner nested non-struct and struct columns" should {
      "be absent" in {
        val df = DataFrameUtils.fromJson(dailyparamsLocation)
        var dropColumns = Seq("id", "ts", "created", "identifiers", "subscriberID", "subscriberIdentifiers",
          "properties.WANAccessType", "properties.wifi", "properties.WANAccessType",
          "properties.acsURL", "properties.arrisNvgDbCheck", "properties.deviceClassifiers",
          "properties.deviceType", "properties.dnsServers", "properties.firstInform", "properties.groups",
          "properties.hardwareVersion", "properties.hncEnable", "properties.lastBoot", "properties.lastInform",
          "properties.lastPeriodic", "properties.macAddress", "properties.manufacturerName",
          "properties.modelName", "properties.ntpServers", "properties.productClass", "properties.protocolVersion",
          "properties.provisioningCode", "properties.softwareVersion", "properties.ts", "properties.id",
          "properties.wanIpAddress","properties.wan.ethDuplexMode"
        )
        var alteredDf = DataFrameUtils.dropFields(df, dropColumns)

        val colMap = DataFrameUtils.toFieldMap(dropColumns)

        colMap.foreach(c =>
          if (c._2.isEmpty)
            assertFalse(alteredDf.columns.contains(c))
          else if (!c._1.contains('.')) {
            val existing = alteredDf.select(s"${c._1}.*").columns
            c._2.foreach(e => assertFalse(s"$e should be dropped", existing.contains(e)))
          }
        )

        val nestedCols = df.select("properties.wan.*").columns
        val nestedFilteredCols = alteredDf.select("properties.wan.*").columns
        logger.info("original columns: {} altered: {}", nestedCols, nestedFilteredCols)
        Seq("ethDuplexMode").foreach(c =>
          if (c.startsWith("ethDuplexMode"))
            assertTrue(s"col $c should exist in original but not in dropped", nestedCols.contains(c) && !nestedFilteredCols.contains(c))
        )
      }
    }
  }

  "drop columns json data" when {
    "filtered" should {
      "be filtered" in {
        val df = DataFrameUtils.fromJson(dailyparamsLocation)
        val dropColumns = Seq("id", "subscriberID",
          "identifiers.Serial Number",
          "subscriberIdentifiers.ISPUsername",
          "properties.tags", "properties.groups", "properties.wifi", "properties.dnsServers", "properties.ntpServers",
          "properties.wan", "properties.deviceClassifiers",
          "properties.wifi.0.SSIDAdvertisementEnabled")

        val filteredDF = DataFrameUtils.dropFields(df, dropColumns)

        assertTrue("id column should be present in original", df.columns.contains("id"))
        assertFalse("id column should be dropped", filteredDF.columns.contains("id"))

        assertTrue("subscriberID column should be in original", df.columns.contains("subscriberID"))
        assertFalse("subscriberID column should be dropped", filteredDF.columns.contains("subscriberID"))

        val originalSchema = Some(df.schema)
        val filteredSchema = Some(filteredDF.schema)
        logger.info(filteredSchema.get.treeString)
        val schemaDiff = SchemaUtils.flatten(originalSchema).get.diff(SchemaUtils.flatten(filteredSchema).get)
        logger.info("length: {} {}", schemaDiff.length, schemaDiff)

        //assertTrue(originalSchema.get.sql.contains("SSIDAdvertisementEnabled"))
        //assertFalse(filteredSchema.get.sql.contains("SSIDAdvertisementEnabled"))

        //dropColumns.foreach(c => assertTrue(Try(filteredDF("id")).isFailure))
      }
    }
  }

  "TimeDims" when {
    "not defined" should {
      val timeDims = None
      "create no additional columns" in {
        val _df = sampleDF.appendTimeDims(timeDims)
        assertTrue(sampleDF.columns.diff(_df.columns).isEmpty)
      }
    }

    "defined" should {
      val timeDims = Some(TimeDims(Seq("year", "month", "day"), "created_at", .001))

      "create additional columns" in {
        val _df = sampleDF.appendTimeDims(timeDims)
        val totalCount = _df.count()
        val expectedAdditionalCols = Array("ts_year", "ts_month", "ts_day")
        val _diff = _df.columns.diff(sampleDF.columns)
        logger.debug("added columns: {}", _diff)
        assertEquals(sampleDF.columns.length + expectedAdditionalCols.length, _df.columns.length)
        assertTrue(_diff.diff(expectedAdditionalCols).isEmpty)

        logger.debug("calendar.year: {} month: {} day: {}", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        assertEquals(totalCount, _df.filter(s"ts_year==${calendar.get(Calendar.YEAR)}").count())
        assertEquals(totalCount, _df.filter(s"ts_month==${calendar.get(Calendar.MONTH)}+1").count())
        assertEquals(totalCount, _df.filter(s"ts_day==${calendar.get(Calendar.DAY_OF_MONTH)}").count())
      }
    }

    "inform dim column" should {
      val timeDims = Some(TimeDims(Seq("year", "month", "day", "inform"), "created_at", .001))

      "be created in" in {
        val _df = sampleDF.appendTimeDims(timeDims)
        val totalCount = _df.count()
        val expectedAdditionalCols = Array("ts_year", "ts_month", "ts_day", "ts_inform")
        val _diff = _df.columns.diff(sampleDF.columns)
        logger.debug("added columns: {}", _diff)
        assertEquals(sampleDF.columns.length + expectedAdditionalCols.length, _df.columns.length)
        assertTrue(_diff.diff(expectedAdditionalCols).isEmpty)

        logger.debug("calendar.year: {} month: {} day: {}", calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH))
        assertEquals(totalCount, _df.filter(s"ts_year==${calendar.get(Calendar.YEAR)}").count())
        assertEquals(totalCount, _df.filter(s"ts_month==${calendar.get(Calendar.MONTH)}+1").count())
        assertEquals(totalCount, _df.filter(s"ts_day==${calendar.get(Calendar.DAY_OF_MONTH)}").count())
        val expectedInform = calendar.get(Calendar.HOUR_OF_DAY)%24 * 4 + calendar.get(Calendar.MINUTE)/15 +1
        _df.select("ts_inform").foreach(r => assertEquals(expectedInform, r.get(0)))
      }
    }

    "defined with non-default prefix" should {
      val timeDims = Some(TimeDims(Seq("year", "month", "day"), "created_at", .001, "tmp_"))

      "create additional columns with expected prefix" in {
        val _df = sampleDF.appendTimeDims(timeDims)
        val totalCount = _df.count()
        val expectedAdditionalCols = Array("tmp_year", "tmp_month", "tmp_day")
        val _diff = _df.columns.diff(sampleDF.columns)
        logger.debug("added columns: {}", _diff)
        assertEquals(sampleDF.columns.length + expectedAdditionalCols.length, _df.columns.length)
        assertTrue(_diff.diff(expectedAdditionalCols).isEmpty)

        assertEquals(totalCount, _df.filter(s"tmp_year==${calendar.get(Calendar.YEAR)}").count())
        assertEquals(totalCount, _df.filter(s"tmp_month==${calendar.get(Calendar.MONTH)}+1").count())
        assertEquals(totalCount, _df.filter(s"tmp_day==${calendar.get(Calendar.DAY_OF_MONTH)}").count())
      }

      "create only missing time dims" in {
        val _df = sampleDF.appendTimeDims(timeDims)
        val totalCount = _df.count()
        val expectedAdditionalCols = Array("tmp_year", "tmp_month", "tmp_day")
        val _diff = _df.columns.diff(sampleDF.columns)
        logger.debug("added columns: {}", _diff)
        assertEquals(sampleDF.columns.length + expectedAdditionalCols.length, _df.columns.length)
        assertTrue(_diff.diff(expectedAdditionalCols).isEmpty)

        assertEquals(totalCount, _df.filter(s"tmp_year==${calendar.get(Calendar.YEAR)}").count())
        assertEquals(totalCount, _df.filter(s"tmp_month==${calendar.get(Calendar.MONTH)}+1").count())
        assertEquals(totalCount, _df.filter(s"tmp_day==${calendar.get(Calendar.DAY_OF_MONTH)}").count())

        val _dfMod = _df.drop("tmp_year", "tmp_day").appendTimeDims(timeDims)
        assertEquals(expectedAdditionalCols.length, _dfMod.columns.count(c => c.startsWith(timeDims.get.timeDimColPrefix)))

        assertEquals(totalCount, _dfMod.filter(s"tmp_year==${calendar.get(Calendar.YEAR)}").count())
        assertEquals(totalCount, _dfMod.filter(s"tmp_month==${calendar.get(Calendar.MONTH)}+1").count())
        assertEquals(totalCount, _dfMod.filter(s"tmp_day==${calendar.get(Calendar.DAY_OF_MONTH)}").count())
      }
    }

    "defined with another ts type column" should {
      val timeDims = Some(TimeDims(Seq("year", "date", "month", "day"), "tob", .001))

      "create additional columns based on expected ts column" in {
        val _df = sampleDF.appendTimeDims(timeDims)

        val expectedAdditionalCols = Array("ts_year", "ts_date", "ts_month", "ts_day")
        val _diff = _df.columns.diff(sampleDF.columns)

        assertEquals(sampleDF.columns.length + expectedAdditionalCols.length, _df.columns.length)
        assertTrue(_diff.diff(expectedAdditionalCols).isEmpty)

        assertEquals(1, _df.filter("ts_year==1969 and ts_month==12 and ts_day=20").count())
        assertEquals(1, _df.filter("ts_year==1957 and ts_month==02").count())
        assertEquals(2, _df.filter("ts_day=7").count())
      }
    }
  }
}


