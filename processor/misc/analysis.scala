import org.apache.spark
import org.apache.spark.sql.expressions.Window

spark.conf.set("spark.sql.session.timeZone","UTC")
System.setProperty("user.timezone", "UTC")

val baseDir = "/Users/sanjeevmishra/data/telmex"


val files = Array(
  s"$baseDir/export/END/20200712",s"$baseDir/export/END/20200713"
  ,s"$baseDir/export/END/20200714",s"$baseDir/export/END/20200715"
  ,s"$baseDir/export/END/20200716",s"$baseDir/export/END/20200717"
  ,s"$baseDir/export/END/20200718",s"$baseDir/export/END/20200719"
)

val files = Array(
  s"$baseDir/export/END/20200718",s"$baseDir/export/END/20200720",
  s"$baseDir/export/END/20200718",s"$baseDir/export/END/20200721"
)

files.foreach(fl => spark.read.option("inferTimestamp","false").option("preferDecimal","false").json(fl).dropDuplicates("id")
  .select("id", "identifiers.*", "subscriberID", "properties.lastInform", "ts")
  .withColumn("ts_date",to_date(from_unixtime(col("ts")/1000))).drop("ts", "OUI")
  .withColumn("lastInform", to_date(from_unixtime(col("lastInform")/1000)))
  .withColumnRenamed("Serial Number", "serial_number")
  .write.partitionBy("ts_date", "lastInform")
  .mode("append")
  .format("delta").save("/Users/sanjeevmishra/data/telmex/processed/element/"))


val dp = spark.read.parquet(s"${baseDir}/processed/element")
val el = spark.read.parquet(s"${baseDir}/export/public.db/element").withColumn("first_inform", to_date(col("first_inform"))).withColumn("last_inform", to_date(col("last_inform"))).withColumn("last_boot", to_date(col("last_boot"))).select("element_id", "subscriber_id", "serialnumber", "last_inform", "first_inform", "last_boot")

val dropCols = Array("id","OUI", "subscriberID","ts","serial_number","ts_date","lanipaddress","wanipaddress","devicetype","element_created","element_created_norm", "elemented_updated_norm", "elemented_updated","productclass", "timezone","last_boot_norm", "first_inform","first_inform_norm","last_inform_norm")


val ts_dates = dp.select("ts_date").distinct.head(10)

val w = Window.partitionBy("element_id").orderBy(desc("last_inform"))

val missing_el = dp_12.join(el, dp_dt("id") === el("element_id"), "right_outer").filter("id is null").select("element_id", "serial_number", "subscriber_id", "last_inform")

val elCountByLastInform =  dp.groupBy("lastInform").count.orderBy("lastInform").head(20)

elCountByLastInform.foreach(e => {
  val dt = e(0)
  val filtered = dp.filter(s"ts_date == '${dt.getDate(0)}'")
  print(filtered
    .join(el, filtered("id") === el("element_id"), "right_outer")
    .filter("id is null").drop(dropCols :_*)
    .withColumn("ts_date", lit(dt.getDate(0)))
    .withColumn("localRank", row_number().over(w))
    .filter("localRank=1").drop("localRank")
    .write.partitionBy("ts_date", "last_inform")
    .mode("append")
    .parquet(s"${baseDir}/missing"))
})



ts_dates.foreach(dt => {
  val filtered = dp.filter(s"ts_date == '${dt.getDate(0)}'")
  print(filtered
    .join(el, filtered("id") === el("element_id"), "right_outer")
    .filter("id is null").drop(dropCols :_*)
    .withColumn("ts_date", lit(dt.getDate(0))).count)
})


//Missing analysis
val el = spark.read.parquet(s"${baseDir}/export/public.db/element").withColumn("first_inform", to_date(col("first_inform"))).withColumn("last_inform", to_date(col("last_inform"))).withColumn("last_boot", to_date(col("last_boot"))).select("element_id", "subscriber_id", "serialnumber", "last_inform", "first_inform", "last_boot")
el.repartition(1).write.csv(s"$baseDir/export/public.db/el_count_by_last_inform")

val dp_12 = spark.read.format("delta").load(s"$baseDir/processed/element").where("ts_date='2020-07-12'").dropDuplicates("id")
val missing_12 = dp_12.join(el, dp_12("id") === el("element_id"), "right_outer").filter("id is null").select("element_id", "subscriber_id", "serialnumber", "last_inform", "first_inform", "last_boot")



val dp17 = dp.where("lastInform='2020-07-17'")

val dp18 = dp.where("lastInform='2020-07-18'")

val dp19 = dp.where("lastInform='2020-07-19'")

val dp20 = dp.where("lastInform='2020-07-20'")

val miss17 = dp17.join(el, dp17("id") === el("element_id"), "right_outer").filter("id is null").select("element_id").withColumn("date_17", to_date(lit("2020-07-17"))).withColumnRenamed("element_id", "miss17_id")

val miss18 = dp18.join(el, dp18("id") === el("element_id"), "right_outer").filter("id is null").select("element_id").withColumn("date_18", to_date(lit("2020-07-18"))).withColumnRenamed("element_id", "miss18_id")

val miss19 = dp19.join(el, dp19("id") === el("element_id"), "right_outer").filter("id is null").select("element_id").withColumn("date_19", to_date(lit("2020-07-19"))).withColumnRenamed("element_id", "miss19_id")

val miss20 = dp20.join(el, dp20("id") === el("element_id"), "right_outer").filter("id is null").select("element_id").withColumn("date_20", to_date(lit("2020-07-20"))).withColumnRenamed("element_id", "miss20_id")

val miss13_14_15_16_17 = miss13_14_15_16.join(miss17, miss13_14_15_16("miss13_id") === miss17("miss17_id")).select("miss13_id")

val miss13__17 = miss13_14_15_16.join(miss17, miss13_14_15_16("miss13_id") === miss17("miss17_id")).select("miss13_id")

val miss13__18 = miss13__17.join(miss18, miss13__17("miss13_id") === miss18("miss18_id")).select("miss13_id")

val miss13__19 = miss13__18.join(miss19, miss13__18("miss13_id") === miss19("miss19_id")).select("miss13_id")

val miss13__20 = miss13__19.join(miss20, miss13__19("miss13_id") === miss20("miss20_id")).select("miss13_id")

val missing13to20 = miss13__20.join(el, miss13__20("miss13_id") === el("element_id")).select("element_id", "subscriber_id", "serialnumber", "last_inform", "last_boot")

val commonMiss = missing13to20.withColumnRenamed("element_id", "el_id").withColumnRenamed("subscriber_id", "sub_id")

val dropCols = Array("id","oui", "subscriber_id","ts","serialnumber","ts_date","lanipaddress","wanipaddress","element_created","element_created_norm", "elemented_updated_norm",
  "elemented_updated", "timezone","last_boot_norm", "first_inform","first_inform_norm","last_inform_norm", "acs_url", "macaddress", "provisioningcode", "last_inform", "last_boot")


val subDropCols = Array("address","address", "Nombre", "NombreEquipo", "Profile", "Technology", "Account_Number", "Phone_Number"
  ,"Serial_Number", "DropId", "created", "norm_created", "updated", "norm_updated", "country")

val manModelGrp = el.filter("inform_days <=90").groupBy("manufacturer", "modelname", "inform_days").count()

val days7 = manModelGrp.filter("inform_days < 7").groupBy("manufacturer", "modelname").sum("count").withColumnRenamed("sum(count)", "7 days").withColumnRenamed("manufacturer", "manufacturer7").withColumnRenamed("modelname", "modelname7")

val days45 = manModelGrp.filter("inform_days < 45").groupBy("manufacturer", "modelname").sum("count").withColumnRenamed("sum(count)", "45 days").withColumnRenamed("manufacturer", "manufacturer45").withColumnRenamed("modelname", "modelname45")

val days90 = manModelGrp.filter("inform_days < 90").groupBy("manufacturer", "modelname").sum("count").withColumnRenamed("sum(count)", "90 days").withColumnRenamed("manufacturer", "manufacturer90").withColumnRenamed("modelname", "modelname90")

val joined = days7.join(days45, days7("manufacturer7")===days45("manufacturer45") and days7("modelname7")===days45("modelname45"), "right_outer")

val joined90 = joined.join(days90, joined("manufacturer45") === days90("manufacturer90") and joined("modelname45") === days90("modelname90"), "right_outer")

val cleanedUp = joined90.drop("manufacturer7", "manufacturer45", "modelname7", "modelname45").withColumnRenamed("manufacturer90", "manufacturer").withColumnRenamed("modelname90", "modelname")

cleanedUp.select("manufacturer","modelname","7 days","45 days","90 days").orderBy("manufacturer","modelname").repartition(1).write.mode("overwrite").option("header", true).csv(s"$baseDir/processed/manufacturerModelByInformDays")

val manufacturerGrp = cleanedUp.groupBy("manufacturer").sum("7 days", "45 days", "90 days").withColumnRenamed("sum(7 days)", "7 days").withColumnRenamed("sum(45 days)", "45 days").withColumnRenamed("sum(90 days)", "90 days")

cleanedUp.groupBy("manufacturer").sum("7 days", "45 days", "90 days").withColumnRenamed("sum(7 days)", "7 days").withColumnRenamed("sum(45 days)", "45 days").withColumnRenamed("sum(90 days)", "90 days").repartition(1).write.mode("overwrite").option("header",true).csv(s"$baseDir/processed/manufacturerByInformDays/")







