

## Inquire Fire Application Description

This application is for creating SQL Tables so that queries can be run against the data. The data is read
from JSON files and stored into a Hive Metastore 

* Data is ingested into a Spark DataFrame from the filesystem by reading regular JSON files.
* A DataFrame has a schema, which looks exactly like a relational table
* The data is registered into SQL tables in an Apache Hive metastore
* All the tables that have been registered can be made available for access as a JDBC data source via Spark Thrift Server. After that, you will be able to use the JDBC tool to connect the embedded Hive Thrift Server inside Spark, and use SQL to query the Data Frame tables.


### Project Layout 

```
tests                      # Contains integration tests
└── inquire/START          # Contains test data used by the test scripts
src
|── udf-dev                # This folder is a java project for SQL user defined functions
|── bin
|    ├── submit_job.sh     # This bash script is used to submit a job
├── jobs                   # This folder contains all the available Spark Jobs
│   ├── FrameDTO
│   │   ├── FrameDTO.py    # Source Code for the Spark Job called FrameDTO
│   │   └── __init__.py
│   ├── __init__.py
│   └── TestEnv            # Source Code for the Spark Job called TestEnv
│       ├── __init__.py
│       └── TestEnv.py
├── main.py               # Generic SparkDrver program that is used to launch Jobs
└── shared                # This folder contains code that can be used by any Spark Job
    ├── FileUtils.py      # Code used for manipulating files 
    ├── __init__.py
    └── SparkUtils.py     # Code for performing generic operations with Spark
```

## Architecture

The design uses a version of the Template Method Pattern. 

The Template Method Pattern pattern defines the skeleton of an algorithm in a method, deferring some steps to subclasses. 
Template Method lets subclasses redefine certain steps of an algorithm without changing the algorithm’s structure.

The parent class is shared/SparkUtils.py (the template) each spark job is a child class of this parent class.
The parent class calls the functions create_tables and read_json methods which are implemented in the child class, the child class is the actual
spark job. SparkUtils should never be used directly but it does contain methods that can be used by any spark job class. 


### Building and running the application

```
$ make dist
```

This will "build" the project. This just puts everything into a zip file and and creates a driver application inside the dist/ directory. This is done because when you submit this application to a Spark Cluster you can pass it the zip file rather than having to either (a) copy all the files to each node or (b) having to list out each file to distribute


### Running a Spark Job

To run a spark job you use the Apache Spark shell command spark-submit. A script has been provided that shows how to pass the correct arguments. 

```
/bin/submit_job.sh KPI
/bin/submit_job.sh HostTable
/bin/submit_job.sh Subscriber
```

To create your own spark job you simply duplicate one of the existing spark jobs and modify the following methods:
init, create_tables, and read_json. You can add other methods but those three are the important ones that are required 
by the parent class.


### Running Tests

Make sure the python testing framework nose is installed
```
apt-get install -y python3 python3-setuptools
ln -s /usr/bin/python3 /ur/bin/python
easy_install3 pip py4j
easy_install3 pip nose
```

Export the following variables to point to the START and END directory where the test data is located
```
export INQUIRE_END_DIR=/home/luis/projects/inquire-fire/tests/inquire/END
export INQUIRE_START_DIR=/home/luis/projects/inquire-fire/tests/inquire/START
```

Run the tests you are interested in
```
make test-kpi
test-kpi:
	nosetests tests/test_KPI.py --nocapture --nologcapture --debug=[TestKPI]
```

Note: Spark must be running and configured to use hive. The database "test" is used.


### Doing a release

Right now the release process is manual. The goal is to change the naming as it appears in Jenkins from 1.0.4-SNAPSHOT to simply 1.0.4

To do that, you chnage two files :

inquire-fire/udf-dev/build.sbt

``
version := "1.0.3"  // Release
version := "1.0.4-SNAPSHOT" // Not Release
``

inquire-fire/Jenkinsfile

```
sh "cd dist; tar -cvzf inquire-fire-1.0.3.tar.gz ./*" // RELEASE
sh "cd dist; tar -cvzf inquire-fire-1.0.4-SNAPSHOT-${env.BUILD_NUMBER}.tar.gz ./*" // NOT RELEASE
```

### Artifact download link ###
https://jenkins.arriseco.com/job/eco_inquire/job/inquire-fire/80/


# Related Pages

* https://arrisworkassure.atlassian.net/wiki/spaces/ECO/pages/145887501/ECO+Inquire+-+Apache+Spark+Pentaho
* http://stash.pace.internal/projects/ECO/repos/inquire-sparksql-dialect/browse
* https://arrisworkassure.atlassian.net/wiki/spaces/ECO/pages/190513236/Telmex+Spark+Schema


