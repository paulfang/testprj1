package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.functions.{col, explode}
import org.junit.Assert

class ClientTest extends SparkBasedTest {
  val logger = Logger(getClass)
  val dataDir = getClass.getClassLoader.getResource("data").getPath
  val configDir = getClass.getClassLoader.getResource("config").getPath

  "without device df, data" should  {
    val config = AppConfig("config/host-table-silver-noDevice.json")
    logger.debug("{}", config)
    val htDf = spark.read.parquet(s"${config.inputLocation}/ht_*.parquet")
    Assert.assertNotNull(htDf)
    logger.debug("original schema: {}", htDf.schema.treeString)

    val origCount = htDf.count()
    val origColCount = htDf.columns.length
    Assert.assertEquals(4, origColCount)
    val origDistinctIds = htDf.select("id").distinct().count()
    Assert.assertEquals(0, htDf.filter("id is null").count())

    "be processed without joining device" in {
      val clientModel = Client.model(htDf, config, args = Map())
      Assert.assertNotNull(clientModel)

      val ppDistinctIds = clientModel.select("device_id").distinct().count()

      val idDiff = htDf.select("id").distinct().collect().diff(clientModel.select("device_id").distinct().collect).flatMap(_.toSeq)

      val missing = htDf.filter(col("id").isin(idDiff: _*))

      //missing.write.mode("overwrite").parquet("/tmp/missing-ht")

      val htMissing = missing.select(explode(col("hostTable")).as("hostTable")).select("hostTable.*")
      val missingMac = htMissing.filter("macAddress is not null").select("macAddress")
      val missingMacCount = missingMac.count()
      logger.info("orig distinctIds {} post proc distinctIds {} (diff: {}) macNotNull from missing {}",
        origDistinctIds, ppDistinctIds, (origDistinctIds - ppDistinctIds), missingMacCount)

      val postProcCount = clientModel.count()

      Assert.assertTrue(origCount < postProcCount)

      val postProcColCount = clientModel.columns.length
      Assert.assertEquals(30, postProcColCount)

      val deviceGroup = clientModel.select("device_group").distinct().head()

      logger.info("deviceGroupCount: {}", deviceGroup.length)

      Assert.assertTrue(clientModel.columns.contains("country"))

      val nullCityCount = clientModel.where("city is null or city == ''").count()
      Assert.assertEquals(postProcCount, nullCityCount)

    }
  }

  "with device df, data" should {
    val config = AppConfig("config/host-table-silver.json")
    logger.debug("{}", config)
    val htDf = spark.read.parquet(s"${config.inputLocation}/ht_*.parquet")
    Assert.assertNotNull(htDf)
    logger.debug("original schema: {}", htDf.schema.treeString)

    val origCount = htDf.count()
    val origColCount = htDf.columns.length
    Assert.assertEquals(4, origColCount)
    val origDistinctIds = htDf.select("id").distinct().count()
    Assert.assertEquals(0, htDf.filter("id is null").count())

    "be processed with device" in {
      val clientModel = Client.model(htDf, config, args = Map())
      Assert.assertNotNull(clientModel)

      val ppDistinctIds = clientModel.select("device_id").distinct().count()

      val idDiff = htDf.select("id").distinct().collect().diff(clientModel.select("device_id").distinct().collect).flatMap(_.toSeq)

      val missing = htDf.filter(col("id").isin(idDiff: _*))

      //missing.write.mode("overwrite").parquet("/tmp/missing-ht")

      val htMissing = missing.select(explode(col("hostTable")).as("hostTable")).select("hostTable.*")
      val missingMac = htMissing.filter("macAddress is not null").select("macAddress")
      val missingMacCount = missingMac.count()
      logger.info("orig distinctIds {} post proc distinctIds {} (diff: {}) macNotNull from missing {}",
        origDistinctIds, ppDistinctIds, (origDistinctIds - ppDistinctIds), missingMacCount)

      val postProcCount = clientModel.count()

      Assert.assertTrue(origCount < postProcCount)

      val postProcColCount = clientModel.columns.length
      Assert.assertEquals(31, postProcColCount)
      Assert.assertTrue(clientModel.columns.contains("active"))

      Assert.assertTrue(clientModel.columns.contains("country"))

      Assert.assertTrue(clientModel.columns.contains("client_manufacturer"))
      Assert.assertTrue(clientModel.columns.contains("client_category"))
      val deviceGroupCount = clientModel.select("client_device_type").distinct().count()
      logger.info("deviceGroupCount: {}", deviceGroupCount)

      val deviceCatCount = clientModel.select("client_category").distinct().count()
      logger.info("deviceCatCount: {}", deviceCatCount)

      val deviceVendorCount = clientModel.select("client_manufacturer").distinct().count()
      logger.info("deviceVendorCount: {}", deviceVendorCount)

      val notNullCityCount = clientModel.where("city is not null and city != ''").count()
      val notNullSubscriberId = clientModel.where("subscriber_id is not null").count()
      Assert.assertEquals(notNullSubscriberId, notNullCityCount)
    }
  }

  /*
  Creating the test data

    val dpTest = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/dailyparams")

    val subs = spark.read.parquet(s"$baseDir/warehouse/silver/subscriber")

    val subsDP = dpTest
      .withColumnRenamed("id", "device_id").join(subs, $"subscriberID" === $"subscriber_id", "left_outer")
      .select("subscriber_id","zip","city","state","country")

    subsDP
      .repartition(1).write.mode("overwrite")
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/subscriber")

    val sub = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/subscriber")

    val dev = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/dailyparams")
      .select($"id".as("device_id"), $"subscriberID".as("subscriber_id"), $"properties.modelName".as("device_model"), $"properties.manufacturerName".as("device_manufacturer"))
      .distinct()

    val devSub = dev
      .join(sub.withColumnRenamed("subscriber_id","sub_id"), col("subscriber_id") === col("sub_id"), "left_outer")
      .drop("sub_id")
      .distinct()

    devSub
      .repartition(1).write.mode("overwrite")
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/device")
   */
}
