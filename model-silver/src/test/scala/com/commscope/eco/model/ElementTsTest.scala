package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.functions._
import org.junit.Assert

class ElementTsTest extends SparkBasedTest {
  val logger = Logger(getClass)
  val dataDir = getClass.getClassLoader.getResource("data").getPath
  val configDir = getClass.getClassLoader.getResource("config").getPath

  "prepare data" ignore {
    val config = AppConfig("config/element-ts-silver-noDevice.json")

    val elTsDf = spark.read.parquet(s"${config.inputLocation}/part*.parquet")
    Assert.assertNotNull(elTsDf)

    val processStats = Map(
      "wifiDevices" ->true
    )

    val elementTsModel = ElementTs.model(elTsDf, config, args = Map("process-stats" -> processStats))
  }

  "process wifiDevices without device" in {
    val config = AppConfig("config/element-ts-silver-noDevice.json")

    val elTsDf = spark.read.parquet(s"${config.inputLocation}/oneRadioWifiEach.parquet")
    Assert.assertNotNull(elTsDf)

    val origDistinctIds = elTsDf.select("device_id").distinct().count()
    Assert.assertEquals(1, origDistinctIds)

    val processStats = Map(
      "wifiDevices" ->true
    )

    val st = System.currentTimeMillis()
    val elementTsModel = ElementTs.model(elTsDf, config, args = Map("process-stats" -> processStats))
    logger.info("time taken in ElementTs modeling: {} sec", (System.currentTimeMillis() - st)/1000)

    logger.info("{}", elementTsModel.printSchema())
    Assert.assertNotNull(elementTsModel)

    val ppDistinctIds = elementTsModel.select("device_id").distinct().count()

    Assert.assertEquals(0, ppDistinctIds - origDistinctIds)

    Assert.assertFalse(elementTsModel.columns.contains("country"))
    Assert.assertTrue(elementTsModel.columns.contains("wifi_devices"))
    Assert.assertFalse(elementTsModel.columns.contains("wifi_clients"))
    Assert.assertFalse(elementTsModel.columns.contains("radio_clients"))
    Assert.assertFalse(elementTsModel.columns.contains("wifi_stats"))
    Assert.assertFalse(elementTsModel.columns.contains("radio_stats"))
    Assert.assertFalse(elementTsModel.columns.contains("steering_stats"))
    Assert.assertFalse(elementTsModel.columns.contains("dns_stats"))
    Assert.assertFalse(elementTsModel.columns.contains("dns_server_stats"))

    val wifiDevices = elementTsModel.select(explode(col("wifi_devices")))
    Assert.assertFalse(wifiDevices.isEmpty)
  }


  "wifiDevices with radioClients" in {
    val config = AppConfig("config/element-ts-silver-noDevice.json")

    val elTsDf = spark.read.parquet(s"${config.inputLocation}/oneRadioWifiEach.parquet")
    Assert.assertNotNull(elTsDf)

    val origDistinctIds = elTsDf.select("device_id").distinct().count()
    Assert.assertEquals(1, origDistinctIds)

    val processStats = Map(
      "wifiDevices" -> true,
      "radioClients" -> true
    )

    val elementTsModel = ElementTs.model(elTsDf, config, args = Map("process-stats" -> processStats))

    logger.info("{}", elementTsModel.printSchema())
    Assert.assertNotNull(elementTsModel)

    val ppDistinctIds = elementTsModel.select("device_id").distinct().count()

    Assert.assertEquals(0, ppDistinctIds - origDistinctIds)
    Assert.assertFalse(elementTsModel.columns.contains("country"))
    Assert.assertTrue(elementTsModel.columns.contains("wifi_devices"))
    Assert.assertTrue(elementTsModel.columns.contains("radio_clients"))
    Assert.assertFalse(elementTsModel.columns.contains("wifi_clients"))
    Assert.assertFalse(elementTsModel.columns.contains("wifi_stats"))
    Assert.assertFalse(elementTsModel.columns.contains("radio_stats"))
    Assert.assertFalse(elementTsModel.columns.contains("steering_stats"))
    Assert.assertFalse(elementTsModel.columns.contains("dns_stats"))
    Assert.assertFalse(elementTsModel.columns.contains("dns_server_stats"))
    val wifiDevices = elementTsModel.select(explode(col("wifi_devices")))
    Assert.assertFalse(wifiDevices.isEmpty)

    val radioClients = elementTsModel.select(explode(col("radio_clients")))
    Assert.assertFalse(radioClients.isEmpty)
  }

  "dnsStats" in {
    val config = AppConfig("config/element-ts-silver-dns.json")

    val elEvDf = spark.read.parquet(s"${config.inputLocation}/dns-test-1.parquet")

    Assert.assertNotNull(elEvDf)
    val processStats = Map(
      "dnsStats" -> true,
      "dnsServerStats" -> true
    )
    val elementTsModel = ElementTs.model(elEvDf, config, args = Map("process-stats" -> processStats))
    Assert.assertNotNull(elementTsModel)

    Assert.assertTrue(elementTsModel.columns.contains("dns_stats"))
    Assert.assertTrue(elementTsModel.columns.contains("dns_server_stats"))

    val dnsStats = elementTsModel.select(explode(col("dns_stats")))
    Assert.assertFalse(dnsStats.isEmpty)
    logger.info("dnsStats: {}", dnsStats.select("col.*").select("dnsName"))
    val dnsServerStats = elementTsModel.select(explode(col("dns_server_stats")))
    Assert.assertFalse(dnsServerStats.isEmpty)
    logger.info("dnsServerStats: {}", dnsServerStats.select("col.*").select("dnsServerName"))
  }

  "hneStats" in {
    val config = AppConfig("config/element-ts-silver-hne.json")

    val elEvDf = spark.read.parquet(s"${config.inputLocation}/part*.parquet").withColumn("ts_date", lit("2021-03-17"))

    Assert.assertNotNull(elEvDf)
    val processStats = Map(
      "hneStats" -> true
    )
    val elementTsModel = ElementTs.model(elEvDf, config, args = Map("process-stats" -> processStats))
    Assert.assertNotNull(elementTsModel)

    Assert.assertTrue(elementTsModel.columns.contains("hne_stats"))

    val hneStats = elementTsModel.select(explode(col("hne_stats")))
    Assert.assertFalse(hneStats.isEmpty)
    Assert.assertEquals(9, hneStats.select("col.*").select("radio24ChanUtilization").head(1)(0)(0))
  }

  "with device df, data" ignore {
    val config = AppConfig("config/element-ts-silver.json")
    logger.debug("{}", config)
    val elEvDf = spark.read.parquet(s"${config.inputLocation}/part-*.parquet")
    Assert.assertNotNull(elEvDf)
    logger.debug("original schema: {}", elEvDf.schema.treeString)

    val origCount = elEvDf.count()
    val origColCount = elEvDf.columns.length
    Assert.assertEquals(9, origColCount)
    val origDistinctIds = elEvDf.select("id").distinct().count()
    Assert.assertEquals(0, elEvDf.filter("id is null").count())

    "be processed with joining device" in {
      val elementTsModel = ElementTs.model(elEvDf, config, args = Map())
      Assert.assertNotNull(elementTsModel)

      val ppDistinctIds = elementTsModel.select("device_id").distinct().count()

      logger.info("orig distinctIds {} post proc distinctIds {} (diff: {})", origDistinctIds, ppDistinctIds, (origDistinctIds - ppDistinctIds))

      Assert.assertTrue(elementTsModel.columns.contains("country"))
      Assert.assertTrue(elementTsModel.columns.contains("wifi_clients"))
      Assert.assertTrue(elementTsModel.columns.contains("wifi_devices"))
      Assert.assertTrue(elementTsModel.columns.contains("radio_clients"))
      Assert.assertTrue(elementTsModel.columns.contains("wifi_stats"))
      Assert.assertTrue(elementTsModel.columns.contains("radio_stats"))
      //      Assert.assertTrue(elementTsModel.columns.contains("steering_stats"))
      //      Assert.assertTrue(elementTsModel.columns.contains("access_point_stats"))
      //      Assert.assertTrue(elementTsModel.columns.contains("voice_stats"))
      //      Assert.assertTrue(elementTsModel.columns.contains("hne_stats"))
    }
  }

  "with model-args data" ignore {
    val config = AppConfig("config/element-ts-silver-model-args.json")
    logger.debug("{}", config)
    val elEvDf = spark.read.parquet(s"${config.inputLocation}/part-*.parquet")
    Assert.assertNotNull(elEvDf)
    logger.debug("original schema: {}", elEvDf.schema.treeString)

    val origColCount = elEvDf.columns.length
    Assert.assertEquals(9, origColCount)
    val origDistinctIds = elEvDf.select("id").distinct().count()
    Assert.assertEquals(0, elEvDf.filter("id is null").count())

    "be processed with joining device" in {
      val elementTsModel = ElementTs.model(elEvDf, config, args = Map())
      Assert.assertNotNull(elementTsModel)

      val ppDistinctIds = elementTsModel.select("device_id").distinct().count()

      logger.info("orig distinctIds {} post proc distinctIds {} (diff: {})", origDistinctIds, ppDistinctIds, (origDistinctIds - ppDistinctIds))

      Assert.assertTrue(elementTsModel.columns.contains("country"))
      Assert.assertFalse(elementTsModel.columns.contains("wifi_clients"))
      Assert.assertTrue(elementTsModel.columns.contains("wifi_devices"))
      Assert.assertFalse(elementTsModel.columns.contains("radio_clients"))
      Assert.assertFalse(elementTsModel.columns.contains("wifi_stats"))
      Assert.assertFalse(elementTsModel.columns.contains("radio_stats"))
    }
  }

  "with device df, big data" ignore {
    val config = AppConfig("config/element-ts-test.json")
    logger.debug("{}", config)
    val elEvDf = spark.read.parquet(s"${config.inputLocation}")
    Assert.assertNotNull(elEvDf)
    logger.debug("original schema: {}", elEvDf.schema.treeString)

    "be processed with joining all stats" in {
      val elementTsModel = ElementTs.model(elEvDf, config, args = Map())
      elementTsModel.write.mode("overwrite").parquet("tmp/element-ts-silver")
    }
  }
}