package com.commscope.eco.model

import com.commscope.eco.AppConfig
import com.typesafe.scalalogging.Logger
import org.junit.Assert
import org.scalatest.WordSpec

class AppConfigTest extends WordSpec {
  val logger = Logger(getClass)
  val dataDir = getClass.getClassLoader.getResource("data").getPath
  val configDir = getClass.getClassLoader.getResource("config").getPath

  "list transforms without outputSchema" in {
    val config = AppConfig("config/application-silver.json")
    val xforms = TransformationSeq(config)
    Assert.assertTrue(xforms.s.isEmpty)
    Assert.assertEquals(None, xforms.findByName("dropColumns"))
  }


  "list transforms with outputSchema" in {
    val config = AppConfig("config/subscriber-withOutputSchema.json")
    val xforms = TransformationSeq(config)
    logger.info("{}", xforms)
    Assert.assertEquals(5, xforms.s.length)
    Assert.assertTrue(xforms.s.exists(t => t.name.equals("dropColumns")))
    Assert.assertNotNull(xforms.findByName("dropColumns").get)
    Assert.assertEquals(None, xforms.findByName("someUnknown"))
  }

  "default value" should {
    "be honored for missing column" in {
      val config = AppConfig("config/subscriber-withOutputSchema.json")
      Assert.assertNotNull(config.outputSchema)
    }
  }
}
