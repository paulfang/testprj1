package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.functions._
import org.junit.Assert

import scala.io.Source
import scala.util.Random

class ElementTsBasicTest extends SparkBasedTest {
  val logger = Logger(getClass)
  val dataDir = getClass.getClassLoader.getResource("data").getPath
  val configDir = getClass.getClassLoader.getResource("config").getPath

  "hncStat updated with chanUtilization" in {
    val hncStat = HNE(1)
    ElementTs.updateHncStat(hncStat, "hneRadio[0]", "10")
    Assert.assertEquals(10, hncStat.radio24ChanUtilization)

    ElementTs.updateHncStat(hncStat, "hneRadio[1]", "25")
    Assert.assertEquals(25, hncStat.radio5ChanUtilization)
  }

  "clientId" should {
    "be extracted" in {
      val clientId = ElementTs.extractClientId("wifi_2[0].device[2].deauth")
      logger.info("{}", clientId)
      Assert.assertEquals(("2.4G",0,2), clientId)
    }
  }

  "dnsIndex" should {
    "be extracted" in {
      var index = ElementTs.extractDnsIndex("WANDNSStatistics[2].maximumResponseTime")
      Assert.assertEquals(2, index)

      index = ElementTs.extractDnsIndex("WANDNSStatistics[24].maximumResponseTime")
      Assert.assertEquals(24, index)
    }
  }

  "dnsServerIndex" should {
    "be extracted" in {
      var index = ElementTs.extractDnsServerIndex("wanDnsDataServer[0].serverDnsFailCount")
      Assert.assertEquals(0, index)

      index = ElementTs.extractDnsServerIndex("wanDnsDataServer[20].serverDnsFailCount")
      Assert.assertEquals(20, index)
    }
  }

  "upsert Radio" ignore {
    val attMap = Source.fromInputStream(getClass.getClassLoader.getResourceAsStream("element-ts-columns.txt"))
      .getLines().toList.head.split(",")
      .filter(c => c.startsWith("Radio") && c.contains("associatedDevice"))
      .foldLeft(Map[String, Any]())((m,c) => {
        val parts = c.split(":")
        val att = parts(0).trim
        val dtype = parts(1).trim

        if (dtype.equalsIgnoreCase("long")) m + (parts(0).trim -> Random.nextLong())
        else if (dtype.equalsIgnoreCase("int")) m + (parts(0).trim -> Random.nextInt())
        else if (dtype.equalsIgnoreCase("boolean")) m + (att -> false)
        else m + (att -> null)
      })

    logger.info("{}", attMap)

    val radioClients = attMap.foldLeft(Map[String, ConnectedDevice]())((m, e) => {
      logger.info("processing {}", e)
      ElementTs.upsertRadioClient(m, e._1, e._2)
    })
    Assert.assertFalse(radioClients.isEmpty)
    logger.info("radioClients: {}", radioClients)
  }
}