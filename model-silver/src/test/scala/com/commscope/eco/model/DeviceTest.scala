package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.junit.Assert

class DeviceTest extends SparkBasedTest {
  val logger = Logger(getClass)
  val dataDir = getClass.getClassLoader.getResource("data").getPath
  val configDir = getClass.getClassLoader.getResource("config").getPath

  "output schema" when {
    "no subscriber df found, data" should {
      val config = AppConfig("config/dailyparams-silver-noSubscriber.json")
      logger.debug("{}", config)
      val devDf = spark.read.parquet(s"${config.inputLocation}/dp_*.parquet")
      Assert.assertNotNull(devDf)
      val origCount = devDf.count()
      val origDevModel = devDf.select("properties.modelName").distinct().count()
      val origDevMan = devDf.select("properties.manufacturerName").distinct().count()

      logger.debug("original schema: {}", devDf.schema.treeString)

      "be processed without joining subscriber" in {
        val deviceModel = Device.model(devDf, config, args = Map())
        Assert.assertNotNull(deviceModel)
        logger.debug("output schema with no subscriber: \n{}", deviceModel.schema.treeString)

        val cnt = deviceModel.where("city is null").count()
        logger.info(s"city null count $cnt")
        Assert.assertTrue(cnt == 0)
        Assert.assertTrue(deviceModel.columns.contains("country"))
        logger.debug("postprocessing schema: {}", deviceModel.schema.treeString)
        val postProcCount = deviceModel.count()
        Assert.assertTrue(origCount > postProcCount)

        val ppDevModel = deviceModel.select("device_model").distinct().count()
        Assert.assertEquals(origDevModel, ppDevModel)

        val ppDevMan = deviceModel.select("device_manufacturer").distinct().count()
        Assert.assertEquals(origDevMan, ppDevMan)
      }
    }

    "subscriber df found, data" should {
      val config = AppConfig("config/dailyparams-silver.json")
      logger.debug("{}", config)
      val devDf = spark.read.parquet(s"${config.inputLocation}/dp_*.parquet")
      Assert.assertNotNull(devDf)

      logger.debug("original schema: {}", devDf.schema.treeString)

      "be processed with joining subscriber" in {
        val deviceModel = Device.model(devDf, config, args = Map())
        Assert.assertNotNull(deviceModel)
        val cnt = deviceModel.where("city is not null").count()
        logger.info(s"city null count $cnt")
        Assert.assertTrue(cnt > 0)
      }
    }
  }
}
