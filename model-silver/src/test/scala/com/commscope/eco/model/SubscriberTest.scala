package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.junit.Assert

class SubscriberTest extends SparkBasedTest {
  val logger = Logger(getClass)
  val dataDir = getClass.getClassLoader.getResource("data").getPath
  val configDir = getClass.getClassLoader.getResource("config").getPath

  "no output schema" when {
    val config = AppConfig("config/subscriber-withoutOutputSchema.json")
    logger.debug("{}", config)
    val subDf = spark.read.json(s"${dataDir}/${config.inputLocation}/sub*.json")
    Assert.assertNotNull(subDf)
    val origCols = subDf.select("*", "details.*", "identifiers.*").drop("details", "identifiers").columns
    logger.info("Original schema \n{}", subDf.schema.treeString)

    "no location df found, data" should {
      "still be processed" in {
        val subModel = Subscriber.model(subDf, config)
        Assert.assertNotNull(subModel)
        Assert.assertFalse(origCols.contains("country"))
        Assert.assertTrue(subModel.columns.contains("country"))
      }
    }

    "location df found, data" should {
      "be processed with location" in {
        val subModel = Subscriber.model(subDf, config, args = Map("locFile" -> "geo_table.json"))
        Assert.assertNotNull(subModel)
        Assert.assertFalse(origCols.contains("country"))
        Assert.assertTrue(subModel.columns.contains("country"))
      }
    }
  }

  "with output schema" when {
    val config = AppConfig("config/subscriber-withOutputSchema.json")
    logger.debug("{}", config)
    val subDf = spark.read.json(s"${config.inputLocation}/sub*.json")
    Assert.assertNotNull(subDf)

    val origCols = subDf.select("*", "details.*", "identifiers.*").drop("details", "identifiers").columns
    logger.debug("Original schema \n{}", subDf.schema.treeString)

    "no location df found, data" should {
      "be processed without joining location" in {
        val subModel = Subscriber.model(subDf, config, args = Map("locFileName" -> "some_table.json"))
        Assert.assertNotNull(subModel)
        logger.debug("output schema with no location: \n{}", subModel.schema.treeString)

        Assert.assertTrue(subModel.columns.contains("country"))
      }
    }

    "default location df found, data" should {
      "be processed with joining default location file" in {
        val subModel = Subscriber.model(subDf, config)
        Assert.assertNotNull(subModel)
        logger.debug("output schema with location: \n{}", subModel.schema.treeString)
        val colDiffs = origCols.diff(subModel.columns)
        val cnt = subModel.where("city is null").count()
        logger.info(s"city null count $cnt")
        Assert.assertEquals(0, cnt)
        Assert.assertTrue(subModel.columns.contains("country"))
      }
    }
  }
}
