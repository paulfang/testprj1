package com.commscope.eco.model

import org.apache.spark.sql.types.DataTypes.{createArrayType, createStructType}
import org.apache.spark.sql.types.{LongType, StringType, StructField, StructType}

import com.commscope.eco.SparkBasedTest

class DeviceAttributeTest extends SparkBasedTest {

  val devAttSchema = new StructType().
    add("created", LongType).
    add(
      StructField("details", createStructType(
        Array(
          StructField("BTN", StringType),
          StructField("Serial Number", StringType),
          StructField("ZIP", StringType),
          StructField("lastName", StringType)
        )
      )
      )).
    add("id", StringType).
    add(
      StructField("identifiers", createStructType(
        Array(
          StructField("ISPUsername", StringType),
          StructField("USI", StringType),
          StructField("WTN", StringType),
          StructField("OUI", StringType)
        )
      )
      )).
    add(
      StructField("properties", createStructType(
        Array(
          StructField("deviceType", StringType),
          StructField("deviceCategory", StringType),
          StructField("dnsServers", createArrayType(StringType))
        )
      )
      )).
    add(
      StructField("services", createArrayType(StringType))
    ).
    add("timestamp", LongType)

  val detailsMod = StructType(Array(StructField("ZIP", StringType), StructField("SerialNumber", StringType)))
}
