package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.junit.Assert

class DeviceUpgradeTest extends SparkBasedTest {
  val logger = Logger(getClass)
  val dataDir = getClass.getClassLoader.getResource("data").getPath
  val configDir = getClass.getClassLoader.getResource("config").getPath

  "without device df, data" should  {
    val config = AppConfig("config/firmware-upgrade-silver-noDevice.json")
    logger.debug("{}", config)
    val elUpDf = spark.read.parquet(s"${config.inputLocation}/firmware*.parquet")
    Assert.assertNotNull(elUpDf)
    logger.debug("original schema: {}", elUpDf.schema.treeString)

    val origCount = elUpDf.count()
    val origColCount = elUpDf.columns.length
    Assert.assertEquals(14, origColCount)
    val origDistinctIds = elUpDf.select("id").distinct().count()
    Assert.assertEquals(0, elUpDf.filter("id is null").count())

    "be processed without joining device" in {
      val upgradeModel = DeviceUpgrade.model(elUpDf, config, args = Map())
      Assert.assertNotNull(upgradeModel)

      val ppDistinctIds = upgradeModel.select("device_id").distinct().count()

      logger.info("orig distinctIds {} post proc distinctIds {} (diff: {})", origDistinctIds, ppDistinctIds, (origDistinctIds - ppDistinctIds))

      val postProcCount = upgradeModel.count()

      Assert.assertEquals(origCount, postProcCount)

      val postProcColCount = upgradeModel.columns.length
      Assert.assertEquals(23, postProcColCount)

      Assert.assertTrue(upgradeModel.columns.contains("country"))

      val notNullCityCount = upgradeModel.where("city is not null and city !='' ").count()
      Assert.assertTrue(notNullCityCount == 0)
    }
  }

  "with device df, data" should  {
    val config = AppConfig("config/firmware-upgrade-silver.json")
    logger.debug("{}", config)
    val elUpDf = spark.read.parquet(s"${config.inputLocation}/firmware*.parquet")
    Assert.assertNotNull(elUpDf)
    logger.debug("original schema: {}", elUpDf.schema.treeString)

    val origCount = elUpDf.count()
    val origColCount = elUpDf.columns.length
    Assert.assertEquals(14, origColCount)
    val origDistinctIds = elUpDf.select("id").distinct().count()
    Assert.assertEquals(0, elUpDf.filter("id is null").count())

    "be processed with joining device" in {
      val upgradeModel = DeviceUpgrade.model(elUpDf, config, args = Map())
      Assert.assertNotNull(upgradeModel)

      val ppDistinctIds = upgradeModel.select("device_id").distinct().count()

      logger.info("orig distinctIds {} post proc distinctIds {} (diff: {})", origDistinctIds, ppDistinctIds, (origDistinctIds - ppDistinctIds))

      val postProcCount = upgradeModel.count()

      Assert.assertEquals(origCount, postProcCount)

      val postProcColCount = upgradeModel.columns.length
      Assert.assertEquals(23, postProcColCount)

      Assert.assertTrue(upgradeModel.columns.contains("country"))

      val notNullCityCount = upgradeModel.where("city is not null and city != ''").count()
      val notNullZip = upgradeModel.where("zip is not null").count()
      Assert.assertTrue(notNullZip > 0)
      Assert.assertEquals(notNullZip, notNullCityCount)
    }
  }


  /*
  Creating the test data

    val devEv = spark.read.parquet(s"$baseDir/bronze/element-event/ts_year=2020/ts_month=8/ts_day=2")
    devEv.repartition(1).write.mode("overwrite")
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/element-event")

    val dpTest = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/dailyparams")

    val subs = spark.read.parquet(s"$baseDir/warehouse/silver/subscriber")

    val subsDP = dpTest
      .withColumnRenamed("id", "device_id").join(subs, $"subscriberID" === $"subscriber_id", "left_outer")
      .select("subscriber_id","zip","city","state","country")

    subsDP
      .repartition(1).write.mode("overwrite")
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/subscriber")

    val sub = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/subscriber")

    val dev = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/dailyparams")
      .select($"id".as("device_id"), $"subscriberID".as("subscriber_id"), $"properties.modelName".as("device_model"), $"properties.manufacturerName".as("device_manufacturer"))
      .distinct()

    val devSub = dev
      .join(sub.withColumnRenamed("subscriber_id","sub_id"), col("subscriber_id") === col("sub_id"), "left_outer")
      .drop("sub_id")
      .distinct()

    devSub
      .repartition(1).write.mode("overwrite")
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/device")
   */
}
