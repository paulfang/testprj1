package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.apache.spark.sql.functions.{col, explode}
import org.junit.Assert

class DeviceEventTest extends SparkBasedTest {
  val logger = Logger(getClass)
  val dataDir = getClass.getClassLoader.getResource("data").getPath
  val configDir = getClass.getClassLoader.getResource("config").getPath

  "without device df, data" should  {
    val config = AppConfig("config/element-event-silver-noDevice.json")
    logger.debug("{}", config)
    val elEvDf = spark.read.parquet(s"${config.inputLocation}/event*.parquet")
    Assert.assertNotNull(elEvDf)
    logger.debug("original schema: {}", elEvDf.schema.treeString)

    val origCount = elEvDf.count()
    val origColCount = elEvDf.columns.length
    Assert.assertEquals(5, origColCount)
    val origDistinctIds = elEvDf.select("id").distinct().count()
    Assert.assertEquals(0, elEvDf.filter("id is null").count())

    "be processed without joining device" in {
      val eventModel = DeviceEvent.model(elEvDf, config, args = Map())
      Assert.assertNotNull(eventModel)

      val ppDistinctIds = eventModel.select("device_id").distinct().count()

      logger.info("orig distinctIds {} post proc distinctIds {} (diff: {})", origDistinctIds, ppDistinctIds, (origDistinctIds - ppDistinctIds))

      val postProcCount = eventModel.count()

      Assert.assertEquals(3471, postProcCount)

      val postProcColCount = eventModel.columns.length
      Assert.assertEquals(16, postProcColCount)

      Assert.assertTrue(eventModel.columns.contains("country"))

      val notNullCityCount = eventModel.where("city is not null and city !='' ").count()
      Assert.assertTrue(notNullCityCount == 0)
    }
  }

  "with device df, data" should  {
    val config = AppConfig("config/element-event-silver.json")
    logger.debug("{}", config)
    val elEvDf = spark.read.parquet(s"${config.inputLocation}")
    Assert.assertNotNull(elEvDf)
    logger.debug("original schema: {}", elEvDf.schema.treeString)

    val origCount = elEvDf.count()
    val origColCount = elEvDf.columns.length
    val origDistinctIds = elEvDf.select("id").distinct().count()
    Assert.assertEquals(0, elEvDf.filter("id is null").count())

    "be processed with joining device" in {
      val eventModel = DeviceEvent.model(elEvDf, config, args = Map())
      Assert.assertNotNull(eventModel)

      val ppDistinctIds = eventModel.select("device_id").distinct().count()

      logger.info("orig distinctIds {} post proc distinctIds {} (diff: {})", origDistinctIds, ppDistinctIds, (origDistinctIds - ppDistinctIds))

      val postProcCount = eventModel.count()

      Assert.assertEquals(4276, postProcCount)

      val postProcColCount = eventModel.columns.length
      Assert.assertEquals(19, postProcColCount)

      Assert.assertTrue(eventModel.columns.contains("country"))
      Assert.assertTrue(eventModel.columns.contains("hardware_version"))
      Assert.assertTrue(eventModel.columns.contains("software_version"))
      Assert.assertTrue(eventModel.columns.contains("hnc_enable"))

      val notNullCityCount = eventModel.where("city is not null and city !='' ").count()
      Assert.assertTrue(notNullCityCount!=0)
      logger.info("{}", eventModel.select("device_id","type", "software_version", "hnc_enable").head(3))
    }
  }

  /*
  Creating the test data

    val devEv = spark.read.parquet(s"$baseDir/bronze/element-event/ts_year=2020/ts_month=8/ts_day=2")
    devEv.repartition(1).write.mode("overwrite")
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/element-event")

    val dpTest = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/dailyparams")

    val subs = spark.read.parquet(s"$baseDir/warehouse/silver/subscriber")

    val subsDP = dpTest
      .withColumnRenamed("id", "device_id").join(subs, $"subscriberID" === $"subscriber_id", "left_outer")
      .select("subscriber_id","zip","city","state","country")

    subsDP
      .repartition(1).write.mode("overwrite")
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/subscriber")

    val sub = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/subscriber")

    val dev = spark.read
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/dailyparams")
      .select($"id".as("device_id"), $"subscriberID".as("subscriber_id"), $"properties.modelName".as("device_model"), $"properties.manufacturerName".as("device_manufacturer"))
      .distinct()

    val devSub = dev
      .join(sub.withColumnRenamed("subscriber_id","sub_id"), col("subscriber_id") === col("sub_id"), "left_outer")
      .drop("sub_id")
      .distinct()

    devSub
      .repartition(1).write.mode("overwrite")
      .parquet("/Users/sanjeevmishra/workspace/arris/eco/inquire-fire/inquire-silver/src/test/resources/data/frontier/warehouse/device")
   */
}
