package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.junit.Assert._

class DeviceEventGoldTest extends SparkBasedTest {
  val logger = Logger(getClass)

  "deviceEvent pipeline" should {
    implicit val appConfig = AppConfig("config/pipelines/device_event-gold.json")
    val df = spark.read.parquet(appConfig.inputLocation)
    assertEquals(4276, df.count())
    val columns = df.columns
    assertEquals(19, columns.length)
    assertTrue(columns.contains("hardware_version"))
    assertTrue(columns.contains("software_version"))

    "run the first processor" in {
      val processors = appConfig.pipelines.head.processors
      assertEquals(2, processors.length)

      val firstProcessorOutput = processors.head.process(df)

      logger.info("firstDf location: {}", firstProcessorOutput._2)
      val firstDf = spark.read.parquet(firstProcessorOutput._2.head)
      logger.debug(firstDf.schema.treeString)
      assertTrue(firstDf.columns.contains("device_id"))
      assertTrue(firstDf.columns.contains("type"))
      assertTrue(firstDf.columns.contains("event_count"))

      assertFalse(firstDf.columns.contains("ts"))

      assertEquals(2899, firstDf.count())
    }

    "complete" in {
      val result = appConfig.pipelines.head.run(df)
      logger.info("{}", result)

      logger.info(result.schema.treeString)
      assertFalse(result.columns.contains("device_id"))
      assertFalse(result.columns.contains("ts"))
      assertTrue(result.columns.contains("event_count"))
      assertTrue(result.columns.contains("type"))

      assertEquals(58, result.count())
    }
  }
}
