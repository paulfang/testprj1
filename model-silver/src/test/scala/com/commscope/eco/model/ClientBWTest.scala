package com.commscope.eco.model

import com.commscope.eco.{AppConfig, SparkBasedTest}
import com.typesafe.scalalogging.Logger
import org.junit.Assert._

class ClientBWTest extends SparkBasedTest {
  val logger = Logger(getClass)

  "clientBW" should {
    "be modeled" in {
      implicit val appConfig = AppConfig("config/pipelines/client-bw-gold.json")
      val df = spark.read.parquet(appConfig.inputLocation)
      assertEquals(90, df.count())
      val columns = df.columns
      assertEquals(16, columns.length)
      assertTrue(columns.contains("wifi_clients"))
      assertTrue(columns.contains("radio_clients"))
      val baseColumns = Seq("device_id", "subscriber_id", "device_model",
        "software_version", "hardware_version", "hnc_enable",
        "zip", "city", "state", "country", "uptime", "ts_year",
        "ts_month", "ts_day", "ts_hour", "ts_inform", "ts")

      val modeled = ClientBW.model(df, appConfig,
        args = Map("base_columns" -> baseColumns))

      val modeledColumns = modeled.columns
      assertFalse(modeledColumns.contains("radio_clients"))
      assertFalse(modeledColumns.contains("wifi_clients"))
      assertEquals(32, modeledColumns.length)
      logger.debug(modeled.schema.treeString)
      assertEquals(313, modeled.count())
      assertEquals(90, modeled.select("ts").distinct().count())
      assertTrue(modeled.filter("device_id is null").count()==0)
      val dfHead = modeled.head(3)
      logger.debug("{}", dfHead)
    }
  }

  "clientBW with category" should {
    "be modeled" in {
      implicit val appConfig = AppConfig("config/pipelines/client-bw-gold-with-category.json")
      val df = spark.read.parquet(appConfig.inputLocation)
      assertEquals(96, df.count())
      val columns = df.columns

      assertTrue(columns.contains("wifi_clients"))
      assertTrue(columns.contains("radio_clients"))
      assertFalse(columns.contains("client_category"))
      assertFalse(columns.contains("client_device_type"))
      assertFalse(columns.contains("client_manufacturer"))

      val baseColumns = Seq("device_id", "subscriber_id", "device_model",
        "software_version", "hardware_version", "hnc_enable",
        "zip", "city", "state", "country", "uptime", "ts_year",
        "ts_month", "ts_day", "ts_hour", "ts_inform", "ts")

      val modeled = ClientBW.model(df, appConfig,
        args = Map("base_columns" -> baseColumns))

      val modeledColumns = modeled.columns
      assertFalse(modeledColumns.contains("radio_clients"))
      assertFalse(modeledColumns.contains("wifi_clients"))

      assertTrue(modeledColumns.contains("client_category"))
      assertTrue(modeledColumns.contains("client_device_type"))
      assertTrue(modeledColumns.contains("client_manufacturer"))

      assertEquals(35, modeledColumns.length)
      logger.debug(modeled.schema.treeString)
      assertEquals(2549, modeled.count())
      assertEquals(96, modeled.select("ts").distinct().count())
      assertTrue(modeled.filter("device_id is null").count()==0)
      val dfHead = modeled.head(3)
      logger.debug("{}", dfHead)
      val expectedCategories = Seq("DLNA","Phone","Set Top Box", "Computer")
      val categories = modeled.select("client_category").distinct().head(100).map(_.getString(0))

      logger.debug("client_categories: {}", categories)
      expectedCategories.foreach(c => assertTrue(categories.contains(c)))
    }
  }

  "clientBW pipeline" should {
    "run" in {
      implicit val appConfig = AppConfig("config/pipelines/client-bw-gold-with-category.json")
      val df = spark.read.parquet(appConfig.inputLocation)
      assertEquals(96, df.count())
      val columns = df.columns

      assertTrue(columns.contains("wifi_clients"))
      assertTrue(columns.contains("radio_clients"))
      assertFalse(columns.contains("client_category"))
      assertFalse(columns.contains("client_device_type"))
      assertFalse(columns.contains("client_manufacturer"))

      val result = appConfig.pipelines.head.run(df)

      val modeledColumns = result.columns
      assertFalse(modeledColumns.contains("radio_clients"))
      assertFalse(modeledColumns.contains("wifi_clients"))

      assertTrue(modeledColumns.contains("client_category"))
      assertTrue(modeledColumns.contains("client_device_type"))
      assertTrue(modeledColumns.contains("client_manufacturer"))

      logger.debug(result.schema.treeString)

      assertEquals(24, modeledColumns.length)

      assertEquals(30, result.count())
      val dfHead = result.head(3)
      logger.debug("{}", dfHead)
      val expectedCategories = Seq("DLNA","Phone","Set Top Box", "Computer")
      val categories = result.select("client_category").distinct().head(100).map(_.getString(0))

      logger.debug("client_categories: {}", categories)
      expectedCategories.foreach(c => assertTrue(categories.contains(c)))
    }
  }
}
