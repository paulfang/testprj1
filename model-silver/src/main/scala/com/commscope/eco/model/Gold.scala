package com.commscope.eco.model

import com.commscope.eco.{AppConfig, Format}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, SparkSession}

object ClientBW extends DataModel {
  /** The client information is gathered from radio_clients and wifi_clients branch of the original dataframe, both of them are array of struct fields.
   * Explode arrays radio_clients and wifi_clients
   * Explode map (struct fields)
   * Rename columns so that we can join them
   * Join outer (some models such as AM525 don't provide radio_clients branch)
   * Coalesce the information from both branches
   * Drop redundant columns
   *
   * @param dataFrame the element-ts from Silver layer
   * @param appConfig the application configuration
   * @param args
   * @param sparkSession
   * @return
   */
  override def model(dataFrame: DataFrame, appConfig: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    val baseCols = args.getOrElse("base_columns",
      Seq("device_id", "subscriber_id", "device_model", "device_manufacturer",
        "zip", "city", "state", "country", "uptime",
        "ts_date", "ts_year", "ts_month", "ts_day", "ts_hour", "ts_inform", "ts")).asInstanceOf[Seq[String]]

    val baseFrame = selectColumns(dataFrame, baseCols)

    var radioClients = dataFrame.
      select("device_id", "ts", "radio_clients").
      select(col("*"), explode(col("radio_clients"))).
      select("*", "col.*").
      drop("col", "radio_clients")

    radioClients = radioClients.columns.foldLeft(radioClients)((df, c) => df.withColumnRenamed(c, s"rc_$c"))

    var wifiClients =
      if (args.contains("wifi_client_columns") && args("wifi_client_columns").toString.equals("wifi_devices")) {
        dataFrame.
          select("device_id", "ts", "wifi_devices").
          select(col("*"), explode(col("wifi_devices"))).
          select("*", "col.*").
          drop("col", "wifi_devices")
      } else {
        dataFrame.
          select("device_id", "ts", "wifi_clients").
          select(col("*"), explode(col("wifi_clients"))).
          select("*", "col.*").
          drop("col", "wifi_clients")
      }

    wifiClients = wifiClients.columns.foldLeft(wifiClients)((df, c) => df.withColumnRenamed(c, s"wifi_$c"))

    val joinExpr =
      radioClients("rc_device_id") === wifiClients("wifi_device_id") &&
        radioClients("rc_ts") === wifiClients("wifi_ts") &&
        radioClients("rc_radio") === wifiClients("wifi_radio") &&
        radioClients("rc_macAddress") === wifiClients("wifi_macAddress")

    var clients = radioClients.join(wifiClients, joinExpr, "outer")

    val coalCols = Seq("device_id", "ts") ++
      dataFrame.select(explode(col("radio_clients"))).
        select("col.*").
        drop("col", "radio_clients").columns

    clients = coalCols.
      foldLeft(clients)((df, c) => df.withColumn(c, coalesce(col(s"wifi_$c"), col(s"rc_$c")))).
      filter("radio is not null")

    clients = clients.drop(clients.columns.filter(c => c.startsWith("rc_") || c.startsWith("wifi_")): _*)
    clients = if (args.contains("categorize-client") && !args("categorize-client").asInstanceOf[Boolean]) {
      baseFrame.join(clients, Seq("device_id", "ts"))
    } else categorize(baseFrame.join(clients, Seq("device_id", "ts")), appConfig)
    logger.info("client columns: {}", clients.columns)
    clients
  }

  /** Joins with client_population (from gold layer) or client from silver layer to get categories etc.
   *
   * @param frame
   * @param appConfig
   * @param sparkSession
   * @return
   */
  def categorize(frame: DataFrame, appConfig: AppConfig)(implicit sparkSession: SparkSession): DataFrame = {
    val clientPopulationLocation =
      if (appConfig.warehouseLocation.endsWith("/")) s"${appConfig.warehouseLocation}gold/client_population"
      else s"${appConfig.warehouseLocation}/gold/client_population"

    var connectedDevices = loadDependency(clientPopulationLocation, Format.parquet)

    if (connectedDevices.isDefined) {
      connectedDevices = Some(connectedDevices.get.select("device_id", "mac_address", "client_category", "client_device_type", "client_manufacturer").distinct())
    }
    else {
      val silverClientLocation =
        if (appConfig.warehouseLocation.endsWith("/")) s"${appConfig.warehouseLocation}silver/client"
        else s"${appConfig.warehouseLocation}/silver/client"

      connectedDevices = loadDependency(silverClientLocation, Format.parquet)
      if (connectedDevices.isDefined) {
        val windowSpec = Window.partitionBy("device_id", "mac_address").orderBy(desc("ts"))
        connectedDevices = Some(connectedDevices.get.
          select(col("device_id"), col("mac_address"),
            col("client_category"), col("client_device_type"), col("client_manufacturer"),
            row_number().over(windowSpec).alias("row_num")).filter("row_num = 1").drop("row_num"))
      }
    }

    if (connectedDevices.isDefined)
      try {
        frame.withColumnRenamed("macAddress", "mac_address").join(connectedDevices.get, Seq("device_id", "mac_address"), "left")
      }
      catch {
        case t: Throwable => logger.warn("Error categorizing clients: {}", t.getMessage, t)
          frame
      }
    else frame
  }
}

object WifiClient extends DataModel {
  override def model(dataFrame: DataFrame, appConfig: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    var wifiClients = dataFrame
      .select(col("device_id"),
        col("ts"), col("ts_date"), col("ts_year"), col("ts_month"),
        col("ts_day"), col("ts_hour"), col("ts_inform"),
        explode(col("wifiDevices"))).select("*", "col.*")
      .drop("wifiDevices", "col")
      .dropDuplicates("device_id", "ts_date", "ts_inform", "radio", "macAddress")

    val deviceDf = Device.deviceDataFrame(appConfig)

    wifiClients = if (deviceDf.isDefined) {
      wifiClients
        .join(deviceDf.get
          .select("device_id", "subscriber_id", "serial_number",
            "device_model", "device_manufacturer",
            "software_version", "hardware_version", "hnc_enable",
            "zip", "city", "state", "country")
          .withColumnRenamed("device_id", "dev_id"),
          col("device_id") === col("dev_id"), "left_outer")
        .drop("dev_id")
    } else wifiClients

    ClientBW.categorize(wifiClients, appConfig)
  }
}

