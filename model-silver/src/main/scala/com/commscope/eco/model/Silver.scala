package com.commscope.eco.model

import com.commscope.eco.model.ElementTs._
import com.commscope.eco.{AppConfig, Format}
import com.typesafe.scalalogging.Logger
import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.expressions.{UserDefinedFunction, Window}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.TimestampType
import org.apache.spark.sql.{Column, DataFrame, Row, SparkSession}

object Subscriber extends DataModel {
  override val logger = Logger(getClass)
  val locFileName = "locFileName"
  val subscriberWindow = Window.partitionBy("subscriber_id").orderBy(desc("timestamp"))

  private[model] def subscriberDataFrame(config: AppConfig, applyWindow: Boolean = true)(implicit sparkSession: SparkSession) = {
    val baseDir = if (!config.warehouseLocation.endsWith("/")) config.warehouseLocation + "/" else config.warehouseLocation
    val df = loadDependency(s"${baseDir}silver/subscriber", Format.parquet)

    if (df.isDefined && applyWindow) {
      Some(df.get.withColumn("row_num", row_number().over(subscriberWindow)).filter("row_num = 1"))
    }
    else df
  }

  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any] = Map.empty)(implicit sparkSession: SparkSession): DataFrame = {
    logger.info("modeling subscriber")

    var _df = applyTransformations(frame, config)

    val baseDir = if (!config.warehouseLocation.endsWith("/")) config.warehouseLocation + "/" else config.warehouseLocation

    val locFile =
      if (args.nonEmpty && args.contains(locFileName) && StringUtils.isNotBlank(args(locFileName).toString)) s"${baseDir}" + args(locFileName)
      else s"${baseDir}" + "common/geo_table.json"

    val locationDf = loadDependency(locFile, Format.json)

    if (locationDf.isDefined) {
      _df = _df.drop("city", "state")
        .join(locationDf.get.withColumnRenamed("zip", "l_zip"), col("zip") === col("l_zip"))
        .drop("l_zip")
    }

    _df = _df.withColumn("country", lit(config.getValueAsString("country")))

    selectColumns(_df, config)
  }
}

object Device extends DataModel {

  import Subscriber._

  val deviceWindow = Window.partitionBy("device_id").orderBy(desc("last_inform"))

  def deviceDataFrame(config: AppConfig, applyWindow: Boolean = true)(implicit sparkSession: SparkSession) = {
    val baseDir = if (!config.warehouseLocation.endsWith("/")) config.warehouseLocation + "/" else config.warehouseLocation
    val df = loadDependency(s"${baseDir}gold/device_population", Format.parquet)
    if (df.isDefined && applyWindow) {
      Some(df.get.withColumn("row_num", row_number().over(deviceWindow)).filter("row_num = 1"))
    }
    else df
  }

  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    logger.info("modeling device")

    var _df = applyTransformations(frame, config)

    val subscriberDf = subscriberDataFrame(config)

    if (subscriberDf.isDefined)
      _df = _df
        .join(subscriberDf.get
          .select("subscriber_id", "zip", "city", "state", "country")
          .withColumnRenamed("subscriber_id", "sub_id"),
          col("subscriber_id") === col("sub_id"), "left_outer")

    selectColumns(_df, config)
  }
}

object Client extends DataModel {

  import Device._

  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    logger.info("modeling client")
    var _df = classify(applyTransformations(frame, config))

    val deviceDf = deviceDataFrame(config)

    if (deviceDf.isDefined)
      _df = _df
        .join(deviceDf.get
          .select("device_id", "subscriber_id", "device_model", "device_manufacturer", "zip", "city", "state", "country")
          .withColumnRenamed("device_id", "dev_id"),
          col("device_id") === col("dev_id"), "left_outer")

    selectColumns(_df, config)
  }

  def classify(frame: DataFrame)(implicit sparkSession: SparkSession): DataFrame = {
    try {
      frame.createOrReplaceTempView("host_table")
      val query =
        """select *,
          |classify(mac_address, host_name) as client_type
          |from host_table
          |""".stripMargin

      sparkSession.sql(query).select("*", "client_type.*")
        .drop("client_type")
        .withColumnRenamed("_1", "client_device_type")
        .withColumnRenamed("_2", "client_category")
        .withColumnRenamed("_3", "client_manufacturer")
    }
    catch {
      case t: Throwable => logger.warn("Exception while classifying: {}", t.getMessage)
        frame
    }
  }
}

object DeviceEvent extends DataModel {
  import Device._
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    logger.info("modeling device-event")

    var _df = distinctEvent(applyTransformations(frame, config))

    val deviceDf = deviceDataFrame(config)

    if (deviceDf.isDefined)
      _df = _df
        .join(deviceDf.get
          .select("device_id", "subscriber_id", "device_model", "device_manufacturer",
            "software_version", "hardware_version", "hnc_enable",
            "zip", "city", "state", "country"),
          Seq("device_id"), "left")

    selectColumns(_df, config)
  }

  def distinctEvent(frame: DataFrame): DataFrame = {
    val window = Window.partitionBy("device_id", "type", "ts").orderBy(desc("ts"))
    frame.select(col("*"), row_number().over(window).alias("row_num")).filter("row_num = 1").drop("row_num")
  }
}

object DeviceUpgrade extends DataModel {

  import Device._

  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    logger.info("modeling device-upgrade")

    var _df = applyTransformations(frame, config)

    val deviceDf = deviceDataFrame(config)

    if (deviceDf.isDefined)
      _df = _df
        .join(deviceDf.get
          .select("device_id", "subscriber_id", "device_model", "device_manufacturer", "zip", "city", "state", "country")
          .withColumnRenamed("device_id", "dev_id"), col("device_id") === col("dev_id"), "left_outer")

    selectColumns(_df, config)
  }
}

case class ConnectedDevice(clientIndex: Int, var radio: String = null, var accessPoint: Int = -1,
                           var macAddress: String = null, var authenticationState: Boolean = false,
                           var bytesReceived: Long = 0, var bytesSent: Long = 0,
                           var deauthenticationCount: Long = -1, var disassociationsCount: Long = -1,
                           var lastTransmitRate: String = null, var rssi: Int = 0, var signalStrength: Int = 0,
                           var timeAssociated: Long = -1, var baudRate: Long = -1, var location: String = null)


case class WifiStat(accessPoint: Int, var radio: String = null,
                    var autoChannelEnable: Boolean = false, var autoChannelSupported: Boolean = false,
                    var beaconType: String = null, var channel: Int = -1,
                    var currentOperatingChannelBandwidth: String = null, var enable: Boolean = false,
                    var enableRadio: Boolean = false, var guestNetwork: Boolean = false,
                    var operatingChannelBandwidth: String = null,
                    var operatingFrequencyBand: String = null, var operatingStandards: String = null,
                    var possibleChannels: String = null, var ssidAdvertisementEnabled: Boolean = false,
                    var ssidMac: String = null, var ssidName: String = null,
                    var status: String = null, var supportedFrequencyBands: String = null,
                    var supportedSecurityMode: String = null, var supportedStandards: String = null,
                    var supportedTransmitPower: String = null, var totalAssociations: Long = -1,
                    var transmitPower: Long = -1, var wlanStatus: String = null,
                    var wpsEnabled: Boolean = false,
                    var bandwidth: String = null,
                    var sent: Long = -1,
                    var received: Long = -1,
                    var errored: Long = -1,
                    var noise: Long = -1,
                    var authSetting: String = null, var dropped: Long = -1
                   )

case class RadioStat(radio: String,
                     var autoChannelEnable: String = null,
                     var bytesReceived: Long = 0, var bytesSent: Long = 0,
                     var channel: String = null, var enableRadio: String = null,
                     var noise: Long = -1, var operatingChannelBandwidth: String = null,
                     var operatingStandards: String = null,
                     var packetsErrored: Long = 0, var packetsReceived: Long = 0,
                     var packetsSent: Long = 0, var possibleChannels: String = null,
                     var status: String = null, var supportedSecurityMode: String = null,
                     var transmitPower: String = null,
                     var wlanUpTime: Long = -1)

/*
WANDNSStatistics[2].collectionTime: string (nullable = true)
 |-- WANDNSStatistics[2].dNSFailCount: string (nullable = true)
 |-- WANDNSStatistics[2].enable: string (nullable = true)
 |-- WANDNSStatistics[2].l3FailCount: string (nullable = true)
 |-- WANDNSStatistics[2].maximumResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].minimumResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].name: string (nullable = true)
 |-- WANDNSStatistics[2].serverNumberOfEntries: string (nullable = true)
 |-- WANDNSStatistics[2].successCount: string (nullable = true)
 */
case class DnsStat(dnsIndex: Int,
                   var dnsName: String = "",
                   var dnsEnable: Int = 0,
                   var dnsCollectionTime: String = null,
                   var dnsSuccessCount: Int = 0,
                   var dnsFailCount: Int = 0,
                   var l3FailCount: Int = 0,
                   var dnsMinRespTime: Long = 0,
                   var dnsMaxRespTime: Long = 0,
                   var dnsServerNumEntries: Int = 0)

/*
|-- WANDNSStatistics[2].wanDnsDataServer[0].serverDnsFailCount: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverMaxResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverMinResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverName: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverSuccessCount: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverDnsFailCount: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverMaxResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverMinResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverName: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverSuccessCount: string (nullable = true)
 */
case class DnsServerStat(dnsIndex: Int,
                         var dnsServerIndex: Int,
                         var dnsServerName: String = null,
                         var dnsServerSuccessCount: Int = -1,
                         var dnsServerDnsFailCount: Int = 0,
                         var dnsServerMinRespTime: Long = -1,
                         var dnsServerMaxRespTime: Long = -1)

case class AccessPointStat(apIndex: Int, var clientIndex: Int = -1, var signalStrength: Int = 0)

case class HNE(index: Int, var configurationId: Int = -1,
               var hneMac: String = null,
               var synchedWithGW: Boolean = false,
               var radio24ChanUtilization: Int = -1,
               var radio5ChanUtilization: Int = -1)

case class EthernetStat(ethernetBroadcastPacketsReceived: Long, ethernetBroadcastPacketsSent: Long,
                        ethernetBytesReceived: Long, ethernetBytesSent: Long, ethernetDiscardPacketsReceived: Long,
                        ethernetDiscardPacketsSent: Long, ethernetErrorsReceived: Long, ethernetErrorsSent: Long,
                        ethernetMulticastPacketsReceived: Long, ethernetMulticastPacketsSent: Long, ethernetPacketsReceived: Long,
                        ethernetPacketsSent: Long, ethernetUnicastPacketsReceived: Long, ethernetUnicastPacketsSent: Long,
                        ethernetUnknownProtoPacketsReceived: Long)

case class SteeringStat(index: Int,
                        var staMacAddress: String = null,
                        var bandSupport: Int = -1,
                        var fiveGSupport: Boolean = false, var twoGSupport: Boolean = false,
                        var staBlacklistAttempts: Int = -1, var staBlacklistSuccesses: Int = -1,
                        var staBtmAttempts: Int = -1, var staBtmSuccesses: Int = -1,
                        var staNumberNoAP: Int = -1,
                        var staSelfSteerCount: Int = -1,
                        var staBHUtilizationEvents: Int = -1,
                        var staSteeringUnfriendly: String = null,
                        var staWiFiChannelUtilizationEvents: Int = -1,
                        var staWiFiLinkQualityEvents: Int = -1,
                        var chanUtilization: Int = -1)

case class VoiceStat(index: Int,
                     var farEndJitter: Int = -1,
                     var incomingCallsFailed: Int = -1, var latency: Int = -1,
                     var packetsLost: Int = -1, var receiveJitter: Int = -1)

object RadioStatModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "Radio", "radioStats", createRadioStats)
}

object RadioClientModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "Radio", "radioClients", createRadioClients)
}

object WifiClientModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "wifi_", "wifiClients", createWifiClients)
}

object WifiDeviceModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "wifi_", "wifiDevices", createWifiDevices)
}

object WifiStatModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "wifi_", "wifiStats", createWifiStats)
}

object SteeringModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "sta[", "steeringStats", createSteeringStats)
}

object DnsServerModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "WANDNSStatistics[", "dnsServerStats", createDnsServerStats)
}

object DnsModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "WANDNSStatistics[", "dnsStats", createDnsStats)
}

object HneModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "hne[", "hneStats", createHneStats)
}

object AccessPointModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "accessPoint[", "accessPointStats", createAccessPointStats)
}

object VoiceModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame =
    ElementTsSubModel.createSubModel(applyTransformations(frame, config), "voiceProfile", "voiceStats", createVoiceStats)
}

object ElementTsSubModel extends DataModel {
  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = frame

  def createSubModel(frame: DataFrame, columnNameStartsWith: String, subModelColName: String, udf: UserDefinedFunction): DataFrame = {
    val rawCols = frame.columns.filter(_.startsWith(columnNameStartsWith))
    val udfColumns = quoteColNames(rawCols)
    frame.withColumn(subModelColName, udf(struct(udfColumns.head, udfColumns.tail: _*))).drop(rawCols: _*)
  }
}

object ElementTsSplit extends DataModel {
  override val logger = Logger.apply(ElementTsSplit.getClass)
  val to_inform = expr("ts_hour % 24*4 + int(minute(ts_datetime)/15) + 1")
  val elementTsWindow = Window.partitionBy("ts_date", "device_id", "ts_inform").orderBy(desc("ts"))

  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    logger.info("modeling element-ts")
    var _df = applyTransformations(frame, config)
    _df = _df.transform(distinctRows)

    val commonColumns = Seq("device_id", "ts", "ts_date", "ts_hour", "ts_processed", "ts_year", "ts_month", "ts_day", "ts_inform")

    _df.transform(splitWifiDevices(commonColumns, "wifi_devices", config))
      .transform(splitWifiClients(commonColumns, "wifi_clients", config))
      .transform(splitRadioClients(commonColumns, "radio_clients", config))
      .transform(splitWifiStats(commonColumns, "wifi_stats", config))
      .transform(splitRadioStats(commonColumns, "radio_stats", config))
      .transform(splitSteeringStats(commonColumns, "steering_stats", config))
      .transform(splitDnsStats(commonColumns, "dns_stats", config))
      .transform(splitDnsServerStats(commonColumns, "dns_server_stats", config))
      .transform(splitAccessPointStats(commonColumns, "access_point_stats", config))
      .transform(splitVoiceStats(commonColumns, "voice_stats", config))
      .transform(splitHneStats(commonColumns, "hne_stats", config))
      .transform(dropAfter).transform(selectBase(commonColumns))
  }

  def selectBase(commonColumns: Seq[String])(frame: DataFrame): DataFrame = {
    val columns = frame.columns.filter(c => !commonColumns.contains(c))
    select(commonColumns, frame, columns)
  }

  def splitRadioClients(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("Radio") && c.contains("associatedDevice"))
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitRadioStats(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("Radio") && c.split('.').length == 2)
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitWifiClients(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("wifi_") && c.contains("associatedDevice"))
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitWifiDevices(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("wifi_") && !c.contains("associatedDevice") && c.contains("device"))
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitWifiStats(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("wifi_") && c.split('.').length == 2)
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitSteeringStats(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("sta["))
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitDnsStats(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("WANDNSStatistics[") && !c.contains("wanDnsDataServer"))
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitDnsServerStats(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("WANDNSStatistics[") && c.contains("wanDnsDataServer"))
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitAccessPointStats(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("accessPoint["))
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitVoiceStats(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("voiceProfile") && c.split('.').length == 3)
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  def splitHneStats(commonColumns: Seq[String], name: String, config: AppConfig)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("hne["))
    save(name, config)(select(commonColumns, frame, rawColNames))
    frame.drop(rawColNames: _*)
  }

  private def select(commonColumns: Seq[String], frame: DataFrame, rawColNames: Array[String]) = {
    val subCols = commonColumns ++ quoteColNames(rawColNames)
    frame.select(subCols.head, subCols.tail: _*)
  }

  def quoteColNames(colNames: Seq[String]) = colNames.map(c => s"`$c`")

  def dropAfter(frame: DataFrame): DataFrame =
    frame.drop(frame.columns.filter(c => c.equals("dev_id") ||
      c.equals("row_num") ||
      c.startsWith("profile[") ||
      c.startsWith("hne[") ||
      c.startsWith("voiceProfile[") ||
      c.startsWith("voiceProfile2[") ||
      c.startsWith("join_") ||
      c.startsWith("Radio") ||
      c.startsWith("sta[") ||
      c.startsWith("wifi_2") ||
      c.startsWith("lanEthInterfaceConfig[") ||
      c.startsWith("network") ||
      c.startsWith("wifi_5") ||
      c.startsWith("wifi_v[") ||
      c.startsWith("WANDNSStatistics[") ||
      c.startsWith("accessPoint[")): _*)

  def renameColumnNames(frame: DataFrame): DataFrame = {
    val toDrop = frame.columns.filter(_.indexOf('.') >= 0)
    val _df = deduplicateColumns(frame.drop(toDrop: _*))
    _df.select(_df.columns.foldLeft(Seq[Column]())((s, c) => s :+ col(c).alias(snakify(c))): _*)
  }

  def distinctRows(frame: DataFrame): DataFrame = {
    val _df =
      if (!frame.columns.contains("ts_inform"))
        frame
          .withColumn("ts_datetime", (col("ts") / 1000).cast(TimestampType))
          .withColumn("ts_inform", to_inform)
      else frame

    _df
      .withColumn("row_num", row_number().over(elementTsWindow))
      .filter("row_num = 1")
  }

  def save(name: String, config: AppConfig)(frame: DataFrame) = {
    frame.write.partitionBy(config.partitions: _*).parquet(s"${config.outputLocation}/$name")
    frame
  }
}

object ElementTs extends DataModel {

  import Device._

  val to_inform = expr("ts_hour % 24*4 + int(minute(ts_datetime)/15) + 1")
  val elementTsWindow = Window.partitionBy("ts_date", "device_id", "ts_inform").orderBy(desc("ts"))

  override def model(frame: DataFrame, config: AppConfig, args: Map[String, Any])(implicit sparkSession: SparkSession): DataFrame = {
    logger.info("modeling element-ts")

    var _df = applyTransformations(frame, config)
    _df = _df.transform(distinctRows)

    val idColumns = Seq("device_id", "ts")

    val joined = joinDeviceDF(config, _df)
    _df = joined._1

    val baseDf = _df.select(joined._2.head, joined._2.tail: _*).dropDuplicates(idColumns)

    val modelArgs =
      if (args.contains("process-stats")) args("process-stats").asInstanceOf[Map[String, Any]]
      else config.getValueAsDict("model-args")

    val st = System.currentTimeMillis()

    val wifiDeviceDf = _df.transform(processWifiDevices(idColumns, modelArgs.getOrElse("wifiDevices", false).asInstanceOf[Boolean]))
    val wifiClientDf = _df.transform(processWifiClients(idColumns, modelArgs.getOrElse("wifiClients", false).asInstanceOf[Boolean]))
    val radioClientDf = _df.transform(processRadioClients(idColumns, modelArgs.getOrElse("radioClients", false).asInstanceOf[Boolean]))
    val wifiStatsDf = _df.transform(processWifiStats(idColumns, modelArgs.getOrElse("wifiStats", false).asInstanceOf[Boolean]))
    val radioStatsDf = _df.transform(processRadioStats(idColumns, modelArgs.getOrElse("radioStats", false).asInstanceOf[Boolean]))
    val steeringStatsDf = _df.transform(processSteeringStats(idColumns, modelArgs.getOrElse("steeringStats", false).asInstanceOf[Boolean]))
    val dnsStatsDf = _df.transform(processDnsStats(idColumns, modelArgs.getOrElse("dnsStats", false).asInstanceOf[Boolean]))
    val dnsServerStatsDf = _df.transform(processDnsServerStats(idColumns, modelArgs.getOrElse("dnsServerStats", false).asInstanceOf[Boolean]))
    val accessPointStatsDf = _df.transform(processAccessPointStats(idColumns, modelArgs.getOrElse("accessPointStats", false).asInstanceOf[Boolean]))
    val voiceStatsDf = _df.transform(processVoiceStats(idColumns, modelArgs.getOrElse("voiceStats", false).asInstanceOf[Boolean]))
    val hneStatsDf = _df.transform(processHneStats(idColumns, modelArgs.getOrElse("hneStats", false).asInstanceOf[Boolean]))

    _df = baseDf
      .transform(joinWifiDevices(wifiDeviceDf, modelArgs.getOrElse("wifiDevices", false).asInstanceOf[Boolean]))
      .transform(joinWifiClients(wifiClientDf, modelArgs.getOrElse("wifiClients", false).asInstanceOf[Boolean]))
      .transform(joinRadioClients(radioClientDf, modelArgs.getOrElse("radioClients", false).asInstanceOf[Boolean]))
      .transform(joinWifiStats(wifiStatsDf, modelArgs.getOrElse("wifiStats", false).asInstanceOf[Boolean]))
      .transform(joinRadioStats(radioStatsDf, modelArgs.getOrElse("radioStats", false).asInstanceOf[Boolean]))
      .transform(joinSteeringStats(steeringStatsDf, modelArgs.getOrElse("steeringStats", false).asInstanceOf[Boolean]))
      .transform(joinDnsStats(dnsStatsDf, modelArgs.getOrElse("dnsStats", false).asInstanceOf[Boolean]))
      .transform(joinDnsServerStats(dnsServerStatsDf, modelArgs.getOrElse("dnsServerStats", false).asInstanceOf[Boolean]))
      .transform(joinAccessPointStats(accessPointStatsDf, modelArgs.getOrElse("accessPointStats", false).asInstanceOf[Boolean]))
      .transform(joinVoiceStats(voiceStatsDf, modelArgs.getOrElse("voiceStats", false).asInstanceOf[Boolean]))
      .transform(joinHneStats(hneStatsDf, modelArgs.getOrElse("hneStats", false).asInstanceOf[Boolean]))
      .transform(ElementTsSplit.dropAfter)
      .transform(renameColumnNames)

    logger.info("time taken in element-ts processing: {} sec", (System.currentTimeMillis() - st) / 1000)
    logger.debug("selected columns: {}", _df.columns)

    selectColumns(_df, config)
  }

  //
  //  def process(what:String, df:DataFrame, args:Map[String, Any], idColumns:Seq[String], processor: (Seq[String]) => DataFrame) = {
  //    if (args.contains(what) && args(what).asInstanceOf[Boolean]) df.transform(processor.apply(idColumns))
  //    df
  //  }

  /**
   * example of radio column names
   *
   * RadioTwoFourGHz[0].wifiTwoFourOne[0].associatedDevice[29].timeAssociated
   * RadioFiveGHz[0].wifiFiveThree[0].associatedDevice[0].disassociationsCount
   */
  val createRadioClients = udf((row: Row) => {
    val fieldNames = row.schema.fieldNames
    fieldNames.filter(f => f.startsWith("Radio") && f.contains("associatedDevice"))
      .foldLeft(Map[String, ConnectedDevice]())((m, f) => {
        val fieldValue = row.get(fieldNames.indexOf(f))
        upsertRadioClient(m, f, fieldValue)
      }
      ).values
      .filter(c => c.macAddress != null && c.macAddress.trim.length > 0)
      .toList
  }
  )

  def upsertRadioClient(m: Map[String, ConnectedDevice], f: String, attValue: Any): Map[String, ConnectedDevice] = {
    val parts = f.split('.')
    val openIndex = parts(2).indexOf('[')
    val closeIndex = parts(2).indexOf(']')
    val assocDevIndex = parts(2).substring(openIndex + 1, closeIndex).toInt

    val radio = parts(0) match {
      case p@"RadioFiveGHz[0]" => "5G"
      case p@"RadioTwoFourGHz[0]" => "2.4G"
      case _ => logger.info("createRadioClients: Unknown radioPart: {} in  {}", parts(0), f)
        "NA"
    }

    val accessPoint = parts(1) match {
      case p@"wifiTwoFourOne[0]" => 1
      case p@"wifiTwoFourTwo[0]" => 2
      case p@"wifiTwoFourThree[0]" => 3
      case p@"wifiTwoFourFour[0]" => 4
      case p@"wifiFiveOne[0]" => 1
      case p@"wifiFiveTwo[0]" => 2
      case p@"wifiFiveThree[0]" => 3
      case p@"wifiFiveFour[0]" => 4
      case _ => logger.info("createRadioClients: Unknown accessPoint: {} in {}", parts(1), {})
        0
    }

    val clientId = s"${radio}_${accessPoint}_$assocDevIndex"
    val assocDev = m.getOrElse(clientId, ConnectedDevice(assocDevIndex, radio = radio, accessPoint = accessPoint))

    if (attValue != null) {
      val attValueStr = String.valueOf(attValue).trim
      try {
        parts(3) match {
          case p@"authenticationState" => assocDev.authenticationState = attValueStr.toBoolean
          case p@"bytesReceived" => assocDev.bytesReceived = attValueStr.toLong
          case p@"bytesSent" => assocDev.bytesSent = attValueStr.toLong
          case p@"deauthenticationCount" => assocDev.deauthenticationCount = attValueStr.toLong
          case p@"disassociationsCount" => assocDev.disassociationsCount = attValueStr.toLong
          case p@"lastTransmitRate" => assocDev.lastTransmitRate = attValueStr
          case p@"macAddress" => assocDev.macAddress = attValueStr
          case p@"rssi" => assocDev.rssi = Integer.parseInt(attValueStr)
          case p@"signalStrength" => assocDev.signalStrength = Integer.parseInt(attValueStr)
          case p@"timeAssociated" => assocDev.timeAssociated = attValueStr.toLong
          case p@"baudRate" => assocDev.baudRate = attValueStr.toLong
          case p@"location" => assocDev.location = attValueStr
          case _ => logger.debug("createRadioClients: Unknown radio device attribute: {} in {}", parts(3), f)
        }
      } catch {
        case t: Throwable => logger.warn("createRadioClients: Error converting attValue {} to correct type: ", parts(3), t.getMessage)
      }
    }

    m + (clientId -> assocDev)
  }

  /**
   * examples of wifi column names
   *
   * wifi_24g_2[0].associatedDevices[0].bytesReceived
   * wifi_24guest_1[0].associatedDevices[2].signalStrength
   *
   * wifi_5g_1[0].associatedDevices[6].bytesReceived
   * wifi_5g_2[0].associatedDevices[1].bytesSent
   * wifi_5g_3[0].associatedDevices[1].macAddress
   * wifi_5guest_1[0].associatedDevices[2].signalStrength
   */
  val createWifiClients = udf((row: Row) => {
    row.schema.fieldNames
      .filter(f => f.startsWith("wifi_") && f.contains("associatedDevices"))
      .foldLeft(Map[String, ConnectedDevice]())((m, f) => {
        val parts = f.split('.')

        val assocPart = parts(1)
        val assocDevIndex = assocPart.substring(assocPart.indexOf('[') + 1, assocPart.indexOf(']')).toInt

        val firstPart = parts(0).substring(0, parts(0).length - 3)

        val accessPointBegin = firstPart.lastIndexOf('_')
        val accessPoint = firstPart.substring(accessPointBegin + 1).toInt

        val radioPart = firstPart.substring(firstPart.indexOf('_') + 1, accessPointBegin)

        val radio = radioPart match {
          case p@"5g" => "5G"
          case p@"24g" => "2.4G"
          case p@"24guest" => "2.4G Guest"
          case p@"5guest" => "5G Guest"
          case _ => logger.info("createWifiClients: Unknown radioPart: {} in {}", parts(0), f)
            "NA"
        }
        val clientId = s"${radio}_${accessPoint}_$assocDevIndex"
        val assocDev = m.getOrElse(clientId, ConnectedDevice(assocDevIndex, radio = radio, accessPoint = accessPoint))

        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)

        if (attValue != null) {
          val attValueStr = String.valueOf(attValue).trim
          try {
            parts(2) match {
              case p@"authenticationState" => assocDev.authenticationState = attValueStr.toBoolean
              case p@"bytesReceived" => assocDev.bytesReceived = attValueStr.toLong
              case p@"bytesSent" => assocDev.bytesSent = attValueStr.toLong
              case p@"deauthenticationCount" => assocDev.deauthenticationCount = attValueStr.toLong
              case p@"disassociationsCount" => assocDev.disassociationsCount = attValueStr.toLong
              case p@"lastTransmitRate" => assocDev.lastTransmitRate = attValueStr
              case p@"macAddress" => assocDev.macAddress = attValueStr
              case p@"rssi" => assocDev.rssi = attValueStr.toInt
              case p@"signalStrength" => assocDev.signalStrength = attValueStr.toInt
              case p@"timeAssociated" => assocDev.timeAssociated = attValueStr.toLong
              case p@"baudRate" => assocDev.baudRate = attValueStr.toLong
              case p@"location" => assocDev.location = attValueStr
              case _ => logger.debug("createWifiClients: Unknown connected device attribute: {} in {}", parts(2), f)
            }
          }
          catch {
            case t: Throwable => logger.warn("createWifiClients: Error converting attValue {} to correct type: ", parts(2), t.getMessage)
          }
        }

        m + (clientId -> assocDev)
      }
      ).values
      .filter(c => c.macAddress != null && c.macAddress.trim.length > 0)
      .toList
  }

  )

  /**
   * wifi_2[0].device[2].deauth: long (nullable = true)
   * |    |-- wifi_2[0].device[2].disassn: long (nullable = true)
   * |    |-- wifi_2[0].device[2].ip: string (nullable = true)
   * |    |-- wifi_2[0].device[2].mac: string (nullable = true)
   * |    |-- wifi_2[0].device[2].rate: string (nullable = true)
   * |    |-- wifi_2[0].device[2].received: long (nullable = true)
   * |    |-- wifi_2[0].device[2].rssi: long (nullable = true)
   * |    |-- wifi_2[0].device[2].sent: long (nullable = true)
   * |    |-- wifi_2[0].device[2].signal: long (nullable = true)
   * |    |-- wifi_2[0].device[2].state: boolean (nullable = true)
   * |    |-- wifi_2[0].device[2].time: long (nullable = true)
   */
  val createWifiDevices = udf((row: Row) => {
    row.schema.fieldNames
      .filter(f => f.startsWith("wifi_") && !f.contains("associatedDevices") && f.contains("device"))
      .foldLeft(Map[String, ConnectedDevice]())((m, f) => {
        val t = extractClientId(f)

        val clientId = s"${t._1}_${t._2}_${t._3}"
        val assocDev = m.getOrElse(clientId, ConnectedDevice(t._3, radio = t._1, accessPoint = t._2))

        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)

        if (attValue != null) {
          val attValueStr = String.valueOf(attValue).trim
          try {
            val parts = f.split('.')
            parts(2) match {
              case p@"state" => assocDev.authenticationState = attValueStr.toBoolean
              case p@"received" => assocDev.bytesReceived = attValueStr.toLong
              case p@"sent" => assocDev.bytesSent = attValueStr.toLong
              case p@"deauth" => assocDev.deauthenticationCount = attValueStr.toLong
              case p@"disassn" => assocDev.disassociationsCount = attValueStr.toLong
              case p@"rate" => assocDev.lastTransmitRate = attValueStr
              case p@"mac" => assocDev.macAddress = attValueStr
              case p@"rssi" => assocDev.rssi = attValueStr.toInt
              case p@"signal" => assocDev.signalStrength = attValueStr.toInt
              case p@"time" => assocDev.timeAssociated = attValueStr.toLong
              case _ =>
            }
          }
          catch {
            case t: Throwable => logger.warn("createWifiDevices: Error converting attValue to correct type: ", t.getMessage)
          }
        }

        m + (clientId -> assocDev)
      }
      ).values
      .filter(c => c.macAddress != null && c.macAddress.trim.length > 0)
      .toList
  }
  )

  def extractClientId(str: String) = {
    val parts = str.split('.')

    val radioPart = parts(0).substring(parts(0).indexOf('_') + 1, parts(0).indexOf('['))

    val radio: String = extractRadio(radioPart)

    val accessPointIndex = parts(0).substring(parts(0).indexOf('[') + 1, parts(0).indexOf(']')).toInt

    val assocDevIndex = parts(1).substring(parts(1).indexOf('[') + 1, parts(1).indexOf(']')).toInt

    (radio, accessPointIndex, assocDevIndex)
  }

  /**
   * examples of dns stats column names
   *
   * |-- WANDNSStatistics[2].collectionTime: string (nullable = true)
   * |-- WANDNSStatistics[2].dNSFailCount: string (nullable = true)
   * |-- WANDNSStatistics[2].enable: string (nullable = true)
   * |-- WANDNSStatistics[2].l3FailCount: string (nullable = true)
   * |-- WANDNSStatistics[2].maximumResponseTime: string (nullable = true)
   * |-- WANDNSStatistics[2].minimumResponseTime: string (nullable = true)
   * |-- WANDNSStatistics[2].name: string (nullable = true)
   * |-- WANDNSStatistics[2].serverNumberOfEntries: string (nullable = true)
   * |-- WANDNSStatistics[2].successCount: string (nullable = true)
   *
   */
  val createDnsStats = udf((row: Row) => {
    row.schema.fieldNames
      .filter(f => f.startsWith("WANDNS") && f.split('.').length == 2)
      .foldLeft(Map[Int, DnsStat]())((m, f) => {
        val parts = f.split('.')

        val index = extractDnsIndex(parts(0))

        val dnsStat = m.getOrElse(index, DnsStat(index))

        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)
        if (attValue != null) {
          val attValueStr = String.valueOf(attValue).trim
          try {
            parts(1) match {
              case p@"collectionTime" => dnsStat.dnsCollectionTime = attValueStr
              case p@"dNSFailCount" => dnsStat.dnsFailCount = attValueStr.toInt
              case p@"enable" => dnsStat.dnsEnable = attValueStr.toInt
              case p@"l3FailCount" => dnsStat.l3FailCount = attValueStr.toInt
              case p@"maximumResponseTime" => dnsStat.dnsMaxRespTime = attValueStr.toLong
              case p@"minimumResponseTime" => dnsStat.dnsMinRespTime = attValueStr.toLong
              case p@"serverNumberOfEntries" => dnsStat.dnsServerNumEntries = attValueStr.toInt
              case p@"successCount" => dnsStat.dnsSuccessCount = attValueStr.toInt
              case _ =>
            }
          }
          catch {
            case t: Throwable => logger.warn("createDnsStats: Error converting attValue {} to correct type: ", parts(1), t.getMessage)
          }
        }
        m + (index -> dnsStat)
      }
      ).values.toList
  }
  )

  /*
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverDnsFailCount: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverMaxResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverMinResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverName: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[0].serverSuccessCount: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverDnsFailCount: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverMaxResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverMinResponseTime: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverName: string (nullable = true)
 |-- WANDNSStatistics[2].wanDnsDataServer[1].serverSuccessCount: string (nullable = true)
 */

  val createDnsServerStats = udf((row: Row) => {
    row.schema.fieldNames
      .filter(f => f.startsWith("WANDNS") && f.contains("wanDnsDataServer"))
      .foldLeft(Map[Int, DnsServerStat]())((m, f) => {
        val parts = f.split('.')
        val dnsIndex = extractDnsIndex(parts(0))
        val serverIndex = extractDnsServerIndex(parts(1))
        val dnsServerStat = m.getOrElse(serverIndex, DnsServerStat(dnsIndex, serverIndex))

        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)
        if (attValue != null) {
          val attValueStr = String.valueOf(attValue).trim
          try {
            parts(2) match {
              case p@"serverName" => dnsServerStat.dnsServerName = attValueStr
              case p@"serverSuccessCount" => dnsServerStat.dnsServerSuccessCount = attValueStr.toInt
              case p@"serverDnsFailCount" => dnsServerStat.dnsServerDnsFailCount = attValueStr.toInt
              case p@"serverMinResponseTime" => dnsServerStat.dnsServerMinRespTime = attValueStr.toInt
              case p@"serverMaxResponseTime" => dnsServerStat.dnsServerMaxRespTime = attValueStr.toLong
              case _ => logger.warn("Not understood: {}", parts(2))
            }
          }
          catch {
            case t: Throwable => logger.warn("createDnsServerStats: Error converting attValue {} to correct type: ", parts(2), t.getMessage)
          }
        }
        m + (serverIndex -> dnsServerStat)
      }
      ).values.toList
  }
  )

  def extractDnsIndex(str: String) = {
    val indexPart = str.substring("WANDNSStatistics[".length, str.indexOf(']'))
    if (indexPart.isEmpty) 0 else indexPart.toInt
  }

  def extractDnsServerIndex(str: String) = {
    val indexPart = str.substring("wanDnsDataServer[".length, str.indexOf(']'))
    if (indexPart.isEmpty) 0 else indexPart.toInt
  }

  private def extractRadio(radioPart: String) =
    radioPart match {
      case p@"5g" => "5G"
      case p@"5" => "5G"
      case p@"v" => "vG"
      case p@"2" => "2.4G"
      case p@"24g" => "2.4G"
      case p@"24guest" => "2.4G Guest"
      case p@"5guest" => "5G Guest"
      case _ => "NA"
    }

  /**
   * examples of radio stats column names
   *
   * RadioFiveGHz[0].autoChannelEnable: string (nullable = true)
   * RadioFiveGHz[0].bytesReceived: long (nullable = true)
   * RadioFiveGHz[0].bytesSent: long (nullable = true)
   * RadioFiveGHz[0].channel: string (nullable = true)
   * RadioFiveGHz[0].enableRadio: string (nullable = true)
   *
   */
  val createRadioStats = udf((row: Row) => {
    val fieldNames = row.schema.fieldNames
    fieldNames
      .filter(f => f.startsWith("Radio") && f.split('.').length >= 2 && !f.contains("associatedDevice"))
      .foldLeft(Map[String, RadioStat]())((m, f) => {
        val fieldValue = row.get(fieldNames.indexOf(f))
        upsertRadioStat(m, f, fieldValue)
      }
      ).values.toList
  }
  )

  def upsertRadioStat(m: Map[String, RadioStat], f: String, attValue: Any): Map[String, RadioStat] = {
    val parts = f.split('.')

    val radio = parts(0) match {
      case p@"RadioFiveGHz[0]" => "5G"
      case p@"RadioFiveGHzA[0]" => "5G"
      case p@"RadioTwoFourGHz[0]" => "2.4G"
      case p@"RadioTwoFourGHzA[0]" => "2.4G"
      case _ => logger.info("createRadioStats: Unknown radioPart: {} in {}", parts(0), f)
        "NA"
    }

    val radioStat = m.getOrElse(radio, RadioStat(radio))

    if (attValue != null) {
      val attValueStr = String.valueOf(attValue).trim
      try {
        if (parts.length == 2) {
          parts(1) match {
            case p@"autoChannelEnable" => radioStat.autoChannelEnable = attValueStr
            case p@"bytesReceived" => radioStat.bytesReceived = attValueStr.toLong
            case p@"bytesSent" => radioStat.bytesSent = attValueStr.toLong
            case p@"channel" => radioStat.channel = attValueStr
            case p@"enableRadio" => radioStat.enableRadio = attValueStr
            case p@"noise" => radioStat.noise = attValueStr.toLong
            case p@"operatingChannelBandwidth" => radioStat.operatingChannelBandwidth = attValueStr
            case p@"operatingStandards" => radioStat.operatingStandards = attValueStr
            case p@"packetsErrored" => radioStat.packetsErrored = attValueStr.toLong
            case p@"packetsReceived" => radioStat.packetsReceived = attValueStr.toLong
            case p@"packetsSent" => radioStat.packetsSent = attValueStr.toLong
            case p@"status" => radioStat.status = attValueStr
            case p@"supportedSecurityMode" => radioStat.supportedSecurityMode = attValueStr
            case p@"transmitPower" => radioStat.transmitPower = attValueStr
            case _ => logger.debug("createRadioStats: Unknown radio stat attribute: {} in {}", parts(1), f)
          }
        } else {
          parts(2) match {
            case p@"wlanUpTime" => radioStat.wlanUpTime = attValueStr.toLong
          }
        }
      } catch {
        case t: Throwable => logger.warn("createRadioStats: Error converting attValue {} to correct type: ", t.getMessage)
      }
    }

    m + (radio -> radioStat)
  }

  /**
   * wifi_5guest_1[0]__ssidName
   * wifi_5g_2[0]__enable
   * wifi_24g_2[0]__wlanStatus
   * wifi_5[0]__received
   */
  val createWifiStats = udf((row: Row) => {
    val fieldNames = row.schema.fieldNames
    fieldNames
      .filter(f => f.startsWith("wifi_") && f.split('.').length == 2)
      .foldLeft(Map[String, WifiStat]())((m, f) => {
        val fieldValue = row.get(fieldNames.indexOf(f))
        upsertWifiStat(m, f, fieldValue)
      })
      .values.toList
  }
  )

  def upsertWifiStat(m: Map[String, WifiStat], f: String, attValue: Any): Map[String, WifiStat] = {
    val parts = f.split('.')
    val wifiPart = parts(0).substring(0, parts(0).length - 3)

    val accessPoint = if (wifiPart.lastIndexOf('_') > wifiPart.indexOf('_'))
      wifiPart.substring(wifiPart.lastIndexOf('_') + 1).toInt
    else 0

    val radioPart = if (wifiPart.lastIndexOf('_') > wifiPart.indexOf('_'))
      wifiPart.substring(wifiPart.indexOf('_') + 1, wifiPart.lastIndexOf('_'))
    else wifiPart.substring(wifiPart.indexOf('_') + 1)

    val radio = extractRadio(radioPart)

    val statId = s"${radio}_$accessPoint"
    val wifiStat = m.getOrElse(statId, WifiStat(accessPoint, radio = radio))

    if (attValue != null) {
      val attValueStr = String.valueOf(attValue).trim
      try {
        parts(1) match {
          case p@"autoChannelEnable" => wifiStat.autoChannelEnable = attValueStr.toBoolean
          case p@"autoChannelSupported" => wifiStat.autoChannelSupported = attValueStr.toBoolean
          case p@"beaconType" => wifiStat.beaconType = attValueStr
          case p@"channel" => wifiStat.channel = attValueStr.toInt
          case p@"currentOperatingChannelBandwidth" => wifiStat.currentOperatingChannelBandwidth = attValueStr
          case p@"enable" => wifiStat.enable = attValueStr.toBoolean
          case p@"enableRadio" => wifiStat.enableRadio = attValueStr.toBoolean
          case p@"guestNetwork" => wifiStat.guestNetwork = attValueStr.toBoolean
          case p@"operatingChannelBandwidth" => wifiStat.operatingChannelBandwidth = attValueStr
          case p@"operatingFrequencyBand" => wifiStat.operatingFrequencyBand = attValueStr
          case p@"operatingStandards" => wifiStat.operatingStandards = attValueStr
          case p@"possibleChannels" => wifiStat.possibleChannels = attValueStr
          case p@"ssidAdvertisementEnabled" => wifiStat.ssidAdvertisementEnabled = attValueStr.toBoolean
          case p@"ssidMac" => wifiStat.ssidMac = attValueStr
          case p@"ssidName" => wifiStat.ssidName = attValueStr
          case p@"status" => wifiStat.status = attValueStr
          case p@"supportedFrequencyBands" => wifiStat.supportedFrequencyBands = attValueStr
          case p@"supportedSecurityMode" => wifiStat.supportedSecurityMode = attValueStr
          case p@"supportedStandards" => wifiStat.supportedStandards = attValueStr
          case p@"supportedTransmitPower" => wifiStat.supportedTransmitPower = attValueStr
          case p@"totalAssociations" => wifiStat.totalAssociations = attValueStr.toLong
          case p@"transmitPower" => wifiStat.transmitPower = attValueStr.toLong
          case p@"wlanStatus" => wifiStat.wlanStatus = attValueStr
          case p@"wpsEnabled" => wifiStat.wpsEnabled = attValueStr.toBoolean
          case p@"bandwidth" => wifiStat.wlanStatus = attValueStr
          case p@"sent" => wifiStat.sent = attValueStr.toLong
          case p@"received" => wifiStat.received = attValueStr.toLong
          case p@"errored" => wifiStat.errored = attValueStr.toLong
          case p@"noise" => wifiStat.noise = attValueStr.toLong
          case p@"authSetting" => wifiStat.authSetting = attValueStr
          case p@"dropped" => wifiStat.dropped = attValueStr.toLong
          case _ =>
        }
      }
      catch {
        case t: Throwable => logger.warn("createWifiStats: Error converting attValue {} to correct type: ", parts(1), t.getMessage)
      }
    }
    m + (statId -> wifiStat)
  }

  /**
   * accessPoint[4].associatedDevice[5].signalStrength
   * accessPoint[4].associatedDevice[6].signalStrength
   * accessPoint[4].associatedDevice[7].signalStrength
   */
  val createAccessPointStats = udf((row: Row) => {
    row.schema.fieldNames
      .filter(f => f.startsWith("accessPoint[") && f.split('.').length == 3)
      .foldLeft(Map[String, AccessPointStat]())((m, f) => {
        val parts = f.split('.')
        val accessPoint = parts(0).substring(parts(0).indexOf('[') + 1, parts(0).indexOf(']')).toInt

        val clientIndex = parts(1).substring(parts(1).indexOf('[') + 1, parts(1).indexOf(']')).toInt
        val clientId = s"${accessPoint}_$clientIndex"
        val apStat = m.getOrElse(clientId, AccessPointStat(accessPoint, clientIndex = clientIndex))

        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)
        if (attValue != null) {
          val attValueStr = String.valueOf(attValue).trim
          try {
            parts(2) match {
              case p@"signalStrength" => apStat.signalStrength = attValueStr.toInt
              case _ =>
            }
          }
          catch {
            case t: Throwable => logger.warn("createWifiStats: Error converting attValue {} to correct type: ", parts(2), t.getMessage)
          }
        }
        m + (clientId -> apStat)
      }
      ).values.toList
  }
  )

  /**
   * voiceProfile2[0].line[0].farEndJitter
   * voiceProfile2[0].line[0].incomingCallsFailed
   * voiceProfile2[0].line[0].latency
   * voiceProfile2[0].line[0].packetsLost
   * voiceProfile2[0].line[0].receiveJitter
   * voiceProfile[0].line[0].farEndJitter
   * voiceProfile[0].line[0].incomingCallsFailed
   * voiceProfile[0].line[0].latency
   * voiceProfile[0].line[0].packetsLost
   * voiceProfile[0].line[0].receiveJitter
   */
  val createVoiceStats = udf((row: Row) => {
    row.schema.fieldNames
      .filter(f => f.startsWith("voiceProfile") && f.split('.').length == 3)
      .foldLeft(Map[Int, VoiceStat]())((m, f) => {
        val parts = f.split('.')
        val indexPart = parts(0).substring("voiceProfile".length, parts(0).indexOf('['))
        val index = if (indexPart.isEmpty) 0 else indexPart.toInt

        val voiceStat = m.getOrElse(index, VoiceStat(index))

        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)
        if (attValue != null) {
          val attValueStr = String.valueOf(attValue).trim
          try {
            parts(2) match {
              case p@"farEndJitter" => voiceStat.farEndJitter = attValueStr.toInt
              case p@"incomingCallsFailed" => voiceStat.incomingCallsFailed = attValueStr.toInt
              case p@"latency" => voiceStat.latency = attValueStr.toInt
              case p@"packetsLost" => voiceStat.packetsLost = attValueStr.toInt
              case p@"receiveJitter" => voiceStat.receiveJitter = attValueStr.toInt
              case _ =>
            }
          }
          catch {
            case t: Throwable => logger.warn("createWifiStats: Error converting attValue {} to correct type: ", parts(2), t.getMessage)
          }
        }
        m + (index -> voiceStat)
      }
      ).values.toList
  }
  )
  /**
   * example of steering column names
   *
   * sta[0].bandSupport
   * sta[0].fiveGSupport
   * sta[0].staBlacklistAttempts
   * sta[0].staBlacklistSuccesses
   * sta[0].staBtmAttempts
   * sta[0].staBtmSuccesses
   * sta[0].staMacAddress
   * sta[0].staNumberNoAP
   * sta[0].twoGSupport
   */
  val createSteeringStats = udf((row: Row) => {
    row.schema.fieldNames
      .filter(f => f.startsWith("sta["))
      .foldLeft(Map[Int, SteeringStat]())((m, f) => {

        val parts = f.split('.')
        val openIndex = parts(0).indexOf('[')
        val closeIndex = parts(0).indexOf(']')
        val index = parts(0).substring(openIndex + 1, closeIndex).toInt
        val steeringStat = m.getOrElse(index, SteeringStat(index))

        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)
        if (attValue != null) {
          val attValueStr = String.valueOf(attValue).trim
          try {
            parts(1) match {
              case p@"staMacAddress" => steeringStat.staMacAddress = attValueStr
              case p@"bandSupport" => steeringStat.bandSupport = Integer.parseInt(attValueStr)
              case p@"fiveGSupport" => steeringStat.fiveGSupport = attValueStr.toBoolean
              case p@"twoGSupport" => steeringStat.twoGSupport = attValueStr.toBoolean
              case p@"staBlacklistAttempts" => steeringStat.staBlacklistAttempts = Integer.parseInt(attValueStr)
              case p@"staBlacklistSuccesses" => steeringStat.staBlacklistSuccesses = Integer.parseInt(attValueStr)
              case p@"staBtmAttempts" => steeringStat.staBtmAttempts = Integer.parseInt(attValueStr)
              case p@"staBtmSuccesses" => steeringStat.staBtmSuccesses = Integer.parseInt(attValueStr)
              case p@"staNumberNoAP" => steeringStat.staNumberNoAP = Integer.parseInt(attValueStr)
              case p@"staBHUtilizationEvents" => steeringStat.staBHUtilizationEvents = Integer.parseInt(attValueStr)
              case p@"staSelfSteerCount" => steeringStat.staSelfSteerCount = Integer.parseInt(attValueStr)
              case p@"staSteeringUnfriendly" => steeringStat.staSteeringUnfriendly = attValueStr
              case p@"staWiFiChannelUtilizationEvents" => steeringStat.staWiFiChannelUtilizationEvents = Integer.parseInt(attValueStr)
              case p@"staWiFiLinkQualityEvents" => steeringStat.staWiFiLinkQualityEvents = Integer.parseInt(attValueStr)
              case p@"chanUtilization" => steeringStat.chanUtilization = Integer.parseInt(attValueStr)

              case _ =>
            }
          } catch {
            case t: Throwable => logger.warn("createSteeringStats: Error converting attValue {} to correct type: ", parts(1), t.getMessage)
          }
        }
        m + (index -> steeringStat)
      }
      ).values
      .filter(c => c.staMacAddress != null && c.staMacAddress.trim.nonEmpty)
      .toList
  }
  )

  val createHneStats = udf((row: Row) => {
    row.schema.fieldNames
      .filter(f => f.startsWith("hne["))
      .foldLeft(Map[Int, HNE]())((m, f) => {
        val parts = f.split('.')
        val openIndex = parts(0).indexOf('[')
        val closeIndex = parts(0).indexOf(']')
        val index = parts(0).substring(openIndex + 1, closeIndex).toInt
        val hncStat = m.getOrElse(index, HNE(index))

        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)
        if (attValue != null)
          updateHncStat(hncStat, parts(1), String.valueOf(attValue).trim)
        m + (index -> hncStat)
      }
      ).values
      .filter(c => c.hneMac != null && c.hneMac.trim.nonEmpty)
      .toList
  }
  )

  /** @FixMe currently everything under hneRadio[*] gets assigned to chanUtilization, fix it if more attributes needed */
  def updateHncStat(hncStat: HNE, attValueName: String, attValueStr: String): Unit = {
    try {
      attValueName match {
        case p@"hneMac" => hncStat.hneMac = attValueStr
        case p@"configurationId" => hncStat.configurationId = Integer.parseInt(attValueStr)
        case p@"hneRadio[0]" => hncStat.radio24ChanUtilization = Integer.parseInt(attValueStr)
        case p@"hneRadio[1]" => hncStat.radio5ChanUtilization = Integer.parseInt(attValueStr)
        case p@"synchedWithGW" => hncStat.synchedWithGW = attValueStr.toBoolean

        case _ =>
      }
    } catch {
      case t: Throwable =>
        logger.warn("createHneStats: Error converting attValue {} to correct type: {}", attValueName, t.getMessage)
    }
  }

  /**
   * EthernetBroadcastPacketsReceived
   * EthernetBroadcastPacketsSent
   * EthernetBytesReceived
   * EthernetBytesSent
   * EthernetDiscardPacketsReceived
   * EthernetDiscardPacketsSent
   * EthernetErrorsReceived
   * EthernetErrorsSent
   * EthernetMulticastPacketsReceived
   * EthernetMulticastPacketsSent
   * EthernetPacketsReceived
   * EthernetPacketsSent
   * EthernetUnicastPacketsReceived
   * EthernetUnicastPacketsSent
   * EthernetUnknownProtoPacketsReceived
   */
  val createEthernetStat = udf((row: Row) => {
    val attVals = row.schema.fieldNames
      .filter(f => f.startsWith("Ethernet"))
      .foldLeft(Map[String, Long]())((m, f) => {
        val attIndex = row.schema.fieldNames.indexOf(f)
        val attValue = row.get(attIndex)
        if (attValue != null) {
          val attValueStr = String.valueOf(attValue).trim
          try {
            m + (f -> attValueStr.toLong)
          } catch {
            case t: Throwable => logger.warn("createSteeringStats: Error converting attValue {} to correct type: ", attValueStr, t.getMessage)
              m
          }
        }
        else m
      }
      )
    EthernetStat(attVals.getOrElse("EthernetBroadcastPacketsReceived", -1),
      attVals.getOrElse("EthernetBroadcastPacketsSent", -1),
      attVals.getOrElse("EthernetBytesReceived", -1),
      attVals.getOrElse("EthernetBytesSent", -1),
      attVals.getOrElse("EthernetDiscardPacketsReceived", -1),
      attVals.getOrElse("EthernetDiscardPacketsSent", -1),
      attVals.getOrElse("EthernetErrorsReceived", -1),
      attVals.getOrElse("EthernetErrorsSent", -1),
      attVals.getOrElse("EthernetMulticastPacketsReceived", -1),
      attVals.getOrElse("EthernetMulticastPacketsSent", -1),
      attVals.getOrElse("EthernetPacketsReceived", -1),
      attVals.getOrElse("EthernetPacketsSent", -1),
      attVals.getOrElse("EthernetUnicastPacketsReceived", -1),
      attVals.getOrElse("EthernetUnicastPacketsSent", -1),
      attVals.getOrElse("EthernetUnknownProtoPacketsReceived", -1))
  }
  )

  def processRadioClients(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns
      .filter(c => c.startsWith("Radio") && c.contains("associatedDevice"))

    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("radioClients", createRadioClients(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_rc")
        .withColumnRenamed("ts", "join_ts_rc")
    } else frame

    df.drop(rawColNames: _*)
  }

  def processRadioStats(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("Radio") && c.split('.').length == 2)

    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("radioStats", createRadioStats(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_rs")
        .withColumnRenamed("ts", "join_ts_rs")
    } else frame
    df.drop(rawColNames: _*)
  }

  def quoteColNames(colNames: Seq[String]) = colNames.map(c => s"`$c`")

  def processWifiClients(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("wifi_") && c.contains("associatedDevice"))

    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)
      subDf.withColumn("wifiClients", createWifiClients(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_wc")
        .withColumnRenamed("ts", "join_ts_wc")
    }
    else frame
    df.drop(rawColNames: _*)
  }

  def processWifiDevices(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("wifi_") && !c.contains("associatedDevice") && c.contains("device"))
    val colNames = frame.columns.filter(c => (c.startsWith("wifi_") && !c.contains("associatedDevice") && c.contains("device")) ||
      (c.startsWith("Radio") && c.contains("associatedDevice")))

    //save(frame, idColumns, colNames, "/tmp/warehouse/wifiRadioData")
    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("wifiDevices", createWifiDevices(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_wd")
        .withColumnRenamed("ts", "join_ts_wd")
    } else frame

    df.drop(rawColNames: _*)
  }

  def processWifiStats(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("wifi_") && c.split('.').length == 2)

    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("wifiStats", createWifiStats(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_ws")
        .withColumnRenamed("ts", "join_ts_ws")
    } else frame
    df.drop(rawColNames: _*)
  }

  def processSteeringStats(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("sta["))
    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("steeringStats", createSteeringStats(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_ss")
        .withColumnRenamed("ts", "join_ts_ss")
    } else frame
    df.drop(rawColNames: _*)
  }

  def processDnsStats(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    //save(frame, idColumns,"WANDNSStatistics[", "/tmp/warehouse/dnsData")
    val rawColNames = frame.columns.filter(c => c.startsWith("WANDNSStatistics[") && !c.contains("wanDnsDataServer"))
    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("dnsStats", createDnsStats(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_dns")
        .withColumnRenamed("ts", "join_ts_dns")
    } else frame
    df.drop(rawColNames: _*)
  }

  def save(frame: DataFrame, idColumns: Seq[String], colNames: Seq[String], location: String): Unit = {
    val cols = frame.columns.filter(c => c.startsWith("ts_")) ++ idColumns ++ quoteColNames(colNames)
    frame.select(cols.head, cols.tail: _*).repartition(1).write.parquet(location)
  }

  def processDnsServerStats(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("WANDNSStatistics[") && c.contains("wanDnsDataServer"))
    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("dnsServerStats", createDnsServerStats(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_dnssrv")
        .withColumnRenamed("ts", "join_ts_dnssrv")
    } else frame

    df.drop(rawColNames: _*)
  }

  def processAccessPointStats(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("accessPoint["))
    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("accessPointStats", createAccessPointStats(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_aps")
        .withColumnRenamed("ts", "join_ts_aps")
    }
    else frame
    df.drop(rawColNames: _*)
  }

  def processVoiceStats(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("voiceProfile") && c.split('.').length == 3)

    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("voiceStats", createVoiceStats(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_vs")
        .withColumnRenamed("ts", "join_ts_vs")
    } else frame
    df.drop(rawColNames: _*)
  }

  def processHneStats(idColumns: Seq[String], toProcess: Boolean)(frame: DataFrame): DataFrame = {
    val rawColNames = frame.columns.filter(c => c.startsWith("hne["))
    val df = if (toProcess) {
      val subCols = idColumns ++ quoteColNames(rawColNames)
      val subDf = frame.select(subCols.head, subCols.tail: _*)

      subDf.withColumn("hneStats", createHneStats(struct(subCols.head, subCols.tail: _*)))
        .withColumnRenamed("device_id", "join_dev_id_hs")
        .withColumnRenamed("ts", "join_ts_hs")
    } else frame
    df.drop(rawColNames: _*)
  }

  def renameColumnNames(frame: DataFrame): DataFrame = {
    val toDrop = frame.columns.filter(_.indexOf('.') >= 0)
    val _df = deduplicateColumns(frame.drop(toDrop: _*))
    _df.select(_df.columns.foldLeft(Seq[Column]())((s, c) => s :+ col(c).alias(snakify(c))): _*)
  }

  def distinctRows(frame: DataFrame): DataFrame = {
    val _df =
      if (!frame.columns.contains("ts_inform"))
        frame
          .withColumn("ts_datetime", (col("ts") / 1000).cast(TimestampType))
          .withColumn("ts_inform", to_inform)
      else frame

    _df
      .withColumn("row_num", row_number().over(elementTsWindow))
      .filter("row_num = 1")
  }

  def joinDeviceDF(config: AppConfig, frame: DataFrame)(implicit sparkSession: SparkSession): (DataFrame, Seq[String]) = {
    var _df = frame
      .withColumnRenamed("cdWan[0].downStreamCurrRate", "wan_0_downstream_curr_rate")
      .withColumnRenamed("cdWan[1].downStreamCurrRate", "wan_1_downstream_curr_rate")
      .withColumnRenamed("cdWan[2].downStreamCurrRate", "wan_2_downstream_curr_rate")
      .withColumnRenamed("cdWan[0].upStreamCurrRate", "wan_0_upstream_curr_rate")
      .withColumnRenamed("cdWan[1].upStreamCurrRate", "wan_1_upstream_curr_rate")
      .withColumnRenamed("cdWan[2].upStreamCurrRate", "wan_2_upstream_curr_rate")
      .withColumnRenamed("lanEthernetInterfaceConfig[0].status", "eth_0_status")
      .withColumnRenamed("lanEthernetInterfaceConfig[1].status", "eth_1_status")
      .withColumnRenamed("lanEthernetInterfaceConfig[2].status", "eth_2_status")
      .withColumnRenamed("lanEthernetInterfaceConfig[3].status", "eth_3_status")

    val nonNestedCols = _df.columns.filter(c => c.indexOf('.') == -1)
    val deviceDf = deviceDataFrame(config)

    if (deviceDf.isDefined) {
      deviceDf.get.cache()
      val colNames = Seq("device_id", "subscriber_id", "device_model", "device_manufacturer",
        "software_version", "hardware_version", "hnc_enable",
        "zip", "city", "state", "country")

      _df = _df.join(selectColumns(deviceDf.get, colNames)
        .withColumnRenamed("device_id", "dev_id"), col("device_id") === col("dev_id"), "left_outer")

      (_df, (colNames ++ Seq("ts", "ts_date", "ts_hour", "ts_processed", "ts_year", "ts_month", "ts_day", "ts_inform") ++ nonNestedCols).distinct)
    }
    else (_df, (Seq("device_id", "ts", "ts_date", "ts_hour", "ts_processed", "ts_year", "ts_month", "ts_day", "ts_inform") ++ nonNestedCols).distinct)
  }

  def joinWifiClients(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_wc") and
        col("ts") === col("join_ts_wc")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinWifiDevices(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_wd") and
        col("ts") === col("join_ts_wd")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinRadioClients(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_rc") and
        col("ts") === col("join_ts_rc")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinWifiStats(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_ws") and
        col("ts") === col("join_ts_ws")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinRadioStats(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_rs") and
        col("ts") === col("join_ts_rs")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinSteeringStats(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_ss") and
        col("ts") === col("join_ts_ss")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinDnsStats(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_dns") and
        col("ts") === col("join_ts_dns")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinDnsServerStats(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_dnssrv") and
        col("ts") === col("join_ts_dnssrv")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinAccessPointStats(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_aps") and
        col("ts") === col("join_ts_aps")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinVoiceStats(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_vs") and
        col("ts") === col("join_ts_vs")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }

  def joinHneStats(right: DataFrame, toProcess: Boolean)(left: DataFrame): DataFrame = {
    if (toProcess) {
      val joinExpr = col("device_id") === col("join_dev_id_hs") and
        col("ts") === col("join_ts_hs")
      left.join(right, joinExpr, joinType = "inner")
    } else left
  }
}

